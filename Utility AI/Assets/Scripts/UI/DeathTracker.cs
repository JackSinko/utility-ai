﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathTracker : MonoBehaviour {

    public GameObject background;
    public GameObject deathText;
    public int spacing = 20;

    public int numberOfDeaths = 0;

    public enum DeathType {
        Hunger,
        Thirst,
        Land_Mine,
        Fighting
    };

    //display the death in the death tracker
    public void ShowDeath(DeathType death) {
        
        GameObject deathObject = Instantiate(deathText);
        deathObject.transform.position = new Vector3(deathText.transform.position.x, deathText.transform.position.y - ((numberOfDeaths+1) * spacing), deathText.transform.position.z);    
        deathObject.GetComponent<Text>().text = death.ToString().Replace("_", " ");
        deathObject.transform.SetParent(deathText.transform.parent);

        numberOfDeaths++;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour {

    public Toggle enableUIToggle;
    public DeathTracker deathTracker;
    public GameObject selected = null;
    public GameObject DeselectedUI;

    void Update() {

        //if left mouse doesn't click gameobject
        if(Input.GetMouseButtonDown(0) && EventSystem.current.IsPointerOverGameObject() == false) {

            //ray cast
            RaycastHit hit; Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit)) {

                if(hit.transform.tag == "Competitor") {

                    //clicked competitor

                    if(selected != null) selected.GetComponent<Competitor>().UI.gameObject.SetActive(false);

                    selected = hit.transform.gameObject;
                    DeselectedUI.SetActive(false);
                    selected.GetComponent<Competitor>().UI.gameObject.SetActive(true);

                } else if(selected != null) {

                    //deselect competitor (clicked off)

                    DeselectedUI.SetActive(true);
                    selected.GetComponent<Competitor>().UI.gameObject.SetActive(false);
                    selected = null;

                }

            }

        }

    }

}

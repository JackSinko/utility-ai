﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

/*
room states:

 activated = false - the room can't be accessed
 activated = true - the room COULD be accessed
 a door is locked if activated = true
 unlocked = true - the activated room has been entered & generated

*/

namespace MapGen {
    public class Room {

        public int ID;
        public bool Activated { get; set; } //activated = true - generate room on in grid space
        public Vector3 Position;
        public GameObject RoomVisualiser;
        public Dictionary<Relative, Door> Doors = new Dictionary<Relative, Door>();
        public Dictionary<Relative, int> ConnectedRooms = new Dictionary<Relative, int>();

        //set following values when room is entered
        public bool Unlocked = false; //if the room has been entered before

        public Room(int ID, Vector3 position) {
            this.ID = ID;
            this.Position = position;

            //create doors
            Doors.Add(Relative.UP, new Door(this, Relative.UP));
            Doors.Add(Relative.RIGHT, new Door(this, Relative.RIGHT));
            Doors.Add(Relative.DOWN, new Door(this, Relative.DOWN));
            Doors.Add(Relative.LEFT, new Door(this, Relative.LEFT));
        }

        public List<Room> GetActiveConnectedRooms() {
            List<Room> activeConnectedRooms = new List<Room>();
            if (MapGrid.Rooms[ConnectedRooms[Relative.UP]].Activated)
                activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.UP]]);
            if (MapGrid.Rooms[ConnectedRooms[Relative.DOWN]].Activated)
                activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.DOWN]]);
            if (MapGrid.Rooms[ConnectedRooms[Relative.LEFT]].Activated)
                activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.LEFT]]);
            if (MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]].Activated)
                activeConnectedRooms.Add(MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]]);
            return activeConnectedRooms;
        }

        public void ConnectDoors() {

            //check each side if relative room is active (eg. if left door, is left connected room active?)
            if (!MapGrid.Rooms[ConnectedRooms[Relative.UP]].Activated) {
                //set doorway to inactive (as player cannot go to an inactive room)
                RoomVisualiser.transform.Find("Wall_UP").gameObject.SetActive(true);
            } else {

                //connect both corresponding doors as rooms are active
                Room activatedRoom = MapGrid.Rooms[ConnectedRooms[Relative.UP]];

                Door currentRoomsDoor = Doors[Relative.UP];
                Door connectedRoomsDoor = activatedRoom.Doors[Relative.DOWN];

                connectedRoomsDoor.Connection = currentRoomsDoor;
                currentRoomsDoor.Connection = connectedRoomsDoor;

            }

            //repeat for other sides:

            if (!MapGrid.Rooms[ConnectedRooms[Relative.DOWN]].Activated) {
                RoomVisualiser.transform.Find("Wall_DOWN").gameObject.SetActive(true);
            } else {

                Room activatedRoom = MapGrid.Rooms[ConnectedRooms[Relative.DOWN]];

                Door currentRoomsDoor = Doors[Relative.DOWN];
                Door connectedRoomsDoor = activatedRoom.Doors[Relative.UP];

                connectedRoomsDoor.Connection = currentRoomsDoor;
                currentRoomsDoor.Connection = connectedRoomsDoor;

            }

            if (!MapGrid.Rooms[ConnectedRooms[Relative.LEFT]].Activated) {
                RoomVisualiser.transform.Find("Wall_LEFT").gameObject.SetActive(true);
            } else {

                Room activatedRoom = MapGrid.Rooms[ConnectedRooms[Relative.LEFT]];

                Door currentRoomsDoor = Doors[Relative.LEFT];
                Door connectedRoomsDoor = activatedRoom.Doors[Relative.RIGHT];

                connectedRoomsDoor.Connection = currentRoomsDoor;
                currentRoomsDoor.Connection = connectedRoomsDoor;

            }

            if (!MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]].Activated) {
                RoomVisualiser.transform.Find("Wall_RIGHT").gameObject.SetActive(true);
            } else {

                Room activatedRoom = MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]];

                Door currentRoomsDoor = Doors[Relative.RIGHT];
                Door connectedRoomsDoor = MapGrid.Rooms[ConnectedRooms[Relative.RIGHT]].Doors[Relative.LEFT];

                connectedRoomsDoor.Connection = currentRoomsDoor;
                currentRoomsDoor.Connection = connectedRoomsDoor;

            }

        }

    }
}
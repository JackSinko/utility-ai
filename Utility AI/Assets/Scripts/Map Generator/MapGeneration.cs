﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

namespace MapGen {
    public class MapGeneration : MonoBehaviour {

        public GameObject loading;

        /*numberOfRoomBranches:
     
          * Definition: the number of 'layers' or 'branches' that will be generated from the middle/starting room
              this is the minimum number of rooms in one direction
              therefore this is the MAXIMUM number of rooms you can play on the floor
          * Example: if 10; it is possible that only 11 rooms could be activated
              Although if 11 rooms are activated, you may find the boss room before 11 rooms.
      
          This variable should be set high enough to generate enough rooms but low enough so it reduces program load time
          It does not relate to how many rooms will be played on the floor by the end of the game

        */
        public int numberOfRoomBranches = 150; //the number of layers that the middle room will expand by

        [Space(10)]
        [Header("Hover over variables for description")]
        [Tooltip("The amount of rooms that one room can expand to when branching")]
        [Range(0, 3)] public int roomExpandConnectionLimit; //MAX IS 3 - the max number of rooms the algorithm COULD expand to
        [Space(10)]
    
        public GameObject[] RoomPrefabs; //an array of room prefabs - pick any of these to randomise rooms
        public GameObject StartingRoomPrefab; //set a specific starting room

        public MapGrid grid; //reference to the grid

        //a list of all active rooms
        public List<Room> activeRooms = new List<Room>();

        void Start() {

            GenerateRooms();

        }

        //remove all rooms - must set inactive first
        public void DeleteRooms() {

            for (int count = 0; count < activeRooms.Count; count++)
                activeRooms[count].RoomVisualiser.SetActive(false);
        
            for (int count = 0; count < activeRooms.Count; count++) 
                Destroy(activeRooms[count].RoomVisualiser);

            activeRooms.Clear();

        }

        public void GenerateRooms() {

            //create map/grid of rooms:
            int mapLengthWidth = (numberOfRoomBranches + numberOfRoomBranches + 1); //eg if 10; 10 rooms in one direction + 10 rooms in other direction + centre room
            grid = new MapGrid(mapLengthWidth);
            grid.Create();

            //begin activating rooms:
            MapGrid.Rooms[MapGrid.MiddleRoom].Activated = true;
            ExpandRooms(MapGrid.Rooms[MapGrid.MiddleRoom]); //begin the first 'layer' expansion

            //create visualisers for rooms:
            for (int count = 1; count <= grid.Size; count++) { //for each grid cell

                Room room = MapGrid.Rooms[count]; //get the room at the grid cell

                if (count == MapGrid.MiddleRoom) {
                    //show starting room prefab:
                    room.RoomVisualiser = (GameObject)Instantiate(StartingRoomPrefab);
                    room.RoomVisualiser.transform.position = room.Position;

                    activeRooms.Add(room);
                } else

                if (room.Activated) { //if grid cell IS a room

                    //create room objects
                    int randomRoom = UnityEngine.Random.Range(0, RoomPrefabs.Length);

                    room.RoomVisualiser = (GameObject)Instantiate(RoomPrefabs[randomRoom]);
                    room.RoomVisualiser.transform.position = room.Position;

                    activeRooms.Add(room);

                }

            }

            // create door connections:

            //for each activated room
            for (int r = 0; r < activeRooms.Count; r++) {
                Room room = activeRooms[r];

                //connect corresponding rooms doors or set door to inactive if no connection
                room.ConnectDoors();

                //build navmeshs' on floors
                room.RoomVisualiser.transform.Find("Floor").GetComponent<NavMeshSurface>().BuildNavMesh();

            }

            loading.SetActive(false);
        }

        //using the start room, start activating rooms outward
        private void ExpandRooms(Room startRoom) {

            List<Room> newRooms = new List<Room>();

            for (int level = 0; level < numberOfRoomBranches; level++) { //expand in branches/layers (numberOfRoomBranches times)

                //deep copy rooms
                List<Room> expandableRooms = new List<Room>();
                for (int i = 0; i < newRooms.Count; i++)
                    expandableRooms.Add(newRooms[i]);
                newRooms.Clear();

                //add start room
                if (level == 0)
                    expandableRooms.Add(startRoom);

                for (int count = 0; count < expandableRooms.Count; count++) {

                    Room room = expandableRooms[count];

                    //make sure a room with no possible ways to expand, doesnt attempt to expand
                    if (room.GetActiveConnectedRooms().Count <= roomExpandConnectionLimit) {

                        //number of rooms to expand to: (random 1,2,3 or 4, depending on connectedRooms)
                        int minimumRooms = 1;
                        int activeRooms = 0;
                        for (int l = 1; l <= MapGrid.Rooms.Count; l++) {
                            if (MapGrid.Rooms[l].Activated == true)
                                activeRooms++;
                        }
                        if (activeRooms >= numberOfRoomBranches)
                            minimumRooms = 0;
                        int roomsToActivate = UnityEngine.Random.Range(minimumRooms, (roomExpandConnectionLimit - room.GetActiveConnectedRooms().Count + 1));

                        if (roomsToActivate > 0) {

                            //for X number of rooms:
                            for (int rooms = 1; rooms <= roomsToActivate; rooms++) {

                                //pick random side (left,up,right,down | 25, 50, 75, 100)
                                Relative side = Relative.NULL;
                                do {
                                    int randomNumberForSide = UnityEngine.Random.Range(0, 100);
                                    if (randomNumberForSide < 25) {
                                        side = Relative.LEFT;
                                    } else if (randomNumberForSide < 50) {
                                        side = Relative.UP;
                                    } else if (randomNumberForSide < 75) {
                                        side = Relative.RIGHT;
                                    } else if (randomNumberForSide < 100) {
                                        side = Relative.DOWN;
                                    }
                                } while (MapGrid.Rooms[room.ConnectedRooms[side]].Activated == true); //ignore if room is already activated

                                //activate the room
                                MapGrid.Rooms[room.ConnectedRooms[side]].Activated = true;
                                newRooms.Add(MapGrid.Rooms[room.ConnectedRooms[side]]);

                            }
                        }

                    }

                }
            }

        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MapGen {
    public class MapGrid {

        //debug settings
        public bool visualise = true;

        //how close together are the room visualisers ?
        public float spacing = 5;

        public int GridWidthHeight; //the grid width and length
        public int Size; //the number of total cells inside grid
        public static int MiddleRoom; //the ID of the middle room
        public static Dictionary<int, Room> Rooms; //able to return room from ID (Room room = Rooms[ID];)
    
        public MapGrid(int gridWidthHeight) {
            Rooms = new Dictionary<int, Room>(); //create empty dictionary - linking the ID's to the room reference

            GridWidthHeight = gridWidthHeight;
            Size = gridWidthHeight * gridWidthHeight;
            MiddleRoom = (int)(Size / 2 + 1);
        }

        public void Create() {

            MapGeneration mapGenerator = (MapGeneration) GameObject.Find("MapGenerator").GetComponent("MapGeneration");

            float x = 0; //x = row
            float z = 0; //z = column

            //for each room:
            for (int ID = 1; ID <= Size; ID++) {

                //create room:
                Vector3 roomPosition = new Vector3(x, 0, z);
                Rooms.Add(ID, new Room(ID, roomPosition));

                //add spacing for next room:
                x += spacing;

                if ((x/spacing) == GridWidthHeight) {
                    z += spacing;
                    x = 0;
                }

            }

            //for each room:
            for (int ID = 1; ID <= Size; ID++) {

                //add it's roomConnections:

                //up
                int downID = ID - (mapGenerator.numberOfRoomBranches * 2) - 1;
                if (downID > 0 && downID <= Size && Rooms.ContainsKey(downID)) {
                    Rooms[ID].ConnectedRooms.Add(Relative.DOWN, downID);
                }

                //down
                int upID = ID + (mapGenerator.numberOfRoomBranches * 2) + 1;
                if (upID > 0 && upID <= Size && Rooms.ContainsKey(upID)) {
                    Rooms[ID].ConnectedRooms.Add(Relative.UP, upID);
                }

                //left
                int leftID = ID - 1;
                if (leftID > 0 && leftID <= Size && Rooms.ContainsKey(leftID)) {
                    Rooms[ID].ConnectedRooms.Add(Relative.LEFT, leftID);
                }

                //right
                int rightID = ID + 1;
                if (rightID > 0 && rightID <= Size && Rooms.ContainsKey(rightID)) {
                    Rooms[ID].ConnectedRooms.Add(Relative.RIGHT, rightID);
                }

            }

        }

    }
}
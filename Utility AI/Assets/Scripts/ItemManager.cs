﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour {

    public int maxWaterInMap = 5;
    public int maxFoodInMap = 2;
    public int maxLandminesInMap = 20;

    public List<GameObject> water = new List<GameObject>();
    public List<GameObject> food = new List<GameObject>();
    public List<GameObject> landmines = new List<GameObject>();
    public GameObject flag;

	void Start () {

        GenerateItems();

	}

    //spawn initial map items
    public void GenerateItems() {
        //spawn initial items
        for(int count = 1; count <= maxWaterInMap; count++)
            SpawnRandomWater();
        for(int count = 1; count <= maxFoodInMap; count++)
            SpawnRandomFood();
        for(int count = 1; count <= maxLandminesInMap; count++)
            SpawnRandomLandmine();
        SpawnRandomFlag();
    }

    //remove all items, must set inactive first
    public void DeleteItems() {

        for(int count = 0; count < water.Count; count++)
            water[count].SetActive(false);
        for(int count = 0; count < water.Count; count++)
            Destroy(water[count]);

        for(int count = 0; count < food.Count; count++)
            food[count].SetActive(false);
        for(int count = 0; count < food.Count; count++)
            Destroy(food[count]);

        for(int count = 0; count < landmines.Count; count++)
            landmines[count].SetActive(false);
        for(int count = 0; count < landmines.Count; count++)
            Destroy(landmines[count]);

        water.Clear();
        food.Clear();
        landmines.Clear();

        Destroy(flag);

    }

    //spawn water in random location
    public void SpawnRandomWater() {
        MapManager mapManager = (MapManager) GameObject.Find("MapManager").GetComponent("MapManager");
        Vector3 randomLocation = mapManager.GetRandomLocation();

        water.Add(Instantiate(Resources.Load<GameObject>("Water"),
            new Vector3(randomLocation.x, 1f, randomLocation.z),
            Quaternion.identity));
    }

    //spawn food in random location
    public void SpawnRandomFood() {
        MapManager mapManager = (MapManager) GameObject.Find("MapManager").GetComponent("MapManager");
        Vector3 randomLocation = mapManager.GetRandomLocation();

        food.Add(Instantiate(Resources.Load<GameObject>("Food"),
            new Vector3(randomLocation.x, 1f, randomLocation.z),
            Quaternion.identity));
    }

    //spawn landmine in random location
    public void SpawnRandomLandmine() {
        MapManager mapManager = (MapManager) GameObject.Find("MapManager").GetComponent("MapManager");
        Vector3 randomLocation = mapManager.GetRandomLocation();

        landmines.Add(Instantiate(Resources.Load<GameObject>("Landmine"),
            new Vector3(randomLocation.x, 1f, randomLocation.z),
            Quaternion.identity));
    }

    //spawn flag in random location
    public void SpawnRandomFlag() {
        MapManager mapManager = (MapManager) GameObject.Find("MapManager").GetComponent("MapManager");
        CompetitorManager competitorManager = (CompetitorManager) GameObject.Find("CompetitorManager").GetComponent("CompetitorManager");

        bool firstCheck = true;
        bool competitorsNearFlag = false;
        Vector3 flagLocation = mapManager.GetRandomLocation();

        //ensure flag is not near competitors
        while(firstCheck || competitorsNearFlag) {
            competitorsNearFlag = false;
            firstCheck = false;

            flagLocation = mapManager.GetRandomLocation();

            foreach(Competitor competitor in competitorManager.competitors) {
                if(Vector3.Distance(competitor.gameObject.transform.position, flagLocation) < mapManager.minFlagDistanceFromCompetitors)
                    competitorsNearFlag = true;
            }
        }

        flag = Instantiate(Resources.Load<GameObject>("Flag"), new Vector3(flagLocation.x, 1, flagLocation.z), Quaternion.identity);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;
using UnityEngine.UI;

public class Competitor : MonoBehaviour {

    public float maxSpeed = 5;
    public float speed;

    public float healthRegenMultiplier = 1f;
    public float hungerMultiplier = 0.06f;
    public float thirstMultiplier = 0.18f;

    public float maxHealth = 10;
    public float maxHunger = 10;
    public float maxThirst = 10;

    public float health;
    public float hunger;
    public float thirst;

    public float hungerVariable = 0.8f;
    public float thirstVariable = 0.4f;
    public float healthVariable = 2;

    public float maxHealthValue;

    public CompetitorUI UI;
    public GameObject firstViewPos;
    public Button changeViewButton;

    public CameraManager cameraManager;

	void Start () {
		health = 10;
        hunger = 3;
        thirst = 3;
        maxHealthValue = hungerVariable + thirstVariable + healthVariable;

        cameraManager = Camera.main.GetComponent<CameraManager>();
        changeViewButton.onClick.AddListener(delegate { cameraManager.Switch(this); } );
	}
	
	void Update () {
		
        //each frame, increase hunger and thirst by lowering:
        hunger -= hungerMultiplier * Time.deltaTime;
        thirst -= thirstMultiplier * Time.deltaTime;

        UI.UpdateUI();

        if(hunger < 0.1) Kill(DeathTracker.DeathType.Hunger);
        if(thirst < 0.1) Kill(DeathTracker.DeathType.Thirst);

        //regen health - speed of regen depends on hunger and thirst
        if(health < 10) health += healthRegenMultiplier / (((maxHunger+1)/hunger) + ((maxThirst+1)/thirst)) * Time.deltaTime;

        float healthPercentage = GetHealthWeighting() / maxHealthValue;
        speed = maxSpeed;
        speed *= healthPercentage;

        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
        
	}

    //kill the competitor 
    public void Kill(DeathTracker.DeathType death) {
        CompetitorManager competitorManager = (CompetitorManager) GameObject.Find("CompetitorManager").GetComponent("CompetitorManager");
        UIManager uiManager = (UIManager) GameObject.Find("UIManager").GetComponent<UIManager>();
          
        //go back to third person view if competitor that's being viewed dies
        CameraManager cameraManager = Camera.main.GetComponent<CameraManager>();
        if(cameraManager.competitorViewing != null && cameraManager.competitorViewing == this) {
            cameraManager.Switch();
        }

        if(uiManager.selected != null && uiManager.selected.gameObject == this.gameObject)
            uiManager.DeselectedUI.SetActive(true);
        
        competitorManager.competitors.Remove(this);

        //play death particles at location
        GameObject bloodSplatter = Instantiate(Resources.Load<GameObject>("BloodSplatter"));
        bloodSplatter.transform.position = transform.position;
        bloodSplatter.GetComponent<ParticleSystem>().Play();

        Destroy(this.gameObject);

        uiManager.deathTracker.ShowDeath(death);    

        //if last competitor remaining, set to winner
        if(competitorManager.competitors.Count == 1) competitorManager.competitors[0].UI.winnerText.gameObject.SetActive(true);

    }

    //calculate the health weighting
    public float GetHealthWeighting() {
        return hungerVariable*(hunger / maxHunger)
                    + thirstVariable*(thirst / maxThirst)
                    + healthVariable*(health / maxHealth);
    }

}

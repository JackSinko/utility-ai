﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : PickupableItem {

    public override void PickedUpItem(Competitor competitor) {
        //don't do anything if flag is picked up
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : PickupableItem {

    public float hungerBoost = 2;

    ItemManager itemManager;
    private void Start() {
        itemManager = (ItemManager) GameObject.Find("ItemManager").GetComponent("ItemManager");
    }

    public override void PickedUpItem(Competitor competitor) {
        itemManager.SpawnRandomFood(); //replace object before it's removed
        itemManager.food.Remove(gameObject);

        //feed player
        competitor.hunger += hungerBoost;
        if(competitor.hunger > competitor.maxHunger)
            competitor.hunger = competitor.maxHunger;
    }

}

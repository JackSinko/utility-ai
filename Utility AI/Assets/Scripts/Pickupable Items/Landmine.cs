﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Pixelplacement;

public class Landmine : PickupableItem {

    public float damage = 2.5f;

    ItemManager itemManager;
    private void Start() {
        itemManager = (ItemManager)GameObject.Find("ItemManager").GetComponent("ItemManager");    
    }

    public override void PickedUpItem(Competitor competitor) {
        GameObject explosion = Instantiate(Resources.Load<GameObject>("Explosion"));
            explosion.transform.position = transform.position;
            explosion.GetComponent<ParticleSystem>().Play();

        itemManager.SpawnRandomLandmine(); //replace object before it's removed
        itemManager.landmines.Remove(gameObject);

        //remove health
        competitor.health -= damage;
        competitor.transform.Find("blood").GetComponent<ParticleSystem>().Play();

        //kill competitor ?
        if(competitor.health < 0.1) competitor.Kill(DeathTracker.DeathType.Land_Mine);
    }

}

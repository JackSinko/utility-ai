﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : PickupableItem {

    public float thirstBoost = 2;

    ItemManager itemManager;
    private void Start() {
        itemManager = (ItemManager) GameObject.Find("ItemManager").GetComponent("ItemManager");
    }

    public override void PickedUpItem(Competitor competitor) {
        itemManager.SpawnRandomWater(); //replace object before it's removed
        itemManager.water.Remove(gameObject);

        competitor.thirst += thirstBoost;
        if(competitor.thirst > competitor.maxThirst)
            competitor.thirst = competitor.maxThirst;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompetitorManager : MonoBehaviour {

    public int numberOfCompetitors = 2;

    public List<Competitor> competitors = new List<Competitor>();

    void Start() {

        GenerateCompetitors();

    }

    //spawn numberOfCompetitors competitors
    public void GenerateCompetitors() {
        for(int count = 1; count <= numberOfCompetitors; count++)
            SpawnCompetitor();
    }

    //remove all competitors from map
    public void DeleteCompetitors() {
        for(int count = 0; count < competitors.Count; count++)
            competitors[count].gameObject.SetActive(false); //must set inactive first before removing
        for(int count = 0; count < competitors.Count; count++)
            Destroy(competitors[count]); //destroy all competitors

        competitors.Clear();
    }

    //get random location on map and instantiate competitor, add to competitors list
    public void SpawnCompetitor() {

        MapManager mapManager = (MapManager) GameObject.Find("MapManager").GetComponent("MapManager");
        Vector3 randomLocation = mapManager.GetRandomLocation();

        GameObject competitorObject = Instantiate(Resources.Load<GameObject>("Competitor"),
            new Vector3(randomLocation.x, 0.5f, randomLocation.z),
            Quaternion.identity);
        Competitor competitor = (Competitor)competitorObject.gameObject.GetComponent("Competitor");

        if(competitors.Count == 1) competitor.UI.winnerText.gameObject.SetActive(false);

        competitors.Add(competitor);

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI {
    public class FIND_FOOD : Decision {

        public DecisionType type = DecisionType.FIND_FOOD;

        public float maxDistanceForAffect = 20; //in metres - the bigger, the more affect closer food has
        public float maxDistanceWeighting = 0.3f;

        public float maxHungerWeighting = 0.7f;

        private Competitor competitor;
        private NavMeshAgent agent;
        private ItemManager itemManager;
        private void Start() {
            competitor = (Competitor)this.gameObject.GetComponent("Competitor");
            agent = competitor.GetComponent<NavMeshAgent>();
            itemManager = (ItemManager)GameObject.Find("ItemManager").GetComponent("ItemManager");
        }
        public override float GetQualityWeighting() {

            if (GetNearestFood() == null)
                return 0;

            //calculate distance weighting
            float distanceToFood = Vector3.Distance(GetNearestFood().transform.position, this.gameObject.transform.position);
            float distanceWeighting;
            if (distanceToFood > maxDistanceForAffect)
                distanceWeighting = 0;
            else distanceWeighting = (maxDistanceForAffect - distanceToFood) / maxDistanceForAffect;

            //calculate thirst weighting
            Competitor competitor = (Competitor)this.gameObject.GetComponent("Competitor");
            float hungerWeighting = (competitor.maxHunger - competitor.hunger) / competitor.maxHunger;

            //multiple by max weightings
            float actualDistanceWeighting = distanceWeighting * maxDistanceWeighting;
            float actualHungerWeighting = hungerWeighting * maxHungerWeighting;

            //add the 2
            return actualDistanceWeighting + actualHungerWeighting;
        }

        public override void ExecuteAction() {
            agent.destination = GetNearestFood().transform.position;
        }

        public GameObject GetNearestFood() {

            GameObject nearestFood = null;

            float nearestFoodDistance = float.MaxValue;
            foreach (GameObject food in itemManager.food) {
                float distance = Vector3.Distance(food.transform.position, this.gameObject.transform.position);
                if (distance < nearestFoodDistance) {
                    nearestFoodDistance = distance;
                    nearestFood = food;
                }
            }

            return nearestFood;
        }

        public override DecisionType GetDecisionType() { return type; }

    }
}
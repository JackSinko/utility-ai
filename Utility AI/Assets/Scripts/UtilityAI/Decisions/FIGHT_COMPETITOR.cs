﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI {
    public class FIGHT_COMPETITOR : Decision {

        public DecisionType type = DecisionType.FIGHT_COMPETITOR;

        public float distanceToFlagVariable = 2.5f;

        public float distanceVariable = 1.5f;
        public float maxDistanceToFight = 20;

        // most likely to ignore = 3.7
        // most likely to attack = (approaching) 0

        private Competitor competitor;
        private NavMeshAgent agent;
        private ItemManager itemManager;
        private CompetitorManager competitorManager;
        private void Start() {
            competitor = (Competitor)this.gameObject.GetComponent("Competitor");
            agent = competitor.GetComponent<NavMeshAgent>();
            itemManager = (ItemManager)GameObject.Find("ItemManager").GetComponent("ItemManager");
            competitorManager = (CompetitorManager)GameObject.Find("CompetitorManager").GetComponent("CompetitorManager");
        }

        public override float GetQualityWeighting() {

            Competitor competitorToFight = GetBestCompetitorToFight();
            if (competitorToFight != null) {

                float maxWeighting = GetComponent<Competitor>().hungerVariable
                    + GetComponent<Competitor>().thirstVariable
                    + GetComponent<Competitor>().healthVariable
                    + distanceVariable;
                // = least likely to fight

                float distanceToFlagWeighting;

                float enemyDistanceToFlag = Vector3.Distance(competitorToFight.gameObject.transform.position, itemManager.flag.transform.position);
                float competitorDistanceToFlag = Vector3.Distance(this.gameObject.transform.position, itemManager.flag.transform.position);
                if (enemyDistanceToFlag < competitorDistanceToFlag)
                    distanceToFlagWeighting = (competitorDistanceToFlag - enemyDistanceToFlag) / competitorDistanceToFlag;
                else distanceToFlagWeighting = 0;

                float distance = Vector3.Distance(competitorToFight.gameObject.transform.position, this.gameObject.transform.position);

                float overallWeighting = GetComponent<Competitor>().hungerVariable * ((competitorToFight.maxHunger - competitorToFight.hunger) / competitorToFight.maxHunger)
                    + GetComponent<Competitor>().thirstVariable * ((competitorToFight.maxThirst - competitorToFight.thirst) / competitorToFight.maxThirst)
                    + GetComponent<Competitor>().healthVariable * ((competitorToFight.maxHealth - competitorToFight.health) / competitorToFight.maxHealth)
                    + distanceToFlagVariable * (distanceToFlagWeighting)
                    + distanceVariable * ((maxDistanceToFight - distance) / maxDistanceToFight);

                return overallWeighting / maxWeighting;

            }
            return 0;
        }

        public override void ExecuteAction() {

            //move toward enemy
            Competitor enemy = GetBestCompetitorToFight();

            agent.destination = enemy.gameObject.transform.position;

            float distance = Vector3.Distance(enemy.gameObject.transform.position, this.gameObject.transform.position);
            if (distance < 2.5f) {
                float damage = GetComponent<Competitor>().GetHealthWeighting(); //max damage is 3.2
                enemy.health -= damage * Time.deltaTime;

                enemy.transform.Find("blood").GetComponent<ParticleSystem>().Play();

                if (enemy.health < 0.1) enemy.Kill(DeathTracker.DeathType.Fighting);
            }

        }

        public Competitor GetBestCompetitorToFight() {
            float bestWeighting = 0;
            Competitor bestCompetitor = null;

            foreach (Competitor competitor in competitorManager.competitors) {

                if (competitor != this.gameObject.GetComponent("Competitor")) {

                    float distance = Vector3.Distance(competitor.gameObject.transform.position, this.gameObject.transform.position);
                    if (distance < maxDistanceToFight) {
                        float overallWeighting = GetComponent<Competitor>().hungerVariable * ((competitor.maxHunger - competitor.hunger) / competitor.maxHunger)
                        + GetComponent<Competitor>().thirstVariable * ((competitor.maxThirst - competitor.thirst) / competitor.maxThirst)
                        + GetComponent<Competitor>().healthVariable * ((competitor.maxHealth - competitor.health) / competitor.maxHealth)
                            + distanceVariable * ((maxDistanceToFight - distance) / maxDistanceToFight);

                        if (overallWeighting > bestWeighting)
                            bestCompetitor = competitor;

                    }

                }
            }

            return bestCompetitor;
        }

        public override DecisionType GetDecisionType() { return type; }

    }
}
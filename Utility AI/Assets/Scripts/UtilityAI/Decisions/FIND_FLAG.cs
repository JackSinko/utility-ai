﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI {
    public class FIND_FLAG : Decision {

        public DecisionType type = DecisionType.FIND_FLAG;

        public int maxDistanceForAffect = 40;
        public float maxDistanceWeighting = 0.2f;

        public float maxEnemyWeighting = 0.5f;

        public float maxHealthWeighting = 0.2f;

        //overall health factors:
        public float hungerVariable = 0.8f;
        public float thirstVariable = 0.4f;
        public float healthVariable = 0.5f;

        private ItemManager itemManager;
        private Competitor competitor;
        private NavMeshAgent agent;
        private CompetitorManager competitorManager;
        private void Start() {
            itemManager = (ItemManager)GameObject.Find("ItemManager").GetComponent("ItemManager");
            competitor = (Competitor)this.gameObject.GetComponent("Competitor");
            agent = competitor.GetComponent<NavMeshAgent>();
            competitorManager = (CompetitorManager)GameObject.Find("CompetitorManager").GetComponent("CompetitorManager");
        }

        public override float GetQualityWeighting() {

            ItemManager itemManager = (ItemManager)GameObject.Find("ItemManager").GetComponent("ItemManager");
            Competitor competitor = (Competitor)this.gameObject.GetComponent("Competitor");

            //calculate distance factor: (closer the better)
            float distanceToFlag = Vector3.Distance(itemManager.flag.transform.position, this.gameObject.transform.position);
            float distanceWeighting;
            if (distanceToFlag > maxDistanceForAffect)
                distanceWeighting = 0;
            else distanceWeighting = (maxDistanceForAffect - distanceToFlag) / maxDistanceForAffect;

            //enemies closer to flag = higher weighting
            float enemyWeighting;
            Competitor closestCompetitor = GetClosestCompetitorToFlag();
            if (closestCompetitor == null) return 0;
            float enemyDistance = Vector3.Distance(closestCompetitor.gameObject.transform.position, itemManager.flag.transform.position);
            float competitorDistance = Vector3.Distance(this.gameObject.transform.position, itemManager.flag.transform.position);
            if (enemyDistance < competitorDistance)
                enemyWeighting = (competitorDistance - enemyDistance) / competitorDistance;
            else enemyWeighting = maxEnemyWeighting;

            //overall health weighting
            float overallWeighting = hungerVariable * (competitor.hunger / competitor.maxHunger)
                    + thirstVariable * (competitor.thirst / competitor.maxThirst)
                    + healthVariable * (competitor.health / competitor.maxHealth);

            float actualDistanceWeighting = maxDistanceWeighting * distanceWeighting;
            float actualEnemyWeighting = maxEnemyWeighting * enemyWeighting;
            float actualOverallWeighting = maxHealthWeighting * overallWeighting;

            return actualDistanceWeighting + actualEnemyWeighting + actualOverallWeighting;
        }

        public override void ExecuteAction() {
            agent.destination = itemManager.flag.transform.position;
        }

        public Competitor GetClosestCompetitorToFlag() {
            float closestDistance = float.MaxValue;
            Competitor closestCompetitor = null;

            foreach (Competitor competitor in competitorManager.competitors) {

                if (competitor != this.gameObject.GetComponent("Competitor")) {

                    float distance = Vector3.Distance(competitor.gameObject.transform.position, itemManager.flag.transform.position);
                    if (distance < closestDistance)
                        closestCompetitor = competitor;

                }
            }

            return closestCompetitor;
        }

        public override DecisionType GetDecisionType() { return type; }

    }
}
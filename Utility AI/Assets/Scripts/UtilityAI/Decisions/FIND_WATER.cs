﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace UtilityAI {
    public class FIND_WATER : Decision {

        public DecisionType type = DecisionType.FIND_WATER;

        public float maxDistanceForAffect = 10; //in metres - the bigger, the more affect closer water has
        public float maxDistanceWeighting = 0.2f;

        public float maxThirstWeighting = 0.8f;

        private Competitor competitor;
        private NavMeshAgent agent;
        private ItemManager itemManager;
        private void Start() {
            competitor = (Competitor)this.gameObject.GetComponent("Competitor");
            agent = competitor.GetComponent<NavMeshAgent>();
            itemManager = (ItemManager)GameObject.Find("ItemManager").GetComponent("ItemManager");
        }

        public override float GetQualityWeighting() {

            if (GetNearestWater() == null)
                return 0;

            //calculate distance weighting
            float distanceToWater = Vector3.Distance(GetNearestWater().transform.position, this.gameObject.transform.position);
            float distanceWeighting;
            if (distanceToWater > maxDistanceForAffect)
                distanceWeighting = 0;
            else distanceWeighting = (maxDistanceForAffect - distanceToWater) / maxDistanceForAffect;

            //calculate thirst weighting
            Competitor competitor = (Competitor)this.gameObject.GetComponent("Competitor");
            float thirstWeighting = (competitor.maxThirst - competitor.thirst) / competitor.maxThirst;

            //multiple by max weightings
            float actualDistanceWeighting = distanceWeighting * maxDistanceWeighting;
            float actualThirstWeighting = thirstWeighting * maxThirstWeighting;

            //add the 2
            return actualDistanceWeighting + actualThirstWeighting;
        }

        public override void ExecuteAction() {
            agent.destination = GetNearestWater().transform.position;
        }

        public GameObject GetNearestWater() {

            GameObject nearestWater = null;

            float nearestWaterDistance = float.MaxValue;
            foreach (GameObject water in itemManager.water) {
                float distance = Vector3.Distance(water.transform.position, this.gameObject.transform.position);
                if (distance < nearestWaterDistance) {
                    nearestWaterDistance = distance;
                    nearestWater = water;
                }
            }

            return nearestWater;
        }

        public override DecisionType GetDecisionType() { return type; }

    }
}
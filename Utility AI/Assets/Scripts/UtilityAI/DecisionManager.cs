﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UtilityAI {
    public class DecisionManager : MonoBehaviour {

        public Decision bestDecision;
        public List<Decision> decisions = new List<Decision>();

        void Start() {
            //initialise bestDecision
            bestDecision = decisions[decisions.Count - 1];
        }

        void Update() {

            // calculate best decision:
            foreach (Decision decision in decisions) {

                if (decision.GetQualityWeighting() > bestDecision.GetQualityWeighting())
                    bestDecision = decision;

            }

            // execute the best decision's action
            bestDecision.ExecuteAction();

        }

    }
}
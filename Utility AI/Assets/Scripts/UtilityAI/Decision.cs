﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UtilityAI {
    public enum DecisionType {
        FIND_WATER,
        FIND_FOOD,
        FIND_FLAG,
        FIGHT_COMPETITOR,
    }

    //a pure virtual class - can't reference directly
    public abstract class Decision : MonoBehaviour {

        //calculate the quality weighting for decision
        public virtual float GetQualityWeighting() { return 0; }

        //execute the decision's action
        public virtual void ExecuteAction() { }

        public virtual DecisionType GetDecisionType() { return 0; }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum View {
    FIRST,
    THIRD,
};

public class CameraManager : MonoBehaviour {

    public Competitor competitorViewing = null; //reference to the competitor that is being viewed - null if none
    public GameObject thirdViewPos; //the transform object for third person view
    
    public View currentView = View.THIRD;

    private CompetitorManager competitorManager;
    public void Start() {
        competitorManager = (CompetitorManager) GameObject.Find("CompetitorManager").GetComponent("CompetitorManager");
    }

    public float lerpTime = 0.05f;

    public bool viewChangeComplete = false;
    public bool isUpdating = false;
    public float time = 1;
    public float t = 0;
    void Update () {

        if(!isUpdating) { //not switching view
            if(currentView == View.THIRD) {
                //lerp camera to centre of all competitors
                UpdateAverageCompetitorPosition(competitorManager);
                transform.position = Vector3.Lerp(transform.position, averageCompetitorPosition, lerpTime);
                transform.rotation = Quaternion.Slerp(transform.rotation, thirdViewPos.transform.rotation, lerpTime);
            } else {
                //copy position/rotation of competitor
                transform.position = competitorViewing.firstViewPos.transform.position;
                transform.rotation = competitorViewing.firstViewPos.transform.rotation;
            }
        } else { //if switching view
            t += Time.unscaledDeltaTime / time;
            if(currentView == View.THIRD) {
                //lerp camera from first person to centre of all competitors
                UpdateAverageCompetitorPosition(competitorManager);
                transform.position = Vector3.Lerp(transform.position, averageCompetitorPosition, t);
                transform.rotation = Quaternion.Slerp(transform.rotation, thirdViewPos.transform.rotation, t);
            } else {
                //lerp camera from third person to first person view
                transform.position = Vector3.Lerp(transform.position, competitorViewing.firstViewPos.transform.position, t);
                transform.rotation = Quaternion.Slerp(transform.rotation, competitorViewing.firstViewPos.transform.rotation, t);
            }
            if(t >= 1) {
                isUpdating = false; t = 0; //reset
                viewChangeComplete = true;
            }
        }

	}

    //switch to third person view
    public void Switch() {
        isUpdating = true; t = 0;
        currentView = View.THIRD;
        if(competitorViewing != null) {
            competitorViewing.changeViewButton.onClick.AddListener(delegate { Switch(competitorViewing); } );
            competitorViewing.changeViewButton.gameObject.transform.Find("Text").GetComponent<Text>().text = "First Person View";
        }
        viewChangeComplete = false; //reset
    }
    //switch to competitor view
    public void Switch(Competitor competitor) { 
        isUpdating = true; t = 0;
        currentView = View.FIRST;
        competitorViewing = competitor;
        competitorViewing.changeViewButton.onClick.AddListener(delegate { Switch(); } );
        competitorViewing.changeViewButton.gameObject.transform.Find("Text").GetComponent<Text>().text = "Third Person View";
        viewChangeComplete = false; //reset
    } 

    //gets the mean position of all competitors
    private Vector3 averageCompetitorPosition;
    public void UpdateAverageCompetitorPosition(CompetitorManager competitorManager) {
        if(competitorManager.competitors.Count > 0) {
            averageCompetitorPosition = Vector3.zero;
            foreach(Competitor competitor in competitorManager.competitors)
                averageCompetitorPosition += competitor.gameObject.transform.position;
            averageCompetitorPosition /= competitorManager.competitors.Count; //get mean position

            averageCompetitorPosition = new Vector3(averageCompetitorPosition.x, thirdViewPos.transform.position.y, averageCompetitorPosition.z-40);
        }
    }

}

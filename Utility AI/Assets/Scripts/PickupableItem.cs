﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//add this class to all items that can be picked up
public class PickupableItem : MonoBehaviour {

    public void OnTriggerEnter(Collider collider) {
        if(collider.gameObject.tag == "Competitor") {
            PickedUpItem((Competitor)collider.gameObject.GetComponent("Competitor"));
            Destroy(gameObject);
        }
    }

    //override in derived class to perform action
    public virtual void PickedUpItem(Competitor competitor) { }

}
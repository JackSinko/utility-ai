﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MapGen;

public class MapManager : MonoBehaviour {

    public GameObject loading;

    public Slider timeSlider;
    public Slider roomBranchSlider;
    public Slider roomExpandSlider;
    public Slider waterAmountSlider;
    public Slider foodAmountSlider;
    public Slider landmineAmountSlider;
    public Slider competitorAmountSlider;

    public int minFlagDistanceFromCompetitors = 1;

    private MapGeneration mapGenerator;
    private CompetitorManager competitorManager;
    private ItemManager itemManager;
    private UIManager uiManager;
    public void Start() {
        ResetMapButton(); //load initial map
        mapGenerator = (MapGeneration) GameObject.Find("MapGenerator").GetComponent("MapGeneration");
        competitorManager = (CompetitorManager) GameObject.Find("CompetitorManager").GetComponent("CompetitorManager");
        itemManager = (ItemManager) GameObject.Find("ItemManager").GetComponent("ItemManager");
        uiManager = (UIManager) GameObject.Find("UIManager").GetComponent("UIManager");
    }

    public Vector3 GetRandomLocation() {
        return mapGenerator.activeRooms[Random.Range(0, mapGenerator.activeRooms.Count)].Position;
    }

    public void SetTimeScale() {
        Time.timeScale = timeSlider.value;
    }

    public void SetRoomBranches() {
        mapGenerator.numberOfRoomBranches = (int) roomBranchSlider.value;
    }

    public void SetRoomExpansions() {
        mapGenerator.roomExpandConnectionLimit = (int) roomExpandSlider.value;
    }

    public void SetWaterInMap() {
        itemManager.maxWaterInMap = (int) waterAmountSlider.value;
    }

    public void SetFoodInMap() {
        itemManager.maxFoodInMap = (int) foodAmountSlider.value;
    }

    public void SetLandminesInMap() {
        itemManager.maxLandminesInMap = (int) landmineAmountSlider.value;
    }

    public void SetCompetitorsInMap() {
        competitorManager.numberOfCompetitors = (int) competitorAmountSlider.value;
    }

    public void ResetMapButton() {
        loading.SetActive(true);
        StartCoroutine(ResetMap());
    }

    //run on different thread:
    public IEnumerator ResetMap() {

        yield return new WaitForSeconds(0.01f);

        Camera.main.GetComponent<CameraManager>().Switch();

        //run on different thread:

        uiManager.selected = null;

        competitorManager.DeleteCompetitors();
        itemManager.DeleteItems();
        mapGenerator.DeleteRooms();

        mapGenerator.GenerateRooms();
        itemManager.GenerateItems();
        competitorManager.GenerateCompetitors();

        loading.SetActive(false);

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UtilityAI;

public class CompetitorUI : MonoBehaviour {

    //managers
    public Competitor competitor;
    public DecisionManager decisionManager;

    //UI:
    public GameObject everything;
    public Text winnerText;
    public GameObject path;
    public Text bestDecisionText;
    public Text healthWeightingText;
    public SimpleHealthBar healthBar;
    public SimpleHealthBar hungerBar;
    public SimpleHealthBar thirstBar;
    public GameObject health;
    public GameObject hunger;
    public GameObject thirst;
    public Text decisionsText;

    //toggles:
	public Toggle everythingToggle;
    public Toggle pathToggle;
    public Toggle bestDecisionToggle;

    public Toggle healthToggle; //turns off healthWeightingText and all health bars too
    public Toggle healthBarToggle;
    public Toggle thirstBarToggle;
    public Toggle hungerBarToggle;

    public Toggle decisionsToggle;

    //tracking:
    public bool lastUIToggleValue = true;

    //events:

    public void ToggleEverything() {
        everything.SetActive(everythingToggle.isOn);
    }

    public void TogglePathLine() {
        path.gameObject.SetActive(pathToggle.isOn);
    }

    public void ToggleBestDecisionText() {
        bestDecisionText.gameObject.SetActive(bestDecisionToggle.isOn);
    }

    public void ToggleHealth() {
        healthWeightingText.gameObject.SetActive(healthToggle.isOn);
        health.gameObject.SetActive(healthToggle.isOn);
        thirst.gameObject.SetActive(healthToggle.isOn);
        hunger.gameObject.SetActive(healthToggle.isOn);
    }

    public void ToggleHealthBar() {
        health.gameObject.SetActive(healthBarToggle.isOn);
    }
    public void ToggleThirstBar() {
        thirst.gameObject.SetActive(thirstBarToggle.isOn);
    }
    public void ToggleHungerBar() {
        hunger.gameObject.SetActive(hungerBarToggle.isOn);
    }

    public void ToggleDecisions() {
        decisionsText.gameObject.SetActive(decisionsToggle.isOn);
    }

    public void UpdateUI() {

        UIManager uiManager = (UIManager) GameObject.Find("UIManager").GetComponent<UIManager>();
        if(lastUIToggleValue == true && uiManager.enableUIToggle.isOn == false) {
             lastUIToggleValue = false; everything.SetActive(false); return;
        }
        if(lastUIToggleValue == false && uiManager.enableUIToggle.isOn == true) {
             lastUIToggleValue = true; everything.SetActive(true);
        }

        // update bars
        healthBar.UpdateBar(competitor.health, competitor.maxHealth);
        hungerBar.UpdateBar(competitor.hunger, competitor.maxHunger);
        thirstBar.UpdateBar(competitor.thirst, competitor.maxThirst);

        // update UI:
        
        //empty string
        string decisionText = "";
        foreach(Decision decision in decisionManager.decisions)
            decisionText = decisionText + decision.GetDecisionType() + " = " + (int)(decision.GetQualityWeighting()*100) + "%\n";
        decisionsText.text = decisionText;
            
        bestDecisionText.text = decisionManager.bestDecision.GetDecisionType() + "\n(" + (int)(decisionManager.bestDecision.GetQualityWeighting()*100) + "%)";
        healthWeightingText.text = "Health Weighting: " + (int)((competitor.GetHealthWeighting()/competitor.maxHealthValue)*100) + "%";

    }

}

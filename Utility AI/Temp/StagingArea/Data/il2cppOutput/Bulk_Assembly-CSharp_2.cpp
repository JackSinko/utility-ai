﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// CameraManager
struct CameraManager_t3272490737;
// Competitor
struct Competitor_t2775849210;
// CompetitorUI
struct CompetitorUI_t472322071;
// Competitor[]
struct CompetitorU5BU5D_t3192099295;
// ItemManager
struct ItemManager_t3254073967;
// PickupableItem
struct PickupableItem_t2078450402;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t1617499438;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1930798642;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<UnityEngine.AI.NavMeshModifier>
struct IEnumerable_1_t4285525985;
// System.Collections.Generic.IEnumerable`1<UnityEngine.AI.NavMeshModifierVolume>
struct IEnumerable_1_t2273740073;
// System.Collections.Generic.List`1<Competitor>
struct List_1_t4247923952;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>
struct List_1_t2785658607;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>
struct List_1_t2160260967;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>
struct List_1_t2482780542;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>
struct List_1_t470994630;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>
struct List_1_t2459021783;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UtilityAI.Decision>
struct List_1_t3817059745;
// System.Delegate
struct Delegate_t1188392813;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.Predicate`1<UnityEngine.AI.NavMeshBuildSource>
struct Predicate_1_t1513480349;
// System.Predicate`1<UnityEngine.AI.NavMeshModifier>
struct Predicate_1_t1835999924;
// System.Predicate`1<UnityEngine.AI.NavMeshModifierVolume>
struct Predicate_1_t4119181308;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate
struct OnNavMeshPreUpdate_t1580782682;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.AI.NavMeshBuildMarkup[]
struct NavMeshBuildMarkupU5BU5D_t3844975876;
// UnityEngine.AI.NavMeshBuildSource[]
struct NavMeshBuildSourceU5BU5D_t2809533356;
// UnityEngine.AI.NavMeshData
struct NavMeshData_t1084598030;
// UnityEngine.AI.NavMeshModifier
struct NavMeshModifier_t1010705800;
// UnityEngine.AI.NavMeshModifierVolume
struct NavMeshModifierVolume_t3293887184;
// UnityEngine.AI.NavMeshModifierVolume[]
struct NavMeshModifierVolumeU5BU5D_t3251859441;
// UnityEngine.AI.NavMeshModifier[]
struct NavMeshModifierU5BU5D_t1673145177;
// UnityEngine.AI.NavMeshObstacle
struct NavMeshObstacle_t3657678309;
// UnityEngine.AI.NavMeshSurface
struct NavMeshSurface_t986947041;
// UnityEngine.AI.NavMeshSurface[]
struct NavMeshSurfaceU5BU5D_t111833468;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.TerrainData
struct TerrainData_t657004131;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UtilityAI.Decision
struct Decision_t2344985003;
// UtilityAI.DecisionManager
struct DecisionManager_t2018219593;
// UtilityAI.Decision[]
struct DecisionU5BU5D_t928875338;
// UtilityAI.FIGHT_COMPETITOR
struct FIGHT_COMPETITOR_t2592966111;
// UtilityAI.FIND_FLAG
struct FIND_FLAG_t1938094438;
// UtilityAI.FIND_FOOD
struct FIND_FOOD_t1534940982;
// UtilityAI.FIND_WATER
struct FIND_WATER_t1027203745;
// Water
struct Water_t1083516957;

extern RuntimeClass* CompetitorManager_t1763731731_il2cpp_TypeInfo_var;
extern RuntimeClass* Competitor_t2775849210_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t385246372_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* ItemManager_t3254073967_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t128053199_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2160260967_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2459021783_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2482780542_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2785658607_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3817059745_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t470994630_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern RuntimeClass* Mesh_t3648964284_il2cpp_TypeInfo_var;
extern RuntimeClass* NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var;
extern RuntimeClass* NavMeshModifier_t1010705800_il2cpp_TypeInfo_var;
extern RuntimeClass* NavMeshSurface_t986947041_il2cpp_TypeInfo_var;
extern RuntimeClass* NavMesh_t1865600375_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* OnNavMeshPreUpdate_t1580782682_il2cpp_TypeInfo_var;
extern RuntimeClass* Predicate_1_t1513480349_il2cpp_TypeInfo_var;
extern RuntimeClass* Predicate_1_t1835999924_il2cpp_TypeInfo_var;
extern RuntimeClass* Predicate_1_t4119181308_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* TerrainData_t657004131_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1189461543;
extern String_t* _stringLiteral1771024816;
extern String_t* _stringLiteral2062271667;
extern String_t* _stringLiteral2249249573;
extern String_t* _stringLiteral758907337;
extern const RuntimeMethod* Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisParticleSystem_t1800779281_m3884485303_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisNavMeshModifierVolume_t3293887184_m3885906544_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisNavMeshModifier_t1010705800_m3813049907_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1341201278_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m2830648961_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3610717444_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m446944907_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m465728948_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m734651789_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1707601366_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m3090114117_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m363784717_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4286844348_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m860927647_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m936905458_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1310551773_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2922767688_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3267056561_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4179928398_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4223334170_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m601408329_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisNavMeshAgent_t1276799816_m2253338685_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisNavMeshObstacle_t3657678309_m1899691350_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2814401212_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2821620141_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3904861453_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3920533274_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m863973498_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m4011681093_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m4164730806_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m939277675_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m1287074780_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m1750140655_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2888270272_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2970145501_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m562693788_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m741968409_RuntimeMethod_var;
extern const RuntimeMethod* List_1_IndexOf_m2639980317_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m1679687378_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m3207188928_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAll_m616612175_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m2403847378_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m4063777476_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m4119032061_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m795306531_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1410039760_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1475854777_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1544123386_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1940349360_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3019387110_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3566324597_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3641213752_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3711269370_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m530909711_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1142946597_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m3200332207_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m361000296_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1770120800_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m2061167563_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m888956288_RuntimeMethod_var;
extern const RuntimeMethod* NavMeshSurface_U3CAppendModifierVolumesU3Em__0_m837462505_RuntimeMethod_var;
extern const RuntimeMethod* NavMeshSurface_U3CCollectSourcesU3Em__1_m4201889746_RuntimeMethod_var;
extern const RuntimeMethod* NavMeshSurface_U3CCollectSourcesU3Em__2_m3444190560_RuntimeMethod_var;
extern const RuntimeMethod* NavMeshSurface_U3CCollectSourcesU3Em__3_m3645889326_RuntimeMethod_var;
extern const RuntimeMethod* NavMeshSurface_UpdateActive_m974852644_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m1542028990_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m2280749923_RuntimeMethod_var;
extern const RuntimeMethod* Predicate_1__ctor_m3152402189_RuntimeMethod_var;
extern const uint32_t DecisionManager_Start_m4131940883_MetadataUsageId;
extern const uint32_t DecisionManager_Update_m3095171479_MetadataUsageId;
extern const uint32_t DecisionManager__ctor_m1584365721_MetadataUsageId;
extern const uint32_t FIGHT_COMPETITOR_ExecuteAction_m520634744_MetadataUsageId;
extern const uint32_t FIGHT_COMPETITOR_GetBestCompetitorToFight_m2559081439_MetadataUsageId;
extern const uint32_t FIGHT_COMPETITOR_GetQualityWeighting_m1694195179_MetadataUsageId;
extern const uint32_t FIND_FLAG_ExecuteAction_m2906779793_MetadataUsageId;
extern const uint32_t FIND_FLAG_GetClosestCompetitorToFlag_m2476932474_MetadataUsageId;
extern const uint32_t FIND_FLAG_GetQualityWeighting_m3316420969_MetadataUsageId;
extern const uint32_t FIND_FOOD_ExecuteAction_m3824330897_MetadataUsageId;
extern const uint32_t FIND_FOOD_GetNearestFood_m2802532437_MetadataUsageId;
extern const uint32_t FIND_FOOD_GetQualityWeighting_m3274836651_MetadataUsageId;
extern const uint32_t FIND_WATER_ExecuteAction_m983409832_MetadataUsageId;
extern const uint32_t FIND_WATER_GetNearestWater_m563822326_MetadataUsageId;
extern const uint32_t FIND_WATER_GetQualityWeighting_m212962131_MetadataUsageId;
extern const uint32_t NavMeshModifierVolume_AffectsAgentType_m2327990194_MetadataUsageId;
extern const uint32_t NavMeshModifierVolume_OnDisable_m253148840_MetadataUsageId;
extern const uint32_t NavMeshModifierVolume_OnEnable_m1858538895_MetadataUsageId;
extern const uint32_t NavMeshModifierVolume__cctor_m83471128_MetadataUsageId;
extern const uint32_t NavMeshModifierVolume__ctor_m2000838765_MetadataUsageId;
extern const uint32_t NavMeshModifierVolume_get_activeModifiers_m244070183_MetadataUsageId;
extern const uint32_t NavMeshModifier_AffectsAgentType_m2140675392_MetadataUsageId;
extern const uint32_t NavMeshModifier_OnDisable_m3259703319_MetadataUsageId;
extern const uint32_t NavMeshModifier_OnEnable_m1031614580_MetadataUsageId;
extern const uint32_t NavMeshModifier__cctor_m3540064630_MetadataUsageId;
extern const uint32_t NavMeshModifier__ctor_m2009292233_MetadataUsageId;
extern const uint32_t NavMeshModifier_get_activeModifiers_m407144863_MetadataUsageId;
extern const uint32_t NavMeshSurface_Abs_m641648730_MetadataUsageId;
extern const uint32_t NavMeshSurface_AddData_m3992805541_MetadataUsageId;
extern const uint32_t NavMeshSurface_AppendModifierVolumes_m4188255682_MetadataUsageId;
extern const uint32_t NavMeshSurface_BuildNavMesh_m3042234026_MetadataUsageId;
extern const uint32_t NavMeshSurface_CalculateWorldBounds_m4178605729_MetadataUsageId;
extern const uint32_t NavMeshSurface_CollectSources_m1904116616_MetadataUsageId;
extern const uint32_t NavMeshSurface_GetBuildSettings_m1758800876_MetadataUsageId;
extern const uint32_t NavMeshSurface_GetWorldBounds_m3220460035_MetadataUsageId;
extern const uint32_t NavMeshSurface_HasTransformChanged_m251008277_MetadataUsageId;
extern const uint32_t NavMeshSurface_OnDisable_m555302565_MetadataUsageId;
extern const uint32_t NavMeshSurface_OnEnable_m3051587968_MetadataUsageId;
extern const uint32_t NavMeshSurface_Register_m2505341092_MetadataUsageId;
extern const uint32_t NavMeshSurface_U3CCollectSourcesU3Em__2_m3444190560_MetadataUsageId;
extern const uint32_t NavMeshSurface_U3CCollectSourcesU3Em__3_m3645889326_MetadataUsageId;
extern const uint32_t NavMeshSurface_Unregister_m2798101876_MetadataUsageId;
extern const uint32_t NavMeshSurface_UpdateActive_m974852644_MetadataUsageId;
extern const uint32_t NavMeshSurface_UpdateNavMesh_m3127138467_MetadataUsageId;
extern const uint32_t NavMeshSurface__cctor_m1610377881_MetadataUsageId;
extern const uint32_t NavMeshSurface__ctor_m56041203_MetadataUsageId;
extern const uint32_t NavMeshSurface_get_activeSurfaces_m457581706_MetadataUsageId;
extern const uint32_t Water_PickedUpItem_m3903491620_MetadataUsageId;

struct Int32U5BU5D_t385246372;
struct NavMeshModifierVolumeU5BU5D_t3251859441;
struct NavMeshModifierU5BU5D_t1673145177;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T4247923952_H
#define LIST_1_T4247923952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Competitor>
struct  List_1_t4247923952  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CompetitorU5BU5D_t3192099295* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4247923952, ____items_1)); }
	inline CompetitorU5BU5D_t3192099295* get__items_1() const { return ____items_1; }
	inline CompetitorU5BU5D_t3192099295** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CompetitorU5BU5D_t3192099295* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4247923952, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4247923952, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t4247923952_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	CompetitorU5BU5D_t3192099295* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t4247923952_StaticFields, ___EmptyArray_4)); }
	inline CompetitorU5BU5D_t3192099295* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline CompetitorU5BU5D_t3192099295** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(CompetitorU5BU5D_t3192099295* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4247923952_H
#ifndef LIST_1_T128053199_H
#define LIST_1_T128053199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t128053199  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t385246372* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____items_1)); }
	inline Int32U5BU5D_t385246372* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t385246372** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t385246372* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t128053199_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Int32U5BU5D_t385246372* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t128053199_StaticFields, ___EmptyArray_4)); }
	inline Int32U5BU5D_t385246372* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Int32U5BU5D_t385246372** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Int32U5BU5D_t385246372* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T128053199_H
#ifndef LIST_1_T2785658607_H
#define LIST_1_T2785658607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>
struct  List_1_t2785658607  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NavMeshBuildMarkupU5BU5D_t3844975876* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2785658607, ____items_1)); }
	inline NavMeshBuildMarkupU5BU5D_t3844975876* get__items_1() const { return ____items_1; }
	inline NavMeshBuildMarkupU5BU5D_t3844975876** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NavMeshBuildMarkupU5BU5D_t3844975876* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2785658607, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2785658607, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2785658607_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	NavMeshBuildMarkupU5BU5D_t3844975876* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2785658607_StaticFields, ___EmptyArray_4)); }
	inline NavMeshBuildMarkupU5BU5D_t3844975876* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline NavMeshBuildMarkupU5BU5D_t3844975876** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(NavMeshBuildMarkupU5BU5D_t3844975876* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2785658607_H
#ifndef LIST_1_T2160260967_H
#define LIST_1_T2160260967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>
struct  List_1_t2160260967  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NavMeshBuildSourceU5BU5D_t2809533356* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2160260967, ____items_1)); }
	inline NavMeshBuildSourceU5BU5D_t2809533356* get__items_1() const { return ____items_1; }
	inline NavMeshBuildSourceU5BU5D_t2809533356** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NavMeshBuildSourceU5BU5D_t2809533356* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2160260967, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2160260967, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2160260967_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	NavMeshBuildSourceU5BU5D_t2809533356* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2160260967_StaticFields, ___EmptyArray_4)); }
	inline NavMeshBuildSourceU5BU5D_t2809533356* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline NavMeshBuildSourceU5BU5D_t2809533356** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(NavMeshBuildSourceU5BU5D_t2809533356* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2160260967_H
#ifndef LIST_1_T2482780542_H
#define LIST_1_T2482780542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>
struct  List_1_t2482780542  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NavMeshModifierU5BU5D_t1673145177* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2482780542, ____items_1)); }
	inline NavMeshModifierU5BU5D_t1673145177* get__items_1() const { return ____items_1; }
	inline NavMeshModifierU5BU5D_t1673145177** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NavMeshModifierU5BU5D_t1673145177* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2482780542, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2482780542, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2482780542_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	NavMeshModifierU5BU5D_t1673145177* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2482780542_StaticFields, ___EmptyArray_4)); }
	inline NavMeshModifierU5BU5D_t1673145177* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline NavMeshModifierU5BU5D_t1673145177** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(NavMeshModifierU5BU5D_t1673145177* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2482780542_H
#ifndef LIST_1_T470994630_H
#define LIST_1_T470994630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>
struct  List_1_t470994630  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NavMeshModifierVolumeU5BU5D_t3251859441* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t470994630, ____items_1)); }
	inline NavMeshModifierVolumeU5BU5D_t3251859441* get__items_1() const { return ____items_1; }
	inline NavMeshModifierVolumeU5BU5D_t3251859441** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NavMeshModifierVolumeU5BU5D_t3251859441* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t470994630, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t470994630, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t470994630_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	NavMeshModifierVolumeU5BU5D_t3251859441* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t470994630_StaticFields, ___EmptyArray_4)); }
	inline NavMeshModifierVolumeU5BU5D_t3251859441* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline NavMeshModifierVolumeU5BU5D_t3251859441** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(NavMeshModifierVolumeU5BU5D_t3251859441* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T470994630_H
#ifndef LIST_1_T2459021783_H
#define LIST_1_T2459021783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>
struct  List_1_t2459021783  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NavMeshSurfaceU5BU5D_t111833468* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2459021783, ____items_1)); }
	inline NavMeshSurfaceU5BU5D_t111833468* get__items_1() const { return ____items_1; }
	inline NavMeshSurfaceU5BU5D_t111833468** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NavMeshSurfaceU5BU5D_t111833468* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2459021783, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2459021783, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2459021783_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	NavMeshSurfaceU5BU5D_t111833468* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2459021783_StaticFields, ___EmptyArray_4)); }
	inline NavMeshSurfaceU5BU5D_t111833468* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline NavMeshSurfaceU5BU5D_t111833468** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(NavMeshSurfaceU5BU5D_t111833468* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2459021783_H
#ifndef LIST_1_T2585711361_H
#define LIST_1_T2585711361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t2585711361  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3328599146* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____items_1)); }
	inline GameObjectU5BU5D_t3328599146* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3328599146* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2585711361_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3328599146* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2585711361_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3328599146* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3328599146* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2585711361_H
#ifndef LIST_1_T3817059745_H
#define LIST_1_T3817059745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UtilityAI.Decision>
struct  List_1_t3817059745  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	DecisionU5BU5D_t928875338* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3817059745, ____items_1)); }
	inline DecisionU5BU5D_t928875338* get__items_1() const { return ____items_1; }
	inline DecisionU5BU5D_t928875338** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(DecisionU5BU5D_t928875338* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3817059745, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3817059745, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3817059745_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	DecisionU5BU5D_t928875338* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3817059745_StaticFields, ___EmptyArray_4)); }
	inline DecisionU5BU5D_t928875338* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline DecisionU5BU5D_t928875338** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(DecisionU5BU5D_t928875338* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3817059745_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef NAVMESH_T1865600375_H
#define NAVMESH_T1865600375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMesh
struct  NavMesh_t1865600375  : public RuntimeObject
{
public:

public:
};

struct NavMesh_t1865600375_StaticFields
{
public:
	// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate UnityEngine.AI.NavMesh::onPreUpdate
	OnNavMeshPreUpdate_t1580782682 * ___onPreUpdate_0;

public:
	inline static int32_t get_offset_of_onPreUpdate_0() { return static_cast<int32_t>(offsetof(NavMesh_t1865600375_StaticFields, ___onPreUpdate_0)); }
	inline OnNavMeshPreUpdate_t1580782682 * get_onPreUpdate_0() const { return ___onPreUpdate_0; }
	inline OnNavMeshPreUpdate_t1580782682 ** get_address_of_onPreUpdate_0() { return &___onPreUpdate_0; }
	inline void set_onPreUpdate_0(OnNavMeshPreUpdate_t1580782682 * value)
	{
		___onPreUpdate_0 = value;
		Il2CppCodeGenWriteBarrier((&___onPreUpdate_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESH_T1865600375_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUMERATOR_T1842200533_H
#define ENUMERATOR_T1842200533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Competitor>
struct  Enumerator_t1842200533 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t4247923952 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Competitor_t2775849210 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1842200533, ___l_0)); }
	inline List_1_t4247923952 * get_l_0() const { return ___l_0; }
	inline List_1_t4247923952 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t4247923952 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1842200533, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1842200533, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1842200533, ___current_3)); }
	inline Competitor_t2775849210 * get_current_3() const { return ___current_3; }
	inline Competitor_t2775849210 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Competitor_t2775849210 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1842200533_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T77057123_H
#define ENUMERATOR_T77057123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifier>
struct  Enumerator_t77057123 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2482780542 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NavMeshModifier_t1010705800 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t77057123, ___l_0)); }
	inline List_1_t2482780542 * get_l_0() const { return ___l_0; }
	inline List_1_t2482780542 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2482780542 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t77057123, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t77057123, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t77057123, ___current_3)); }
	inline NavMeshModifier_t1010705800 * get_current_3() const { return ___current_3; }
	inline NavMeshModifier_t1010705800 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NavMeshModifier_t1010705800 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T77057123_H
#ifndef ENUMERATOR_T2360238507_H
#define ENUMERATOR_T2360238507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifierVolume>
struct  Enumerator_t2360238507 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t470994630 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NavMeshModifierVolume_t3293887184 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2360238507, ___l_0)); }
	inline List_1_t470994630 * get_l_0() const { return ___l_0; }
	inline List_1_t470994630 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t470994630 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2360238507, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2360238507, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2360238507, ___current_3)); }
	inline NavMeshModifierVolume_t3293887184 * get_current_3() const { return ___current_3; }
	inline NavMeshModifierVolume_t3293887184 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NavMeshModifierVolume_t3293887184 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2360238507_H
#ifndef ENUMERATOR_T179987942_H
#define ENUMERATOR_T179987942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct  Enumerator_t179987942 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2585711361 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GameObject_t1113636619 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___l_0)); }
	inline List_1_t2585711361 * get_l_0() const { return ___l_0; }
	inline List_1_t2585711361 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2585711361 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___current_3)); }
	inline GameObject_t1113636619 * get_current_3() const { return ___current_3; }
	inline GameObject_t1113636619 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_t1113636619 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T179987942_H
#ifndef ENUMERATOR_T1411336326_H
#define ENUMERATOR_T1411336326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UtilityAI.Decision>
struct  Enumerator_t1411336326 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3817059745 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Decision_t2344985003 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t1411336326, ___l_0)); }
	inline List_1_t3817059745 * get_l_0() const { return ___l_0; }
	inline List_1_t3817059745 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3817059745 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1411336326, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t1411336326, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1411336326, ___current_3)); }
	inline Decision_t2344985003 * get_current_3() const { return ___current_3; }
	inline Decision_t2344985003 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Decision_t2344985003 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1411336326_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef NAVMESHBUILDDEBUGSETTINGS_T1257292354_H
#define NAVMESHBUILDDEBUGSETTINGS_T1257292354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshBuildDebugSettings
struct  NavMeshBuildDebugSettings_t1257292354 
{
public:
	// System.Byte UnityEngine.AI.NavMeshBuildDebugSettings::m_Flags
	uint8_t ___m_Flags_0;

public:
	inline static int32_t get_offset_of_m_Flags_0() { return static_cast<int32_t>(offsetof(NavMeshBuildDebugSettings_t1257292354, ___m_Flags_0)); }
	inline uint8_t get_m_Flags_0() const { return ___m_Flags_0; }
	inline uint8_t* get_address_of_m_Flags_0() { return &___m_Flags_0; }
	inline void set_m_Flags_0(uint8_t value)
	{
		___m_Flags_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHBUILDDEBUGSETTINGS_T1257292354_H
#ifndef NAVMESHBUILDMARKUP_T1313583865_H
#define NAVMESHBUILDMARKUP_T1313583865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshBuildMarkup
struct  NavMeshBuildMarkup_t1313583865 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshBuildMarkup::m_OverrideArea
	int32_t ___m_OverrideArea_0;
	// System.Int32 UnityEngine.AI.NavMeshBuildMarkup::m_Area
	int32_t ___m_Area_1;
	// System.Int32 UnityEngine.AI.NavMeshBuildMarkup::m_IgnoreFromBuild
	int32_t ___m_IgnoreFromBuild_2;
	// System.Int32 UnityEngine.AI.NavMeshBuildMarkup::m_InstanceID
	int32_t ___m_InstanceID_3;

public:
	inline static int32_t get_offset_of_m_OverrideArea_0() { return static_cast<int32_t>(offsetof(NavMeshBuildMarkup_t1313583865, ___m_OverrideArea_0)); }
	inline int32_t get_m_OverrideArea_0() const { return ___m_OverrideArea_0; }
	inline int32_t* get_address_of_m_OverrideArea_0() { return &___m_OverrideArea_0; }
	inline void set_m_OverrideArea_0(int32_t value)
	{
		___m_OverrideArea_0 = value;
	}

	inline static int32_t get_offset_of_m_Area_1() { return static_cast<int32_t>(offsetof(NavMeshBuildMarkup_t1313583865, ___m_Area_1)); }
	inline int32_t get_m_Area_1() const { return ___m_Area_1; }
	inline int32_t* get_address_of_m_Area_1() { return &___m_Area_1; }
	inline void set_m_Area_1(int32_t value)
	{
		___m_Area_1 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreFromBuild_2() { return static_cast<int32_t>(offsetof(NavMeshBuildMarkup_t1313583865, ___m_IgnoreFromBuild_2)); }
	inline int32_t get_m_IgnoreFromBuild_2() const { return ___m_IgnoreFromBuild_2; }
	inline int32_t* get_address_of_m_IgnoreFromBuild_2() { return &___m_IgnoreFromBuild_2; }
	inline void set_m_IgnoreFromBuild_2(int32_t value)
	{
		___m_IgnoreFromBuild_2 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_3() { return static_cast<int32_t>(offsetof(NavMeshBuildMarkup_t1313583865, ___m_InstanceID_3)); }
	inline int32_t get_m_InstanceID_3() const { return ___m_InstanceID_3; }
	inline int32_t* get_address_of_m_InstanceID_3() { return &___m_InstanceID_3; }
	inline void set_m_InstanceID_3(int32_t value)
	{
		___m_InstanceID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHBUILDMARKUP_T1313583865_H
#ifndef NAVMESHDATAINSTANCE_T1498462893_H
#define NAVMESHDATAINSTANCE_T1498462893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshDataInstance
struct  NavMeshDataInstance_t1498462893 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshDataInstance::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(NavMeshDataInstance_t1498462893, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHDATAINSTANCE_T1498462893_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef DEATHTYPE_T3371451543_H
#define DEATHTYPE_T3371451543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathTracker/DeathType
struct  DeathType_t3371451543 
{
public:
	// System.Int32 DeathTracker/DeathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DeathType_t3371451543, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHTYPE_T3371451543_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef COLLECTOBJECTS_T1049357338_H
#define COLLECTOBJECTS_T1049357338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.CollectObjects
struct  CollectObjects_t1049357338 
{
public:
	// System.Int32 UnityEngine.AI.CollectObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollectObjects_t1049357338, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTOBJECTS_T1049357338_H
#ifndef NAVMESHBUILDSETTINGS_T1985378544_H
#define NAVMESHBUILDSETTINGS_T1985378544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshBuildSettings
struct  NavMeshBuildSettings_t1985378544 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshBuildSettings::m_AgentTypeID
	int32_t ___m_AgentTypeID_0;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_AgentRadius
	float ___m_AgentRadius_1;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_AgentHeight
	float ___m_AgentHeight_2;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_AgentSlope
	float ___m_AgentSlope_3;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_AgentClimb
	float ___m_AgentClimb_4;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_LedgeDropHeight
	float ___m_LedgeDropHeight_5;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_MaxJumpAcrossDistance
	float ___m_MaxJumpAcrossDistance_6;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_MinRegionArea
	float ___m_MinRegionArea_7;
	// System.Int32 UnityEngine.AI.NavMeshBuildSettings::m_OverrideVoxelSize
	int32_t ___m_OverrideVoxelSize_8;
	// System.Single UnityEngine.AI.NavMeshBuildSettings::m_VoxelSize
	float ___m_VoxelSize_9;
	// System.Int32 UnityEngine.AI.NavMeshBuildSettings::m_OverrideTileSize
	int32_t ___m_OverrideTileSize_10;
	// System.Int32 UnityEngine.AI.NavMeshBuildSettings::m_TileSize
	int32_t ___m_TileSize_11;
	// System.Int32 UnityEngine.AI.NavMeshBuildSettings::m_AccuratePlacement
	int32_t ___m_AccuratePlacement_12;
	// UnityEngine.AI.NavMeshBuildDebugSettings UnityEngine.AI.NavMeshBuildSettings::m_Debug
	NavMeshBuildDebugSettings_t1257292354  ___m_Debug_13;

public:
	inline static int32_t get_offset_of_m_AgentTypeID_0() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_AgentTypeID_0)); }
	inline int32_t get_m_AgentTypeID_0() const { return ___m_AgentTypeID_0; }
	inline int32_t* get_address_of_m_AgentTypeID_0() { return &___m_AgentTypeID_0; }
	inline void set_m_AgentTypeID_0(int32_t value)
	{
		___m_AgentTypeID_0 = value;
	}

	inline static int32_t get_offset_of_m_AgentRadius_1() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_AgentRadius_1)); }
	inline float get_m_AgentRadius_1() const { return ___m_AgentRadius_1; }
	inline float* get_address_of_m_AgentRadius_1() { return &___m_AgentRadius_1; }
	inline void set_m_AgentRadius_1(float value)
	{
		___m_AgentRadius_1 = value;
	}

	inline static int32_t get_offset_of_m_AgentHeight_2() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_AgentHeight_2)); }
	inline float get_m_AgentHeight_2() const { return ___m_AgentHeight_2; }
	inline float* get_address_of_m_AgentHeight_2() { return &___m_AgentHeight_2; }
	inline void set_m_AgentHeight_2(float value)
	{
		___m_AgentHeight_2 = value;
	}

	inline static int32_t get_offset_of_m_AgentSlope_3() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_AgentSlope_3)); }
	inline float get_m_AgentSlope_3() const { return ___m_AgentSlope_3; }
	inline float* get_address_of_m_AgentSlope_3() { return &___m_AgentSlope_3; }
	inline void set_m_AgentSlope_3(float value)
	{
		___m_AgentSlope_3 = value;
	}

	inline static int32_t get_offset_of_m_AgentClimb_4() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_AgentClimb_4)); }
	inline float get_m_AgentClimb_4() const { return ___m_AgentClimb_4; }
	inline float* get_address_of_m_AgentClimb_4() { return &___m_AgentClimb_4; }
	inline void set_m_AgentClimb_4(float value)
	{
		___m_AgentClimb_4 = value;
	}

	inline static int32_t get_offset_of_m_LedgeDropHeight_5() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_LedgeDropHeight_5)); }
	inline float get_m_LedgeDropHeight_5() const { return ___m_LedgeDropHeight_5; }
	inline float* get_address_of_m_LedgeDropHeight_5() { return &___m_LedgeDropHeight_5; }
	inline void set_m_LedgeDropHeight_5(float value)
	{
		___m_LedgeDropHeight_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxJumpAcrossDistance_6() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_MaxJumpAcrossDistance_6)); }
	inline float get_m_MaxJumpAcrossDistance_6() const { return ___m_MaxJumpAcrossDistance_6; }
	inline float* get_address_of_m_MaxJumpAcrossDistance_6() { return &___m_MaxJumpAcrossDistance_6; }
	inline void set_m_MaxJumpAcrossDistance_6(float value)
	{
		___m_MaxJumpAcrossDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_MinRegionArea_7() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_MinRegionArea_7)); }
	inline float get_m_MinRegionArea_7() const { return ___m_MinRegionArea_7; }
	inline float* get_address_of_m_MinRegionArea_7() { return &___m_MinRegionArea_7; }
	inline void set_m_MinRegionArea_7(float value)
	{
		___m_MinRegionArea_7 = value;
	}

	inline static int32_t get_offset_of_m_OverrideVoxelSize_8() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_OverrideVoxelSize_8)); }
	inline int32_t get_m_OverrideVoxelSize_8() const { return ___m_OverrideVoxelSize_8; }
	inline int32_t* get_address_of_m_OverrideVoxelSize_8() { return &___m_OverrideVoxelSize_8; }
	inline void set_m_OverrideVoxelSize_8(int32_t value)
	{
		___m_OverrideVoxelSize_8 = value;
	}

	inline static int32_t get_offset_of_m_VoxelSize_9() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_VoxelSize_9)); }
	inline float get_m_VoxelSize_9() const { return ___m_VoxelSize_9; }
	inline float* get_address_of_m_VoxelSize_9() { return &___m_VoxelSize_9; }
	inline void set_m_VoxelSize_9(float value)
	{
		___m_VoxelSize_9 = value;
	}

	inline static int32_t get_offset_of_m_OverrideTileSize_10() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_OverrideTileSize_10)); }
	inline int32_t get_m_OverrideTileSize_10() const { return ___m_OverrideTileSize_10; }
	inline int32_t* get_address_of_m_OverrideTileSize_10() { return &___m_OverrideTileSize_10; }
	inline void set_m_OverrideTileSize_10(int32_t value)
	{
		___m_OverrideTileSize_10 = value;
	}

	inline static int32_t get_offset_of_m_TileSize_11() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_TileSize_11)); }
	inline int32_t get_m_TileSize_11() const { return ___m_TileSize_11; }
	inline int32_t* get_address_of_m_TileSize_11() { return &___m_TileSize_11; }
	inline void set_m_TileSize_11(int32_t value)
	{
		___m_TileSize_11 = value;
	}

	inline static int32_t get_offset_of_m_AccuratePlacement_12() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_AccuratePlacement_12)); }
	inline int32_t get_m_AccuratePlacement_12() const { return ___m_AccuratePlacement_12; }
	inline int32_t* get_address_of_m_AccuratePlacement_12() { return &___m_AccuratePlacement_12; }
	inline void set_m_AccuratePlacement_12(int32_t value)
	{
		___m_AccuratePlacement_12 = value;
	}

	inline static int32_t get_offset_of_m_Debug_13() { return static_cast<int32_t>(offsetof(NavMeshBuildSettings_t1985378544, ___m_Debug_13)); }
	inline NavMeshBuildDebugSettings_t1257292354  get_m_Debug_13() const { return ___m_Debug_13; }
	inline NavMeshBuildDebugSettings_t1257292354 * get_address_of_m_Debug_13() { return &___m_Debug_13; }
	inline void set_m_Debug_13(NavMeshBuildDebugSettings_t1257292354  value)
	{
		___m_Debug_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHBUILDSETTINGS_T1985378544_H
#ifndef NAVMESHBUILDSOURCESHAPE_T3460247977_H
#define NAVMESHBUILDSOURCESHAPE_T3460247977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshBuildSourceShape
struct  NavMeshBuildSourceShape_t3460247977 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshBuildSourceShape::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NavMeshBuildSourceShape_t3460247977, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHBUILDSOURCESHAPE_T3460247977_H
#ifndef NAVMESHCOLLECTGEOMETRY_T1778480714_H
#define NAVMESHCOLLECTGEOMETRY_T1778480714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshCollectGeometry
struct  NavMeshCollectGeometry_t1778480714 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshCollectGeometry::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NavMeshCollectGeometry_t1778480714, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHCOLLECTGEOMETRY_T1778480714_H
#ifndef ASYNCOPERATION_T1445031843_H
#define ASYNCOPERATION_T1445031843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t1445031843  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t1617499438 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t1445031843, ___m_completeCallback_1)); }
	inline Action_1_t1617499438 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t1617499438 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t1617499438 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T1445031843_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DECISIONTYPE_T3995638880_H
#define DECISIONTYPE_T3995638880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.DecisionType
struct  DecisionType_t3995638880 
{
public:
	// System.Int32 UtilityAI.DecisionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DecisionType_t3995638880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECISIONTYPE_T3995638880_H
#ifndef VIEW_T4100663493_H
#define VIEW_T4100663493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// View
struct  View_t4100663493 
{
public:
	// System.Int32 View::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(View_t4100663493, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEW_T4100663493_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef NAVMESHBUILDSOURCE_T688186225_H
#define NAVMESHBUILDSOURCE_T688186225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshBuildSource
struct  NavMeshBuildSource_t688186225 
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.AI.NavMeshBuildSource::m_Transform
	Matrix4x4_t1817901843  ___m_Transform_0;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshBuildSource::m_Size
	Vector3_t3722313464  ___m_Size_1;
	// UnityEngine.AI.NavMeshBuildSourceShape UnityEngine.AI.NavMeshBuildSource::m_Shape
	int32_t ___m_Shape_2;
	// System.Int32 UnityEngine.AI.NavMeshBuildSource::m_Area
	int32_t ___m_Area_3;
	// System.Int32 UnityEngine.AI.NavMeshBuildSource::m_InstanceID
	int32_t ___m_InstanceID_4;
	// System.Int32 UnityEngine.AI.NavMeshBuildSource::m_ComponentID
	int32_t ___m_ComponentID_5;

public:
	inline static int32_t get_offset_of_m_Transform_0() { return static_cast<int32_t>(offsetof(NavMeshBuildSource_t688186225, ___m_Transform_0)); }
	inline Matrix4x4_t1817901843  get_m_Transform_0() const { return ___m_Transform_0; }
	inline Matrix4x4_t1817901843 * get_address_of_m_Transform_0() { return &___m_Transform_0; }
	inline void set_m_Transform_0(Matrix4x4_t1817901843  value)
	{
		___m_Transform_0 = value;
	}

	inline static int32_t get_offset_of_m_Size_1() { return static_cast<int32_t>(offsetof(NavMeshBuildSource_t688186225, ___m_Size_1)); }
	inline Vector3_t3722313464  get_m_Size_1() const { return ___m_Size_1; }
	inline Vector3_t3722313464 * get_address_of_m_Size_1() { return &___m_Size_1; }
	inline void set_m_Size_1(Vector3_t3722313464  value)
	{
		___m_Size_1 = value;
	}

	inline static int32_t get_offset_of_m_Shape_2() { return static_cast<int32_t>(offsetof(NavMeshBuildSource_t688186225, ___m_Shape_2)); }
	inline int32_t get_m_Shape_2() const { return ___m_Shape_2; }
	inline int32_t* get_address_of_m_Shape_2() { return &___m_Shape_2; }
	inline void set_m_Shape_2(int32_t value)
	{
		___m_Shape_2 = value;
	}

	inline static int32_t get_offset_of_m_Area_3() { return static_cast<int32_t>(offsetof(NavMeshBuildSource_t688186225, ___m_Area_3)); }
	inline int32_t get_m_Area_3() const { return ___m_Area_3; }
	inline int32_t* get_address_of_m_Area_3() { return &___m_Area_3; }
	inline void set_m_Area_3(int32_t value)
	{
		___m_Area_3 = value;
	}

	inline static int32_t get_offset_of_m_InstanceID_4() { return static_cast<int32_t>(offsetof(NavMeshBuildSource_t688186225, ___m_InstanceID_4)); }
	inline int32_t get_m_InstanceID_4() const { return ___m_InstanceID_4; }
	inline int32_t* get_address_of_m_InstanceID_4() { return &___m_InstanceID_4; }
	inline void set_m_InstanceID_4(int32_t value)
	{
		___m_InstanceID_4 = value;
	}

	inline static int32_t get_offset_of_m_ComponentID_5() { return static_cast<int32_t>(offsetof(NavMeshBuildSource_t688186225, ___m_ComponentID_5)); }
	inline int32_t get_m_ComponentID_5() const { return ___m_ComponentID_5; }
	inline int32_t* get_address_of_m_ComponentID_5() { return &___m_ComponentID_5; }
	inline void set_m_ComponentID_5(int32_t value)
	{
		___m_ComponentID_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHBUILDSOURCE_T688186225_H
#ifndef NAVMESHDATA_T1084598030_H
#define NAVMESHDATA_T1084598030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshData
struct  NavMeshData_t1084598030  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHDATA_T1084598030_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef MESH_T3648964284_H
#define MESH_T3648964284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t3648964284  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T3648964284_H
#ifndef TERRAINDATA_T657004131_H
#define TERRAINDATA_T657004131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TerrainData
struct  TerrainData_t657004131  : public Object_t631007953
{
public:

public:
};

struct TerrainData_t657004131_StaticFields
{
public:
	// System.Int32 UnityEngine.TerrainData::k_MaximumResolution
	int32_t ___k_MaximumResolution_4;
	// System.Int32 UnityEngine.TerrainData::k_MinimumDetailResolutionPerPatch
	int32_t ___k_MinimumDetailResolutionPerPatch_5;
	// System.Int32 UnityEngine.TerrainData::k_MaximumDetailResolutionPerPatch
	int32_t ___k_MaximumDetailResolutionPerPatch_6;
	// System.Int32 UnityEngine.TerrainData::k_MaximumDetailPatchCount
	int32_t ___k_MaximumDetailPatchCount_7;
	// System.Int32 UnityEngine.TerrainData::k_MinimumAlphamapResolution
	int32_t ___k_MinimumAlphamapResolution_8;
	// System.Int32 UnityEngine.TerrainData::k_MaximumAlphamapResolution
	int32_t ___k_MaximumAlphamapResolution_9;
	// System.Int32 UnityEngine.TerrainData::k_MinimumBaseMapResolution
	int32_t ___k_MinimumBaseMapResolution_10;
	// System.Int32 UnityEngine.TerrainData::k_MaximumBaseMapResolution
	int32_t ___k_MaximumBaseMapResolution_11;

public:
	inline static int32_t get_offset_of_k_MaximumResolution_4() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumResolution_4)); }
	inline int32_t get_k_MaximumResolution_4() const { return ___k_MaximumResolution_4; }
	inline int32_t* get_address_of_k_MaximumResolution_4() { return &___k_MaximumResolution_4; }
	inline void set_k_MaximumResolution_4(int32_t value)
	{
		___k_MaximumResolution_4 = value;
	}

	inline static int32_t get_offset_of_k_MinimumDetailResolutionPerPatch_5() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MinimumDetailResolutionPerPatch_5)); }
	inline int32_t get_k_MinimumDetailResolutionPerPatch_5() const { return ___k_MinimumDetailResolutionPerPatch_5; }
	inline int32_t* get_address_of_k_MinimumDetailResolutionPerPatch_5() { return &___k_MinimumDetailResolutionPerPatch_5; }
	inline void set_k_MinimumDetailResolutionPerPatch_5(int32_t value)
	{
		___k_MinimumDetailResolutionPerPatch_5 = value;
	}

	inline static int32_t get_offset_of_k_MaximumDetailResolutionPerPatch_6() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumDetailResolutionPerPatch_6)); }
	inline int32_t get_k_MaximumDetailResolutionPerPatch_6() const { return ___k_MaximumDetailResolutionPerPatch_6; }
	inline int32_t* get_address_of_k_MaximumDetailResolutionPerPatch_6() { return &___k_MaximumDetailResolutionPerPatch_6; }
	inline void set_k_MaximumDetailResolutionPerPatch_6(int32_t value)
	{
		___k_MaximumDetailResolutionPerPatch_6 = value;
	}

	inline static int32_t get_offset_of_k_MaximumDetailPatchCount_7() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumDetailPatchCount_7)); }
	inline int32_t get_k_MaximumDetailPatchCount_7() const { return ___k_MaximumDetailPatchCount_7; }
	inline int32_t* get_address_of_k_MaximumDetailPatchCount_7() { return &___k_MaximumDetailPatchCount_7; }
	inline void set_k_MaximumDetailPatchCount_7(int32_t value)
	{
		___k_MaximumDetailPatchCount_7 = value;
	}

	inline static int32_t get_offset_of_k_MinimumAlphamapResolution_8() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MinimumAlphamapResolution_8)); }
	inline int32_t get_k_MinimumAlphamapResolution_8() const { return ___k_MinimumAlphamapResolution_8; }
	inline int32_t* get_address_of_k_MinimumAlphamapResolution_8() { return &___k_MinimumAlphamapResolution_8; }
	inline void set_k_MinimumAlphamapResolution_8(int32_t value)
	{
		___k_MinimumAlphamapResolution_8 = value;
	}

	inline static int32_t get_offset_of_k_MaximumAlphamapResolution_9() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumAlphamapResolution_9)); }
	inline int32_t get_k_MaximumAlphamapResolution_9() const { return ___k_MaximumAlphamapResolution_9; }
	inline int32_t* get_address_of_k_MaximumAlphamapResolution_9() { return &___k_MaximumAlphamapResolution_9; }
	inline void set_k_MaximumAlphamapResolution_9(int32_t value)
	{
		___k_MaximumAlphamapResolution_9 = value;
	}

	inline static int32_t get_offset_of_k_MinimumBaseMapResolution_10() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MinimumBaseMapResolution_10)); }
	inline int32_t get_k_MinimumBaseMapResolution_10() const { return ___k_MinimumBaseMapResolution_10; }
	inline int32_t* get_address_of_k_MinimumBaseMapResolution_10() { return &___k_MinimumBaseMapResolution_10; }
	inline void set_k_MinimumBaseMapResolution_10(int32_t value)
	{
		___k_MinimumBaseMapResolution_10 = value;
	}

	inline static int32_t get_offset_of_k_MaximumBaseMapResolution_11() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumBaseMapResolution_11)); }
	inline int32_t get_k_MaximumBaseMapResolution_11() const { return ___k_MaximumBaseMapResolution_11; }
	inline int32_t* get_address_of_k_MaximumBaseMapResolution_11() { return &___k_MaximumBaseMapResolution_11; }
	inline void set_k_MaximumBaseMapResolution_11(int32_t value)
	{
		___k_MaximumBaseMapResolution_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINDATA_T657004131_H
#ifndef ENUMERATOR_T4049504844_H
#define ENUMERATOR_T4049504844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshBuildSource>
struct  Enumerator_t4049504844 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2160260967 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	NavMeshBuildSource_t688186225  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t4049504844, ___l_0)); }
	inline List_1_t2160260967 * get_l_0() const { return ___l_0; }
	inline List_1_t2160260967 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2160260967 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t4049504844, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t4049504844, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t4049504844, ___current_3)); }
	inline NavMeshBuildSource_t688186225  get_current_3() const { return ___current_3; }
	inline NavMeshBuildSource_t688186225 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(NavMeshBuildSource_t688186225  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T4049504844_H
#ifndef PREDICATE_1_T1513480349_H
#define PREDICATE_1_T1513480349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.AI.NavMeshBuildSource>
struct  Predicate_1_t1513480349  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1513480349_H
#ifndef PREDICATE_1_T1835999924_H
#define PREDICATE_1_T1835999924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.AI.NavMeshModifier>
struct  Predicate_1_t1835999924  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T1835999924_H
#ifndef PREDICATE_1_T4119181308_H
#define PREDICATE_1_T4119181308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Predicate`1<UnityEngine.AI.NavMeshModifierVolume>
struct  Predicate_1_t4119181308  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREDICATE_1_T4119181308_H
#ifndef ONNAVMESHPREUPDATE_T1580782682_H
#define ONNAVMESHPREUPDATE_T1580782682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate
struct  OnNavMeshPreUpdate_t1580782682  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONNAVMESHPREUPDATE_T1580782682_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef PARTICLESYSTEM_T1800779281_H
#define PARTICLESYSTEM_T1800779281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t1800779281  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T1800779281_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef NAVMESHAGENT_T1276799816_H
#define NAVMESHAGENT_T1276799816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshAgent
struct  NavMeshAgent_t1276799816  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHAGENT_T1276799816_H
#ifndef NAVMESHOBSTACLE_T3657678309_H
#define NAVMESHOBSTACLE_T3657678309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshObstacle
struct  NavMeshObstacle_t3657678309  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHOBSTACLE_T3657678309_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef COMPETITOR_T2775849210_H
#define COMPETITOR_T2775849210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Competitor
struct  Competitor_t2775849210  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Competitor::maxSpeed
	float ___maxSpeed_4;
	// System.Single Competitor::speed
	float ___speed_5;
	// System.Single Competitor::healthRegenMultiplier
	float ___healthRegenMultiplier_6;
	// System.Single Competitor::hungerMultiplier
	float ___hungerMultiplier_7;
	// System.Single Competitor::thirstMultiplier
	float ___thirstMultiplier_8;
	// System.Single Competitor::maxHealth
	float ___maxHealth_9;
	// System.Single Competitor::maxHunger
	float ___maxHunger_10;
	// System.Single Competitor::maxThirst
	float ___maxThirst_11;
	// System.Single Competitor::health
	float ___health_12;
	// System.Single Competitor::hunger
	float ___hunger_13;
	// System.Single Competitor::thirst
	float ___thirst_14;
	// System.Single Competitor::hungerVariable
	float ___hungerVariable_15;
	// System.Single Competitor::thirstVariable
	float ___thirstVariable_16;
	// System.Single Competitor::healthVariable
	float ___healthVariable_17;
	// System.Single Competitor::maxHealthValue
	float ___maxHealthValue_18;
	// CompetitorUI Competitor::UI
	CompetitorUI_t472322071 * ___UI_19;
	// UnityEngine.GameObject Competitor::firstViewPos
	GameObject_t1113636619 * ___firstViewPos_20;
	// UnityEngine.UI.Button Competitor::changeViewButton
	Button_t4055032469 * ___changeViewButton_21;
	// CameraManager Competitor::cameraManager
	CameraManager_t3272490737 * ___cameraManager_22;

public:
	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxSpeed_4)); }
	inline float get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline float* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(float value)
	{
		___maxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_healthRegenMultiplier_6() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___healthRegenMultiplier_6)); }
	inline float get_healthRegenMultiplier_6() const { return ___healthRegenMultiplier_6; }
	inline float* get_address_of_healthRegenMultiplier_6() { return &___healthRegenMultiplier_6; }
	inline void set_healthRegenMultiplier_6(float value)
	{
		___healthRegenMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_hungerMultiplier_7() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___hungerMultiplier_7)); }
	inline float get_hungerMultiplier_7() const { return ___hungerMultiplier_7; }
	inline float* get_address_of_hungerMultiplier_7() { return &___hungerMultiplier_7; }
	inline void set_hungerMultiplier_7(float value)
	{
		___hungerMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_thirstMultiplier_8() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___thirstMultiplier_8)); }
	inline float get_thirstMultiplier_8() const { return ___thirstMultiplier_8; }
	inline float* get_address_of_thirstMultiplier_8() { return &___thirstMultiplier_8; }
	inline void set_thirstMultiplier_8(float value)
	{
		___thirstMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_maxHealth_9() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxHealth_9)); }
	inline float get_maxHealth_9() const { return ___maxHealth_9; }
	inline float* get_address_of_maxHealth_9() { return &___maxHealth_9; }
	inline void set_maxHealth_9(float value)
	{
		___maxHealth_9 = value;
	}

	inline static int32_t get_offset_of_maxHunger_10() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxHunger_10)); }
	inline float get_maxHunger_10() const { return ___maxHunger_10; }
	inline float* get_address_of_maxHunger_10() { return &___maxHunger_10; }
	inline void set_maxHunger_10(float value)
	{
		___maxHunger_10 = value;
	}

	inline static int32_t get_offset_of_maxThirst_11() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxThirst_11)); }
	inline float get_maxThirst_11() const { return ___maxThirst_11; }
	inline float* get_address_of_maxThirst_11() { return &___maxThirst_11; }
	inline void set_maxThirst_11(float value)
	{
		___maxThirst_11 = value;
	}

	inline static int32_t get_offset_of_health_12() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___health_12)); }
	inline float get_health_12() const { return ___health_12; }
	inline float* get_address_of_health_12() { return &___health_12; }
	inline void set_health_12(float value)
	{
		___health_12 = value;
	}

	inline static int32_t get_offset_of_hunger_13() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___hunger_13)); }
	inline float get_hunger_13() const { return ___hunger_13; }
	inline float* get_address_of_hunger_13() { return &___hunger_13; }
	inline void set_hunger_13(float value)
	{
		___hunger_13 = value;
	}

	inline static int32_t get_offset_of_thirst_14() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___thirst_14)); }
	inline float get_thirst_14() const { return ___thirst_14; }
	inline float* get_address_of_thirst_14() { return &___thirst_14; }
	inline void set_thirst_14(float value)
	{
		___thirst_14 = value;
	}

	inline static int32_t get_offset_of_hungerVariable_15() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___hungerVariable_15)); }
	inline float get_hungerVariable_15() const { return ___hungerVariable_15; }
	inline float* get_address_of_hungerVariable_15() { return &___hungerVariable_15; }
	inline void set_hungerVariable_15(float value)
	{
		___hungerVariable_15 = value;
	}

	inline static int32_t get_offset_of_thirstVariable_16() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___thirstVariable_16)); }
	inline float get_thirstVariable_16() const { return ___thirstVariable_16; }
	inline float* get_address_of_thirstVariable_16() { return &___thirstVariable_16; }
	inline void set_thirstVariable_16(float value)
	{
		___thirstVariable_16 = value;
	}

	inline static int32_t get_offset_of_healthVariable_17() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___healthVariable_17)); }
	inline float get_healthVariable_17() const { return ___healthVariable_17; }
	inline float* get_address_of_healthVariable_17() { return &___healthVariable_17; }
	inline void set_healthVariable_17(float value)
	{
		___healthVariable_17 = value;
	}

	inline static int32_t get_offset_of_maxHealthValue_18() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxHealthValue_18)); }
	inline float get_maxHealthValue_18() const { return ___maxHealthValue_18; }
	inline float* get_address_of_maxHealthValue_18() { return &___maxHealthValue_18; }
	inline void set_maxHealthValue_18(float value)
	{
		___maxHealthValue_18 = value;
	}

	inline static int32_t get_offset_of_UI_19() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___UI_19)); }
	inline CompetitorUI_t472322071 * get_UI_19() const { return ___UI_19; }
	inline CompetitorUI_t472322071 ** get_address_of_UI_19() { return &___UI_19; }
	inline void set_UI_19(CompetitorUI_t472322071 * value)
	{
		___UI_19 = value;
		Il2CppCodeGenWriteBarrier((&___UI_19), value);
	}

	inline static int32_t get_offset_of_firstViewPos_20() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___firstViewPos_20)); }
	inline GameObject_t1113636619 * get_firstViewPos_20() const { return ___firstViewPos_20; }
	inline GameObject_t1113636619 ** get_address_of_firstViewPos_20() { return &___firstViewPos_20; }
	inline void set_firstViewPos_20(GameObject_t1113636619 * value)
	{
		___firstViewPos_20 = value;
		Il2CppCodeGenWriteBarrier((&___firstViewPos_20), value);
	}

	inline static int32_t get_offset_of_changeViewButton_21() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___changeViewButton_21)); }
	inline Button_t4055032469 * get_changeViewButton_21() const { return ___changeViewButton_21; }
	inline Button_t4055032469 ** get_address_of_changeViewButton_21() { return &___changeViewButton_21; }
	inline void set_changeViewButton_21(Button_t4055032469 * value)
	{
		___changeViewButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___changeViewButton_21), value);
	}

	inline static int32_t get_offset_of_cameraManager_22() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___cameraManager_22)); }
	inline CameraManager_t3272490737 * get_cameraManager_22() const { return ___cameraManager_22; }
	inline CameraManager_t3272490737 ** get_address_of_cameraManager_22() { return &___cameraManager_22; }
	inline void set_cameraManager_22(CameraManager_t3272490737 * value)
	{
		___cameraManager_22 = value;
		Il2CppCodeGenWriteBarrier((&___cameraManager_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPETITOR_T2775849210_H
#ifndef COMPETITORMANAGER_T1763731731_H
#define COMPETITORMANAGER_T1763731731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CompetitorManager
struct  CompetitorManager_t1763731731  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CompetitorManager::numberOfCompetitors
	int32_t ___numberOfCompetitors_4;
	// System.Collections.Generic.List`1<Competitor> CompetitorManager::competitors
	List_1_t4247923952 * ___competitors_5;

public:
	inline static int32_t get_offset_of_numberOfCompetitors_4() { return static_cast<int32_t>(offsetof(CompetitorManager_t1763731731, ___numberOfCompetitors_4)); }
	inline int32_t get_numberOfCompetitors_4() const { return ___numberOfCompetitors_4; }
	inline int32_t* get_address_of_numberOfCompetitors_4() { return &___numberOfCompetitors_4; }
	inline void set_numberOfCompetitors_4(int32_t value)
	{
		___numberOfCompetitors_4 = value;
	}

	inline static int32_t get_offset_of_competitors_5() { return static_cast<int32_t>(offsetof(CompetitorManager_t1763731731, ___competitors_5)); }
	inline List_1_t4247923952 * get_competitors_5() const { return ___competitors_5; }
	inline List_1_t4247923952 ** get_address_of_competitors_5() { return &___competitors_5; }
	inline void set_competitors_5(List_1_t4247923952 * value)
	{
		___competitors_5 = value;
		Il2CppCodeGenWriteBarrier((&___competitors_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPETITORMANAGER_T1763731731_H
#ifndef ITEMMANAGER_T3254073967_H
#define ITEMMANAGER_T3254073967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemManager
struct  ItemManager_t3254073967  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 ItemManager::maxWaterInMap
	int32_t ___maxWaterInMap_4;
	// System.Int32 ItemManager::maxFoodInMap
	int32_t ___maxFoodInMap_5;
	// System.Int32 ItemManager::maxLandminesInMap
	int32_t ___maxLandminesInMap_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ItemManager::water
	List_1_t2585711361 * ___water_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ItemManager::food
	List_1_t2585711361 * ___food_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ItemManager::landmines
	List_1_t2585711361 * ___landmines_9;
	// UnityEngine.GameObject ItemManager::flag
	GameObject_t1113636619 * ___flag_10;

public:
	inline static int32_t get_offset_of_maxWaterInMap_4() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___maxWaterInMap_4)); }
	inline int32_t get_maxWaterInMap_4() const { return ___maxWaterInMap_4; }
	inline int32_t* get_address_of_maxWaterInMap_4() { return &___maxWaterInMap_4; }
	inline void set_maxWaterInMap_4(int32_t value)
	{
		___maxWaterInMap_4 = value;
	}

	inline static int32_t get_offset_of_maxFoodInMap_5() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___maxFoodInMap_5)); }
	inline int32_t get_maxFoodInMap_5() const { return ___maxFoodInMap_5; }
	inline int32_t* get_address_of_maxFoodInMap_5() { return &___maxFoodInMap_5; }
	inline void set_maxFoodInMap_5(int32_t value)
	{
		___maxFoodInMap_5 = value;
	}

	inline static int32_t get_offset_of_maxLandminesInMap_6() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___maxLandminesInMap_6)); }
	inline int32_t get_maxLandminesInMap_6() const { return ___maxLandminesInMap_6; }
	inline int32_t* get_address_of_maxLandminesInMap_6() { return &___maxLandminesInMap_6; }
	inline void set_maxLandminesInMap_6(int32_t value)
	{
		___maxLandminesInMap_6 = value;
	}

	inline static int32_t get_offset_of_water_7() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___water_7)); }
	inline List_1_t2585711361 * get_water_7() const { return ___water_7; }
	inline List_1_t2585711361 ** get_address_of_water_7() { return &___water_7; }
	inline void set_water_7(List_1_t2585711361 * value)
	{
		___water_7 = value;
		Il2CppCodeGenWriteBarrier((&___water_7), value);
	}

	inline static int32_t get_offset_of_food_8() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___food_8)); }
	inline List_1_t2585711361 * get_food_8() const { return ___food_8; }
	inline List_1_t2585711361 ** get_address_of_food_8() { return &___food_8; }
	inline void set_food_8(List_1_t2585711361 * value)
	{
		___food_8 = value;
		Il2CppCodeGenWriteBarrier((&___food_8), value);
	}

	inline static int32_t get_offset_of_landmines_9() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___landmines_9)); }
	inline List_1_t2585711361 * get_landmines_9() const { return ___landmines_9; }
	inline List_1_t2585711361 ** get_address_of_landmines_9() { return &___landmines_9; }
	inline void set_landmines_9(List_1_t2585711361 * value)
	{
		___landmines_9 = value;
		Il2CppCodeGenWriteBarrier((&___landmines_9), value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___flag_10)); }
	inline GameObject_t1113636619 * get_flag_10() const { return ___flag_10; }
	inline GameObject_t1113636619 ** get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(GameObject_t1113636619 * value)
	{
		___flag_10 = value;
		Il2CppCodeGenWriteBarrier((&___flag_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMMANAGER_T3254073967_H
#ifndef PICKUPABLEITEM_T2078450402_H
#define PICKUPABLEITEM_T2078450402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickupableItem
struct  PickupableItem_t2078450402  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPABLEITEM_T2078450402_H
#ifndef NAVMESHMODIFIER_T1010705800_H
#define NAVMESHMODIFIER_T1010705800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshModifier
struct  NavMeshModifier_t1010705800  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.AI.NavMeshModifier::m_OverrideArea
	bool ___m_OverrideArea_4;
	// System.Int32 UnityEngine.AI.NavMeshModifier::m_Area
	int32_t ___m_Area_5;
	// System.Boolean UnityEngine.AI.NavMeshModifier::m_IgnoreFromBuild
	bool ___m_IgnoreFromBuild_6;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.AI.NavMeshModifier::m_AffectedAgents
	List_1_t128053199 * ___m_AffectedAgents_7;

public:
	inline static int32_t get_offset_of_m_OverrideArea_4() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_OverrideArea_4)); }
	inline bool get_m_OverrideArea_4() const { return ___m_OverrideArea_4; }
	inline bool* get_address_of_m_OverrideArea_4() { return &___m_OverrideArea_4; }
	inline void set_m_OverrideArea_4(bool value)
	{
		___m_OverrideArea_4 = value;
	}

	inline static int32_t get_offset_of_m_Area_5() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_Area_5)); }
	inline int32_t get_m_Area_5() const { return ___m_Area_5; }
	inline int32_t* get_address_of_m_Area_5() { return &___m_Area_5; }
	inline void set_m_Area_5(int32_t value)
	{
		___m_Area_5 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreFromBuild_6() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_IgnoreFromBuild_6)); }
	inline bool get_m_IgnoreFromBuild_6() const { return ___m_IgnoreFromBuild_6; }
	inline bool* get_address_of_m_IgnoreFromBuild_6() { return &___m_IgnoreFromBuild_6; }
	inline void set_m_IgnoreFromBuild_6(bool value)
	{
		___m_IgnoreFromBuild_6 = value;
	}

	inline static int32_t get_offset_of_m_AffectedAgents_7() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_AffectedAgents_7)); }
	inline List_1_t128053199 * get_m_AffectedAgents_7() const { return ___m_AffectedAgents_7; }
	inline List_1_t128053199 ** get_address_of_m_AffectedAgents_7() { return &___m_AffectedAgents_7; }
	inline void set_m_AffectedAgents_7(List_1_t128053199 * value)
	{
		___m_AffectedAgents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AffectedAgents_7), value);
	}
};

struct NavMeshModifier_t1010705800_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier> UnityEngine.AI.NavMeshModifier::s_NavMeshModifiers
	List_1_t2482780542 * ___s_NavMeshModifiers_8;

public:
	inline static int32_t get_offset_of_s_NavMeshModifiers_8() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800_StaticFields, ___s_NavMeshModifiers_8)); }
	inline List_1_t2482780542 * get_s_NavMeshModifiers_8() const { return ___s_NavMeshModifiers_8; }
	inline List_1_t2482780542 ** get_address_of_s_NavMeshModifiers_8() { return &___s_NavMeshModifiers_8; }
	inline void set_s_NavMeshModifiers_8(List_1_t2482780542 * value)
	{
		___s_NavMeshModifiers_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_NavMeshModifiers_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHMODIFIER_T1010705800_H
#ifndef NAVMESHMODIFIERVOLUME_T3293887184_H
#define NAVMESHMODIFIERVOLUME_T3293887184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshModifierVolume
struct  NavMeshModifierVolume_t3293887184  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::m_Size
	Vector3_t3722313464  ___m_Size_4;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::m_Center
	Vector3_t3722313464  ___m_Center_5;
	// System.Int32 UnityEngine.AI.NavMeshModifierVolume::m_Area
	int32_t ___m_Area_6;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.AI.NavMeshModifierVolume::m_AffectedAgents
	List_1_t128053199 * ___m_AffectedAgents_7;

public:
	inline static int32_t get_offset_of_m_Size_4() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_Size_4)); }
	inline Vector3_t3722313464  get_m_Size_4() const { return ___m_Size_4; }
	inline Vector3_t3722313464 * get_address_of_m_Size_4() { return &___m_Size_4; }
	inline void set_m_Size_4(Vector3_t3722313464  value)
	{
		___m_Size_4 = value;
	}

	inline static int32_t get_offset_of_m_Center_5() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_Center_5)); }
	inline Vector3_t3722313464  get_m_Center_5() const { return ___m_Center_5; }
	inline Vector3_t3722313464 * get_address_of_m_Center_5() { return &___m_Center_5; }
	inline void set_m_Center_5(Vector3_t3722313464  value)
	{
		___m_Center_5 = value;
	}

	inline static int32_t get_offset_of_m_Area_6() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_Area_6)); }
	inline int32_t get_m_Area_6() const { return ___m_Area_6; }
	inline int32_t* get_address_of_m_Area_6() { return &___m_Area_6; }
	inline void set_m_Area_6(int32_t value)
	{
		___m_Area_6 = value;
	}

	inline static int32_t get_offset_of_m_AffectedAgents_7() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_AffectedAgents_7)); }
	inline List_1_t128053199 * get_m_AffectedAgents_7() const { return ___m_AffectedAgents_7; }
	inline List_1_t128053199 ** get_address_of_m_AffectedAgents_7() { return &___m_AffectedAgents_7; }
	inline void set_m_AffectedAgents_7(List_1_t128053199 * value)
	{
		___m_AffectedAgents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AffectedAgents_7), value);
	}
};

struct NavMeshModifierVolume_t3293887184_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume> UnityEngine.AI.NavMeshModifierVolume::s_NavMeshModifiers
	List_1_t470994630 * ___s_NavMeshModifiers_8;

public:
	inline static int32_t get_offset_of_s_NavMeshModifiers_8() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184_StaticFields, ___s_NavMeshModifiers_8)); }
	inline List_1_t470994630 * get_s_NavMeshModifiers_8() const { return ___s_NavMeshModifiers_8; }
	inline List_1_t470994630 ** get_address_of_s_NavMeshModifiers_8() { return &___s_NavMeshModifiers_8; }
	inline void set_s_NavMeshModifiers_8(List_1_t470994630 * value)
	{
		___s_NavMeshModifiers_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_NavMeshModifiers_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHMODIFIERVOLUME_T3293887184_H
#ifndef NAVMESHSURFACE_T986947041_H
#define NAVMESHSURFACE_T986947041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshSurface
struct  NavMeshSurface_t986947041  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.AI.NavMeshSurface::m_AgentTypeID
	int32_t ___m_AgentTypeID_4;
	// UnityEngine.AI.CollectObjects UnityEngine.AI.NavMeshSurface::m_CollectObjects
	int32_t ___m_CollectObjects_5;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::m_Size
	Vector3_t3722313464  ___m_Size_6;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::m_Center
	Vector3_t3722313464  ___m_Center_7;
	// UnityEngine.LayerMask UnityEngine.AI.NavMeshSurface::m_LayerMask
	LayerMask_t3493934918  ___m_LayerMask_8;
	// UnityEngine.AI.NavMeshCollectGeometry UnityEngine.AI.NavMeshSurface::m_UseGeometry
	int32_t ___m_UseGeometry_9;
	// System.Int32 UnityEngine.AI.NavMeshSurface::m_DefaultArea
	int32_t ___m_DefaultArea_10;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_IgnoreNavMeshAgent
	bool ___m_IgnoreNavMeshAgent_11;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_IgnoreNavMeshObstacle
	bool ___m_IgnoreNavMeshObstacle_12;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_OverrideTileSize
	bool ___m_OverrideTileSize_13;
	// System.Int32 UnityEngine.AI.NavMeshSurface::m_TileSize
	int32_t ___m_TileSize_14;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_OverrideVoxelSize
	bool ___m_OverrideVoxelSize_15;
	// System.Single UnityEngine.AI.NavMeshSurface::m_VoxelSize
	float ___m_VoxelSize_16;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_BuildHeightMesh
	bool ___m_BuildHeightMesh_17;
	// UnityEngine.AI.NavMeshData UnityEngine.AI.NavMeshSurface::m_NavMeshData
	NavMeshData_t1084598030 * ___m_NavMeshData_18;
	// UnityEngine.AI.NavMeshDataInstance UnityEngine.AI.NavMeshSurface::m_NavMeshDataInstance
	NavMeshDataInstance_t1498462893  ___m_NavMeshDataInstance_19;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::m_LastPosition
	Vector3_t3722313464  ___m_LastPosition_20;
	// UnityEngine.Quaternion UnityEngine.AI.NavMeshSurface::m_LastRotation
	Quaternion_t2301928331  ___m_LastRotation_21;

public:
	inline static int32_t get_offset_of_m_AgentTypeID_4() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_AgentTypeID_4)); }
	inline int32_t get_m_AgentTypeID_4() const { return ___m_AgentTypeID_4; }
	inline int32_t* get_address_of_m_AgentTypeID_4() { return &___m_AgentTypeID_4; }
	inline void set_m_AgentTypeID_4(int32_t value)
	{
		___m_AgentTypeID_4 = value;
	}

	inline static int32_t get_offset_of_m_CollectObjects_5() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_CollectObjects_5)); }
	inline int32_t get_m_CollectObjects_5() const { return ___m_CollectObjects_5; }
	inline int32_t* get_address_of_m_CollectObjects_5() { return &___m_CollectObjects_5; }
	inline void set_m_CollectObjects_5(int32_t value)
	{
		___m_CollectObjects_5 = value;
	}

	inline static int32_t get_offset_of_m_Size_6() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_Size_6)); }
	inline Vector3_t3722313464  get_m_Size_6() const { return ___m_Size_6; }
	inline Vector3_t3722313464 * get_address_of_m_Size_6() { return &___m_Size_6; }
	inline void set_m_Size_6(Vector3_t3722313464  value)
	{
		___m_Size_6 = value;
	}

	inline static int32_t get_offset_of_m_Center_7() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_Center_7)); }
	inline Vector3_t3722313464  get_m_Center_7() const { return ___m_Center_7; }
	inline Vector3_t3722313464 * get_address_of_m_Center_7() { return &___m_Center_7; }
	inline void set_m_Center_7(Vector3_t3722313464  value)
	{
		___m_Center_7 = value;
	}

	inline static int32_t get_offset_of_m_LayerMask_8() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_LayerMask_8)); }
	inline LayerMask_t3493934918  get_m_LayerMask_8() const { return ___m_LayerMask_8; }
	inline LayerMask_t3493934918 * get_address_of_m_LayerMask_8() { return &___m_LayerMask_8; }
	inline void set_m_LayerMask_8(LayerMask_t3493934918  value)
	{
		___m_LayerMask_8 = value;
	}

	inline static int32_t get_offset_of_m_UseGeometry_9() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_UseGeometry_9)); }
	inline int32_t get_m_UseGeometry_9() const { return ___m_UseGeometry_9; }
	inline int32_t* get_address_of_m_UseGeometry_9() { return &___m_UseGeometry_9; }
	inline void set_m_UseGeometry_9(int32_t value)
	{
		___m_UseGeometry_9 = value;
	}

	inline static int32_t get_offset_of_m_DefaultArea_10() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_DefaultArea_10)); }
	inline int32_t get_m_DefaultArea_10() const { return ___m_DefaultArea_10; }
	inline int32_t* get_address_of_m_DefaultArea_10() { return &___m_DefaultArea_10; }
	inline void set_m_DefaultArea_10(int32_t value)
	{
		___m_DefaultArea_10 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreNavMeshAgent_11() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_IgnoreNavMeshAgent_11)); }
	inline bool get_m_IgnoreNavMeshAgent_11() const { return ___m_IgnoreNavMeshAgent_11; }
	inline bool* get_address_of_m_IgnoreNavMeshAgent_11() { return &___m_IgnoreNavMeshAgent_11; }
	inline void set_m_IgnoreNavMeshAgent_11(bool value)
	{
		___m_IgnoreNavMeshAgent_11 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreNavMeshObstacle_12() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_IgnoreNavMeshObstacle_12)); }
	inline bool get_m_IgnoreNavMeshObstacle_12() const { return ___m_IgnoreNavMeshObstacle_12; }
	inline bool* get_address_of_m_IgnoreNavMeshObstacle_12() { return &___m_IgnoreNavMeshObstacle_12; }
	inline void set_m_IgnoreNavMeshObstacle_12(bool value)
	{
		___m_IgnoreNavMeshObstacle_12 = value;
	}

	inline static int32_t get_offset_of_m_OverrideTileSize_13() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_OverrideTileSize_13)); }
	inline bool get_m_OverrideTileSize_13() const { return ___m_OverrideTileSize_13; }
	inline bool* get_address_of_m_OverrideTileSize_13() { return &___m_OverrideTileSize_13; }
	inline void set_m_OverrideTileSize_13(bool value)
	{
		___m_OverrideTileSize_13 = value;
	}

	inline static int32_t get_offset_of_m_TileSize_14() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_TileSize_14)); }
	inline int32_t get_m_TileSize_14() const { return ___m_TileSize_14; }
	inline int32_t* get_address_of_m_TileSize_14() { return &___m_TileSize_14; }
	inline void set_m_TileSize_14(int32_t value)
	{
		___m_TileSize_14 = value;
	}

	inline static int32_t get_offset_of_m_OverrideVoxelSize_15() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_OverrideVoxelSize_15)); }
	inline bool get_m_OverrideVoxelSize_15() const { return ___m_OverrideVoxelSize_15; }
	inline bool* get_address_of_m_OverrideVoxelSize_15() { return &___m_OverrideVoxelSize_15; }
	inline void set_m_OverrideVoxelSize_15(bool value)
	{
		___m_OverrideVoxelSize_15 = value;
	}

	inline static int32_t get_offset_of_m_VoxelSize_16() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_VoxelSize_16)); }
	inline float get_m_VoxelSize_16() const { return ___m_VoxelSize_16; }
	inline float* get_address_of_m_VoxelSize_16() { return &___m_VoxelSize_16; }
	inline void set_m_VoxelSize_16(float value)
	{
		___m_VoxelSize_16 = value;
	}

	inline static int32_t get_offset_of_m_BuildHeightMesh_17() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_BuildHeightMesh_17)); }
	inline bool get_m_BuildHeightMesh_17() const { return ___m_BuildHeightMesh_17; }
	inline bool* get_address_of_m_BuildHeightMesh_17() { return &___m_BuildHeightMesh_17; }
	inline void set_m_BuildHeightMesh_17(bool value)
	{
		___m_BuildHeightMesh_17 = value;
	}

	inline static int32_t get_offset_of_m_NavMeshData_18() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_NavMeshData_18)); }
	inline NavMeshData_t1084598030 * get_m_NavMeshData_18() const { return ___m_NavMeshData_18; }
	inline NavMeshData_t1084598030 ** get_address_of_m_NavMeshData_18() { return &___m_NavMeshData_18; }
	inline void set_m_NavMeshData_18(NavMeshData_t1084598030 * value)
	{
		___m_NavMeshData_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_NavMeshData_18), value);
	}

	inline static int32_t get_offset_of_m_NavMeshDataInstance_19() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_NavMeshDataInstance_19)); }
	inline NavMeshDataInstance_t1498462893  get_m_NavMeshDataInstance_19() const { return ___m_NavMeshDataInstance_19; }
	inline NavMeshDataInstance_t1498462893 * get_address_of_m_NavMeshDataInstance_19() { return &___m_NavMeshDataInstance_19; }
	inline void set_m_NavMeshDataInstance_19(NavMeshDataInstance_t1498462893  value)
	{
		___m_NavMeshDataInstance_19 = value;
	}

	inline static int32_t get_offset_of_m_LastPosition_20() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_LastPosition_20)); }
	inline Vector3_t3722313464  get_m_LastPosition_20() const { return ___m_LastPosition_20; }
	inline Vector3_t3722313464 * get_address_of_m_LastPosition_20() { return &___m_LastPosition_20; }
	inline void set_m_LastPosition_20(Vector3_t3722313464  value)
	{
		___m_LastPosition_20 = value;
	}

	inline static int32_t get_offset_of_m_LastRotation_21() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_LastRotation_21)); }
	inline Quaternion_t2301928331  get_m_LastRotation_21() const { return ___m_LastRotation_21; }
	inline Quaternion_t2301928331 * get_address_of_m_LastRotation_21() { return &___m_LastRotation_21; }
	inline void set_m_LastRotation_21(Quaternion_t2301928331  value)
	{
		___m_LastRotation_21 = value;
	}
};

struct NavMeshSurface_t986947041_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface> UnityEngine.AI.NavMeshSurface::s_NavMeshSurfaces
	List_1_t2459021783 * ___s_NavMeshSurfaces_22;
	// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate UnityEngine.AI.NavMeshSurface::<>f__mg$cache0
	OnNavMeshPreUpdate_t1580782682 * ___U3CU3Ef__mgU24cache0_23;
	// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate UnityEngine.AI.NavMeshSurface::<>f__mg$cache1
	OnNavMeshPreUpdate_t1580782682 * ___U3CU3Ef__mgU24cache1_24;
	// System.Predicate`1<UnityEngine.AI.NavMeshModifierVolume> UnityEngine.AI.NavMeshSurface::<>f__am$cache0
	Predicate_1_t4119181308 * ___U3CU3Ef__amU24cache0_25;
	// System.Predicate`1<UnityEngine.AI.NavMeshModifier> UnityEngine.AI.NavMeshSurface::<>f__am$cache1
	Predicate_1_t1835999924 * ___U3CU3Ef__amU24cache1_26;
	// System.Predicate`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface::<>f__am$cache2
	Predicate_1_t1513480349 * ___U3CU3Ef__amU24cache2_27;
	// System.Predicate`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface::<>f__am$cache3
	Predicate_1_t1513480349 * ___U3CU3Ef__amU24cache3_28;

public:
	inline static int32_t get_offset_of_s_NavMeshSurfaces_22() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___s_NavMeshSurfaces_22)); }
	inline List_1_t2459021783 * get_s_NavMeshSurfaces_22() const { return ___s_NavMeshSurfaces_22; }
	inline List_1_t2459021783 ** get_address_of_s_NavMeshSurfaces_22() { return &___s_NavMeshSurfaces_22; }
	inline void set_s_NavMeshSurfaces_22(List_1_t2459021783 * value)
	{
		___s_NavMeshSurfaces_22 = value;
		Il2CppCodeGenWriteBarrier((&___s_NavMeshSurfaces_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_23() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__mgU24cache0_23)); }
	inline OnNavMeshPreUpdate_t1580782682 * get_U3CU3Ef__mgU24cache0_23() const { return ___U3CU3Ef__mgU24cache0_23; }
	inline OnNavMeshPreUpdate_t1580782682 ** get_address_of_U3CU3Ef__mgU24cache0_23() { return &___U3CU3Ef__mgU24cache0_23; }
	inline void set_U3CU3Ef__mgU24cache0_23(OnNavMeshPreUpdate_t1580782682 * value)
	{
		___U3CU3Ef__mgU24cache0_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_24() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__mgU24cache1_24)); }
	inline OnNavMeshPreUpdate_t1580782682 * get_U3CU3Ef__mgU24cache1_24() const { return ___U3CU3Ef__mgU24cache1_24; }
	inline OnNavMeshPreUpdate_t1580782682 ** get_address_of_U3CU3Ef__mgU24cache1_24() { return &___U3CU3Ef__mgU24cache1_24; }
	inline void set_U3CU3Ef__mgU24cache1_24(OnNavMeshPreUpdate_t1580782682 * value)
	{
		___U3CU3Ef__mgU24cache1_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_25() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache0_25)); }
	inline Predicate_1_t4119181308 * get_U3CU3Ef__amU24cache0_25() const { return ___U3CU3Ef__amU24cache0_25; }
	inline Predicate_1_t4119181308 ** get_address_of_U3CU3Ef__amU24cache0_25() { return &___U3CU3Ef__amU24cache0_25; }
	inline void set_U3CU3Ef__amU24cache0_25(Predicate_1_t4119181308 * value)
	{
		___U3CU3Ef__amU24cache0_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_26() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache1_26)); }
	inline Predicate_1_t1835999924 * get_U3CU3Ef__amU24cache1_26() const { return ___U3CU3Ef__amU24cache1_26; }
	inline Predicate_1_t1835999924 ** get_address_of_U3CU3Ef__amU24cache1_26() { return &___U3CU3Ef__amU24cache1_26; }
	inline void set_U3CU3Ef__amU24cache1_26(Predicate_1_t1835999924 * value)
	{
		___U3CU3Ef__amU24cache1_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_27() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache2_27)); }
	inline Predicate_1_t1513480349 * get_U3CU3Ef__amU24cache2_27() const { return ___U3CU3Ef__amU24cache2_27; }
	inline Predicate_1_t1513480349 ** get_address_of_U3CU3Ef__amU24cache2_27() { return &___U3CU3Ef__amU24cache2_27; }
	inline void set_U3CU3Ef__amU24cache2_27(Predicate_1_t1513480349 * value)
	{
		___U3CU3Ef__amU24cache2_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_28() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache3_28)); }
	inline Predicate_1_t1513480349 * get_U3CU3Ef__amU24cache3_28() const { return ___U3CU3Ef__amU24cache3_28; }
	inline Predicate_1_t1513480349 ** get_address_of_U3CU3Ef__amU24cache3_28() { return &___U3CU3Ef__amU24cache3_28; }
	inline void set_U3CU3Ef__amU24cache3_28(Predicate_1_t1513480349 * value)
	{
		___U3CU3Ef__amU24cache3_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHSURFACE_T986947041_H
#ifndef DECISION_T2344985003_H
#define DECISION_T2344985003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.Decision
struct  Decision_t2344985003  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECISION_T2344985003_H
#ifndef DECISIONMANAGER_T2018219593_H
#define DECISIONMANAGER_T2018219593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.DecisionManager
struct  DecisionManager_t2018219593  : public MonoBehaviour_t3962482529
{
public:
	// UtilityAI.Decision UtilityAI.DecisionManager::bestDecision
	Decision_t2344985003 * ___bestDecision_4;
	// System.Collections.Generic.List`1<UtilityAI.Decision> UtilityAI.DecisionManager::decisions
	List_1_t3817059745 * ___decisions_5;

public:
	inline static int32_t get_offset_of_bestDecision_4() { return static_cast<int32_t>(offsetof(DecisionManager_t2018219593, ___bestDecision_4)); }
	inline Decision_t2344985003 * get_bestDecision_4() const { return ___bestDecision_4; }
	inline Decision_t2344985003 ** get_address_of_bestDecision_4() { return &___bestDecision_4; }
	inline void set_bestDecision_4(Decision_t2344985003 * value)
	{
		___bestDecision_4 = value;
		Il2CppCodeGenWriteBarrier((&___bestDecision_4), value);
	}

	inline static int32_t get_offset_of_decisions_5() { return static_cast<int32_t>(offsetof(DecisionManager_t2018219593, ___decisions_5)); }
	inline List_1_t3817059745 * get_decisions_5() const { return ___decisions_5; }
	inline List_1_t3817059745 ** get_address_of_decisions_5() { return &___decisions_5; }
	inline void set_decisions_5(List_1_t3817059745 * value)
	{
		___decisions_5 = value;
		Il2CppCodeGenWriteBarrier((&___decisions_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECISIONMANAGER_T2018219593_H
#ifndef FIGHT_COMPETITOR_T2592966111_H
#define FIGHT_COMPETITOR_T2592966111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIGHT_COMPETITOR
struct  FIGHT_COMPETITOR_t2592966111  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIGHT_COMPETITOR::type
	int32_t ___type_4;
	// System.Single UtilityAI.FIGHT_COMPETITOR::distanceToFlagVariable
	float ___distanceToFlagVariable_5;
	// System.Single UtilityAI.FIGHT_COMPETITOR::distanceVariable
	float ___distanceVariable_6;
	// System.Single UtilityAI.FIGHT_COMPETITOR::maxDistanceToFight
	float ___maxDistanceToFight_7;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_distanceToFlagVariable_5() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___distanceToFlagVariable_5)); }
	inline float get_distanceToFlagVariable_5() const { return ___distanceToFlagVariable_5; }
	inline float* get_address_of_distanceToFlagVariable_5() { return &___distanceToFlagVariable_5; }
	inline void set_distanceToFlagVariable_5(float value)
	{
		___distanceToFlagVariable_5 = value;
	}

	inline static int32_t get_offset_of_distanceVariable_6() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___distanceVariable_6)); }
	inline float get_distanceVariable_6() const { return ___distanceVariable_6; }
	inline float* get_address_of_distanceVariable_6() { return &___distanceVariable_6; }
	inline void set_distanceVariable_6(float value)
	{
		___distanceVariable_6 = value;
	}

	inline static int32_t get_offset_of_maxDistanceToFight_7() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___maxDistanceToFight_7)); }
	inline float get_maxDistanceToFight_7() const { return ___maxDistanceToFight_7; }
	inline float* get_address_of_maxDistanceToFight_7() { return &___maxDistanceToFight_7; }
	inline void set_maxDistanceToFight_7(float value)
	{
		___maxDistanceToFight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIGHT_COMPETITOR_T2592966111_H
#ifndef FIND_FLAG_T1938094438_H
#define FIND_FLAG_T1938094438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIND_FLAG
struct  FIND_FLAG_t1938094438  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIND_FLAG::type
	int32_t ___type_4;
	// System.Int32 UtilityAI.FIND_FLAG::maxDistanceForAffect
	int32_t ___maxDistanceForAffect_5;
	// System.Single UtilityAI.FIND_FLAG::maxDistanceWeighting
	float ___maxDistanceWeighting_6;
	// System.Single UtilityAI.FIND_FLAG::maxEnemyWeighting
	float ___maxEnemyWeighting_7;
	// System.Single UtilityAI.FIND_FLAG::maxHealthWeighting
	float ___maxHealthWeighting_8;
	// System.Single UtilityAI.FIND_FLAG::hungerVariable
	float ___hungerVariable_9;
	// System.Single UtilityAI.FIND_FLAG::thirstVariable
	float ___thirstVariable_10;
	// System.Single UtilityAI.FIND_FLAG::healthVariable
	float ___healthVariable_11;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_maxDistanceForAffect_5() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxDistanceForAffect_5)); }
	inline int32_t get_maxDistanceForAffect_5() const { return ___maxDistanceForAffect_5; }
	inline int32_t* get_address_of_maxDistanceForAffect_5() { return &___maxDistanceForAffect_5; }
	inline void set_maxDistanceForAffect_5(int32_t value)
	{
		___maxDistanceForAffect_5 = value;
	}

	inline static int32_t get_offset_of_maxDistanceWeighting_6() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxDistanceWeighting_6)); }
	inline float get_maxDistanceWeighting_6() const { return ___maxDistanceWeighting_6; }
	inline float* get_address_of_maxDistanceWeighting_6() { return &___maxDistanceWeighting_6; }
	inline void set_maxDistanceWeighting_6(float value)
	{
		___maxDistanceWeighting_6 = value;
	}

	inline static int32_t get_offset_of_maxEnemyWeighting_7() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxEnemyWeighting_7)); }
	inline float get_maxEnemyWeighting_7() const { return ___maxEnemyWeighting_7; }
	inline float* get_address_of_maxEnemyWeighting_7() { return &___maxEnemyWeighting_7; }
	inline void set_maxEnemyWeighting_7(float value)
	{
		___maxEnemyWeighting_7 = value;
	}

	inline static int32_t get_offset_of_maxHealthWeighting_8() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxHealthWeighting_8)); }
	inline float get_maxHealthWeighting_8() const { return ___maxHealthWeighting_8; }
	inline float* get_address_of_maxHealthWeighting_8() { return &___maxHealthWeighting_8; }
	inline void set_maxHealthWeighting_8(float value)
	{
		___maxHealthWeighting_8 = value;
	}

	inline static int32_t get_offset_of_hungerVariable_9() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___hungerVariable_9)); }
	inline float get_hungerVariable_9() const { return ___hungerVariable_9; }
	inline float* get_address_of_hungerVariable_9() { return &___hungerVariable_9; }
	inline void set_hungerVariable_9(float value)
	{
		___hungerVariable_9 = value;
	}

	inline static int32_t get_offset_of_thirstVariable_10() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___thirstVariable_10)); }
	inline float get_thirstVariable_10() const { return ___thirstVariable_10; }
	inline float* get_address_of_thirstVariable_10() { return &___thirstVariable_10; }
	inline void set_thirstVariable_10(float value)
	{
		___thirstVariable_10 = value;
	}

	inline static int32_t get_offset_of_healthVariable_11() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___healthVariable_11)); }
	inline float get_healthVariable_11() const { return ___healthVariable_11; }
	inline float* get_address_of_healthVariable_11() { return &___healthVariable_11; }
	inline void set_healthVariable_11(float value)
	{
		___healthVariable_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIND_FLAG_T1938094438_H
#ifndef FIND_FOOD_T1534940982_H
#define FIND_FOOD_T1534940982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIND_FOOD
struct  FIND_FOOD_t1534940982  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIND_FOOD::type
	int32_t ___type_4;
	// System.Single UtilityAI.FIND_FOOD::maxDistanceForAffect
	float ___maxDistanceForAffect_5;
	// System.Single UtilityAI.FIND_FOOD::maxDistanceWeighting
	float ___maxDistanceWeighting_6;
	// System.Single UtilityAI.FIND_FOOD::maxHungerWeighting
	float ___maxHungerWeighting_7;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_maxDistanceForAffect_5() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___maxDistanceForAffect_5)); }
	inline float get_maxDistanceForAffect_5() const { return ___maxDistanceForAffect_5; }
	inline float* get_address_of_maxDistanceForAffect_5() { return &___maxDistanceForAffect_5; }
	inline void set_maxDistanceForAffect_5(float value)
	{
		___maxDistanceForAffect_5 = value;
	}

	inline static int32_t get_offset_of_maxDistanceWeighting_6() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___maxDistanceWeighting_6)); }
	inline float get_maxDistanceWeighting_6() const { return ___maxDistanceWeighting_6; }
	inline float* get_address_of_maxDistanceWeighting_6() { return &___maxDistanceWeighting_6; }
	inline void set_maxDistanceWeighting_6(float value)
	{
		___maxDistanceWeighting_6 = value;
	}

	inline static int32_t get_offset_of_maxHungerWeighting_7() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___maxHungerWeighting_7)); }
	inline float get_maxHungerWeighting_7() const { return ___maxHungerWeighting_7; }
	inline float* get_address_of_maxHungerWeighting_7() { return &___maxHungerWeighting_7; }
	inline void set_maxHungerWeighting_7(float value)
	{
		___maxHungerWeighting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIND_FOOD_T1534940982_H
#ifndef FIND_WATER_T1027203745_H
#define FIND_WATER_T1027203745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIND_WATER
struct  FIND_WATER_t1027203745  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIND_WATER::type
	int32_t ___type_4;
	// System.Single UtilityAI.FIND_WATER::maxDistanceForAffect
	float ___maxDistanceForAffect_5;
	// System.Single UtilityAI.FIND_WATER::maxDistanceWeighting
	float ___maxDistanceWeighting_6;
	// System.Single UtilityAI.FIND_WATER::maxThirstWeighting
	float ___maxThirstWeighting_7;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_maxDistanceForAffect_5() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___maxDistanceForAffect_5)); }
	inline float get_maxDistanceForAffect_5() const { return ___maxDistanceForAffect_5; }
	inline float* get_address_of_maxDistanceForAffect_5() { return &___maxDistanceForAffect_5; }
	inline void set_maxDistanceForAffect_5(float value)
	{
		___maxDistanceForAffect_5 = value;
	}

	inline static int32_t get_offset_of_maxDistanceWeighting_6() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___maxDistanceWeighting_6)); }
	inline float get_maxDistanceWeighting_6() const { return ___maxDistanceWeighting_6; }
	inline float* get_address_of_maxDistanceWeighting_6() { return &___maxDistanceWeighting_6; }
	inline void set_maxDistanceWeighting_6(float value)
	{
		___maxDistanceWeighting_6 = value;
	}

	inline static int32_t get_offset_of_maxThirstWeighting_7() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___maxThirstWeighting_7)); }
	inline float get_maxThirstWeighting_7() const { return ___maxThirstWeighting_7; }
	inline float* get_address_of_maxThirstWeighting_7() { return &___maxThirstWeighting_7; }
	inline void set_maxThirstWeighting_7(float value)
	{
		___maxThirstWeighting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIND_WATER_T1027203745_H
#ifndef WATER_T1083516957_H
#define WATER_T1083516957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Water
struct  Water_t1083516957  : public PickupableItem_t2078450402
{
public:
	// System.Single Water::thirstBoost
	float ___thirstBoost_4;

public:
	inline static int32_t get_offset_of_thirstBoost_4() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___thirstBoost_4)); }
	inline float get_thirstBoost_4() const { return ___thirstBoost_4; }
	inline float* get_address_of_thirstBoost_4() { return &___thirstBoost_4; }
	inline void set_thirstBoost_4(float value)
	{
		___thirstBoost_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T1083516957_H
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.AI.NavMeshModifierVolume[]
struct NavMeshModifierVolumeU5BU5D_t3251859441  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) NavMeshModifierVolume_t3293887184 * m_Items[1];

public:
	inline NavMeshModifierVolume_t3293887184 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NavMeshModifierVolume_t3293887184 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NavMeshModifierVolume_t3293887184 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NavMeshModifierVolume_t3293887184 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NavMeshModifierVolume_t3293887184 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NavMeshModifierVolume_t3293887184 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.AI.NavMeshModifier[]
struct NavMeshModifierU5BU5D_t1673145177  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) NavMeshModifier_t1010705800 * m_Items[1];

public:
	inline NavMeshModifier_t1010705800 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NavMeshModifier_t1010705800 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NavMeshModifier_t1010705800 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline NavMeshModifier_t1010705800 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NavMeshModifier_t1010705800 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NavMeshModifier_t1010705800 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m3641213752_gshared (List_1_t128053199 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Contains_m2654125393_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m1416767016_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m361000296_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m888956288_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(!0)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_IndexOf_m2639980317_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisRuntimeObject_m1643422945_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m1328752868_gshared (List_1_t257213610 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Predicate_1__ctor_m327447107_gshared (Predicate_1_t3905400288 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_RemoveAll_m4292035398_gshared (List_1_t257213610 * __this, Predicate_1_t3905400288 * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m863973498_gshared (List_1_t2160260967 * __this, NavMeshBuildSource_t688186225  p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m3711269370_gshared (List_1_t2160260967 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m1940349360_gshared (List_1_t2785658607 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m2814401212_gshared (List_1_t2785658607 * __this, NavMeshBuildMarkup_t1313583865  p0, const RuntimeMethod* method);
// System.Void System.Predicate`1<UnityEngine.AI.NavMeshBuildSource>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Predicate_1__ctor_m1542028990_gshared (Predicate_1_t1513480349 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::RemoveAll(System.Predicate`1<!0>)
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_RemoveAll_m1679687378_gshared (List_1_t2160260967 * __this, Predicate_1_t1513480349 * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t4049504844  List_1_GetEnumerator_m1287074780_gshared (List_1_t2160260967 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshBuildSource>::get_Current()
extern "C" IL2CPP_METHOD_ATTR NavMeshBuildSource_t688186225  Enumerator_get_Current_m601408329_gshared (Enumerator_t4049504844 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshBuildSource>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m363784717_gshared (Enumerator_t4049504844 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshBuildSource>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m2830648961_gshared (Enumerator_t4049504844 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m3641213752 (List_1_t128053199 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t128053199 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m3641213752_gshared)(__this, p0, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>::Contains(!0)
inline bool List_1_Contains_m4011681093 (List_1_t2482780542 * __this, NavMeshModifier_t1010705800 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t2482780542 *, NavMeshModifier_t1010705800 *, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>::Add(!0)
inline void List_1_Add_m3904861453 (List_1_t2482780542 * __this, NavMeshModifier_t1010705800 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2482780542 *, NavMeshModifier_t1010705800 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>::Remove(!0)
inline bool List_1_Remove_m4119032061 (List_1_t2482780542 * __this, NavMeshModifier_t1010705800 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t2482780542 *, NavMeshModifier_t1010705800 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
inline int32_t List_1_get_Count_m361000296 (List_1_t128053199 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t128053199 *, const RuntimeMethod*))List_1_get_Count_m361000296_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_m888956288 (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_get_Item_m888956288_gshared)(__this, p0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(!0)
inline int32_t List_1_IndexOf_m2639980317 (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_IndexOf_m2639980317_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>::.ctor()
inline void List_1__ctor_m1475854777 (List_1_t2482780542 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2482780542 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>::Contains(!0)
inline bool List_1_Contains_m4164730806 (List_1_t470994630 * __this, NavMeshModifierVolume_t3293887184 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t470994630 *, NavMeshModifierVolume_t3293887184 *, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>::Add(!0)
inline void List_1_Add_m2821620141 (List_1_t470994630 * __this, NavMeshModifierVolume_t3293887184 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t470994630 *, NavMeshModifierVolume_t3293887184 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>::Remove(!0)
inline bool List_1_Remove_m2403847378 (List_1_t470994630 * __this, NavMeshModifierVolume_t3293887184 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t470994630 *, NavMeshModifierVolume_t3293887184 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>::.ctor()
inline void List_1__ctor_m3566324597 (List_1_t470994630 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t470994630 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C" IL2CPP_METHOD_ATTR LayerMask_t3493934918  LayerMask_op_Implicit_m90232283 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshSurface::Register(UnityEngine.AI.NavMeshSurface)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_Register_m2505341092 (RuntimeObject * __this /* static, unused */, NavMeshSurface_t986947041 * ___surface0, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshSurface::AddData()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_AddData_m3992805541 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshSurface::RemoveData()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_RemoveData_m3790252040 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshSurface::Unregister(UnityEngine.AI.NavMeshSurface)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_Unregister_m2798101876 (RuntimeObject * __this /* static, unused */, NavMeshSurface_t986947041 * ___surface0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshDataInstance::get_valid()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshDataInstance_get_valid_m2577459468 (NavMeshDataInstance_t1498462893 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.AI.NavMeshDataInstance UnityEngine.AI.NavMesh::AddNavMeshData(UnityEngine.AI.NavMeshData,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR NavMeshDataInstance_t1498462893  NavMesh_AddNavMeshData_m1391221680 (RuntimeObject * __this /* static, unused */, NavMeshData_t1084598030 * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshDataInstance::set_owner(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void NavMeshDataInstance_set_owner_m70396501 (NavMeshDataInstance_t1498462893 * __this, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshDataInstance::Remove()
extern "C" IL2CPP_METHOD_ATTR void NavMeshDataInstance_Remove_m996936756 (NavMeshDataInstance_t1498462893 * __this, const RuntimeMethod* method);
// UnityEngine.AI.NavMeshBuildSettings UnityEngine.AI.NavMesh::GetSettingsByID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR NavMeshBuildSettings_t1985378544  NavMesh_GetSettingsByID_m546770701 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AI.NavMeshBuildSettings::get_agentTypeID()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshBuildSettings_get_agentTypeID_m3314078478 (NavMeshBuildSettings_t1985378544 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.AI.NavMeshSurface::get_agentTypeID()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshSurface_get_agentTypeID_m334719145 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogWarning_m831581295 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSettings::set_agentTypeID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSettings_set_agentTypeID_m3995891764 (NavMeshBuildSettings_t1985378544 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshSurface::get_overrideTileSize()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_get_overrideTileSize_m3617479521 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSettings::set_overrideTileSize(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSettings_set_overrideTileSize_m1668241129 (NavMeshBuildSettings_t1985378544 * __this, bool p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AI.NavMeshSurface::get_tileSize()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshSurface_get_tileSize_m1315768493 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSettings::set_tileSize(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSettings_set_tileSize_m1243065940 (NavMeshBuildSettings_t1985378544 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshSurface::get_overrideVoxelSize()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_get_overrideVoxelSize_m3411287898 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSettings::set_overrideVoxelSize(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSettings_set_overrideVoxelSize_m1423447633 (NavMeshBuildSettings_t1985378544 * __this, bool p0, const RuntimeMethod* method);
// System.Single UnityEngine.AI.NavMeshSurface::get_voxelSize()
extern "C" IL2CPP_METHOD_ATTR float NavMeshSurface_get_voxelSize_m1506958526 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSettings::set_voxelSize(System.Single)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSettings_set_voxelSize_m2794829786 (NavMeshBuildSettings_t1985378544 * __this, float p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface::CollectSources()
extern "C" IL2CPP_METHOD_ATTR List_1_t2160260967 * NavMeshSurface_CollectSources_m1904116616 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::Abs(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshSurface_Abs_m641648730 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Bounds__ctor_m1937678907 (Bounds_t2266837910 * __this, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.AI.NavMeshSurface::CalculateWorldBounds(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  NavMeshSurface_CalculateWorldBounds_m4178605729 (NavMeshSurface_t986947041 * __this, List_1_t2160260967 * ___sources0, const RuntimeMethod* method);
// UnityEngine.AI.NavMeshBuildSettings UnityEngine.AI.NavMeshSurface::GetBuildSettings()
extern "C" IL2CPP_METHOD_ATTR NavMeshBuildSettings_t1985378544  NavMeshSurface_GetBuildSettings_m1758800876 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// UnityEngine.AI.NavMeshData UnityEngine.AI.NavMeshBuilder::BuildNavMeshData(UnityEngine.AI.NavMeshBuildSettings,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.Bounds,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR NavMeshData_t1084598030 * NavMeshBuilder_BuildNavMeshData_m2250664010 (RuntimeObject * __this /* static, unused */, NavMeshBuildSettings_t1985378544  p0, List_1_t2160260967 * p1, Bounds_t2266837910  p2, Vector3_t3722313464  p3, Quaternion_t2301928331  p4, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Object_set_name_m291480324 (Object_t631007953 * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C" IL2CPP_METHOD_ATTR bool Behaviour_get_isActiveAndEnabled_m3143666263 (Behaviour_t1437897464 * __this, const RuntimeMethod* method);
// UnityEngine.AsyncOperation UnityEngine.AI.NavMeshBuilder::UpdateNavMeshDataAsync(UnityEngine.AI.NavMeshData,UnityEngine.AI.NavMeshBuildSettings,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>,UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR AsyncOperation_t1445031843 * NavMeshBuilder_UpdateNavMeshDataAsync_m159354334 (RuntimeObject * __this /* static, unused */, NavMeshData_t1084598030 * p0, NavMeshBuildSettings_t1985378544  p1, List_1_t2160260967 * p2, Bounds_t2266837910  p3, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>::get_Count()
inline int32_t List_1_get_Count_m3200332207 (List_1_t2459021783 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2459021783 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnNavMeshPreUpdate__ctor_m2758342548 (OnNavMeshPreUpdate_t1580782682 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>::Contains(!0)
inline bool List_1_Contains_m939277675 (List_1_t2459021783 * __this, NavMeshSurface_t986947041 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t2459021783 *, NavMeshSurface_t986947041 *, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>::Add(!0)
inline void List_1_Add_m3920533274 (List_1_t2459021783 * __this, NavMeshSurface_t986947041 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2459021783 *, NavMeshSurface_t986947041 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>::Remove(!0)
inline bool List_1_Remove_m795306531 (List_1_t2459021783 * __this, NavMeshSurface_t986947041 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t2459021783 *, NavMeshSurface_t986947041 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method);
}
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C" IL2CPP_METHOD_ATTR Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>::get_Item(System.Int32)
inline NavMeshSurface_t986947041 * List_1_get_Item_m1770120800 (List_1_t2459021783 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  NavMeshSurface_t986947041 * (*) (List_1_t2459021783 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method);
}
// System.Void UnityEngine.AI.NavMeshSurface::UpdateDataIfTransformChanged()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_UpdateDataIfTransformChanged_m1853993666 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.AI.NavMeshModifierVolume>()
inline NavMeshModifierVolumeU5BU5D_t3251859441* Component_GetComponentsInChildren_TisNavMeshModifierVolume_t3293887184_m3885906544 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshModifierVolumeU5BU5D_t3251859441* (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m1643422945_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m530909711 (List_1_t470994630 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t470994630 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1328752868_gshared)(__this, p0, method);
}
// System.Void System.Predicate`1<UnityEngine.AI.NavMeshModifierVolume>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m3152402189 (Predicate_1_t4119181308 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_t4119181308 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m327447107_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_m3207188928 (List_1_t470994630 * __this, Predicate_1_t4119181308 * p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t470994630 *, Predicate_1_t4119181308 *, const RuntimeMethod*))List_1_RemoveAll_m4292035398_gshared)(__this, p0, method);
}
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume> UnityEngine.AI.NavMeshModifierVolume::get_activeModifiers()
extern "C" IL2CPP_METHOD_ATTR List_1_t470994630 * NavMeshModifierVolume_get_activeModifiers_m244070183 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>::GetEnumerator()
inline Enumerator_t2360238507  List_1_GetEnumerator_m741968409 (List_1_t470994630 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t2360238507  (*) (List_1_t470994630 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifierVolume>::get_Current()
inline NavMeshModifierVolume_t3293887184 * Enumerator_get_Current_m1310551773 (Enumerator_t2360238507 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshModifierVolume_t3293887184 * (*) (Enumerator_t2360238507 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method);
}
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C" IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_m3296792737 (RuntimeObject * __this /* static, unused */, LayerMask_t3493934918  p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" IL2CPP_METHOD_ATTR int32_t GameObject_get_layer_m4158800245 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshModifierVolume::AffectsAgentType(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifierVolume_AffectsAgentType_m2327990194 (NavMeshModifierVolume_t3293887184 * __this, int32_t ___agentTypeID0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshModifierVolume_get_center_m2830775605 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_TransformPoint_m226827784 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_lossyScale_m465496651 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshModifierVolume_get_size_m1842721009 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSource::set_shape(UnityEngine.AI.NavMeshBuildSourceShape)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSource_set_shape_m2067493469 (NavMeshBuildSource_t688186225 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_one_m1629952498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Matrix4x4_TRS_m3801934620 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Quaternion_t2301928331  p1, Vector3_t3722313464  p2, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSource::set_transform(UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSource_set_transform_m1212452088 (NavMeshBuildSource_t688186225 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSource::set_size(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSource_set_size_m682598383 (NavMeshBuildSource_t688186225 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AI.NavMeshModifierVolume::get_area()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshModifierVolume_get_area_m3186387738 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildSource::set_area(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildSource_set_area_m2888906222 (NavMeshBuildSource_t688186225 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::Add(!0)
inline void List_1_Add_m863973498 (List_1_t2160260967 * __this, NavMeshBuildSource_t688186225  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2160260967 *, NavMeshBuildSource_t688186225 , const RuntimeMethod*))List_1_Add_m863973498_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifierVolume>::MoveNext()
inline bool Enumerator_MoveNext_m3090114117 (Enumerator_t2360238507 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t2360238507 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifierVolume>::Dispose()
inline void Enumerator_Dispose_m734651789 (Enumerator_t2360238507 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t2360238507 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::.ctor()
inline void List_1__ctor_m3711269370 (List_1_t2160260967 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2160260967 *, const RuntimeMethod*))List_1__ctor_m3711269370_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>::.ctor()
inline void List_1__ctor_m1940349360 (List_1_t2785658607 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2785658607 *, const RuntimeMethod*))List_1__ctor_m1940349360_gshared)(__this, method);
}
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.AI.NavMeshModifier>()
inline NavMeshModifierU5BU5D_t1673145177* Component_GetComponentsInChildren_TisNavMeshModifier_t1010705800_m3813049907 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshModifierU5BU5D_t1673145177* (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m1643422945_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void List_1__ctor_m1410039760 (List_1_t2482780542 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2482780542 *, RuntimeObject*, const RuntimeMethod*))List_1__ctor_m1328752868_gshared)(__this, p0, method);
}
// System.Void System.Predicate`1<UnityEngine.AI.NavMeshModifier>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m2280749923 (Predicate_1_t1835999924 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_t1835999924 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m327447107_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_m616612175 (List_1_t2482780542 * __this, Predicate_1_t1835999924 * p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2482780542 *, Predicate_1_t1835999924 *, const RuntimeMethod*))List_1_RemoveAll_m4292035398_gshared)(__this, p0, method);
}
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier> UnityEngine.AI.NavMeshModifier::get_activeModifiers()
extern "C" IL2CPP_METHOD_ATTR List_1_t2482780542 * NavMeshModifier_get_activeModifiers_m407144863 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>::GetEnumerator()
inline Enumerator_t77057123  List_1_GetEnumerator_m562693788 (List_1_t2482780542 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t77057123  (*) (List_1_t2482780542 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifier>::get_Current()
inline NavMeshModifier_t1010705800 * Enumerator_get_Current_m4223334170 (Enumerator_t77057123 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshModifier_t1010705800 * (*) (Enumerator_t77057123 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method);
}
// System.Boolean UnityEngine.AI.NavMeshModifier::AffectsAgentType(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifier_AffectsAgentType_m2140675392 (NavMeshModifier_t1010705800 * __this, int32_t ___agentTypeID0, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildMarkup::set_root(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildMarkup_set_root_m518958946 (NavMeshBuildMarkup_t1313583865 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshModifier::get_overrideArea()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifier_get_overrideArea_m3734533896 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildMarkup::set_overrideArea(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildMarkup_set_overrideArea_m229372463 (NavMeshBuildMarkup_t1313583865 * __this, bool p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.AI.NavMeshModifier::get_area()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshModifier_get_area_m2942160106 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildMarkup::set_area(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildMarkup_set_area_m1361147558 (NavMeshBuildMarkup_t1313583865 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshModifier::get_ignoreFromBuild()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifier_get_ignoreFromBuild_m4183908247 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuildMarkup::set_ignoreFromBuild(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuildMarkup_set_ignoreFromBuild_m709640216 (NavMeshBuildMarkup_t1313583865 * __this, bool p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>::Add(!0)
inline void List_1_Add_m2814401212 (List_1_t2785658607 * __this, NavMeshBuildMarkup_t1313583865  p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2785658607 *, NavMeshBuildMarkup_t1313583865 , const RuntimeMethod*))List_1_Add_m2814401212_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifier>::MoveNext()
inline bool Enumerator_MoveNext_m936905458 (Enumerator_t77057123 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t77057123 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshModifier>::Dispose()
inline void Enumerator_Dispose_m465728948 (Enumerator_t77057123 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t77057123 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void UnityEngine.AI.NavMeshBuilder::CollectSources(UnityEngine.Transform,System.Int32,UnityEngine.AI.NavMeshCollectGeometry,System.Int32,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuilder_CollectSources_m1517047385 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * p0, int32_t p1, int32_t p2, int32_t p3, List_1_t2785658607 * p4, List_1_t2160260967 * p5, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.AI.NavMeshSurface::GetWorldBounds(UnityEngine.Matrix4x4,UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  NavMeshSurface_GetWorldBounds_m3220460035 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___mat0, Bounds_t2266837910  ___bounds1, const RuntimeMethod* method);
// System.Void UnityEngine.AI.NavMeshBuilder::CollectSources(UnityEngine.Bounds,System.Int32,UnityEngine.AI.NavMeshCollectGeometry,System.Int32,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildMarkup>,System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern "C" IL2CPP_METHOD_ATTR void NavMeshBuilder_CollectSources_m553736863 (RuntimeObject * __this /* static, unused */, Bounds_t2266837910  p0, int32_t p1, int32_t p2, int32_t p3, List_1_t2785658607 * p4, List_1_t2160260967 * p5, const RuntimeMethod* method);
// System.Void System.Predicate`1<UnityEngine.AI.NavMeshBuildSource>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m1542028990 (Predicate_1_t1513480349 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_t1513480349 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m1542028990_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_m1679687378 (List_1_t2160260967 * __this, Predicate_1_t1513480349 * p0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2160260967 *, Predicate_1_t1513480349 *, const RuntimeMethod*))List_1_RemoveAll_m1679687378_gshared)(__this, p0, method);
}
// System.Void UnityEngine.AI.NavMeshSurface::AppendModifierVolumes(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>&)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_AppendModifierVolumes_m4188255682 (NavMeshSurface_t986947041 * __this, List_1_t2160260967 ** ___sources0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_right_m1913784872 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Matrix4x4_MultiplyVector_m3808798942 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Bounds_get_center_m1418449258 (Bounds_t2266837910 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Matrix4x4_MultiplyPoint_m1575665487 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Bounds_get_size_m1178783246 (Bounds_t2266837910 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Matrix4x4_get_inverse_m1870592360 (Matrix4x4_t1817901843 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>::GetEnumerator()
inline Enumerator_t4049504844  List_1_GetEnumerator_m1287074780 (List_1_t2160260967 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t4049504844  (*) (List_1_t2160260967 *, const RuntimeMethod*))List_1_GetEnumerator_m1287074780_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshBuildSource>::get_Current()
inline NavMeshBuildSource_t688186225  Enumerator_get_Current_m601408329 (Enumerator_t4049504844 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshBuildSource_t688186225  (*) (Enumerator_t4049504844 *, const RuntimeMethod*))Enumerator_get_Current_m601408329_gshared)(__this, method);
}
// UnityEngine.AI.NavMeshBuildSourceShape UnityEngine.AI.NavMeshBuildSource::get_shape()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshBuildSource_get_shape_m2698270452 (NavMeshBuildSource_t688186225 * __this, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.AI.NavMeshBuildSource::get_sourceObject()
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * NavMeshBuildSource_get_sourceObject_m603070145 (NavMeshBuildSource_t688186225 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.AI.NavMeshBuildSource::get_transform()
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  NavMeshBuildSource_get_transform_m868807620 (NavMeshBuildSource_t688186225 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" IL2CPP_METHOD_ATTR Matrix4x4_t1817901843  Matrix4x4_op_Multiply_m1876492807 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  Mesh_get_bounds_m2004960313 (Mesh_t3648964284 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR void Bounds_Encapsulate_m1263362003 (Bounds_t2266837910 * __this, Bounds_t2266837910  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.TerrainData::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  TerrainData_get_size_m1871576403 (TerrainData_t657004131 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Multiply_m2104357790 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.AI.NavMeshBuildSource::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshBuildSource_get_size_m1205786197 (NavMeshBuildSource_t688186225 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshBuildSource>::MoveNext()
inline bool Enumerator_MoveNext_m363784717 (Enumerator_t4049504844 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t4049504844 *, const RuntimeMethod*))Enumerator_MoveNext_m363784717_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AI.NavMeshBuildSource>::Dispose()
inline void Enumerator_Dispose_m2830648961 (Enumerator_t4049504844 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t4049504844 *, const RuntimeMethod*))Enumerator_Dispose_m2830648961_gshared)(__this, method);
}
// System.Void UnityEngine.Bounds::Expand(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Bounds_Expand_m2714456408 (Bounds_t2266837910 * __this, float p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m315980366 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR bool Quaternion_op_Inequality_m1948345154 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.AI.NavMeshSurface::HasTransformChanged()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_HasTransformChanged_m251008277 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>::.ctor()
inline void List_1__ctor_m1544123386 (List_1_t2459021783 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2459021783 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// UnityEngine.Component UnityEngine.AI.NavMeshBuildSource::get_component()
extern "C" IL2CPP_METHOD_ATTR Component_t1923634451 * NavMeshBuildSource_get_component_m3010658605 (NavMeshBuildSource_t688186225 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AI.NavMeshAgent>()
inline NavMeshAgent_t1276799816 * GameObject_GetComponent_TisNavMeshAgent_t1276799816_m2253338685 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshAgent_t1276799816 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AI.NavMeshObstacle>()
inline NavMeshObstacle_t3657678309 * GameObject_GetComponent_TisNavMeshObstacle_t3657678309_m1899691350 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshObstacle_t3657678309 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UtilityAI.Decision>::.ctor()
inline void List_1__ctor_m3019387110 (List_1_t3817059745 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3817059745 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<UtilityAI.Decision>::get_Count()
inline int32_t List_1_get_Count_m1142946597 (List_1_t3817059745 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3817059745 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<UtilityAI.Decision>::get_Item(System.Int32)
inline Decision_t2344985003 * List_1_get_Item_m2061167563 (List_1_t3817059745 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Decision_t2344985003 * (*) (List_1_t3817059745 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UtilityAI.Decision>::GetEnumerator()
inline Enumerator_t1411336326  List_1_GetEnumerator_m2970145501 (List_1_t3817059745 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t1411336326  (*) (List_1_t3817059745 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UtilityAI.Decision>::get_Current()
inline Decision_t2344985003 * Enumerator_get_Current_m2922767688 (Enumerator_t1411336326 * __this, const RuntimeMethod* method)
{
	return ((  Decision_t2344985003 * (*) (Enumerator_t1411336326 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UtilityAI.Decision>::MoveNext()
inline bool Enumerator_MoveNext_m1707601366 (Enumerator_t1411336326 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t1411336326 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UtilityAI.Decision>::Dispose()
inline void Enumerator_Dispose_m3610717444 (Enumerator_t1411336326 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t1411336326 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void UtilityAI.Decision::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Decision__ctor_m1412182017 (Decision_t2344985003 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.String)
extern "C" IL2CPP_METHOD_ATTR Component_t1923634451 * GameObject_GetComponent_m3266446757 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// Competitor UtilityAI.FIGHT_COMPETITOR::GetBestCompetitorToFight()
extern "C" IL2CPP_METHOD_ATTR Competitor_t2775849210 * FIGHT_COMPETITOR_GetBestCompetitorToFight_m2559081439 (FIGHT_COMPETITOR_t2592966111 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Competitor>()
inline Competitor_t2775849210 * Component_GetComponent_TisCompetitor_t2775849210_m662455226 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Competitor_t2775849210 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR float Vector3_Distance_m886789632 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AI.NavMeshAgent>()
inline NavMeshAgent_t1276799816 * Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  NavMeshAgent_t1276799816 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.AI.NavMeshAgent::set_destination(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void NavMeshAgent_set_destination_m41262300 (NavMeshAgent_t1276799816 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Single Competitor::GetHealthWeighting()
extern "C" IL2CPP_METHOD_ATTR float Competitor_GetHealthWeighting_m1154415733 (Competitor_t2775849210 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
inline ParticleSystem_t1800779281 * Component_GetComponent_TisParticleSystem_t1800779281_m3884485303 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  ParticleSystem_t1800779281 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C" IL2CPP_METHOD_ATTR void ParticleSystem_Play_m882713458 (ParticleSystem_t1800779281 * __this, const RuntimeMethod* method);
// System.Void Competitor::Kill(DeathTracker/DeathType)
extern "C" IL2CPP_METHOD_ATTR void Competitor_Kill_m580404221 (Competitor_t2775849210 * __this, int32_t ___death0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Competitor>::GetEnumerator()
inline Enumerator_t1842200533  List_1_GetEnumerator_m2888270272 (List_1_t4247923952 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t1842200533  (*) (List_1_t4247923952 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Competitor>::get_Current()
inline Competitor_t2775849210 * Enumerator_get_Current_m3267056561 (Enumerator_t1842200533 * __this, const RuntimeMethod* method)
{
	return ((  Competitor_t2775849210 * (*) (Enumerator_t1842200533 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Competitor>::MoveNext()
inline bool Enumerator_MoveNext_m860927647 (Enumerator_t1842200533 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t1842200533 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Competitor>::Dispose()
inline void Enumerator_Dispose_m446944907 (Enumerator_t1842200533 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t1842200533 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// Competitor UtilityAI.FIND_FLAG::GetClosestCompetitorToFlag()
extern "C" IL2CPP_METHOD_ATTR Competitor_t2775849210 * FIND_FLAG_GetClosestCompetitorToFlag_m2476932474 (FIND_FLAG_t1938094438 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.GameObject UtilityAI.FIND_FOOD::GetNearestFood()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * FIND_FOOD_GetNearestFood_m2802532437 (FIND_FOOD_t1534940982 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
inline Enumerator_t179987942  List_1_GetEnumerator_m1750140655 (List_1_t2585711361 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t179987942  (*) (List_1_t2585711361 *, const RuntimeMethod*))List_1_GetEnumerator_m2930774921_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
inline GameObject_t1113636619 * Enumerator_get_Current_m4179928398 (Enumerator_t179987942 * __this, const RuntimeMethod* method)
{
	return ((  GameObject_t1113636619 * (*) (Enumerator_t179987942 *, const RuntimeMethod*))Enumerator_get_Current_m470245444_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
inline bool Enumerator_MoveNext_m4286844348 (Enumerator_t179987942 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t179987942 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
inline void Enumerator_Dispose_m1341201278 (Enumerator_t179987942 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t179987942 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// UnityEngine.GameObject UtilityAI.FIND_WATER::GetNearestWater()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * FIND_WATER_GetNearestWater_m563822326 (FIND_WATER_t1027203745 * __this, const RuntimeMethod* method);
// System.Void PickupableItem::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PickupableItem__ctor_m2278186837 (PickupableItem_t2078450402 * __this, const RuntimeMethod* method);
// System.Void ItemManager::SpawnRandomWater()
extern "C" IL2CPP_METHOD_ATTR void ItemManager_SpawnRandomWater_m3398524623 (ItemManager_t3254073967 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Remove(!0)
inline bool List_1_Remove_m4063777476 (List_1_t2585711361 * __this, GameObject_t1113636619 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t2585711361 *, GameObject_t1113636619 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AI.NavMeshModifier::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifier__ctor_m2009292233 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifier__ctor_m2009292233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t385246372* L_0 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1);
		Int32U5BU5D_t385246372* L_1 = L_0;
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)(-1));
		List_1_t128053199 * L_2 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m3641213752(L_2, (RuntimeObject*)(RuntimeObject*)L_1, /*hidden argument*/List_1__ctor_m3641213752_RuntimeMethod_var);
		__this->set_m_AffectedAgents_7(L_2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshModifier::get_overrideArea()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifier_get_overrideArea_m3734533896 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_OverrideArea_4();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifier::set_overrideArea(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifier_set_overrideArea_m2007856712 (NavMeshModifier_t1010705800 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_OverrideArea_4(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AI.NavMeshModifier::get_area()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshModifier_get_area_m2942160106 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_Area_5();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifier::set_area(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifier_set_area_m2792417636 (NavMeshModifier_t1010705800 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_Area_5(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshModifier::get_ignoreFromBuild()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifier_get_ignoreFromBuild_m4183908247 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_IgnoreFromBuild_6();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifier::set_ignoreFromBuild(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifier_set_ignoreFromBuild_m1614588292 (NavMeshModifier_t1010705800 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_IgnoreFromBuild_6(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier> UnityEngine.AI.NavMeshModifier::get_activeModifiers()
extern "C" IL2CPP_METHOD_ATTR List_1_t2482780542 * NavMeshModifier_get_activeModifiers_m407144863 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifier_get_activeModifiers_m407144863_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var);
		List_1_t2482780542 * L_0 = ((NavMeshModifier_t1010705800_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifier::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifier_OnEnable_m1031614580 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifier_OnEnable_m1031614580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var);
		List_1_t2482780542 * L_0 = ((NavMeshModifier_t1010705800_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		bool L_1 = List_1_Contains_m4011681093(L_0, __this, /*hidden argument*/List_1_Contains_m4011681093_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var);
		List_1_t2482780542 * L_2 = ((NavMeshModifier_t1010705800_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		List_1_Add_m3904861453(L_2, __this, /*hidden argument*/List_1_Add_m3904861453_RuntimeMethod_var);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshModifier::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifier_OnDisable_m3259703319 (NavMeshModifier_t1010705800 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifier_OnDisable_m3259703319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var);
		List_1_t2482780542 * L_0 = ((NavMeshModifier_t1010705800_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		List_1_Remove_m4119032061(L_0, __this, /*hidden argument*/List_1_Remove_m4119032061_RuntimeMethod_var);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshModifier::AffectsAgentType(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifier_AffectsAgentType_m2140675392 (NavMeshModifier_t1010705800 * __this, int32_t ___agentTypeID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifier_AffectsAgentType_m2140675392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t128053199 * L_0 = __this->get_m_AffectedAgents_7();
		int32_t L_1 = List_1_get_Count_m361000296(L_0, /*hidden argument*/List_1_get_Count_m361000296_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		List_1_t128053199 * L_2 = __this->get_m_AffectedAgents_7();
		int32_t L_3 = List_1_get_Item_m888956288(L_2, 0, /*hidden argument*/List_1_get_Item_m888956288_RuntimeMethod_var);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0026;
		}
	}
	{
		return (bool)1;
	}

IL_0026:
	{
		List_1_t128053199 * L_4 = __this->get_m_AffectedAgents_7();
		int32_t L_5 = ___agentTypeID0;
		int32_t L_6 = List_1_IndexOf_m2639980317(L_4, L_5, /*hidden argument*/List_1_IndexOf_m2639980317_RuntimeMethod_var);
		return (bool)((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.AI.NavMeshModifier::.cctor()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifier__cctor_m3540064630 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifier__cctor_m3540064630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2482780542 * L_0 = (List_1_t2482780542 *)il2cpp_codegen_object_new(List_1_t2482780542_il2cpp_TypeInfo_var);
		List_1__ctor_m1475854777(L_0, /*hidden argument*/List_1__ctor_m1475854777_RuntimeMethod_var);
		((NavMeshModifier_t1010705800_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var))->set_s_NavMeshModifiers_8(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AI.NavMeshModifierVolume::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifierVolume__ctor_m2000838765 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifierVolume__ctor_m2000838765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (4.0f), (3.0f), (4.0f), /*hidden argument*/NULL);
		__this->set_m_Size_4(L_0);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_m_Center_5(L_1);
		Int32U5BU5D_t385246372* L_2 = (Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)1);
		Int32U5BU5D_t385246372* L_3 = L_2;
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)(-1));
		List_1_t128053199 * L_4 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m3641213752(L_4, (RuntimeObject*)(RuntimeObject*)L_3, /*hidden argument*/List_1__ctor_m3641213752_RuntimeMethod_var);
		__this->set_m_AffectedAgents_7(L_4);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshModifierVolume_get_size_m1842721009 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = __this->get_m_Size_4();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifierVolume::set_size(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifierVolume_set_size_m605336620 (NavMeshModifierVolume_t3293887184 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Size_4(L_0);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshModifierVolume_get_center_m2830775605 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = __this->get_m_Center_5();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifierVolume::set_center(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifierVolume_set_center_m428300834 (NavMeshModifierVolume_t3293887184 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Center_5(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AI.NavMeshModifierVolume::get_area()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshModifierVolume_get_area_m3186387738 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_Area_6();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifierVolume::set_area(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifierVolume_set_area_m2943862049 (NavMeshModifierVolume_t3293887184 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_Area_6(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume> UnityEngine.AI.NavMeshModifierVolume::get_activeModifiers()
extern "C" IL2CPP_METHOD_ATTR List_1_t470994630 * NavMeshModifierVolume_get_activeModifiers_m244070183 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifierVolume_get_activeModifiers_m244070183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var);
		List_1_t470994630 * L_0 = ((NavMeshModifierVolume_t3293887184_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshModifierVolume::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifierVolume_OnEnable_m1858538895 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifierVolume_OnEnable_m1858538895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var);
		List_1_t470994630 * L_0 = ((NavMeshModifierVolume_t3293887184_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		bool L_1 = List_1_Contains_m4164730806(L_0, __this, /*hidden argument*/List_1_Contains_m4164730806_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var);
		List_1_t470994630 * L_2 = ((NavMeshModifierVolume_t3293887184_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		List_1_Add_m2821620141(L_2, __this, /*hidden argument*/List_1_Add_m2821620141_RuntimeMethod_var);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshModifierVolume::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifierVolume_OnDisable_m253148840 (NavMeshModifierVolume_t3293887184 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifierVolume_OnDisable_m253148840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var);
		List_1_t470994630 * L_0 = ((NavMeshModifierVolume_t3293887184_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var))->get_s_NavMeshModifiers_8();
		List_1_Remove_m2403847378(L_0, __this, /*hidden argument*/List_1_Remove_m2403847378_RuntimeMethod_var);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshModifierVolume::AffectsAgentType(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshModifierVolume_AffectsAgentType_m2327990194 (NavMeshModifierVolume_t3293887184 * __this, int32_t ___agentTypeID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifierVolume_AffectsAgentType_m2327990194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t128053199 * L_0 = __this->get_m_AffectedAgents_7();
		int32_t L_1 = List_1_get_Count_m361000296(L_0, /*hidden argument*/List_1_get_Count_m361000296_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return (bool)0;
	}

IL_0012:
	{
		List_1_t128053199 * L_2 = __this->get_m_AffectedAgents_7();
		int32_t L_3 = List_1_get_Item_m888956288(L_2, 0, /*hidden argument*/List_1_get_Item_m888956288_RuntimeMethod_var);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0026;
		}
	}
	{
		return (bool)1;
	}

IL_0026:
	{
		List_1_t128053199 * L_4 = __this->get_m_AffectedAgents_7();
		int32_t L_5 = ___agentTypeID0;
		int32_t L_6 = List_1_IndexOf_m2639980317(L_4, L_5, /*hidden argument*/List_1_IndexOf_m2639980317_RuntimeMethod_var);
		return (bool)((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UnityEngine.AI.NavMeshModifierVolume::.cctor()
extern "C" IL2CPP_METHOD_ATTR void NavMeshModifierVolume__cctor_m83471128 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshModifierVolume__cctor_m83471128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t470994630 * L_0 = (List_1_t470994630 *)il2cpp_codegen_object_new(List_1_t470994630_il2cpp_TypeInfo_var);
		List_1__ctor_m3566324597(L_0, /*hidden argument*/List_1__ctor_m3566324597_RuntimeMethod_var);
		((NavMeshModifierVolume_t3293887184_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var))->set_s_NavMeshModifiers_8(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AI.NavMeshSurface::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface__ctor_m56041203 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface__ctor_m56041203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m3353183577((&L_0), (10.0f), (10.0f), (10.0f), /*hidden argument*/NULL);
		__this->set_m_Size_6(L_0);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (0.0f), (2.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_m_Center_7(L_1);
		LayerMask_t3493934918  L_2 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		__this->set_m_LayerMask_8(L_2);
		__this->set_m_IgnoreNavMeshAgent_11((bool)1);
		__this->set_m_IgnoreNavMeshObstacle_12((bool)1);
		__this->set_m_TileSize_14(((int32_t)256));
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_LastPosition_20(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_4 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_LastRotation_21(L_4);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.AI.NavMeshSurface::get_agentTypeID()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshSurface_get_agentTypeID_m334719145 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_AgentTypeID_4();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_agentTypeID(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_agentTypeID_m2898200609 (NavMeshSurface_t986947041 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_AgentTypeID_4(L_0);
		return;
	}
}
// UnityEngine.AI.CollectObjects UnityEngine.AI.NavMeshSurface::get_collectObjects()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshSurface_get_collectObjects_m2525611790 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_CollectObjects_5();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_collectObjects(UnityEngine.AI.CollectObjects)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_collectObjects_m652815532 (NavMeshSurface_t986947041 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_CollectObjects_5(L_0);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::get_size()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshSurface_get_size_m2950570184 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = __this->get_m_Size_6();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_size(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_size_m627911781 (NavMeshSurface_t986947041 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Size_6(L_0);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::get_center()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshSurface_get_center_m395084771 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = __this->get_m_Center_7();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_center(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_center_m1767100708 (NavMeshSurface_t986947041 * __this, Vector3_t3722313464  ___value0, const RuntimeMethod* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Center_7(L_0);
		return;
	}
}
// UnityEngine.LayerMask UnityEngine.AI.NavMeshSurface::get_layerMask()
extern "C" IL2CPP_METHOD_ATTR LayerMask_t3493934918  NavMeshSurface_get_layerMask_m427892683 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		LayerMask_t3493934918  L_0 = __this->get_m_LayerMask_8();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_layerMask(UnityEngine.LayerMask)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_layerMask_m936354944 (NavMeshSurface_t986947041 * __this, LayerMask_t3493934918  ___value0, const RuntimeMethod* method)
{
	{
		LayerMask_t3493934918  L_0 = ___value0;
		__this->set_m_LayerMask_8(L_0);
		return;
	}
}
// UnityEngine.AI.NavMeshCollectGeometry UnityEngine.AI.NavMeshSurface::get_useGeometry()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshSurface_get_useGeometry_m382240807 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_UseGeometry_9();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_useGeometry(UnityEngine.AI.NavMeshCollectGeometry)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_useGeometry_m367823641 (NavMeshSurface_t986947041 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_UseGeometry_9(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AI.NavMeshSurface::get_defaultArea()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshSurface_get_defaultArea_m174027869 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_DefaultArea_10();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_defaultArea(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_defaultArea_m1171146056 (NavMeshSurface_t986947041 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_DefaultArea_10(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::get_ignoreNavMeshAgent()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_get_ignoreNavMeshAgent_m1284319772 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_IgnoreNavMeshAgent_11();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_ignoreNavMeshAgent(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_ignoreNavMeshAgent_m2544331620 (NavMeshSurface_t986947041 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_IgnoreNavMeshAgent_11(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::get_ignoreNavMeshObstacle()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_get_ignoreNavMeshObstacle_m2691260088 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_IgnoreNavMeshObstacle_12();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_ignoreNavMeshObstacle(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_ignoreNavMeshObstacle_m2316853168 (NavMeshSurface_t986947041 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_IgnoreNavMeshObstacle_12(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::get_overrideTileSize()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_get_overrideTileSize_m3617479521 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_OverrideTileSize_13();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_overrideTileSize(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_overrideTileSize_m1537922757 (NavMeshSurface_t986947041 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_OverrideTileSize_13(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AI.NavMeshSurface::get_tileSize()
extern "C" IL2CPP_METHOD_ATTR int32_t NavMeshSurface_get_tileSize_m1315768493 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_TileSize_14();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_tileSize(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_tileSize_m2247422290 (NavMeshSurface_t986947041 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_TileSize_14(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::get_overrideVoxelSize()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_get_overrideVoxelSize_m3411287898 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_OverrideVoxelSize_15();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_overrideVoxelSize(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_overrideVoxelSize_m1751941699 (NavMeshSurface_t986947041 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_OverrideVoxelSize_15(L_0);
		return;
	}
}
// System.Single UnityEngine.AI.NavMeshSurface::get_voxelSize()
extern "C" IL2CPP_METHOD_ATTR float NavMeshSurface_get_voxelSize_m1506958526 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_m_VoxelSize_16();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_voxelSize(System.Single)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_voxelSize_m577301528 (NavMeshSurface_t986947041 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_VoxelSize_16(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::get_buildHeightMesh()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_get_buildHeightMesh_m917342529 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_BuildHeightMesh_17();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_buildHeightMesh(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_buildHeightMesh_m764567046 (NavMeshSurface_t986947041 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_BuildHeightMesh_17(L_0);
		return;
	}
}
// UnityEngine.AI.NavMeshData UnityEngine.AI.NavMeshSurface::get_navMeshData()
extern "C" IL2CPP_METHOD_ATTR NavMeshData_t1084598030 * NavMeshSurface_get_navMeshData_m477219221 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		NavMeshData_t1084598030 * L_0 = __this->get_m_NavMeshData_18();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::set_navMeshData(UnityEngine.AI.NavMeshData)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_set_navMeshData_m3436999127 (NavMeshSurface_t986947041 * __this, NavMeshData_t1084598030 * ___value0, const RuntimeMethod* method)
{
	{
		NavMeshData_t1084598030 * L_0 = ___value0;
		__this->set_m_NavMeshData_18(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface> UnityEngine.AI.NavMeshSurface::get_activeSurfaces()
extern "C" IL2CPP_METHOD_ATTR List_1_t2459021783 * NavMeshSurface_get_activeSurfaces_m457581706 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_get_activeSurfaces_m457581706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		List_1_t2459021783 * L_0 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		return L_0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_OnEnable_m3051587968 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_OnEnable_m3051587968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		NavMeshSurface_Register_m2505341092(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		NavMeshSurface_AddData_m3992805541(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_OnDisable_m555302565 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_OnDisable_m555302565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NavMeshSurface_RemoveData_m3790252040(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		NavMeshSurface_Unregister_m2798101876(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::AddData()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_AddData_m3992805541 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_AddData_m3992805541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NavMeshDataInstance_t1498462893 * L_0 = __this->get_address_of_m_NavMeshDataInstance_19();
		bool L_1 = NavMeshDataInstance_get_valid_m2577459468((NavMeshDataInstance_t1498462893 *)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		NavMeshData_t1084598030 * L_2 = __this->get_m_NavMeshData_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		NavMeshData_t1084598030 * L_4 = __this->get_m_NavMeshData_18();
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_8 = Transform_get_rotation_m3502953881(L_7, /*hidden argument*/NULL);
		NavMeshDataInstance_t1498462893  L_9 = NavMesh_AddNavMeshData_m1391221680(NULL /*static, unused*/, L_4, L_6, L_8, /*hidden argument*/NULL);
		__this->set_m_NavMeshDataInstance_19(L_9);
		NavMeshDataInstance_t1498462893 * L_10 = __this->get_address_of_m_NavMeshDataInstance_19();
		NavMeshDataInstance_set_owner_m70396501((NavMeshDataInstance_t1498462893 *)L_10, __this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		__this->set_m_LastPosition_20(L_12);
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_14 = Transform_get_rotation_m3502953881(L_13, /*hidden argument*/NULL);
		__this->set_m_LastRotation_21(L_14);
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::RemoveData()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_RemoveData_m3790252040 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	NavMeshDataInstance_t1498462893  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NavMeshDataInstance_t1498462893 * L_0 = __this->get_address_of_m_NavMeshDataInstance_19();
		NavMeshDataInstance_Remove_m996936756((NavMeshDataInstance_t1498462893 *)L_0, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_0), sizeof(NavMeshDataInstance_t1498462893 ));
		NavMeshDataInstance_t1498462893  L_1 = V_0;
		__this->set_m_NavMeshDataInstance_19(L_1);
		return;
	}
}
// UnityEngine.AI.NavMeshBuildSettings UnityEngine.AI.NavMeshSurface::GetBuildSettings()
extern "C" IL2CPP_METHOD_ATTR NavMeshBuildSettings_t1985378544  NavMeshSurface_GetBuildSettings_m1758800876 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_GetBuildSettings_m1758800876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NavMeshBuildSettings_t1985378544  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = __this->get_m_AgentTypeID_4();
		NavMeshBuildSettings_t1985378544  L_1 = NavMesh_GetSettingsByID_m546770701(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = NavMeshBuildSettings_get_agentTypeID_m3314078478((NavMeshBuildSettings_t1985378544 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_3 = NavMeshSurface_get_agentTypeID_m334719145(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2062271667, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m831581295(NULL /*static, unused*/, L_6, __this, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_m_AgentTypeID_4();
		NavMeshBuildSettings_set_agentTypeID_m3995891764((NavMeshBuildSettings_t1985378544 *)(&V_0), L_7, /*hidden argument*/NULL);
	}

IL_0041:
	{
		bool L_8 = NavMeshSurface_get_overrideTileSize_m3617479521(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0061;
		}
	}
	{
		NavMeshBuildSettings_set_overrideTileSize_m1668241129((NavMeshBuildSettings_t1985378544 *)(&V_0), (bool)1, /*hidden argument*/NULL);
		int32_t L_9 = NavMeshSurface_get_tileSize_m1315768493(__this, /*hidden argument*/NULL);
		NavMeshBuildSettings_set_tileSize_m1243065940((NavMeshBuildSettings_t1985378544 *)(&V_0), L_9, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_10 = NavMeshSurface_get_overrideVoxelSize_m3411287898(__this, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		NavMeshBuildSettings_set_overrideVoxelSize_m1423447633((NavMeshBuildSettings_t1985378544 *)(&V_0), (bool)1, /*hidden argument*/NULL);
		float L_11 = NavMeshSurface_get_voxelSize_m1506958526(__this, /*hidden argument*/NULL);
		NavMeshBuildSettings_set_voxelSize_m2794829786((NavMeshBuildSettings_t1985378544 *)(&V_0), L_11, /*hidden argument*/NULL);
	}

IL_0081:
	{
		NavMeshBuildSettings_t1985378544  L_12 = V_0;
		return L_12;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::BuildNavMesh()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_BuildNavMesh_m3042234026 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_BuildNavMesh_m3042234026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2160260967 * V_0 = NULL;
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NavMeshData_t1084598030 * V_2 = NULL;
	{
		List_1_t2160260967 * L_0 = NavMeshSurface_CollectSources_m1904116616(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t3722313464  L_1 = __this->get_m_Center_7();
		Vector3_t3722313464  L_2 = __this->get_m_Size_6();
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = NavMeshSurface_Abs_m641648730(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Bounds__ctor_m1937678907((Bounds_t2266837910 *)(&V_1), L_1, L_3, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_CollectObjects_5();
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = __this->get_m_CollectObjects_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}

IL_0036:
	{
		List_1_t2160260967 * L_6 = V_0;
		Bounds_t2266837910  L_7 = NavMeshSurface_CalculateWorldBounds_m4178605729(__this, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_003e:
	{
		NavMeshBuildSettings_t1985378544  L_8 = NavMeshSurface_GetBuildSettings_m1758800876(__this, /*hidden argument*/NULL);
		List_1_t2160260967 * L_9 = V_0;
		Bounds_t2266837910  L_10 = V_1;
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_14 = Transform_get_rotation_m3502953881(L_13, /*hidden argument*/NULL);
		NavMeshData_t1084598030 * L_15 = NavMeshBuilder_BuildNavMeshData_m2250664010(NULL /*static, unused*/, L_8, L_9, L_10, L_12, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		NavMeshData_t1084598030 * L_16 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_16, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009d;
		}
	}
	{
		NavMeshData_t1084598030 * L_18 = V_2;
		GameObject_t1113636619 * L_19 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		String_t* L_20 = Object_get_name_m4211327027(L_19, /*hidden argument*/NULL);
		Object_set_name_m291480324(L_18, L_20, /*hidden argument*/NULL);
		NavMeshSurface_RemoveData_m3790252040(__this, /*hidden argument*/NULL);
		NavMeshData_t1084598030 * L_21 = V_2;
		__this->set_m_NavMeshData_18(L_21);
		bool L_22 = Behaviour_get_isActiveAndEnabled_m3143666263(__this, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009d;
		}
	}
	{
		NavMeshSurface_AddData_m3992805541(__this, /*hidden argument*/NULL);
	}

IL_009d:
	{
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.AI.NavMeshSurface::UpdateNavMesh(UnityEngine.AI.NavMeshData)
extern "C" IL2CPP_METHOD_ATTR AsyncOperation_t1445031843 * NavMeshSurface_UpdateNavMesh_m3127138467 (NavMeshSurface_t986947041 * __this, NavMeshData_t1084598030 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_UpdateNavMesh_m3127138467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2160260967 * V_0 = NULL;
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		List_1_t2160260967 * L_0 = NavMeshSurface_CollectSources_m1904116616(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t3722313464  L_1 = __this->get_m_Center_7();
		Vector3_t3722313464  L_2 = __this->get_m_Size_6();
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = NavMeshSurface_Abs_m641648730(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Bounds__ctor_m1937678907((Bounds_t2266837910 *)(&V_1), L_1, L_3, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_m_CollectObjects_5();
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = __this->get_m_CollectObjects_5();
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_003e;
		}
	}

IL_0036:
	{
		List_1_t2160260967 * L_6 = V_0;
		Bounds_t2266837910  L_7 = NavMeshSurface_CalculateWorldBounds_m4178605729(__this, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_003e:
	{
		NavMeshData_t1084598030 * L_8 = ___data0;
		NavMeshBuildSettings_t1985378544  L_9 = NavMeshSurface_GetBuildSettings_m1758800876(__this, /*hidden argument*/NULL);
		List_1_t2160260967 * L_10 = V_0;
		Bounds_t2266837910  L_11 = V_1;
		AsyncOperation_t1445031843 * L_12 = NavMeshBuilder_UpdateNavMeshDataAsync_m159354334(NULL /*static, unused*/, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::Register(UnityEngine.AI.NavMeshSurface)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_Register_m2505341092 (RuntimeObject * __this /* static, unused */, NavMeshSurface_t986947041 * ___surface0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_Register_m2505341092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNavMeshPreUpdate_t1580782682 * G_B3_0 = NULL;
	OnNavMeshPreUpdate_t1580782682 * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		List_1_t2459021783 * L_0 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		int32_t L_1 = List_1_get_Count_m3200332207(L_0, /*hidden argument*/List_1_get_Count_m3200332207_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0040;
		}
	}
	{
		OnNavMeshPreUpdate_t1580782682 * L_2 = ((NavMesh_t1865600375_StaticFields*)il2cpp_codegen_static_fields_for(NavMesh_t1865600375_il2cpp_TypeInfo_var))->get_onPreUpdate_0();
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		OnNavMeshPreUpdate_t1580782682 * L_3 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_23();
		G_B2_0 = L_2;
		if (L_3)
		{
			G_B3_0 = L_2;
			goto IL_002c;
		}
	}
	{
		intptr_t L_4 = (intptr_t)NavMeshSurface_UpdateActive_m974852644_RuntimeMethod_var;
		OnNavMeshPreUpdate_t1580782682 * L_5 = (OnNavMeshPreUpdate_t1580782682 *)il2cpp_codegen_object_new(OnNavMeshPreUpdate_t1580782682_il2cpp_TypeInfo_var);
		OnNavMeshPreUpdate__ctor_m2758342548(L_5, NULL, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_23(L_5);
		G_B3_0 = G_B2_0;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		OnNavMeshPreUpdate_t1580782682 * L_6 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_23();
		Delegate_t1188392813 * L_7 = Delegate_Combine_m1859655160(NULL /*static, unused*/, G_B3_0, L_6, /*hidden argument*/NULL);
		((NavMesh_t1865600375_StaticFields*)il2cpp_codegen_static_fields_for(NavMesh_t1865600375_il2cpp_TypeInfo_var))->set_onPreUpdate_0(((OnNavMeshPreUpdate_t1580782682 *)CastclassSealed((RuntimeObject*)L_7, OnNavMeshPreUpdate_t1580782682_il2cpp_TypeInfo_var)));
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		List_1_t2459021783 * L_8 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		NavMeshSurface_t986947041 * L_9 = ___surface0;
		bool L_10 = List_1_Contains_m939277675(L_8, L_9, /*hidden argument*/List_1_Contains_m939277675_RuntimeMethod_var);
		if (L_10)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		List_1_t2459021783 * L_11 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		NavMeshSurface_t986947041 * L_12 = ___surface0;
		List_1_Add_m3920533274(L_11, L_12, /*hidden argument*/List_1_Add_m3920533274_RuntimeMethod_var);
	}

IL_005b:
	{
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::Unregister(UnityEngine.AI.NavMeshSurface)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_Unregister_m2798101876 (RuntimeObject * __this /* static, unused */, NavMeshSurface_t986947041 * ___surface0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_Unregister_m2798101876_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OnNavMeshPreUpdate_t1580782682 * G_B3_0 = NULL;
	OnNavMeshPreUpdate_t1580782682 * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		List_1_t2459021783 * L_0 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		NavMeshSurface_t986947041 * L_1 = ___surface0;
		List_1_Remove_m795306531(L_0, L_1, /*hidden argument*/List_1_Remove_m795306531_RuntimeMethod_var);
		List_1_t2459021783 * L_2 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		int32_t L_3 = List_1_get_Count_m3200332207(L_2, /*hidden argument*/List_1_get_Count_m3200332207_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_004c;
		}
	}
	{
		OnNavMeshPreUpdate_t1580782682 * L_4 = ((NavMesh_t1865600375_StaticFields*)il2cpp_codegen_static_fields_for(NavMesh_t1865600375_il2cpp_TypeInfo_var))->get_onPreUpdate_0();
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		OnNavMeshPreUpdate_t1580782682 * L_5 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_24();
		G_B2_0 = L_4;
		if (L_5)
		{
			G_B3_0 = L_4;
			goto IL_0038;
		}
	}
	{
		intptr_t L_6 = (intptr_t)NavMeshSurface_UpdateActive_m974852644_RuntimeMethod_var;
		OnNavMeshPreUpdate_t1580782682 * L_7 = (OnNavMeshPreUpdate_t1580782682 *)il2cpp_codegen_object_new(OnNavMeshPreUpdate_t1580782682_il2cpp_TypeInfo_var);
		OnNavMeshPreUpdate__ctor_m2758342548(L_7, NULL, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache1_24(L_7);
		G_B3_0 = G_B2_0;
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		OnNavMeshPreUpdate_t1580782682 * L_8 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache1_24();
		Delegate_t1188392813 * L_9 = Delegate_Remove_m334097152(NULL /*static, unused*/, G_B3_0, L_8, /*hidden argument*/NULL);
		((NavMesh_t1865600375_StaticFields*)il2cpp_codegen_static_fields_for(NavMesh_t1865600375_il2cpp_TypeInfo_var))->set_onPreUpdate_0(((OnNavMeshPreUpdate_t1580782682 *)CastclassSealed((RuntimeObject*)L_9, OnNavMeshPreUpdate_t1580782682_il2cpp_TypeInfo_var)));
	}

IL_004c:
	{
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::UpdateActive()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_UpdateActive_m974852644 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_UpdateActive_m974852644_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001b;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		List_1_t2459021783 * L_0 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		int32_t L_1 = V_0;
		NavMeshSurface_t986947041 * L_2 = List_1_get_Item_m1770120800(L_0, L_1, /*hidden argument*/List_1_get_Item_m1770120800_RuntimeMethod_var);
		NavMeshSurface_UpdateDataIfTransformChanged_m1853993666(L_2, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
	}

IL_001b:
	{
		int32_t L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		List_1_t2459021783 * L_5 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_s_NavMeshSurfaces_22();
		int32_t L_6 = List_1_get_Count_m3200332207(L_5, /*hidden argument*/List_1_get_Count_m3200332207_RuntimeMethod_var);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::AppendModifierVolumes(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>&)
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_AppendModifierVolumes_m4188255682 (NavMeshSurface_t986947041 * __this, List_1_t2160260967 ** ___sources0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_AppendModifierVolumes_m4188255682_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t470994630 * V_0 = NULL;
	NavMeshModifierVolume_t3293887184 * V_1 = NULL;
	Enumerator_t2360238507  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	NavMeshBuildSource_t688186225  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t470994630 * G_B3_0 = NULL;
	List_1_t470994630 * G_B2_0 = NULL;
	{
		int32_t L_0 = __this->get_m_CollectObjects_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0041;
		}
	}
	{
		NavMeshModifierVolumeU5BU5D_t3251859441* L_1 = Component_GetComponentsInChildren_TisNavMeshModifierVolume_t3293887184_m3885906544(__this, /*hidden argument*/Component_GetComponentsInChildren_TisNavMeshModifierVolume_t3293887184_m3885906544_RuntimeMethod_var);
		List_1_t470994630 * L_2 = (List_1_t470994630 *)il2cpp_codegen_object_new(List_1_t470994630_il2cpp_TypeInfo_var);
		List_1__ctor_m530909711(L_2, (RuntimeObject*)(RuntimeObject*)L_1, /*hidden argument*/List_1__ctor_m530909711_RuntimeMethod_var);
		V_0 = L_2;
		List_1_t470994630 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t4119181308 * L_4 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_25();
		G_B2_0 = L_3;
		if (L_4)
		{
			G_B3_0 = L_3;
			goto IL_0031;
		}
	}
	{
		intptr_t L_5 = (intptr_t)NavMeshSurface_U3CAppendModifierVolumesU3Em__0_m837462505_RuntimeMethod_var;
		Predicate_1_t4119181308 * L_6 = (Predicate_1_t4119181308 *)il2cpp_codegen_object_new(Predicate_1_t4119181308_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m3152402189(L_6, NULL, L_5, /*hidden argument*/Predicate_1__ctor_m3152402189_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_25(L_6);
		G_B3_0 = G_B2_0;
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t4119181308 * L_7 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_25();
		List_1_RemoveAll_m3207188928(G_B3_0, L_7, /*hidden argument*/List_1_RemoveAll_m3207188928_RuntimeMethod_var);
		goto IL_0047;
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifierVolume_t3293887184_il2cpp_TypeInfo_var);
		List_1_t470994630 * L_8 = NavMeshModifierVolume_get_activeModifiers_m244070183(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0047:
	{
		List_1_t470994630 * L_9 = V_0;
		Enumerator_t2360238507  L_10 = List_1_GetEnumerator_m741968409(L_9, /*hidden argument*/List_1_GetEnumerator_m741968409_RuntimeMethod_var);
		V_2 = L_10;
	}

IL_004e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_015d;
		}

IL_0053:
		{
			NavMeshModifierVolume_t3293887184 * L_11 = Enumerator_get_Current_m1310551773((Enumerator_t2360238507 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m1310551773_RuntimeMethod_var);
			V_1 = L_11;
			LayerMask_t3493934918  L_12 = __this->get_m_LayerMask_8();
			int32_t L_13 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
			NavMeshModifierVolume_t3293887184 * L_14 = V_1;
			GameObject_t1113636619 * L_15 = Component_get_gameObject_m442555142(L_14, /*hidden argument*/NULL);
			int32_t L_16 = GameObject_get_layer_m4158800245(L_15, /*hidden argument*/NULL);
			if (((int32_t)((int32_t)L_13&(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)31))))))))
			{
				goto IL_0081;
			}
		}

IL_007c:
		{
			goto IL_015d;
		}

IL_0081:
		{
			NavMeshModifierVolume_t3293887184 * L_17 = V_1;
			int32_t L_18 = __this->get_m_AgentTypeID_4();
			bool L_19 = NavMeshModifierVolume_AffectsAgentType_m2327990194(L_17, L_18, /*hidden argument*/NULL);
			if (L_19)
			{
				goto IL_0097;
			}
		}

IL_0092:
		{
			goto IL_015d;
		}

IL_0097:
		{
			NavMeshModifierVolume_t3293887184 * L_20 = V_1;
			Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(L_20, /*hidden argument*/NULL);
			NavMeshModifierVolume_t3293887184 * L_22 = V_1;
			Vector3_t3722313464  L_23 = NavMeshModifierVolume_get_center_m2830775605(L_22, /*hidden argument*/NULL);
			Vector3_t3722313464  L_24 = Transform_TransformPoint_m226827784(L_21, L_23, /*hidden argument*/NULL);
			V_3 = L_24;
			NavMeshModifierVolume_t3293887184 * L_25 = V_1;
			Transform_t3600365921 * L_26 = Component_get_transform_m3162698980(L_25, /*hidden argument*/NULL);
			Vector3_t3722313464  L_27 = Transform_get_lossyScale_m465496651(L_26, /*hidden argument*/NULL);
			V_4 = L_27;
			NavMeshModifierVolume_t3293887184 * L_28 = V_1;
			Vector3_t3722313464  L_29 = NavMeshModifierVolume_get_size_m1842721009(L_28, /*hidden argument*/NULL);
			V_6 = L_29;
			float L_30 = (&V_6)->get_x_2();
			float L_31 = (&V_4)->get_x_2();
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
			float L_32 = fabsf(L_31);
			NavMeshModifierVolume_t3293887184 * L_33 = V_1;
			Vector3_t3722313464  L_34 = NavMeshModifierVolume_get_size_m1842721009(L_33, /*hidden argument*/NULL);
			V_7 = L_34;
			float L_35 = (&V_7)->get_y_3();
			float L_36 = (&V_4)->get_y_3();
			float L_37 = fabsf(L_36);
			NavMeshModifierVolume_t3293887184 * L_38 = V_1;
			Vector3_t3722313464  L_39 = NavMeshModifierVolume_get_size_m1842721009(L_38, /*hidden argument*/NULL);
			V_8 = L_39;
			float L_40 = (&V_8)->get_z_4();
			float L_41 = (&V_4)->get_z_4();
			float L_42 = fabsf(L_41);
			Vector3__ctor_m3353183577((Vector3_t3722313464 *)(&V_5), ((float)il2cpp_codegen_multiply((float)L_30, (float)L_32)), ((float)il2cpp_codegen_multiply((float)L_35, (float)L_37)), ((float)il2cpp_codegen_multiply((float)L_40, (float)L_42)), /*hidden argument*/NULL);
			il2cpp_codegen_initobj((&V_9), sizeof(NavMeshBuildSource_t688186225 ));
			NavMeshBuildSource_set_shape_m2067493469((NavMeshBuildSource_t688186225 *)(&V_9), 5, /*hidden argument*/NULL);
			Vector3_t3722313464  L_43 = V_3;
			NavMeshModifierVolume_t3293887184 * L_44 = V_1;
			Transform_t3600365921 * L_45 = Component_get_transform_m3162698980(L_44, /*hidden argument*/NULL);
			Quaternion_t2301928331  L_46 = Transform_get_rotation_m3502953881(L_45, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			Vector3_t3722313464  L_47 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
			Matrix4x4_t1817901843  L_48 = Matrix4x4_TRS_m3801934620(NULL /*static, unused*/, L_43, L_46, L_47, /*hidden argument*/NULL);
			NavMeshBuildSource_set_transform_m1212452088((NavMeshBuildSource_t688186225 *)(&V_9), L_48, /*hidden argument*/NULL);
			Vector3_t3722313464  L_49 = V_5;
			NavMeshBuildSource_set_size_m682598383((NavMeshBuildSource_t688186225 *)(&V_9), L_49, /*hidden argument*/NULL);
			NavMeshModifierVolume_t3293887184 * L_50 = V_1;
			int32_t L_51 = NavMeshModifierVolume_get_area_m3186387738(L_50, /*hidden argument*/NULL);
			NavMeshBuildSource_set_area_m2888906222((NavMeshBuildSource_t688186225 *)(&V_9), L_51, /*hidden argument*/NULL);
			List_1_t2160260967 ** L_52 = ___sources0;
			List_1_t2160260967 * L_53 = *((List_1_t2160260967 **)L_52);
			NavMeshBuildSource_t688186225  L_54 = V_9;
			List_1_Add_m863973498(L_53, L_54, /*hidden argument*/List_1_Add_m863973498_RuntimeMethod_var);
		}

IL_015d:
		{
			bool L_55 = Enumerator_MoveNext_m3090114117((Enumerator_t2360238507 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m3090114117_RuntimeMethod_var);
			if (L_55)
			{
				goto IL_0053;
			}
		}

IL_0169:
		{
			IL2CPP_LEAVE(0x17C, FINALLY_016e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_016e;
	}

FINALLY_016e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m734651789((Enumerator_t2360238507 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m734651789_RuntimeMethod_var);
		IL2CPP_END_FINALLY(366)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(366)
	{
		IL2CPP_JUMP_TBL(0x17C, IL_017c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_017c:
	{
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface::CollectSources()
extern "C" IL2CPP_METHOD_ATTR List_1_t2160260967 * NavMeshSurface_CollectSources_m1904116616 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_CollectSources_m1904116616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2160260967 * V_0 = NULL;
	List_1_t2785658607 * V_1 = NULL;
	List_1_t2482780542 * V_2 = NULL;
	NavMeshModifier_t1010705800 * V_3 = NULL;
	Enumerator_t77057123  V_4;
	memset(&V_4, 0, sizeof(V_4));
	NavMeshBuildMarkup_t1313583865  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Matrix4x4_t1817901843  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Bounds_t2266837910  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	List_1_t2482780542 * G_B3_0 = NULL;
	List_1_t2482780542 * G_B2_0 = NULL;
	List_1_t2160260967 * G_B24_0 = NULL;
	List_1_t2160260967 * G_B23_0 = NULL;
	List_1_t2160260967 * G_B28_0 = NULL;
	List_1_t2160260967 * G_B27_0 = NULL;
	{
		List_1_t2160260967 * L_0 = (List_1_t2160260967 *)il2cpp_codegen_object_new(List_1_t2160260967_il2cpp_TypeInfo_var);
		List_1__ctor_m3711269370(L_0, /*hidden argument*/List_1__ctor_m3711269370_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t2785658607 * L_1 = (List_1_t2785658607 *)il2cpp_codegen_object_new(List_1_t2785658607_il2cpp_TypeInfo_var);
		List_1__ctor_m1940349360(L_1, /*hidden argument*/List_1__ctor_m1940349360_RuntimeMethod_var);
		V_1 = L_1;
		int32_t L_2 = __this->get_m_CollectObjects_5();
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_004d;
		}
	}
	{
		NavMeshModifierU5BU5D_t1673145177* L_3 = Component_GetComponentsInChildren_TisNavMeshModifier_t1010705800_m3813049907(__this, /*hidden argument*/Component_GetComponentsInChildren_TisNavMeshModifier_t1010705800_m3813049907_RuntimeMethod_var);
		List_1_t2482780542 * L_4 = (List_1_t2482780542 *)il2cpp_codegen_object_new(List_1_t2482780542_il2cpp_TypeInfo_var);
		List_1__ctor_m1410039760(L_4, (RuntimeObject*)(RuntimeObject*)L_3, /*hidden argument*/List_1__ctor_m1410039760_RuntimeMethod_var);
		V_2 = L_4;
		List_1_t2482780542 * L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t1835999924 * L_6 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_26();
		G_B2_0 = L_5;
		if (L_6)
		{
			G_B3_0 = L_5;
			goto IL_003d;
		}
	}
	{
		intptr_t L_7 = (intptr_t)NavMeshSurface_U3CCollectSourcesU3Em__1_m4201889746_RuntimeMethod_var;
		Predicate_1_t1835999924 * L_8 = (Predicate_1_t1835999924 *)il2cpp_codegen_object_new(Predicate_1_t1835999924_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m2280749923(L_8, NULL, L_7, /*hidden argument*/Predicate_1__ctor_m2280749923_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache1_26(L_8);
		G_B3_0 = G_B2_0;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t1835999924 * L_9 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_26();
		List_1_RemoveAll_m616612175(G_B3_0, L_9, /*hidden argument*/List_1_RemoveAll_m616612175_RuntimeMethod_var);
		goto IL_0053;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshModifier_t1010705800_il2cpp_TypeInfo_var);
		List_1_t2482780542 * L_10 = NavMeshModifier_get_activeModifiers_m407144863(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_10;
	}

IL_0053:
	{
		List_1_t2482780542 * L_11 = V_2;
		Enumerator_t77057123  L_12 = List_1_GetEnumerator_m562693788(L_11, /*hidden argument*/List_1_GetEnumerator_m562693788_RuntimeMethod_var);
		V_4 = L_12;
	}

IL_005b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e8;
		}

IL_0060:
		{
			NavMeshModifier_t1010705800 * L_13 = Enumerator_get_Current_m4223334170((Enumerator_t77057123 *)(&V_4), /*hidden argument*/Enumerator_get_Current_m4223334170_RuntimeMethod_var);
			V_3 = L_13;
			LayerMask_t3493934918  L_14 = __this->get_m_LayerMask_8();
			int32_t L_15 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			NavMeshModifier_t1010705800 * L_16 = V_3;
			GameObject_t1113636619 * L_17 = Component_get_gameObject_m442555142(L_16, /*hidden argument*/NULL);
			int32_t L_18 = GameObject_get_layer_m4158800245(L_17, /*hidden argument*/NULL);
			if (((int32_t)((int32_t)L_15&(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_18&(int32_t)((int32_t)31))))))))
			{
				goto IL_008e;
			}
		}

IL_0089:
		{
			goto IL_00e8;
		}

IL_008e:
		{
			NavMeshModifier_t1010705800 * L_19 = V_3;
			int32_t L_20 = __this->get_m_AgentTypeID_4();
			bool L_21 = NavMeshModifier_AffectsAgentType_m2140675392(L_19, L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_00a4;
			}
		}

IL_009f:
		{
			goto IL_00e8;
		}

IL_00a4:
		{
			il2cpp_codegen_initobj((&V_5), sizeof(NavMeshBuildMarkup_t1313583865 ));
			NavMeshModifier_t1010705800 * L_22 = V_3;
			Transform_t3600365921 * L_23 = Component_get_transform_m3162698980(L_22, /*hidden argument*/NULL);
			NavMeshBuildMarkup_set_root_m518958946((NavMeshBuildMarkup_t1313583865 *)(&V_5), L_23, /*hidden argument*/NULL);
			NavMeshModifier_t1010705800 * L_24 = V_3;
			bool L_25 = NavMeshModifier_get_overrideArea_m3734533896(L_24, /*hidden argument*/NULL);
			NavMeshBuildMarkup_set_overrideArea_m229372463((NavMeshBuildMarkup_t1313583865 *)(&V_5), L_25, /*hidden argument*/NULL);
			NavMeshModifier_t1010705800 * L_26 = V_3;
			int32_t L_27 = NavMeshModifier_get_area_m2942160106(L_26, /*hidden argument*/NULL);
			NavMeshBuildMarkup_set_area_m1361147558((NavMeshBuildMarkup_t1313583865 *)(&V_5), L_27, /*hidden argument*/NULL);
			NavMeshModifier_t1010705800 * L_28 = V_3;
			bool L_29 = NavMeshModifier_get_ignoreFromBuild_m4183908247(L_28, /*hidden argument*/NULL);
			NavMeshBuildMarkup_set_ignoreFromBuild_m709640216((NavMeshBuildMarkup_t1313583865 *)(&V_5), L_29, /*hidden argument*/NULL);
			List_1_t2785658607 * L_30 = V_1;
			NavMeshBuildMarkup_t1313583865  L_31 = V_5;
			List_1_Add_m2814401212(L_30, L_31, /*hidden argument*/List_1_Add_m2814401212_RuntimeMethod_var);
		}

IL_00e8:
		{
			bool L_32 = Enumerator_MoveNext_m936905458((Enumerator_t77057123 *)(&V_4), /*hidden argument*/Enumerator_MoveNext_m936905458_RuntimeMethod_var);
			if (L_32)
			{
				goto IL_0060;
			}
		}

IL_00f4:
		{
			IL2CPP_LEAVE(0x107, FINALLY_00f9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00f9;
	}

FINALLY_00f9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m465728948((Enumerator_t77057123 *)(&V_4), /*hidden argument*/Enumerator_Dispose_m465728948_RuntimeMethod_var);
		IL2CPP_END_FINALLY(249)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(249)
	{
		IL2CPP_JUMP_TBL(0x107, IL_0107)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0107:
	{
		int32_t L_33 = __this->get_m_CollectObjects_5();
		if (L_33)
		{
			goto IL_0136;
		}
	}
	{
		LayerMask_t3493934918  L_34 = __this->get_m_LayerMask_8();
		int32_t L_35 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		int32_t L_36 = __this->get_m_UseGeometry_9();
		int32_t L_37 = __this->get_m_DefaultArea_10();
		List_1_t2785658607 * L_38 = V_1;
		List_1_t2160260967 * L_39 = V_0;
		NavMeshBuilder_CollectSources_m1517047385(NULL /*static, unused*/, (Transform_t3600365921 *)NULL, L_35, L_36, L_37, L_38, L_39, /*hidden argument*/NULL);
		goto IL_01d3;
	}

IL_0136:
	{
		int32_t L_40 = __this->get_m_CollectObjects_5();
		if ((!(((uint32_t)L_40) == ((uint32_t)2))))
		{
			goto IL_016b;
		}
	}
	{
		Transform_t3600365921 * L_41 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		LayerMask_t3493934918  L_42 = __this->get_m_LayerMask_8();
		int32_t L_43 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		int32_t L_44 = __this->get_m_UseGeometry_9();
		int32_t L_45 = __this->get_m_DefaultArea_10();
		List_1_t2785658607 * L_46 = V_1;
		List_1_t2160260967 * L_47 = V_0;
		NavMeshBuilder_CollectSources_m1517047385(NULL /*static, unused*/, L_41, L_43, L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
		goto IL_01d3;
	}

IL_016b:
	{
		int32_t L_48 = __this->get_m_CollectObjects_5();
		if ((!(((uint32_t)L_48) == ((uint32_t)1))))
		{
			goto IL_01d3;
		}
	}
	{
		Transform_t3600365921 * L_49 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_50 = Transform_get_position_m36019626(L_49, /*hidden argument*/NULL);
		Transform_t3600365921 * L_51 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_52 = Transform_get_rotation_m3502953881(L_51, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_53 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_54 = Matrix4x4_TRS_m3801934620(NULL /*static, unused*/, L_50, L_52, L_53, /*hidden argument*/NULL);
		V_6 = L_54;
		Matrix4x4_t1817901843  L_55 = V_6;
		Vector3_t3722313464  L_56 = __this->get_m_Center_7();
		Vector3_t3722313464  L_57 = __this->get_m_Size_6();
		Bounds_t2266837910  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Bounds__ctor_m1937678907((&L_58), L_56, L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Bounds_t2266837910  L_59 = NavMeshSurface_GetWorldBounds_m3220460035(NULL /*static, unused*/, L_55, L_58, /*hidden argument*/NULL);
		V_7 = L_59;
		Bounds_t2266837910  L_60 = V_7;
		LayerMask_t3493934918  L_61 = __this->get_m_LayerMask_8();
		int32_t L_62 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		int32_t L_63 = __this->get_m_UseGeometry_9();
		int32_t L_64 = __this->get_m_DefaultArea_10();
		List_1_t2785658607 * L_65 = V_1;
		List_1_t2160260967 * L_66 = V_0;
		NavMeshBuilder_CollectSources_m553736863(NULL /*static, unused*/, L_60, L_62, L_63, L_64, L_65, L_66, /*hidden argument*/NULL);
	}

IL_01d3:
	{
		bool L_67 = __this->get_m_IgnoreNavMeshAgent_11();
		if (!L_67)
		{
			goto IL_0202;
		}
	}
	{
		List_1_t2160260967 * L_68 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t1513480349 * L_69 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache2_27();
		G_B23_0 = L_68;
		if (L_69)
		{
			G_B24_0 = L_68;
			goto IL_01f7;
		}
	}
	{
		intptr_t L_70 = (intptr_t)NavMeshSurface_U3CCollectSourcesU3Em__2_m3444190560_RuntimeMethod_var;
		Predicate_1_t1513480349 * L_71 = (Predicate_1_t1513480349 *)il2cpp_codegen_object_new(Predicate_1_t1513480349_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1542028990(L_71, NULL, L_70, /*hidden argument*/Predicate_1__ctor_m1542028990_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache2_27(L_71);
		G_B24_0 = G_B23_0;
	}

IL_01f7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t1513480349 * L_72 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache2_27();
		List_1_RemoveAll_m1679687378(G_B24_0, L_72, /*hidden argument*/List_1_RemoveAll_m1679687378_RuntimeMethod_var);
	}

IL_0202:
	{
		bool L_73 = __this->get_m_IgnoreNavMeshObstacle_12();
		if (!L_73)
		{
			goto IL_0231;
		}
	}
	{
		List_1_t2160260967 * L_74 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t1513480349 * L_75 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache3_28();
		G_B27_0 = L_74;
		if (L_75)
		{
			G_B28_0 = L_74;
			goto IL_0226;
		}
	}
	{
		intptr_t L_76 = (intptr_t)NavMeshSurface_U3CCollectSourcesU3Em__3_m3645889326_RuntimeMethod_var;
		Predicate_1_t1513480349 * L_77 = (Predicate_1_t1513480349 *)il2cpp_codegen_object_new(Predicate_1_t1513480349_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m1542028990(L_77, NULL, L_76, /*hidden argument*/Predicate_1__ctor_m1542028990_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache3_28(L_77);
		G_B28_0 = G_B27_0;
	}

IL_0226:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Predicate_1_t1513480349 * L_78 = ((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache3_28();
		List_1_RemoveAll_m1679687378(G_B28_0, L_78, /*hidden argument*/List_1_RemoveAll_m1679687378_RuntimeMethod_var);
	}

IL_0231:
	{
		NavMeshSurface_AppendModifierVolumes_m4188255682(__this, (List_1_t2160260967 **)(&V_0), /*hidden argument*/NULL);
		List_1_t2160260967 * L_79 = V_0;
		return L_79;
	}
}
// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::Abs(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  NavMeshSurface_Abs_m641648730 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___v0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_Abs_m641648730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = (&___v0)->get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = (&___v0)->get_y_3();
		float L_3 = fabsf(L_2);
		float L_4 = (&___v0)->get_z_4();
		float L_5 = fabsf(L_4);
		Vector3_t3722313464  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3353183577((&L_6), L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Bounds UnityEngine.AI.NavMeshSurface::GetWorldBounds(UnityEngine.Matrix4x4,UnityEngine.Bounds)
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  NavMeshSurface_GetWorldBounds_m3220460035 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  ___mat0, Bounds_t2266837910  ___bounds1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_GetWorldBounds_m3220460035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Matrix4x4_MultiplyVector_m3808798942((Matrix4x4_t1817901843 *)(&___mat0), L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = NavMeshSurface_Abs_m641648730(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Matrix4x4_MultiplyVector_m3808798942((Matrix4x4_t1817901843 *)(&___mat0), L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = NavMeshSurface_Abs_m641648730(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t3722313464  L_6 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Matrix4x4_MultiplyVector_m3808798942((Matrix4x4_t1817901843 *)(&___mat0), L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = NavMeshSurface_Abs_m641648730(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t3722313464  L_9 = Bounds_get_center_m1418449258((Bounds_t2266837910 *)(&___bounds1), /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Matrix4x4_MultiplyPoint_m1575665487((Matrix4x4_t1817901843 *)(&___mat0), L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		Vector3_t3722313464  L_11 = V_0;
		Vector3_t3722313464  L_12 = Bounds_get_size_m1178783246((Bounds_t2266837910 *)(&___bounds1), /*hidden argument*/NULL);
		V_5 = L_12;
		float L_13 = (&V_5)->get_x_2();
		Vector3_t3722313464  L_14 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		Vector3_t3722313464  L_15 = V_1;
		Vector3_t3722313464  L_16 = Bounds_get_size_m1178783246((Bounds_t2266837910 *)(&___bounds1), /*hidden argument*/NULL);
		V_6 = L_16;
		float L_17 = (&V_6)->get_y_3();
		Vector3_t3722313464  L_18 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_14, L_18, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = V_2;
		Vector3_t3722313464  L_21 = Bounds_get_size_m1178783246((Bounds_t2266837910 *)(&___bounds1), /*hidden argument*/NULL);
		V_7 = L_21;
		float L_22 = (&V_7)->get_z_4();
		Vector3_t3722313464  L_23 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		Vector3_t3722313464  L_24 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_19, L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		Vector3_t3722313464  L_25 = V_3;
		Vector3_t3722313464  L_26 = V_4;
		Bounds_t2266837910  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Bounds__ctor_m1937678907((&L_27), L_25, L_26, /*hidden argument*/NULL);
		return L_27;
	}
}
// UnityEngine.Bounds UnityEngine.AI.NavMeshSurface::CalculateWorldBounds(System.Collections.Generic.List`1<UnityEngine.AI.NavMeshBuildSource>)
extern "C" IL2CPP_METHOD_ATTR Bounds_t2266837910  NavMeshSurface_CalculateWorldBounds_m4178605729 (NavMeshSurface_t986947041 * __this, List_1_t2160260967 * ___sources0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_CalculateWorldBounds_m4178605729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t1817901843  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	NavMeshBuildSource_t688186225  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t4049504844  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Mesh_t3648964284 * V_5 = NULL;
	TerrainData_t657004131 * V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_3 = Transform_get_rotation_m3502953881(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_5 = Matrix4x4_TRS_m3801934620(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Matrix4x4_t1817901843  L_6 = Matrix4x4_get_inverse_m1870592360((Matrix4x4_t1817901843 *)(&V_0), /*hidden argument*/NULL);
		V_0 = L_6;
		il2cpp_codegen_initobj((&V_1), sizeof(Bounds_t2266837910 ));
		List_1_t2160260967 * L_7 = ___sources0;
		Enumerator_t4049504844  L_8 = List_1_GetEnumerator_m1287074780(L_7, /*hidden argument*/List_1_GetEnumerator_m1287074780_RuntimeMethod_var);
		V_3 = L_8;
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		{
			goto IL_011d;
		}

IL_003d:
		{
			NavMeshBuildSource_t688186225  L_9 = Enumerator_get_Current_m601408329((Enumerator_t4049504844 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m601408329_RuntimeMethod_var);
			V_2 = L_9;
			int32_t L_10 = NavMeshBuildSource_get_shape_m2698270452((NavMeshBuildSource_t688186225 *)(&V_2), /*hidden argument*/NULL);
			V_4 = L_10;
			int32_t L_11 = V_4;
			switch (L_11)
			{
				case 0:
				{
					goto IL_0072;
				}
				case 1:
				{
					goto IL_00a5;
				}
				case 2:
				{
					goto IL_00ee;
				}
				case 3:
				{
					goto IL_00ee;
				}
				case 4:
				{
					goto IL_00ee;
				}
				case 5:
				{
					goto IL_00ee;
				}
			}
		}

IL_006d:
		{
			goto IL_011d;
		}

IL_0072:
		{
			Object_t631007953 * L_12 = NavMeshBuildSource_get_sourceObject_m603070145((NavMeshBuildSource_t688186225 *)(&V_2), /*hidden argument*/NULL);
			V_5 = ((Mesh_t3648964284 *)IsInstSealed((RuntimeObject*)L_12, Mesh_t3648964284_il2cpp_TypeInfo_var));
			Matrix4x4_t1817901843  L_13 = V_0;
			Matrix4x4_t1817901843  L_14 = NavMeshBuildSource_get_transform_m868807620((NavMeshBuildSource_t688186225 *)(&V_2), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
			Matrix4x4_t1817901843  L_15 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
			Mesh_t3648964284 * L_16 = V_5;
			Bounds_t2266837910  L_17 = Mesh_get_bounds_m2004960313(L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
			Bounds_t2266837910  L_18 = NavMeshSurface_GetWorldBounds_m3220460035(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
			Bounds_Encapsulate_m1263362003((Bounds_t2266837910 *)(&V_1), L_18, /*hidden argument*/NULL);
			goto IL_011d;
		}

IL_00a5:
		{
			Object_t631007953 * L_19 = NavMeshBuildSource_get_sourceObject_m603070145((NavMeshBuildSource_t688186225 *)(&V_2), /*hidden argument*/NULL);
			V_6 = ((TerrainData_t657004131 *)IsInstSealed((RuntimeObject*)L_19, TerrainData_t657004131_il2cpp_TypeInfo_var));
			Matrix4x4_t1817901843  L_20 = V_0;
			Matrix4x4_t1817901843  L_21 = NavMeshBuildSource_get_transform_m868807620((NavMeshBuildSource_t688186225 *)(&V_2), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
			Matrix4x4_t1817901843  L_22 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
			TerrainData_t657004131 * L_23 = V_6;
			Vector3_t3722313464  L_24 = TerrainData_get_size_m1871576403(L_23, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			Vector3_t3722313464  L_25 = Vector3_op_Multiply_m2104357790(NULL /*static, unused*/, (0.5f), L_24, /*hidden argument*/NULL);
			TerrainData_t657004131 * L_26 = V_6;
			Vector3_t3722313464  L_27 = TerrainData_get_size_m1871576403(L_26, /*hidden argument*/NULL);
			Bounds_t2266837910  L_28;
			memset(&L_28, 0, sizeof(L_28));
			Bounds__ctor_m1937678907((&L_28), L_25, L_27, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
			Bounds_t2266837910  L_29 = NavMeshSurface_GetWorldBounds_m3220460035(NULL /*static, unused*/, L_22, L_28, /*hidden argument*/NULL);
			Bounds_Encapsulate_m1263362003((Bounds_t2266837910 *)(&V_1), L_29, /*hidden argument*/NULL);
			goto IL_011d;
		}

IL_00ee:
		{
			Matrix4x4_t1817901843  L_30 = V_0;
			Matrix4x4_t1817901843  L_31 = NavMeshBuildSource_get_transform_m868807620((NavMeshBuildSource_t688186225 *)(&V_2), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
			Matrix4x4_t1817901843  L_32 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			Vector3_t3722313464  L_33 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
			Vector3_t3722313464  L_34 = NavMeshBuildSource_get_size_m1205786197((NavMeshBuildSource_t688186225 *)(&V_2), /*hidden argument*/NULL);
			Bounds_t2266837910  L_35;
			memset(&L_35, 0, sizeof(L_35));
			Bounds__ctor_m1937678907((&L_35), L_33, L_34, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(NavMeshSurface_t986947041_il2cpp_TypeInfo_var);
			Bounds_t2266837910  L_36 = NavMeshSurface_GetWorldBounds_m3220460035(NULL /*static, unused*/, L_32, L_35, /*hidden argument*/NULL);
			Bounds_Encapsulate_m1263362003((Bounds_t2266837910 *)(&V_1), L_36, /*hidden argument*/NULL);
			goto IL_011d;
		}

IL_011d:
		{
			bool L_37 = Enumerator_MoveNext_m363784717((Enumerator_t4049504844 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m363784717_RuntimeMethod_var);
			if (L_37)
			{
				goto IL_003d;
			}
		}

IL_0129:
		{
			IL2CPP_LEAVE(0x13C, FINALLY_012e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_012e;
	}

FINALLY_012e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2830648961((Enumerator_t4049504844 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m2830648961_RuntimeMethod_var);
		IL2CPP_END_FINALLY(302)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(302)
	{
		IL2CPP_JUMP_TBL(0x13C, IL_013c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_013c:
	{
		Bounds_Expand_m2714456408((Bounds_t2266837910 *)(&V_1), (0.1f), /*hidden argument*/NULL);
		Bounds_t2266837910  L_38 = V_1;
		return L_38;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::HasTransformChanged()
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_HasTransformChanged_m251008277 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_HasTransformChanged_m251008277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t3722313464  L_0 = __this->get_m_LastPosition_20();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		bool L_3 = Vector3_op_Inequality_m315980366(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		return (bool)1;
	}

IL_001d:
	{
		Quaternion_t2301928331  L_4 = __this->get_m_LastRotation_21();
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		bool L_7 = Quaternion_op_Inequality_m1948345154(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		return (bool)1;
	}

IL_003a:
	{
		return (bool)0;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::UpdateDataIfTransformChanged()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface_UpdateDataIfTransformChanged_m1853993666 (NavMeshSurface_t986947041 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = NavMeshSurface_HasTransformChanged_m251008277(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		NavMeshSurface_RemoveData_m3790252040(__this, /*hidden argument*/NULL);
		NavMeshSurface_AddData_m3992805541(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AI.NavMeshSurface::.cctor()
extern "C" IL2CPP_METHOD_ATTR void NavMeshSurface__cctor_m1610377881 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface__cctor_m1610377881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2459021783 * L_0 = (List_1_t2459021783 *)il2cpp_codegen_object_new(List_1_t2459021783_il2cpp_TypeInfo_var);
		List_1__ctor_m1544123386(L_0, /*hidden argument*/List_1__ctor_m1544123386_RuntimeMethod_var);
		((NavMeshSurface_t986947041_StaticFields*)il2cpp_codegen_static_fields_for(NavMeshSurface_t986947041_il2cpp_TypeInfo_var))->set_s_NavMeshSurfaces_22(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::<AppendModifierVolumes>m__0(UnityEngine.AI.NavMeshModifierVolume)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_U3CAppendModifierVolumesU3Em__0_m837462505 (RuntimeObject * __this /* static, unused */, NavMeshModifierVolume_t3293887184 * ___x0, const RuntimeMethod* method)
{
	{
		NavMeshModifierVolume_t3293887184 * L_0 = ___x0;
		bool L_1 = Behaviour_get_isActiveAndEnabled_m3143666263(L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::<CollectSources>m__1(UnityEngine.AI.NavMeshModifier)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_U3CCollectSourcesU3Em__1_m4201889746 (RuntimeObject * __this /* static, unused */, NavMeshModifier_t1010705800 * ___x0, const RuntimeMethod* method)
{
	{
		NavMeshModifier_t1010705800 * L_0 = ___x0;
		bool L_1 = Behaviour_get_isActiveAndEnabled_m3143666263(L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::<CollectSources>m__2(UnityEngine.AI.NavMeshBuildSource)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_U3CCollectSourcesU3Em__2_m3444190560 (RuntimeObject * __this /* static, unused */, NavMeshBuildSource_t688186225  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_U3CCollectSourcesU3Em__2_m3444190560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Component_t1923634451 * L_0 = NavMeshBuildSource_get_component_m3010658605((NavMeshBuildSource_t688186225 *)(&___x0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		Component_t1923634451 * L_2 = NavMeshBuildSource_get_component_m3010658605((NavMeshBuildSource_t688186225 *)(&___x0), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NavMeshAgent_t1276799816 * L_4 = GameObject_GetComponent_TisNavMeshAgent_t1276799816_m2253338685(L_3, /*hidden argument*/GameObject_GetComponent_TisNavMeshAgent_t1276799816_m2253338685_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean UnityEngine.AI.NavMeshSurface::<CollectSources>m__3(UnityEngine.AI.NavMeshBuildSource)
extern "C" IL2CPP_METHOD_ATTR bool NavMeshSurface_U3CCollectSourcesU3Em__3_m3645889326 (RuntimeObject * __this /* static, unused */, NavMeshBuildSource_t688186225  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMeshSurface_U3CCollectSourcesU3Em__3_m3645889326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Component_t1923634451 * L_0 = NavMeshBuildSource_get_component_m3010658605((NavMeshBuildSource_t688186225 *)(&___x0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		Component_t1923634451 * L_2 = NavMeshBuildSource_get_component_m3010658605((NavMeshBuildSource_t688186225 *)(&___x0), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		NavMeshObstacle_t3657678309 * L_4 = GameObject_GetComponent_TisNavMeshObstacle_t3657678309_m1899691350(L_3, /*hidden argument*/GameObject_GetComponent_TisNavMeshObstacle_t3657678309_m1899691350_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		return (bool)G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UtilityAI.Decision::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Decision__ctor_m1412182017 (Decision_t2344985003 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UtilityAI.Decision::GetQualityWeighting()
extern "C" IL2CPP_METHOD_ATTR float Decision_GetQualityWeighting_m2313068549 (Decision_t2344985003 * __this, const RuntimeMethod* method)
{
	{
		return (0.0f);
	}
}
// System.Void UtilityAI.Decision::ExecuteAction()
extern "C" IL2CPP_METHOD_ATTR void Decision_ExecuteAction_m1227749490 (Decision_t2344985003 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// UtilityAI.DecisionType UtilityAI.Decision::GetDecisionType()
extern "C" IL2CPP_METHOD_ATTR int32_t Decision_GetDecisionType_m2045168067 (Decision_t2344985003 * __this, const RuntimeMethod* method)
{
	{
		return (int32_t)(0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UtilityAI.DecisionManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DecisionManager__ctor_m1584365721 (DecisionManager_t2018219593 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DecisionManager__ctor_m1584365721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3817059745 * L_0 = (List_1_t3817059745 *)il2cpp_codegen_object_new(List_1_t3817059745_il2cpp_TypeInfo_var);
		List_1__ctor_m3019387110(L_0, /*hidden argument*/List_1__ctor_m3019387110_RuntimeMethod_var);
		__this->set_decisions_5(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UtilityAI.DecisionManager::Start()
extern "C" IL2CPP_METHOD_ATTR void DecisionManager_Start_m4131940883 (DecisionManager_t2018219593 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DecisionManager_Start_m4131940883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3817059745 * L_0 = __this->get_decisions_5();
		List_1_t3817059745 * L_1 = __this->get_decisions_5();
		int32_t L_2 = List_1_get_Count_m1142946597(L_1, /*hidden argument*/List_1_get_Count_m1142946597_RuntimeMethod_var);
		Decision_t2344985003 * L_3 = List_1_get_Item_m2061167563(L_0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)), /*hidden argument*/List_1_get_Item_m2061167563_RuntimeMethod_var);
		__this->set_bestDecision_4(L_3);
		return;
	}
}
// System.Void UtilityAI.DecisionManager::Update()
extern "C" IL2CPP_METHOD_ATTR void DecisionManager_Update_m3095171479 (DecisionManager_t2018219593 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DecisionManager_Update_m3095171479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Decision_t2344985003 * V_0 = NULL;
	Enumerator_t1411336326  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3817059745 * L_0 = __this->get_decisions_5();
		Enumerator_t1411336326  L_1 = List_1_GetEnumerator_m2970145501(L_0, /*hidden argument*/List_1_GetEnumerator_m2970145501_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0036;
		}

IL_0011:
		{
			Decision_t2344985003 * L_2 = Enumerator_get_Current_m2922767688((Enumerator_t1411336326 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m2922767688_RuntimeMethod_var);
			V_0 = L_2;
			Decision_t2344985003 * L_3 = V_0;
			float L_4 = VirtFuncInvoker0< float >::Invoke(4 /* System.Single UtilityAI.Decision::GetQualityWeighting() */, L_3);
			Decision_t2344985003 * L_5 = __this->get_bestDecision_4();
			float L_6 = VirtFuncInvoker0< float >::Invoke(4 /* System.Single UtilityAI.Decision::GetQualityWeighting() */, L_5);
			if ((!(((float)L_4) > ((float)L_6))))
			{
				goto IL_0036;
			}
		}

IL_002f:
		{
			Decision_t2344985003 * L_7 = V_0;
			__this->set_bestDecision_4(L_7);
		}

IL_0036:
		{
			bool L_8 = Enumerator_MoveNext_m1707601366((Enumerator_t1411336326 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m1707601366_RuntimeMethod_var);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3610717444((Enumerator_t1411336326 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m3610717444_RuntimeMethod_var);
		IL2CPP_END_FINALLY(71)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0055:
	{
		Decision_t2344985003 * L_9 = __this->get_bestDecision_4();
		VirtActionInvoker0::Invoke(5 /* System.Void UtilityAI.Decision::ExecuteAction() */, L_9);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UtilityAI.FIGHT_COMPETITOR::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FIGHT_COMPETITOR__ctor_m421503881 (FIGHT_COMPETITOR_t2592966111 * __this, const RuntimeMethod* method)
{
	{
		__this->set_type_4(3);
		__this->set_distanceToFlagVariable_5((2.5f));
		__this->set_distanceVariable_6((1.5f));
		__this->set_maxDistanceToFight_7((20.0f));
		Decision__ctor_m1412182017(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UtilityAI.FIGHT_COMPETITOR::GetQualityWeighting()
extern "C" IL2CPP_METHOD_ATTR float FIGHT_COMPETITOR_GetQualityWeighting_m1694195179 (FIGHT_COMPETITOR_t2592966111 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIGHT_COMPETITOR_GetQualityWeighting_m1694195179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ItemManager_t3254073967 * V_0 = NULL;
	Competitor_t2775849210 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2249249573, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral2249249573, /*hidden argument*/NULL);
		V_0 = ((ItemManager_t3254073967 *)CastclassClass((RuntimeObject*)L_1, ItemManager_t3254073967_il2cpp_TypeInfo_var));
		Competitor_t2775849210 * L_2 = FIGHT_COMPETITOR_GetBestCompetitorToFight_m2559081439(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Competitor_t2775849210 * L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0174;
		}
	}
	{
		Competitor_t2775849210 * L_5 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
		float L_6 = L_5->get_hungerVariable_15();
		Competitor_t2775849210 * L_7 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
		float L_8 = L_7->get_thirstVariable_16();
		Competitor_t2775849210 * L_9 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
		float L_10 = L_9->get_healthVariable_17();
		float L_11 = __this->get_distanceVariable_6();
		V_2 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_6, (float)L_8)), (float)L_10)), (float)L_11));
		Competitor_t2775849210 * L_12 = V_1;
		GameObject_t1113636619 * L_13 = Component_get_gameObject_m442555142(L_12, /*hidden argument*/NULL);
		Transform_t3600365921 * L_14 = GameObject_get_transform_m1369836730(L_13, /*hidden argument*/NULL);
		Vector3_t3722313464  L_15 = Transform_get_position_m36019626(L_14, /*hidden argument*/NULL);
		ItemManager_t3254073967 * L_16 = V_0;
		GameObject_t1113636619 * L_17 = L_16->get_flag_10();
		Transform_t3600365921 * L_18 = GameObject_get_transform_m1369836730(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = Transform_get_position_m36019626(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_20 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_15, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_22 = GameObject_get_transform_m1369836730(L_21, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = Transform_get_position_m36019626(L_22, /*hidden argument*/NULL);
		ItemManager_t3254073967 * L_24 = V_0;
		GameObject_t1113636619 * L_25 = L_24->get_flag_10();
		Transform_t3600365921 * L_26 = GameObject_get_transform_m1369836730(L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = Transform_get_position_m36019626(L_26, /*hidden argument*/NULL);
		float L_28 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_23, L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = V_4;
		float L_30 = V_5;
		if ((!(((float)L_29) < ((float)L_30))))
		{
			goto IL_00bd;
		}
	}
	{
		float L_31 = V_5;
		float L_32 = V_4;
		float L_33 = V_5;
		V_3 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_31, (float)L_32))/(float)L_33));
		goto IL_00c3;
	}

IL_00bd:
	{
		V_3 = (0.0f);
	}

IL_00c3:
	{
		Competitor_t2775849210 * L_34 = V_1;
		GameObject_t1113636619 * L_35 = Component_get_gameObject_m442555142(L_34, /*hidden argument*/NULL);
		Transform_t3600365921 * L_36 = GameObject_get_transform_m1369836730(L_35, /*hidden argument*/NULL);
		Vector3_t3722313464  L_37 = Transform_get_position_m36019626(L_36, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_38 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_39 = GameObject_get_transform_m1369836730(L_38, /*hidden argument*/NULL);
		Vector3_t3722313464  L_40 = Transform_get_position_m36019626(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_41 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_37, L_40, /*hidden argument*/NULL);
		V_6 = L_41;
		Competitor_t2775849210 * L_42 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
		float L_43 = L_42->get_hungerVariable_15();
		Competitor_t2775849210 * L_44 = V_1;
		float L_45 = L_44->get_maxHunger_10();
		Competitor_t2775849210 * L_46 = V_1;
		float L_47 = L_46->get_hunger_13();
		Competitor_t2775849210 * L_48 = V_1;
		float L_49 = L_48->get_maxHunger_10();
		Competitor_t2775849210 * L_50 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
		float L_51 = L_50->get_thirstVariable_16();
		Competitor_t2775849210 * L_52 = V_1;
		float L_53 = L_52->get_maxThirst_11();
		Competitor_t2775849210 * L_54 = V_1;
		float L_55 = L_54->get_thirst_14();
		Competitor_t2775849210 * L_56 = V_1;
		float L_57 = L_56->get_maxThirst_11();
		Competitor_t2775849210 * L_58 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
		float L_59 = L_58->get_healthVariable_17();
		Competitor_t2775849210 * L_60 = V_1;
		float L_61 = L_60->get_maxHealth_9();
		Competitor_t2775849210 * L_62 = V_1;
		float L_63 = L_62->get_health_12();
		Competitor_t2775849210 * L_64 = V_1;
		float L_65 = L_64->get_maxHealth_9();
		float L_66 = __this->get_distanceToFlagVariable_5();
		float L_67 = V_3;
		float L_68 = __this->get_distanceVariable_6();
		float L_69 = __this->get_maxDistanceToFight_7();
		float L_70 = V_6;
		float L_71 = __this->get_maxDistanceToFight_7();
		V_7 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_43, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_45, (float)L_47))/(float)L_49)))), (float)((float)il2cpp_codegen_multiply((float)L_51, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_53, (float)L_55))/(float)L_57)))))), (float)((float)il2cpp_codegen_multiply((float)L_59, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_61, (float)L_63))/(float)L_65)))))), (float)((float)il2cpp_codegen_multiply((float)L_66, (float)L_67)))), (float)((float)il2cpp_codegen_multiply((float)L_68, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_69, (float)L_70))/(float)L_71))))));
		float L_72 = V_7;
		float L_73 = V_2;
		return ((float)((float)L_72/(float)L_73));
	}

IL_0174:
	{
		return (0.0f);
	}
}
// System.Void UtilityAI.FIGHT_COMPETITOR::ExecuteAction()
extern "C" IL2CPP_METHOD_ATTR void FIGHT_COMPETITOR_ExecuteAction_m520634744 (FIGHT_COMPETITOR_t2592966111 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIGHT_COMPETITOR_ExecuteAction_m520634744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Competitor_t2775849210 * V_0 = NULL;
	NavMeshAgent_t1276799816 * V_1 = NULL;
	Competitor_t2775849210 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral1771024816, /*hidden argument*/NULL);
		V_0 = ((Competitor_t2775849210 *)CastclassClass((RuntimeObject*)L_1, Competitor_t2775849210_il2cpp_TypeInfo_var));
		Competitor_t2775849210 * L_2 = V_0;
		NavMeshAgent_t1276799816 * L_3 = Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532(L_2, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532_RuntimeMethod_var);
		V_1 = L_3;
		Competitor_t2775849210 * L_4 = FIGHT_COMPETITOR_GetBestCompetitorToFight_m2559081439(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		NavMeshAgent_t1276799816 * L_5 = V_1;
		Competitor_t2775849210 * L_6 = V_2;
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = GameObject_get_transform_m1369836730(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		NavMeshAgent_set_destination_m41262300(L_5, L_9, /*hidden argument*/NULL);
		Competitor_t2775849210 * L_10 = V_2;
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(L_10, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = GameObject_get_transform_m1369836730(L_14, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_17 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		float L_18 = V_3;
		if ((!(((float)L_18) < ((float)(2.5f)))))
		{
			goto IL_00c3;
		}
	}
	{
		Competitor_t2775849210 * L_19 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
		float L_20 = Competitor_GetHealthWeighting_m1154415733(L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		Competitor_t2775849210 * L_21 = V_2;
		Competitor_t2775849210 * L_22 = L_21;
		float L_23 = L_22->get_health_12();
		float L_24 = V_4;
		float L_25 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_22->set_health_12(((float)il2cpp_codegen_subtract((float)L_23, (float)((float)il2cpp_codegen_multiply((float)L_24, (float)L_25)))));
		Competitor_t2775849210 * L_26 = V_2;
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(L_26, /*hidden argument*/NULL);
		Transform_t3600365921 * L_28 = Transform_Find_m1729760951(L_27, _stringLiteral1189461543, /*hidden argument*/NULL);
		ParticleSystem_t1800779281 * L_29 = Component_GetComponent_TisParticleSystem_t1800779281_m3884485303(L_28, /*hidden argument*/Component_GetComponent_TisParticleSystem_t1800779281_m3884485303_RuntimeMethod_var);
		ParticleSystem_Play_m882713458(L_29, /*hidden argument*/NULL);
		Competitor_t2775849210 * L_30 = V_2;
		float L_31 = L_30->get_health_12();
		if ((!(((double)(((double)((double)L_31)))) < ((double)(0.1)))))
		{
			goto IL_00c3;
		}
	}
	{
		Competitor_t2775849210 * L_32 = V_2;
		Competitor_Kill_m580404221(L_32, 3, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		return;
	}
}
// Competitor UtilityAI.FIGHT_COMPETITOR::GetBestCompetitorToFight()
extern "C" IL2CPP_METHOD_ATTR Competitor_t2775849210 * FIGHT_COMPETITOR_GetBestCompetitorToFight_m2559081439 (FIGHT_COMPETITOR_t2592966111 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIGHT_COMPETITOR_GetBestCompetitorToFight_m2559081439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CompetitorManager_t1763731731 * V_0 = NULL;
	float V_1 = 0.0f;
	Competitor_t2775849210 * V_2 = NULL;
	Competitor_t2775849210 * V_3 = NULL;
	Enumerator_t1842200533  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral758907337, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral758907337, /*hidden argument*/NULL);
		V_0 = ((CompetitorManager_t1763731731 *)CastclassClass((RuntimeObject*)L_1, CompetitorManager_t1763731731_il2cpp_TypeInfo_var));
		V_1 = (0.0f);
		V_2 = (Competitor_t2775849210 *)NULL;
		CompetitorManager_t1763731731 * L_2 = V_0;
		List_1_t4247923952 * L_3 = L_2->get_competitors_5();
		Enumerator_t1842200533  L_4 = List_1_GetEnumerator_m2888270272(L_3, /*hidden argument*/List_1_GetEnumerator_m2888270272_RuntimeMethod_var);
		V_4 = L_4;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0111;
		}

IL_0034:
		{
			Competitor_t2775849210 * L_5 = Enumerator_get_Current_m3267056561((Enumerator_t1842200533 *)(&V_4), /*hidden argument*/Enumerator_get_Current_m3267056561_RuntimeMethod_var);
			V_3 = L_5;
			Competitor_t2775849210 * L_6 = V_3;
			GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
			Component_t1923634451 * L_8 = GameObject_GetComponent_m3266446757(L_7, _stringLiteral1771024816, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_0111;
			}
		}

IL_0057:
		{
			Competitor_t2775849210 * L_10 = V_3;
			GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(L_10, /*hidden argument*/NULL);
			Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
			Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
			GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
			Transform_t3600365921 * L_15 = GameObject_get_transform_m1369836730(L_14, /*hidden argument*/NULL);
			Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			float L_17 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
			V_5 = L_17;
			float L_18 = V_5;
			float L_19 = __this->get_maxDistanceToFight_7();
			if ((!(((float)L_18) < ((float)L_19))))
			{
				goto IL_0111;
			}
		}

IL_008b:
		{
			Competitor_t2775849210 * L_20 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
			float L_21 = L_20->get_hungerVariable_15();
			Competitor_t2775849210 * L_22 = V_3;
			float L_23 = L_22->get_maxHunger_10();
			Competitor_t2775849210 * L_24 = V_3;
			float L_25 = L_24->get_hunger_13();
			Competitor_t2775849210 * L_26 = V_3;
			float L_27 = L_26->get_maxHunger_10();
			Competitor_t2775849210 * L_28 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
			float L_29 = L_28->get_thirstVariable_16();
			Competitor_t2775849210 * L_30 = V_3;
			float L_31 = L_30->get_maxThirst_11();
			Competitor_t2775849210 * L_32 = V_3;
			float L_33 = L_32->get_thirst_14();
			Competitor_t2775849210 * L_34 = V_3;
			float L_35 = L_34->get_maxThirst_11();
			Competitor_t2775849210 * L_36 = Component_GetComponent_TisCompetitor_t2775849210_m662455226(__this, /*hidden argument*/Component_GetComponent_TisCompetitor_t2775849210_m662455226_RuntimeMethod_var);
			float L_37 = L_36->get_healthVariable_17();
			Competitor_t2775849210 * L_38 = V_3;
			float L_39 = L_38->get_maxHealth_9();
			Competitor_t2775849210 * L_40 = V_3;
			float L_41 = L_40->get_health_12();
			Competitor_t2775849210 * L_42 = V_3;
			float L_43 = L_42->get_maxHealth_9();
			float L_44 = __this->get_distanceVariable_6();
			float L_45 = __this->get_maxDistanceToFight_7();
			float L_46 = V_5;
			float L_47 = __this->get_maxDistanceToFight_7();
			V_6 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_21, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_23, (float)L_25))/(float)L_27)))), (float)((float)il2cpp_codegen_multiply((float)L_29, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_31, (float)L_33))/(float)L_35)))))), (float)((float)il2cpp_codegen_multiply((float)L_37, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_39, (float)L_41))/(float)L_43)))))), (float)((float)il2cpp_codegen_multiply((float)L_44, (float)((float)((float)((float)il2cpp_codegen_subtract((float)L_45, (float)L_46))/(float)L_47))))));
			float L_48 = V_6;
			float L_49 = V_1;
			if ((!(((float)L_48) > ((float)L_49))))
			{
				goto IL_0111;
			}
		}

IL_010f:
		{
			Competitor_t2775849210 * L_50 = V_3;
			V_2 = L_50;
		}

IL_0111:
		{
			bool L_51 = Enumerator_MoveNext_m860927647((Enumerator_t1842200533 *)(&V_4), /*hidden argument*/Enumerator_MoveNext_m860927647_RuntimeMethod_var);
			if (L_51)
			{
				goto IL_0034;
			}
		}

IL_011d:
		{
			IL2CPP_LEAVE(0x130, FINALLY_0122);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0122;
	}

FINALLY_0122:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m446944907((Enumerator_t1842200533 *)(&V_4), /*hidden argument*/Enumerator_Dispose_m446944907_RuntimeMethod_var);
		IL2CPP_END_FINALLY(290)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(290)
	{
		IL2CPP_JUMP_TBL(0x130, IL_0130)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0130:
	{
		Competitor_t2775849210 * L_52 = V_2;
		return L_52;
	}
}
// UtilityAI.DecisionType UtilityAI.FIGHT_COMPETITOR::GetDecisionType()
extern "C" IL2CPP_METHOD_ATTR int32_t FIGHT_COMPETITOR_GetDecisionType_m4264758935 (FIGHT_COMPETITOR_t2592966111 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_type_4();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UtilityAI.FIND_FLAG::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FIND_FLAG__ctor_m1028246092 (FIND_FLAG_t1938094438 * __this, const RuntimeMethod* method)
{
	{
		__this->set_type_4(2);
		__this->set_maxDistanceForAffect_5(((int32_t)40));
		__this->set_maxDistanceWeighting_6((0.2f));
		__this->set_maxEnemyWeighting_7((0.5f));
		__this->set_maxHealthWeighting_8((0.2f));
		__this->set_hungerVariable_9((0.8f));
		__this->set_thirstVariable_10((0.4f));
		__this->set_healthVariable_11((0.5f));
		Decision__ctor_m1412182017(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UtilityAI.FIND_FLAG::GetQualityWeighting()
extern "C" IL2CPP_METHOD_ATTR float FIND_FLAG_GetQualityWeighting_m3316420969 (FIND_FLAG_t1938094438 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_FLAG_GetQualityWeighting_m3316420969_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ItemManager_t3254073967 * V_0 = NULL;
	Competitor_t2775849210 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Competitor_t2775849210 * V_5 = NULL;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2249249573, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral2249249573, /*hidden argument*/NULL);
		V_0 = ((ItemManager_t3254073967 *)CastclassClass((RuntimeObject*)L_1, ItemManager_t3254073967_il2cpp_TypeInfo_var));
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Component_t1923634451 * L_3 = GameObject_GetComponent_m3266446757(L_2, _stringLiteral1771024816, /*hidden argument*/NULL);
		V_1 = ((Competitor_t2775849210 *)CastclassClass((RuntimeObject*)L_3, Competitor_t2775849210_il2cpp_TypeInfo_var));
		ItemManager_t3254073967 * L_4 = V_0;
		GameObject_t1113636619 * L_5 = L_4->get_flag_10();
		Transform_t3600365921 * L_6 = GameObject_get_transform_m1369836730(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_11 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		float L_12 = V_2;
		int32_t L_13 = __this->get_maxDistanceForAffect_5();
		if ((!(((float)L_12) > ((float)(((float)((float)L_13)))))))
		{
			goto IL_006e;
		}
	}
	{
		V_3 = (0.0f);
		goto IL_0080;
	}

IL_006e:
	{
		int32_t L_14 = __this->get_maxDistanceForAffect_5();
		float L_15 = V_2;
		int32_t L_16 = __this->get_maxDistanceForAffect_5();
		V_3 = ((float)((float)((float)il2cpp_codegen_subtract((float)(((float)((float)L_14))), (float)L_15))/(float)(((float)((float)L_16)))));
	}

IL_0080:
	{
		Competitor_t2775849210 * L_17 = FIND_FLAG_GetClosestCompetitorToFlag_m2476932474(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		Competitor_t2775849210 * L_18 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_18, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_009b;
		}
	}
	{
		return (0.0f);
	}

IL_009b:
	{
		Competitor_t2775849210 * L_20 = V_5;
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(L_20, /*hidden argument*/NULL);
		Transform_t3600365921 * L_22 = GameObject_get_transform_m1369836730(L_21, /*hidden argument*/NULL);
		Vector3_t3722313464  L_23 = Transform_get_position_m36019626(L_22, /*hidden argument*/NULL);
		ItemManager_t3254073967 * L_24 = V_0;
		GameObject_t1113636619 * L_25 = L_24->get_flag_10();
		Transform_t3600365921 * L_26 = GameObject_get_transform_m1369836730(L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = Transform_get_position_m36019626(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_28 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_23, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		GameObject_t1113636619 * L_29 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_30 = GameObject_get_transform_m1369836730(L_29, /*hidden argument*/NULL);
		Vector3_t3722313464  L_31 = Transform_get_position_m36019626(L_30, /*hidden argument*/NULL);
		ItemManager_t3254073967 * L_32 = V_0;
		GameObject_t1113636619 * L_33 = L_32->get_flag_10();
		Transform_t3600365921 * L_34 = GameObject_get_transform_m1369836730(L_33, /*hidden argument*/NULL);
		Vector3_t3722313464  L_35 = Transform_get_position_m36019626(L_34, /*hidden argument*/NULL);
		float L_36 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_31, L_35, /*hidden argument*/NULL);
		V_7 = L_36;
		float L_37 = V_6;
		float L_38 = V_7;
		if ((!(((float)L_37) < ((float)L_38))))
		{
			goto IL_0102;
		}
	}
	{
		float L_39 = V_7;
		float L_40 = V_6;
		float L_41 = V_7;
		V_4 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_39, (float)L_40))/(float)L_41));
		goto IL_010a;
	}

IL_0102:
	{
		float L_42 = __this->get_maxEnemyWeighting_7();
		V_4 = L_42;
	}

IL_010a:
	{
		float L_43 = __this->get_hungerVariable_9();
		Competitor_t2775849210 * L_44 = V_1;
		float L_45 = L_44->get_hunger_13();
		Competitor_t2775849210 * L_46 = V_1;
		float L_47 = L_46->get_maxHunger_10();
		float L_48 = __this->get_thirstVariable_10();
		Competitor_t2775849210 * L_49 = V_1;
		float L_50 = L_49->get_thirst_14();
		Competitor_t2775849210 * L_51 = V_1;
		float L_52 = L_51->get_maxThirst_11();
		float L_53 = __this->get_healthVariable_11();
		Competitor_t2775849210 * L_54 = V_1;
		float L_55 = L_54->get_health_12();
		Competitor_t2775849210 * L_56 = V_1;
		float L_57 = L_56->get_maxHealth_9();
		V_8 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_43, (float)((float)((float)L_45/(float)L_47)))), (float)((float)il2cpp_codegen_multiply((float)L_48, (float)((float)((float)L_50/(float)L_52)))))), (float)((float)il2cpp_codegen_multiply((float)L_53, (float)((float)((float)L_55/(float)L_57))))));
		float L_58 = __this->get_maxDistanceWeighting_6();
		float L_59 = V_3;
		V_9 = ((float)il2cpp_codegen_multiply((float)L_58, (float)L_59));
		float L_60 = __this->get_maxEnemyWeighting_7();
		float L_61 = V_4;
		V_10 = ((float)il2cpp_codegen_multiply((float)L_60, (float)L_61));
		float L_62 = __this->get_maxHealthWeighting_8();
		float L_63 = V_8;
		V_11 = ((float)il2cpp_codegen_multiply((float)L_62, (float)L_63));
		float L_64 = V_9;
		float L_65 = V_10;
		float L_66 = V_11;
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_64, (float)L_65)), (float)L_66));
	}
}
// System.Void UtilityAI.FIND_FLAG::ExecuteAction()
extern "C" IL2CPP_METHOD_ATTR void FIND_FLAG_ExecuteAction_m2906779793 (FIND_FLAG_t1938094438 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_FLAG_ExecuteAction_m2906779793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ItemManager_t3254073967 * V_0 = NULL;
	Competitor_t2775849210 * V_1 = NULL;
	NavMeshAgent_t1276799816 * V_2 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2249249573, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral2249249573, /*hidden argument*/NULL);
		V_0 = ((ItemManager_t3254073967 *)CastclassClass((RuntimeObject*)L_1, ItemManager_t3254073967_il2cpp_TypeInfo_var));
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Component_t1923634451 * L_3 = GameObject_GetComponent_m3266446757(L_2, _stringLiteral1771024816, /*hidden argument*/NULL);
		V_1 = ((Competitor_t2775849210 *)CastclassClass((RuntimeObject*)L_3, Competitor_t2775849210_il2cpp_TypeInfo_var));
		Competitor_t2775849210 * L_4 = V_1;
		NavMeshAgent_t1276799816 * L_5 = Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532(L_4, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532_RuntimeMethod_var);
		V_2 = L_5;
		NavMeshAgent_t1276799816 * L_6 = V_2;
		ItemManager_t3254073967 * L_7 = V_0;
		GameObject_t1113636619 * L_8 = L_7->get_flag_10();
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		NavMeshAgent_set_destination_m41262300(L_6, L_10, /*hidden argument*/NULL);
		return;
	}
}
// Competitor UtilityAI.FIND_FLAG::GetClosestCompetitorToFlag()
extern "C" IL2CPP_METHOD_ATTR Competitor_t2775849210 * FIND_FLAG_GetClosestCompetitorToFlag_m2476932474 (FIND_FLAG_t1938094438 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_FLAG_GetClosestCompetitorToFlag_m2476932474_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CompetitorManager_t1763731731 * V_0 = NULL;
	ItemManager_t3254073967 * V_1 = NULL;
	float V_2 = 0.0f;
	Competitor_t2775849210 * V_3 = NULL;
	Competitor_t2775849210 * V_4 = NULL;
	Enumerator_t1842200533  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral758907337, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral758907337, /*hidden argument*/NULL);
		V_0 = ((CompetitorManager_t1763731731 *)CastclassClass((RuntimeObject*)L_1, CompetitorManager_t1763731731_il2cpp_TypeInfo_var));
		GameObject_t1113636619 * L_2 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2249249573, /*hidden argument*/NULL);
		Component_t1923634451 * L_3 = GameObject_GetComponent_m3266446757(L_2, _stringLiteral2249249573, /*hidden argument*/NULL);
		V_1 = ((ItemManager_t3254073967 *)CastclassClass((RuntimeObject*)L_3, ItemManager_t3254073967_il2cpp_TypeInfo_var));
		V_2 = (std::numeric_limits<float>::max());
		V_3 = (Competitor_t2775849210 *)NULL;
		CompetitorManager_t1763731731 * L_4 = V_0;
		List_1_t4247923952 * L_5 = L_4->get_competitors_5();
		Enumerator_t1842200533  L_6 = List_1_GetEnumerator_m2888270272(L_5, /*hidden argument*/List_1_GetEnumerator_m2888270272_RuntimeMethod_var);
		V_5 = L_6;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a6;
		}

IL_004e:
		{
			Competitor_t2775849210 * L_7 = Enumerator_get_Current_m3267056561((Enumerator_t1842200533 *)(&V_5), /*hidden argument*/Enumerator_get_Current_m3267056561_RuntimeMethod_var);
			V_4 = L_7;
			Competitor_t2775849210 * L_8 = V_4;
			GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
			Component_t1923634451 * L_10 = GameObject_GetComponent_m3266446757(L_9, _stringLiteral1771024816, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			bool L_11 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_00a6;
			}
		}

IL_0073:
		{
			Competitor_t2775849210 * L_12 = V_4;
			GameObject_t1113636619 * L_13 = Component_get_gameObject_m442555142(L_12, /*hidden argument*/NULL);
			Transform_t3600365921 * L_14 = GameObject_get_transform_m1369836730(L_13, /*hidden argument*/NULL);
			Vector3_t3722313464  L_15 = Transform_get_position_m36019626(L_14, /*hidden argument*/NULL);
			ItemManager_t3254073967 * L_16 = V_1;
			GameObject_t1113636619 * L_17 = L_16->get_flag_10();
			Transform_t3600365921 * L_18 = GameObject_get_transform_m1369836730(L_17, /*hidden argument*/NULL);
			Vector3_t3722313464  L_19 = Transform_get_position_m36019626(L_18, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			float L_20 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_15, L_19, /*hidden argument*/NULL);
			V_6 = L_20;
			float L_21 = V_6;
			float L_22 = V_2;
			if ((!(((float)L_21) < ((float)L_22))))
			{
				goto IL_00a6;
			}
		}

IL_00a3:
		{
			Competitor_t2775849210 * L_23 = V_4;
			V_3 = L_23;
		}

IL_00a6:
		{
			bool L_24 = Enumerator_MoveNext_m860927647((Enumerator_t1842200533 *)(&V_5), /*hidden argument*/Enumerator_MoveNext_m860927647_RuntimeMethod_var);
			if (L_24)
			{
				goto IL_004e;
			}
		}

IL_00b2:
		{
			IL2CPP_LEAVE(0xC5, FINALLY_00b7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00b7;
	}

FINALLY_00b7:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m446944907((Enumerator_t1842200533 *)(&V_5), /*hidden argument*/Enumerator_Dispose_m446944907_RuntimeMethod_var);
		IL2CPP_END_FINALLY(183)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(183)
	{
		IL2CPP_JUMP_TBL(0xC5, IL_00c5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00c5:
	{
		Competitor_t2775849210 * L_25 = V_3;
		return L_25;
	}
}
// UtilityAI.DecisionType UtilityAI.FIND_FLAG::GetDecisionType()
extern "C" IL2CPP_METHOD_ATTR int32_t FIND_FLAG_GetDecisionType_m2868180156 (FIND_FLAG_t1938094438 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_type_4();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UtilityAI.FIND_FOOD::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FIND_FOOD__ctor_m2063949906 (FIND_FOOD_t1534940982 * __this, const RuntimeMethod* method)
{
	{
		__this->set_type_4(1);
		__this->set_maxDistanceForAffect_5((20.0f));
		__this->set_maxDistanceWeighting_6((0.3f));
		__this->set_maxHungerWeighting_7((0.7f));
		Decision__ctor_m1412182017(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UtilityAI.FIND_FOOD::GetQualityWeighting()
extern "C" IL2CPP_METHOD_ATTR float FIND_FOOD_GetQualityWeighting_m3274836651 (FIND_FOOD_t1534940982 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_FOOD_GetQualityWeighting_m3274836651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Competitor_t2775849210 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = FIND_FOOD_GetNearestFood_m2802532437(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (0.0f);
	}

IL_0017:
	{
		GameObject_t1113636619 * L_2 = FIND_FOOD_GetNearestFood_m2802532437(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = GameObject_get_transform_m1369836730(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_8 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = V_0;
		float L_10 = __this->get_maxDistanceForAffect_5();
		if ((!(((float)L_9) > ((float)L_10))))
		{
			goto IL_0054;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_0064;
	}

IL_0054:
	{
		float L_11 = __this->get_maxDistanceForAffect_5();
		float L_12 = V_0;
		float L_13 = __this->get_maxDistanceForAffect_5();
		V_1 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_11, (float)L_12))/(float)L_13));
	}

IL_0064:
	{
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Component_t1923634451 * L_15 = GameObject_GetComponent_m3266446757(L_14, _stringLiteral1771024816, /*hidden argument*/NULL);
		V_2 = ((Competitor_t2775849210 *)CastclassClass((RuntimeObject*)L_15, Competitor_t2775849210_il2cpp_TypeInfo_var));
		Competitor_t2775849210 * L_16 = V_2;
		float L_17 = L_16->get_maxHunger_10();
		Competitor_t2775849210 * L_18 = V_2;
		float L_19 = L_18->get_hunger_13();
		Competitor_t2775849210 * L_20 = V_2;
		float L_21 = L_20->get_maxHunger_10();
		V_3 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_17, (float)L_19))/(float)L_21));
		float L_22 = V_1;
		float L_23 = __this->get_maxDistanceWeighting_6();
		V_4 = ((float)il2cpp_codegen_multiply((float)L_22, (float)L_23));
		float L_24 = V_3;
		float L_25 = __this->get_maxHungerWeighting_7();
		V_5 = ((float)il2cpp_codegen_multiply((float)L_24, (float)L_25));
		float L_26 = V_4;
		float L_27 = V_5;
		return ((float)il2cpp_codegen_add((float)L_26, (float)L_27));
	}
}
// System.Void UtilityAI.FIND_FOOD::ExecuteAction()
extern "C" IL2CPP_METHOD_ATTR void FIND_FOOD_ExecuteAction_m3824330897 (FIND_FOOD_t1534940982 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_FOOD_ExecuteAction_m3824330897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Competitor_t2775849210 * V_0 = NULL;
	NavMeshAgent_t1276799816 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral1771024816, /*hidden argument*/NULL);
		V_0 = ((Competitor_t2775849210 *)CastclassClass((RuntimeObject*)L_1, Competitor_t2775849210_il2cpp_TypeInfo_var));
		Competitor_t2775849210 * L_2 = V_0;
		NavMeshAgent_t1276799816 * L_3 = Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532(L_2, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532_RuntimeMethod_var);
		V_1 = L_3;
		NavMeshAgent_t1276799816 * L_4 = V_1;
		GameObject_t1113636619 * L_5 = FIND_FOOD_GetNearestFood_m2802532437(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = GameObject_get_transform_m1369836730(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		NavMeshAgent_set_destination_m41262300(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject UtilityAI.FIND_FOOD::GetNearestFood()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * FIND_FOOD_GetNearestFood_m2802532437 (FIND_FOOD_t1534940982 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_FOOD_GetNearestFood_m2802532437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ItemManager_t3254073967 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	float V_2 = 0.0f;
	GameObject_t1113636619 * V_3 = NULL;
	Enumerator_t179987942  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2249249573, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral2249249573, /*hidden argument*/NULL);
		V_0 = ((ItemManager_t3254073967 *)CastclassClass((RuntimeObject*)L_1, ItemManager_t3254073967_il2cpp_TypeInfo_var));
		V_1 = (GameObject_t1113636619 *)NULL;
		V_2 = (std::numeric_limits<float>::max());
		ItemManager_t3254073967 * L_2 = V_0;
		List_1_t2585711361 * L_3 = L_2->get_food_8();
		Enumerator_t179987942  L_4 = List_1_GetEnumerator_m1750140655(L_3, /*hidden argument*/List_1_GetEnumerator_m1750140655_RuntimeMethod_var);
		V_4 = L_4;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006b;
		}

IL_0034:
		{
			GameObject_t1113636619 * L_5 = Enumerator_get_Current_m4179928398((Enumerator_t179987942 *)(&V_4), /*hidden argument*/Enumerator_get_Current_m4179928398_RuntimeMethod_var);
			V_3 = L_5;
			GameObject_t1113636619 * L_6 = V_3;
			Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730(L_6, /*hidden argument*/NULL);
			Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
			GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
			Transform_t3600365921 * L_10 = GameObject_get_transform_m1369836730(L_9, /*hidden argument*/NULL);
			Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			float L_12 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_8, L_11, /*hidden argument*/NULL);
			V_5 = L_12;
			float L_13 = V_5;
			float L_14 = V_2;
			if ((!(((float)L_13) < ((float)L_14))))
			{
				goto IL_006b;
			}
		}

IL_0066:
		{
			float L_15 = V_5;
			V_2 = L_15;
			GameObject_t1113636619 * L_16 = V_3;
			V_1 = L_16;
		}

IL_006b:
		{
			bool L_17 = Enumerator_MoveNext_m4286844348((Enumerator_t179987942 *)(&V_4), /*hidden argument*/Enumerator_MoveNext_m4286844348_RuntimeMethod_var);
			if (L_17)
			{
				goto IL_0034;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_007c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1341201278((Enumerator_t179987942 *)(&V_4), /*hidden argument*/Enumerator_Dispose_m1341201278_RuntimeMethod_var);
		IL2CPP_END_FINALLY(124)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_008a:
	{
		GameObject_t1113636619 * L_18 = V_1;
		return L_18;
	}
}
// UtilityAI.DecisionType UtilityAI.FIND_FOOD::GetDecisionType()
extern "C" IL2CPP_METHOD_ATTR int32_t FIND_FOOD_GetDecisionType_m1721334263 (FIND_FOOD_t1534940982 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_type_4();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UtilityAI.FIND_WATER::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FIND_WATER__ctor_m3442072258 (FIND_WATER_t1027203745 * __this, const RuntimeMethod* method)
{
	{
		__this->set_maxDistanceForAffect_5((10.0f));
		__this->set_maxDistanceWeighting_6((0.2f));
		__this->set_maxThirstWeighting_7((0.8f));
		Decision__ctor_m1412182017(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UtilityAI.FIND_WATER::GetQualityWeighting()
extern "C" IL2CPP_METHOD_ATTR float FIND_WATER_GetQualityWeighting_m212962131 (FIND_WATER_t1027203745 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_WATER_GetQualityWeighting_m212962131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Competitor_t2775849210 * V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = FIND_WATER_GetNearestWater_m563822326(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (0.0f);
	}

IL_0017:
	{
		GameObject_t1113636619 * L_2 = FIND_WATER_GetNearestWater_m563822326(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = GameObject_get_transform_m1369836730(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_8 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = V_0;
		float L_10 = __this->get_maxDistanceForAffect_5();
		if ((!(((float)L_9) > ((float)L_10))))
		{
			goto IL_0054;
		}
	}
	{
		V_1 = (0.0f);
		goto IL_0064;
	}

IL_0054:
	{
		float L_11 = __this->get_maxDistanceForAffect_5();
		float L_12 = V_0;
		float L_13 = __this->get_maxDistanceForAffect_5();
		V_1 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_11, (float)L_12))/(float)L_13));
	}

IL_0064:
	{
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Component_t1923634451 * L_15 = GameObject_GetComponent_m3266446757(L_14, _stringLiteral1771024816, /*hidden argument*/NULL);
		V_2 = ((Competitor_t2775849210 *)CastclassClass((RuntimeObject*)L_15, Competitor_t2775849210_il2cpp_TypeInfo_var));
		Competitor_t2775849210 * L_16 = V_2;
		float L_17 = L_16->get_maxThirst_11();
		Competitor_t2775849210 * L_18 = V_2;
		float L_19 = L_18->get_thirst_14();
		Competitor_t2775849210 * L_20 = V_2;
		float L_21 = L_20->get_maxThirst_11();
		V_3 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_17, (float)L_19))/(float)L_21));
		float L_22 = V_1;
		float L_23 = __this->get_maxDistanceWeighting_6();
		V_4 = ((float)il2cpp_codegen_multiply((float)L_22, (float)L_23));
		float L_24 = V_3;
		float L_25 = __this->get_maxThirstWeighting_7();
		V_5 = ((float)il2cpp_codegen_multiply((float)L_24, (float)L_25));
		float L_26 = V_4;
		float L_27 = V_5;
		return ((float)il2cpp_codegen_add((float)L_26, (float)L_27));
	}
}
// System.Void UtilityAI.FIND_WATER::ExecuteAction()
extern "C" IL2CPP_METHOD_ATTR void FIND_WATER_ExecuteAction_m983409832 (FIND_WATER_t1027203745 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_WATER_ExecuteAction_m983409832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Competitor_t2775849210 * V_0 = NULL;
	NavMeshAgent_t1276799816 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral1771024816, /*hidden argument*/NULL);
		V_0 = ((Competitor_t2775849210 *)CastclassClass((RuntimeObject*)L_1, Competitor_t2775849210_il2cpp_TypeInfo_var));
		Competitor_t2775849210 * L_2 = V_0;
		NavMeshAgent_t1276799816 * L_3 = Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532(L_2, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532_RuntimeMethod_var);
		V_1 = L_3;
		NavMeshAgent_t1276799816 * L_4 = V_1;
		GameObject_t1113636619 * L_5 = FIND_WATER_GetNearestWater_m563822326(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_6 = GameObject_get_transform_m1369836730(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		NavMeshAgent_set_destination_m41262300(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject UtilityAI.FIND_WATER::GetNearestWater()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * FIND_WATER_GetNearestWater_m563822326 (FIND_WATER_t1027203745 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FIND_WATER_GetNearestWater_m563822326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ItemManager_t3254073967 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	float V_2 = 0.0f;
	GameObject_t1113636619 * V_3 = NULL;
	Enumerator_t179987942  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2249249573, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral2249249573, /*hidden argument*/NULL);
		V_0 = ((ItemManager_t3254073967 *)CastclassClass((RuntimeObject*)L_1, ItemManager_t3254073967_il2cpp_TypeInfo_var));
		V_1 = (GameObject_t1113636619 *)NULL;
		V_2 = (std::numeric_limits<float>::max());
		ItemManager_t3254073967 * L_2 = V_0;
		List_1_t2585711361 * L_3 = L_2->get_water_7();
		Enumerator_t179987942  L_4 = List_1_GetEnumerator_m1750140655(L_3, /*hidden argument*/List_1_GetEnumerator_m1750140655_RuntimeMethod_var);
		V_4 = L_4;
	}

IL_002f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006b;
		}

IL_0034:
		{
			GameObject_t1113636619 * L_5 = Enumerator_get_Current_m4179928398((Enumerator_t179987942 *)(&V_4), /*hidden argument*/Enumerator_get_Current_m4179928398_RuntimeMethod_var);
			V_3 = L_5;
			GameObject_t1113636619 * L_6 = V_3;
			Transform_t3600365921 * L_7 = GameObject_get_transform_m1369836730(L_6, /*hidden argument*/NULL);
			Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
			GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
			Transform_t3600365921 * L_10 = GameObject_get_transform_m1369836730(L_9, /*hidden argument*/NULL);
			Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			float L_12 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_8, L_11, /*hidden argument*/NULL);
			V_5 = L_12;
			float L_13 = V_5;
			float L_14 = V_2;
			if ((!(((float)L_13) < ((float)L_14))))
			{
				goto IL_006b;
			}
		}

IL_0066:
		{
			float L_15 = V_5;
			V_2 = L_15;
			GameObject_t1113636619 * L_16 = V_3;
			V_1 = L_16;
		}

IL_006b:
		{
			bool L_17 = Enumerator_MoveNext_m4286844348((Enumerator_t179987942 *)(&V_4), /*hidden argument*/Enumerator_MoveNext_m4286844348_RuntimeMethod_var);
			if (L_17)
			{
				goto IL_0034;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x8A, FINALLY_007c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007c;
	}

FINALLY_007c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1341201278((Enumerator_t179987942 *)(&V_4), /*hidden argument*/Enumerator_Dispose_m1341201278_RuntimeMethod_var);
		IL2CPP_END_FINALLY(124)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(124)
	{
		IL2CPP_JUMP_TBL(0x8A, IL_008a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_008a:
	{
		GameObject_t1113636619 * L_18 = V_1;
		return L_18;
	}
}
// UtilityAI.DecisionType UtilityAI.FIND_WATER::GetDecisionType()
extern "C" IL2CPP_METHOD_ATTR int32_t FIND_WATER_GetDecisionType_m1038546088 (FIND_WATER_t1027203745 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_type_4();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Water::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Water__ctor_m1387834787 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		__this->set_thirstBoost_4((2.0f));
		PickupableItem__ctor_m2278186837(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::PickedUpItem(Competitor)
extern "C" IL2CPP_METHOD_ATTR void Water_PickedUpItem_m3903491620 (Water_t1083516957 * __this, Competitor_t2775849210 * ___competitor0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_PickedUpItem_m3903491620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ItemManager_t3254073967 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2249249573, /*hidden argument*/NULL);
		Component_t1923634451 * L_1 = GameObject_GetComponent_m3266446757(L_0, _stringLiteral2249249573, /*hidden argument*/NULL);
		V_0 = ((ItemManager_t3254073967 *)CastclassClass((RuntimeObject*)L_1, ItemManager_t3254073967_il2cpp_TypeInfo_var));
		ItemManager_t3254073967 * L_2 = V_0;
		ItemManager_SpawnRandomWater_m3398524623(L_2, /*hidden argument*/NULL);
		ItemManager_t3254073967 * L_3 = V_0;
		List_1_t2585711361 * L_4 = L_3->get_water_7();
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		List_1_Remove_m4063777476(L_4, L_5, /*hidden argument*/List_1_Remove_m4063777476_RuntimeMethod_var);
		Competitor_t2775849210 * L_6 = ___competitor0;
		Competitor_t2775849210 * L_7 = L_6;
		float L_8 = L_7->get_thirst_14();
		float L_9 = __this->get_thirstBoost_4();
		L_7->set_thirst_14(((float)il2cpp_codegen_add((float)L_8, (float)L_9)));
		Competitor_t2775849210 * L_10 = ___competitor0;
		float L_11 = L_10->get_thirst_14();
		Competitor_t2775849210 * L_12 = ___competitor0;
		float L_13 = L_12->get_maxThirst_11();
		if ((!(((float)L_11) > ((float)L_13))))
		{
			goto IL_0062;
		}
	}
	{
		Competitor_t2775849210 * L_14 = ___competitor0;
		Competitor_t2775849210 * L_15 = ___competitor0;
		float L_16 = L_15->get_maxThirst_11();
		L_14->set_thirst_14(L_16);
	}

IL_0062:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif

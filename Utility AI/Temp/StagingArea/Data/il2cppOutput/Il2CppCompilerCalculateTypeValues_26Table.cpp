﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// CameraManager
struct CameraManager_t3272490737;
// Competitor
struct Competitor_t2775849210;
// CompetitorManager
struct CompetitorManager_t1763731731;
// CompetitorUI
struct CompetitorUI_t472322071;
// DeathTracker
struct DeathTracker_t1220569700;
// EnvMapAnimator
struct EnvMapAnimator_t1140999784;
// ItemManager
struct ItemManager_t3254073967;
// MapGen.MapGeneration
struct MapGeneration_t47126671;
// MapGen.MapGrid
struct MapGrid_t3370239467;
// MapGen.Room
struct Room_t1080437681;
// MapManager
struct MapManager_t3095496401;
// Pixelplacement.DisplayObject
struct DisplayObject_t314287876;
// Pixelplacement.StateMachine
struct StateMachine_t4243775295;
// SimpleHealthBar
struct SimpleHealthBar_t721070758;
// SimpleHealthBar_SpaceshipExample.AsteroidController
struct AsteroidController_t3828879615;
// SimpleHealthBar_SpaceshipExample.GameManager
struct GameManager_t3924726104;
// SimpleHealthBar_SpaceshipExample.HealthPickupController
struct HealthPickupController_t1709822218;
// SimpleHealthBar_SpaceshipExample.PlayerController
struct PlayerController_t2576101795;
// SimpleHealthBar_SpaceshipExample.PlayerHealth
struct PlayerHealth_t992877501;
// System.Action
struct Action_t1264377477;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<Relative,MapGen.Door>
struct Dictionary_2_t339125784;
// System.Collections.Generic.Dictionary`2<Relative,System.Int32>
struct Dictionary_2_t286520093;
// System.Collections.Generic.Dictionary`2<System.Int32,MapGen.Room>
struct Dictionary_2_t4264118308;
// System.Collections.Generic.List`1<Competitor>
struct List_1_t4247923952;
// System.Collections.Generic.List`1<MapGen.Room>
struct List_1_t2552512423;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshLink>
struct List_1_t671749682;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier>
struct List_1_t2482780542;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume>
struct List_1_t470994630;
// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface>
struct List_1_t2459021783;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UtilityAI.Decision>
struct List_1_t3817059745;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Predicate`1<UnityEngine.AI.NavMeshBuildSource>
struct Predicate_1_t1513480349;
// System.Predicate`1<UnityEngine.AI.NavMeshModifier>
struct Predicate_1_t1835999924;
// System.Predicate`1<UnityEngine.AI.NavMeshModifierVolume>
struct Predicate_1_t4119181308;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.Examples.Benchmark01
struct Benchmark01_t1571072624;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3264177817;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t3617420994;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_t3460249701;
// TMPro.Examples.TeleType
struct TeleType_t2409835159;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t3766250034;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t845872552;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t1869054637;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t3109943174;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_t2868010532;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_t1590929858;
// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct SpriteSelectionEvent_t2798445241;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t1841909953;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.TextContainer
struct TextContainer_t97923372;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UIManager
struct UIManager_t1042050227;
// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate
struct OnNavMeshPreUpdate_t1580782682;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// UnityEngine.AI.NavMeshData
struct NavMeshData_t1084598030;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UtilityAI.Decision
struct Decision_t2344985003;
// UtilityAI.DecisionManager
struct DecisionManager_t2018219593;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MAPGRID_T3370239467_H
#define MAPGRID_T3370239467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapGen.MapGrid
struct  MapGrid_t3370239467  : public RuntimeObject
{
public:
	// System.Boolean MapGen.MapGrid::visualise
	bool ___visualise_0;
	// System.Single MapGen.MapGrid::spacing
	float ___spacing_1;
	// System.Int32 MapGen.MapGrid::GridWidthHeight
	int32_t ___GridWidthHeight_2;
	// System.Int32 MapGen.MapGrid::Size
	int32_t ___Size_3;

public:
	inline static int32_t get_offset_of_visualise_0() { return static_cast<int32_t>(offsetof(MapGrid_t3370239467, ___visualise_0)); }
	inline bool get_visualise_0() const { return ___visualise_0; }
	inline bool* get_address_of_visualise_0() { return &___visualise_0; }
	inline void set_visualise_0(bool value)
	{
		___visualise_0 = value;
	}

	inline static int32_t get_offset_of_spacing_1() { return static_cast<int32_t>(offsetof(MapGrid_t3370239467, ___spacing_1)); }
	inline float get_spacing_1() const { return ___spacing_1; }
	inline float* get_address_of_spacing_1() { return &___spacing_1; }
	inline void set_spacing_1(float value)
	{
		___spacing_1 = value;
	}

	inline static int32_t get_offset_of_GridWidthHeight_2() { return static_cast<int32_t>(offsetof(MapGrid_t3370239467, ___GridWidthHeight_2)); }
	inline int32_t get_GridWidthHeight_2() const { return ___GridWidthHeight_2; }
	inline int32_t* get_address_of_GridWidthHeight_2() { return &___GridWidthHeight_2; }
	inline void set_GridWidthHeight_2(int32_t value)
	{
		___GridWidthHeight_2 = value;
	}

	inline static int32_t get_offset_of_Size_3() { return static_cast<int32_t>(offsetof(MapGrid_t3370239467, ___Size_3)); }
	inline int32_t get_Size_3() const { return ___Size_3; }
	inline int32_t* get_address_of_Size_3() { return &___Size_3; }
	inline void set_Size_3(int32_t value)
	{
		___Size_3 = value;
	}
};

struct MapGrid_t3370239467_StaticFields
{
public:
	// System.Int32 MapGen.MapGrid::MiddleRoom
	int32_t ___MiddleRoom_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,MapGen.Room> MapGen.MapGrid::Rooms
	Dictionary_2_t4264118308 * ___Rooms_5;

public:
	inline static int32_t get_offset_of_MiddleRoom_4() { return static_cast<int32_t>(offsetof(MapGrid_t3370239467_StaticFields, ___MiddleRoom_4)); }
	inline int32_t get_MiddleRoom_4() const { return ___MiddleRoom_4; }
	inline int32_t* get_address_of_MiddleRoom_4() { return &___MiddleRoom_4; }
	inline void set_MiddleRoom_4(int32_t value)
	{
		___MiddleRoom_4 = value;
	}

	inline static int32_t get_offset_of_Rooms_5() { return static_cast<int32_t>(offsetof(MapGrid_t3370239467_StaticFields, ___Rooms_5)); }
	inline Dictionary_2_t4264118308 * get_Rooms_5() const { return ___Rooms_5; }
	inline Dictionary_2_t4264118308 ** get_address_of_Rooms_5() { return &___Rooms_5; }
	inline void set_Rooms_5(Dictionary_2_t4264118308 * value)
	{
		___Rooms_5 = value;
		Il2CppCodeGenWriteBarrier((&___Rooms_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPGRID_T3370239467_H
#ifndef U3CRESETMAPU3EC__ITERATOR0_T1993735930_H
#define U3CRESETMAPU3EC__ITERATOR0_T1993735930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager/<ResetMap>c__Iterator0
struct  U3CResetMapU3Ec__Iterator0_t1993735930  : public RuntimeObject
{
public:
	// MapGen.MapGeneration MapManager/<ResetMap>c__Iterator0::<mapGenerator>__0
	MapGeneration_t47126671 * ___U3CmapGeneratorU3E__0_0;
	// CompetitorManager MapManager/<ResetMap>c__Iterator0::<competitorManager>__0
	CompetitorManager_t1763731731 * ___U3CcompetitorManagerU3E__0_1;
	// ItemManager MapManager/<ResetMap>c__Iterator0::<itemManager>__0
	ItemManager_t3254073967 * ___U3CitemManagerU3E__0_2;
	// UIManager MapManager/<ResetMap>c__Iterator0::<uiManager>__0
	UIManager_t1042050227 * ___U3CuiManagerU3E__0_3;
	// MapManager MapManager/<ResetMap>c__Iterator0::$this
	MapManager_t3095496401 * ___U24this_4;
	// System.Object MapManager/<ResetMap>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean MapManager/<ResetMap>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 MapManager/<ResetMap>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CmapGeneratorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U3CmapGeneratorU3E__0_0)); }
	inline MapGeneration_t47126671 * get_U3CmapGeneratorU3E__0_0() const { return ___U3CmapGeneratorU3E__0_0; }
	inline MapGeneration_t47126671 ** get_address_of_U3CmapGeneratorU3E__0_0() { return &___U3CmapGeneratorU3E__0_0; }
	inline void set_U3CmapGeneratorU3E__0_0(MapGeneration_t47126671 * value)
	{
		___U3CmapGeneratorU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmapGeneratorU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcompetitorManagerU3E__0_1() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U3CcompetitorManagerU3E__0_1)); }
	inline CompetitorManager_t1763731731 * get_U3CcompetitorManagerU3E__0_1() const { return ___U3CcompetitorManagerU3E__0_1; }
	inline CompetitorManager_t1763731731 ** get_address_of_U3CcompetitorManagerU3E__0_1() { return &___U3CcompetitorManagerU3E__0_1; }
	inline void set_U3CcompetitorManagerU3E__0_1(CompetitorManager_t1763731731 * value)
	{
		___U3CcompetitorManagerU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcompetitorManagerU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CitemManagerU3E__0_2() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U3CitemManagerU3E__0_2)); }
	inline ItemManager_t3254073967 * get_U3CitemManagerU3E__0_2() const { return ___U3CitemManagerU3E__0_2; }
	inline ItemManager_t3254073967 ** get_address_of_U3CitemManagerU3E__0_2() { return &___U3CitemManagerU3E__0_2; }
	inline void set_U3CitemManagerU3E__0_2(ItemManager_t3254073967 * value)
	{
		___U3CitemManagerU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemManagerU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CuiManagerU3E__0_3() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U3CuiManagerU3E__0_3)); }
	inline UIManager_t1042050227 * get_U3CuiManagerU3E__0_3() const { return ___U3CuiManagerU3E__0_3; }
	inline UIManager_t1042050227 ** get_address_of_U3CuiManagerU3E__0_3() { return &___U3CuiManagerU3E__0_3; }
	inline void set_U3CuiManagerU3E__0_3(UIManager_t1042050227 * value)
	{
		___U3CuiManagerU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuiManagerU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U24this_4)); }
	inline MapManager_t3095496401 * get_U24this_4() const { return ___U24this_4; }
	inline MapManager_t3095496401 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MapManager_t3095496401 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CResetMapU3Ec__Iterator0_t1993735930, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETMAPU3EC__ITERATOR0_T1993735930_H
#ifndef COREMATH_T1603124386_H
#define COREMATH_T1603124386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.CoreMath
struct  CoreMath_t1603124386  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COREMATH_T1603124386_H
#ifndef LAYERMASKHELPER_T4276256109_H
#define LAYERMASKHELPER_T4276256109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.LayerMaskHelper
struct  LayerMaskHelper_t4276256109  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASKHELPER_T4276256109_H
#ifndef U3CDELAYINITIALDESTRUCTIONU3EC__ITERATOR0_T2232836592_H
#define U3CDELAYINITIALDESTRUCTIONU3EC__ITERATOR0_T2232836592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>c__Iterator0
struct  U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592  : public RuntimeObject
{
public:
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>c__Iterator0::delayTime
	float ___delayTime_0;
	// SimpleHealthBar_SpaceshipExample.AsteroidController SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>c__Iterator0::$this
	AsteroidController_t3828879615 * ___U24this_1;
	// System.Object SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SimpleHealthBar_SpaceshipExample.AsteroidController/<DelayInitialDestruction>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_delayTime_0() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592, ___delayTime_0)); }
	inline float get_delayTime_0() const { return ___delayTime_0; }
	inline float* get_address_of_delayTime_0() { return &___delayTime_0; }
	inline void set_delayTime_0(float value)
	{
		___delayTime_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592, ___U24this_1)); }
	inline AsteroidController_t3828879615 * get_U24this_1() const { return ___U24this_1; }
	inline AsteroidController_t3828879615 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AsteroidController_t3828879615 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYINITIALDESTRUCTIONU3EC__ITERATOR0_T2232836592_H
#ifndef U3CSPAWNHEALTHTIMERU3EC__ITERATOR0_T4082045009_H
#define U3CSPAWNHEALTHTIMERU3EC__ITERATOR0_T4082045009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>c__Iterator0
struct  U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009  : public RuntimeObject
{
public:
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>c__Iterator0::$this
	GameManager_t3924726104 * ___U24this_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<SpawnHealthTimer>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009, ___U24this_0)); }
	inline GameManager_t3924726104 * get_U24this_0() const { return ___U24this_0; }
	inline GameManager_t3924726104 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameManager_t3924726104 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNHEALTHTIMERU3EC__ITERATOR0_T4082045009_H
#ifndef U3CSPAWNTIMERU3EC__ITERATOR1_T3789933097_H
#define U3CSPAWNTIMERU3EC__ITERATOR1_T3789933097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>c__Iterator1
struct  U3CSpawnTimerU3Ec__Iterator1_t3789933097  : public RuntimeObject
{
public:
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>c__Iterator1::$this
	GameManager_t3924726104 * ___U24this_0;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<SpawnTimer>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ec__Iterator1_t3789933097, ___U24this_0)); }
	inline GameManager_t3924726104 * get_U24this_0() const { return ___U24this_0; }
	inline GameManager_t3924726104 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameManager_t3924726104 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ec__Iterator1_t3789933097, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ec__Iterator1_t3789933097, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSpawnTimerU3Ec__Iterator1_t3789933097, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNTIMERU3EC__ITERATOR1_T3789933097_H
#ifndef U3CDELAYINITIALDESTRUCTIONU3EC__ITERATOR0_T2740395325_H
#define U3CDELAYINITIALDESTRUCTIONU3EC__ITERATOR0_T2740395325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>c__Iterator0
struct  U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325  : public RuntimeObject
{
public:
	// System.Single SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>c__Iterator0::delayTime
	float ___delayTime_0;
	// SimpleHealthBar_SpaceshipExample.HealthPickupController SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>c__Iterator0::$this
	HealthPickupController_t1709822218 * ___U24this_1;
	// System.Object SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SimpleHealthBar_SpaceshipExample.HealthPickupController/<DelayInitialDestruction>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_delayTime_0() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325, ___delayTime_0)); }
	inline float get_delayTime_0() const { return ___delayTime_0; }
	inline float* get_address_of_delayTime_0() { return &___delayTime_0; }
	inline void set_delayTime_0(float value)
	{
		___delayTime_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325, ___U24this_1)); }
	inline HealthPickupController_t1709822218 * get_U24this_1() const { return ___U24this_1; }
	inline HealthPickupController_t1709822218 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HealthPickupController_t1709822218 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYINITIALDESTRUCTIONU3EC__ITERATOR0_T2740395325_H
#ifndef U3CDELAYOVERHEATU3EC__ITERATOR0_T3589708321_H
#define U3CDELAYOVERHEATU3EC__ITERATOR0_T3589708321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>c__Iterator0
struct  U3CDelayOverheatU3Ec__Iterator0_t3589708321  : public RuntimeObject
{
public:
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>c__Iterator0::<t>__1
	float ___U3CtU3E__1_0;
	// SimpleHealthBar_SpaceshipExample.PlayerController SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>c__Iterator0::$this
	PlayerController_t2576101795 * ___U24this_1;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerController/<DelayOverheat>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__1_0() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ec__Iterator0_t3589708321, ___U3CtU3E__1_0)); }
	inline float get_U3CtU3E__1_0() const { return ___U3CtU3E__1_0; }
	inline float* get_address_of_U3CtU3E__1_0() { return &___U3CtU3E__1_0; }
	inline void set_U3CtU3E__1_0(float value)
	{
		___U3CtU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ec__Iterator0_t3589708321, ___U24this_1)); }
	inline PlayerController_t2576101795 * get_U24this_1() const { return ___U24this_1; }
	inline PlayerController_t2576101795 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PlayerController_t2576101795 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ec__Iterator0_t3589708321, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ec__Iterator0_t3589708321, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayOverheatU3Ec__Iterator0_t3589708321, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYOVERHEATU3EC__ITERATOR0_T3589708321_H
#ifndef U3CINVULNERABLILTYU3EC__ITERATOR0_T1065921800_H
#define U3CINVULNERABLILTYU3EC__ITERATOR0_T1065921800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>c__Iterator0
struct  U3CInvulnerabliltyU3Ec__Iterator0_t1065921800  : public RuntimeObject
{
public:
	// SimpleHealthBar_SpaceshipExample.PlayerHealth SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>c__Iterator0::$this
	PlayerHealth_t992877501 * ___U24this_0;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth/<Invulnerablilty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ec__Iterator0_t1065921800, ___U24this_0)); }
	inline PlayerHealth_t992877501 * get_U24this_0() const { return ___U24this_0; }
	inline PlayerHealth_t992877501 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PlayerHealth_t992877501 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ec__Iterator0_t1065921800, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ec__Iterator0_t1065921800, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CInvulnerabliltyU3Ec__Iterator0_t1065921800, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVULNERABLILTYU3EC__ITERATOR0_T1065921800_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2216151886_H
#define U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2216151886  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t1571072624 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24this_1)); }
	inline Benchmark01_t1571072624 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t1571072624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t1571072624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2622988697_H
#define U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2622988697  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3264177817 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24this_1)); }
	inline Benchmark01_UGUI_t3264177817 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3264177817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3264177817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t4041402054  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t3617420994 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24this_1)); }
	inline ShaderPropAnimator_t3617420994 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t3617420994 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t3617420994 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifndef U3CSTARTU3EC__ITERATOR0_T3341539328_H
#define U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3341539328  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_0;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_1;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_2;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>c__Iterator0::$this
	TeleType_t2409835159 * ___U24this_3;
	// System.Object TMPro.Examples.TeleType/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TMPro.Examples.TeleType/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CtotalVisibleCharactersU3E__0_0)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_0() const { return ___U3CtotalVisibleCharactersU3E__0_0; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_0() { return &___U3CtotalVisibleCharactersU3E__0_0; }
	inline void set_U3CtotalVisibleCharactersU3E__0_0(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CcounterU3E__0_1)); }
	inline int32_t get_U3CcounterU3E__0_1() const { return ___U3CcounterU3E__0_1; }
	inline int32_t* get_address_of_U3CcounterU3E__0_1() { return &___U3CcounterU3E__0_1; }
	inline void set_U3CcounterU3E__0_1(int32_t value)
	{
		___U3CcounterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U3CvisibleCountU3E__0_2)); }
	inline int32_t get_U3CvisibleCountU3E__0_2() const { return ___U3CvisibleCountU3E__0_2; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_2() { return &___U3CvisibleCountU3E__0_2; }
	inline void set_U3CvisibleCountU3E__0_2(int32_t value)
	{
		___U3CvisibleCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24this_3)); }
	inline TeleType_t2409835159 * get_U24this_3() const { return ___U24this_3; }
	inline TeleType_t2409835159 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TeleType_t2409835159 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3341539328, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T3341539328_H
#ifndef U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#define U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0
struct  U3CRevealCharactersU3Ec__Iterator0_t860191687  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_3;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$this
	TextConsoleSimulator_t3766250034 * ___U24this_4;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtextInfoU3E__0_1)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__0_1() const { return ___U3CtextInfoU3E__0_1; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__0_1() { return &___U3CtextInfoU3E__0_1; }
	inline void set_U3CtextInfoU3E__0_1(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U3CvisibleCountU3E__0_3)); }
	inline int32_t get_U3CvisibleCountU3E__0_3() const { return ___U3CvisibleCountU3E__0_3; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_3() { return &___U3CvisibleCountU3E__0_3; }
	inline void set_U3CvisibleCountU3E__0_3(int32_t value)
	{
		___U3CvisibleCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24this_4)); }
	inline TextConsoleSimulator_t3766250034 * get_U24this_4() const { return ___U24this_4; }
	inline TextConsoleSimulator_t3766250034 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TextConsoleSimulator_t3766250034 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t860191687, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALCHARACTERSU3EC__ITERATOR0_T860191687_H
#ifndef U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#define U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1
struct  U3CRevealWordsU3Ec__Iterator1_t1343183262  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalWordCount>__0
	int32_t ___U3CtotalWordCountU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<counter>__0
	int32_t ___U3CcounterU3E__0_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<currentWord>__0
	int32_t ___U3CcurrentWordU3E__0_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_5;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalWordCountU3E__0_1)); }
	inline int32_t get_U3CtotalWordCountU3E__0_1() const { return ___U3CtotalWordCountU3E__0_1; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E__0_1() { return &___U3CtotalWordCountU3E__0_1; }
	inline void set_U3CtotalWordCountU3E__0_1(int32_t value)
	{
		___U3CtotalWordCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcounterU3E__0_3)); }
	inline int32_t get_U3CcounterU3E__0_3() const { return ___U3CcounterU3E__0_3; }
	inline int32_t* get_address_of_U3CcounterU3E__0_3() { return &___U3CcounterU3E__0_3; }
	inline void set_U3CcounterU3E__0_3(int32_t value)
	{
		___U3CcounterU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWordU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CcurrentWordU3E__0_4)); }
	inline int32_t get_U3CcurrentWordU3E__0_4() const { return ___U3CcurrentWordU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentWordU3E__0_4() { return &___U3CcurrentWordU3E__0_4; }
	inline void set_U3CcurrentWordU3E__0_4(int32_t value)
	{
		___U3CcurrentWordU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U3CvisibleCountU3E__0_5)); }
	inline int32_t get_U3CvisibleCountU3E__0_5() const { return ___U3CvisibleCountU3E__0_5; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_5() { return &___U3CvisibleCountU3E__0_5; }
	inline void set_U3CvisibleCountU3E__0_5(int32_t value)
	{
		___U3CvisibleCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t1343183262, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALWORDSU3EC__ITERATOR1_T1343183262_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NAVMESHDATAINSTANCE_T1498462893_H
#define NAVMESHDATAINSTANCE_T1498462893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshDataInstance
struct  NavMeshDataInstance_t1498462893 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshDataInstance::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(NavMeshDataInstance_t1498462893, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHDATAINSTANCE_T1498462893_H
#ifndef NAVMESHLINKINSTANCE_T3753324387_H
#define NAVMESHLINKINSTANCE_T3753324387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshLinkInstance
struct  NavMeshLinkInstance_t3753324387 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshLinkInstance::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(NavMeshLinkInstance_t3753324387, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHLINKINSTANCE_T3753324387_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_2_T1169440328_H
#define UNITYEVENT_2_T1169440328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct  UnityEvent_2_t1169440328  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1169440328, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1169440328_H
#ifndef UNITYEVENT_3_T1597070127_H
#define UNITYEVENT_3_T1597070127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t1597070127  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1597070127, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1597070127_H
#ifndef UNITYEVENT_3_T2493613095_H
#define UNITYEVENT_3_T2493613095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct  UnityEvent_3_t2493613095  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2493613095, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2493613095_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef DEATHTYPE_T3371451543_H
#define DEATHTYPE_T3371451543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathTracker/DeathType
struct  DeathType_t3371451543 
{
public:
	// System.Int32 DeathTracker/DeathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DeathType_t3371451543, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHTYPE_T3371451543_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1520811813_H
#define U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1520811813  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t1817901843  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1140999784 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24this_1)); }
	inline EnvMapAnimator_t1140999784 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1140999784 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1140999784 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifndef ROOM_T1080437681_H
#define ROOM_T1080437681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapGen.Room
struct  Room_t1080437681  : public RuntimeObject
{
public:
	// System.Int32 MapGen.Room::ID
	int32_t ___ID_0;
	// System.Boolean MapGen.Room::<Activated>k__BackingField
	bool ___U3CActivatedU3Ek__BackingField_1;
	// UnityEngine.Vector3 MapGen.Room::Position
	Vector3_t3722313464  ___Position_2;
	// UnityEngine.GameObject MapGen.Room::RoomVisualiser
	GameObject_t1113636619 * ___RoomVisualiser_3;
	// System.Collections.Generic.Dictionary`2<Relative,MapGen.Door> MapGen.Room::Doors
	Dictionary_2_t339125784 * ___Doors_4;
	// System.Collections.Generic.Dictionary`2<Relative,System.Int32> MapGen.Room::ConnectedRooms
	Dictionary_2_t286520093 * ___ConnectedRooms_5;
	// System.Boolean MapGen.Room::Unlocked
	bool ___Unlocked_6;

public:
	inline static int32_t get_offset_of_ID_0() { return static_cast<int32_t>(offsetof(Room_t1080437681, ___ID_0)); }
	inline int32_t get_ID_0() const { return ___ID_0; }
	inline int32_t* get_address_of_ID_0() { return &___ID_0; }
	inline void set_ID_0(int32_t value)
	{
		___ID_0 = value;
	}

	inline static int32_t get_offset_of_U3CActivatedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Room_t1080437681, ___U3CActivatedU3Ek__BackingField_1)); }
	inline bool get_U3CActivatedU3Ek__BackingField_1() const { return ___U3CActivatedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CActivatedU3Ek__BackingField_1() { return &___U3CActivatedU3Ek__BackingField_1; }
	inline void set_U3CActivatedU3Ek__BackingField_1(bool value)
	{
		___U3CActivatedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(Room_t1080437681, ___Position_2)); }
	inline Vector3_t3722313464  get_Position_2() const { return ___Position_2; }
	inline Vector3_t3722313464 * get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(Vector3_t3722313464  value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_RoomVisualiser_3() { return static_cast<int32_t>(offsetof(Room_t1080437681, ___RoomVisualiser_3)); }
	inline GameObject_t1113636619 * get_RoomVisualiser_3() const { return ___RoomVisualiser_3; }
	inline GameObject_t1113636619 ** get_address_of_RoomVisualiser_3() { return &___RoomVisualiser_3; }
	inline void set_RoomVisualiser_3(GameObject_t1113636619 * value)
	{
		___RoomVisualiser_3 = value;
		Il2CppCodeGenWriteBarrier((&___RoomVisualiser_3), value);
	}

	inline static int32_t get_offset_of_Doors_4() { return static_cast<int32_t>(offsetof(Room_t1080437681, ___Doors_4)); }
	inline Dictionary_2_t339125784 * get_Doors_4() const { return ___Doors_4; }
	inline Dictionary_2_t339125784 ** get_address_of_Doors_4() { return &___Doors_4; }
	inline void set_Doors_4(Dictionary_2_t339125784 * value)
	{
		___Doors_4 = value;
		Il2CppCodeGenWriteBarrier((&___Doors_4), value);
	}

	inline static int32_t get_offset_of_ConnectedRooms_5() { return static_cast<int32_t>(offsetof(Room_t1080437681, ___ConnectedRooms_5)); }
	inline Dictionary_2_t286520093 * get_ConnectedRooms_5() const { return ___ConnectedRooms_5; }
	inline Dictionary_2_t286520093 ** get_address_of_ConnectedRooms_5() { return &___ConnectedRooms_5; }
	inline void set_ConnectedRooms_5(Dictionary_2_t286520093 * value)
	{
		___ConnectedRooms_5 = value;
		Il2CppCodeGenWriteBarrier((&___ConnectedRooms_5), value);
	}

	inline static int32_t get_offset_of_Unlocked_6() { return static_cast<int32_t>(offsetof(Room_t1080437681, ___Unlocked_6)); }
	inline bool get_Unlocked_6() const { return ___Unlocked_6; }
	inline bool* get_address_of_Unlocked_6() { return &___Unlocked_6; }
	inline void set_Unlocked_6(bool value)
	{
		___Unlocked_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOM_T1080437681_H
#ifndef LOOPTYPE_T2942026839_H
#define LOOPTYPE_T2942026839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Tween/LoopType
struct  LoopType_t2942026839 
{
public:
	// System.Int32 Pixelplacement.Tween/LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t2942026839, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T2942026839_H
#ifndef TWEENSTATUS_T3213007367_H
#define TWEENSTATUS_T3213007367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Tween/TweenStatus
struct  TweenStatus_t3213007367 
{
public:
	// System.Int32 Pixelplacement.Tween/TweenStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenStatus_t3213007367, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSTATUS_T3213007367_H
#ifndef TWEENTYPE_T2719964487_H
#define TWEENTYPE_T2719964487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Tween/TweenType
struct  TweenType_t2719964487 
{
public:
	// System.Int32 Pixelplacement.Tween/TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t2719964487, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T2719964487_H
#ifndef RELATIVE_T934610588_H
#define RELATIVE_T934610588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Relative
struct  Relative_t934610588 
{
public:
	// System.Int32 Relative::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Relative_t934610588, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELATIVE_T934610588_H
#ifndef COLORMODE_T635806747_H
#define COLORMODE_T635806747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar/ColorMode
struct  ColorMode_t635806747 
{
public:
	// System.Int32 SimpleHealthBar/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t635806747, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T635806747_H
#ifndef DISPLAYTEXT_T3312458044_H
#define DISPLAYTEXT_T3312458044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar/DisplayText
struct  DisplayText_t3312458044 
{
public:
	// System.Int32 SimpleHealthBar/DisplayText::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DisplayText_t3312458044, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYTEXT_T3312458044_H
#ifndef U3CFADEDEATHSCREENU3EC__ITERATOR2_T688632665_H
#define U3CFADEDEATHSCREENU3EC__ITERATOR2_T688632665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2
struct  U3CFadeDeathScreenU3Ec__Iterator2_t688632665  : public RuntimeObject
{
public:
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::<imageColor>__0
	Color_t2555686324  ___U3CimageColorU3E__0_0;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::<textColor>__0
	Color_t2555686324  ___U3CtextColorU3E__0_1;
	// UnityEngine.Color SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::<finalScoreTextColor>__0
	Color_t2555686324  ___U3CfinalScoreTextColorU3E__0_2;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::<t>__1
	float ___U3CtU3E__1_3;
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::$this
	GameManager_t3924726104 * ___U24this_4;
	// System.Object SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager/<FadeDeathScreen>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CimageColorU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U3CimageColorU3E__0_0)); }
	inline Color_t2555686324  get_U3CimageColorU3E__0_0() const { return ___U3CimageColorU3E__0_0; }
	inline Color_t2555686324 * get_address_of_U3CimageColorU3E__0_0() { return &___U3CimageColorU3E__0_0; }
	inline void set_U3CimageColorU3E__0_0(Color_t2555686324  value)
	{
		___U3CimageColorU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtextColorU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U3CtextColorU3E__0_1)); }
	inline Color_t2555686324  get_U3CtextColorU3E__0_1() const { return ___U3CtextColorU3E__0_1; }
	inline Color_t2555686324 * get_address_of_U3CtextColorU3E__0_1() { return &___U3CtextColorU3E__0_1; }
	inline void set_U3CtextColorU3E__0_1(Color_t2555686324  value)
	{
		___U3CtextColorU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CfinalScoreTextColorU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U3CfinalScoreTextColorU3E__0_2)); }
	inline Color_t2555686324  get_U3CfinalScoreTextColorU3E__0_2() const { return ___U3CfinalScoreTextColorU3E__0_2; }
	inline Color_t2555686324 * get_address_of_U3CfinalScoreTextColorU3E__0_2() { return &___U3CfinalScoreTextColorU3E__0_2; }
	inline void set_U3CfinalScoreTextColorU3E__0_2(Color_t2555686324  value)
	{
		___U3CfinalScoreTextColorU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_3() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U3CtU3E__1_3)); }
	inline float get_U3CtU3E__1_3() const { return ___U3CtU3E__1_3; }
	inline float* get_address_of_U3CtU3E__1_3() { return &___U3CtU3E__1_3; }
	inline void set_U3CtU3E__1_3(float value)
	{
		___U3CtU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U24this_4)); }
	inline GameManager_t3924726104 * get_U24this_4() const { return ___U24this_4; }
	inline GameManager_t3924726104 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GameManager_t3924726104 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFadeDeathScreenU3Ec__Iterator2_t688632665, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEDEATHSCREENU3EC__ITERATOR2_T688632665_H
#ifndef U3CSHAKECAMERAU3EC__ITERATOR1_T2884820085_H
#define U3CSHAKECAMERAU3EC__ITERATOR1_T2884820085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>c__Iterator1
struct  U3CShakeCameraU3Ec__Iterator1_t2884820085  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>c__Iterator1::<origPos>__0
	Vector2_t2156229523  ___U3CorigPosU3E__0_0;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>c__Iterator1::<t>__1
	float ___U3CtU3E__1_1;
	// UnityEngine.Vector2 SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>c__Iterator1::<tempVec>__2
	Vector2_t2156229523  ___U3CtempVecU3E__2_2;
	// System.Object SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth/<ShakeCamera>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CorigPosU3E__0_0() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ec__Iterator1_t2884820085, ___U3CorigPosU3E__0_0)); }
	inline Vector2_t2156229523  get_U3CorigPosU3E__0_0() const { return ___U3CorigPosU3E__0_0; }
	inline Vector2_t2156229523 * get_address_of_U3CorigPosU3E__0_0() { return &___U3CorigPosU3E__0_0; }
	inline void set_U3CorigPosU3E__0_0(Vector2_t2156229523  value)
	{
		___U3CorigPosU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_1() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ec__Iterator1_t2884820085, ___U3CtU3E__1_1)); }
	inline float get_U3CtU3E__1_1() const { return ___U3CtU3E__1_1; }
	inline float* get_address_of_U3CtU3E__1_1() { return &___U3CtU3E__1_1; }
	inline void set_U3CtU3E__1_1(float value)
	{
		___U3CtU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CtempVecU3E__2_2() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ec__Iterator1_t2884820085, ___U3CtempVecU3E__2_2)); }
	inline Vector2_t2156229523  get_U3CtempVecU3E__2_2() const { return ___U3CtempVecU3E__2_2; }
	inline Vector2_t2156229523 * get_address_of_U3CtempVecU3E__2_2() { return &___U3CtempVecU3E__2_2; }
	inline void set_U3CtempVecU3E__2_2(Vector2_t2156229523  value)
	{
		___U3CtempVecU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ec__Iterator1_t2884820085, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ec__Iterator1_t2884820085, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CShakeCameraU3Ec__Iterator1_t2884820085, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHAKECAMERAU3EC__ITERATOR1_T2884820085_H
#ifndef CAMERAMODES_T3200559075_H
#define CAMERAMODES_T3200559075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t3200559075 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t3200559075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T3200559075_H
#ifndef MOTIONTYPE_T1905163921_H
#define MOTIONTYPE_T1905163921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t1905163921 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t1905163921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T1905163921_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#define U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t116130919  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_ShearValue>__0
	float ___U3Cold_ShearValueU3E__0_1;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3046754366 * ___U3Cold_curveU3E__0_2;
	// TMPro.TMP_TextInfo TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t3598145122 * ___U3CtextInfoU3E__1_3;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_5;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_6;
	// UnityEngine.Vector3[] TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1718750761* ___U3CverticesU3E__2_7;
	// UnityEngine.Matrix4x4 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t1817901843  ___U3CmatrixU3E__2_8;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$this
	SkewTextExample_t3460249701 * ___U24this_9;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_ShearValueU3E__0_1)); }
	inline float get_U3Cold_ShearValueU3E__0_1() const { return ___U3Cold_ShearValueU3E__0_1; }
	inline float* get_address_of_U3Cold_ShearValueU3E__0_1() { return &___U3Cold_ShearValueU3E__0_1; }
	inline void set_U3Cold_ShearValueU3E__0_1(float value)
	{
		___U3Cold_ShearValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3Cold_curveU3E__0_2)); }
	inline AnimationCurve_t3046754366 * get_U3Cold_curveU3E__0_2() const { return ___U3Cold_curveU3E__0_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3Cold_curveU3E__0_2() { return &___U3Cold_curveU3E__0_2; }
	inline void set_U3Cold_curveU3E__0_2(AnimationCurve_t3046754366 * value)
	{
		___U3Cold_curveU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CtextInfoU3E__1_3)); }
	inline TMP_TextInfo_t3598145122 * get_U3CtextInfoU3E__1_3() const { return ___U3CtextInfoU3E__1_3; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_U3CtextInfoU3E__1_3() { return &___U3CtextInfoU3E__1_3; }
	inline void set_U3CtextInfoU3E__1_3(TMP_TextInfo_t3598145122 * value)
	{
		___U3CtextInfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMinXU3E__1_5)); }
	inline float get_U3CboundsMinXU3E__1_5() const { return ___U3CboundsMinXU3E__1_5; }
	inline float* get_address_of_U3CboundsMinXU3E__1_5() { return &___U3CboundsMinXU3E__1_5; }
	inline void set_U3CboundsMinXU3E__1_5(float value)
	{
		___U3CboundsMinXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CboundsMaxXU3E__1_6)); }
	inline float get_U3CboundsMaxXU3E__1_6() const { return ___U3CboundsMaxXU3E__1_6; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_6() { return &___U3CboundsMaxXU3E__1_6; }
	inline void set_U3CboundsMaxXU3E__1_6(float value)
	{
		___U3CboundsMaxXU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CverticesU3E__2_7)); }
	inline Vector3U5BU5D_t1718750761* get_U3CverticesU3E__2_7() const { return ___U3CverticesU3E__2_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_U3CverticesU3E__2_7() { return &___U3CverticesU3E__2_7; }
	inline void set_U3CverticesU3E__2_7(Vector3U5BU5D_t1718750761* value)
	{
		___U3CverticesU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U3CmatrixU3E__2_8)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__2_8() const { return ___U3CmatrixU3E__2_8; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__2_8() { return &___U3CmatrixU3E__2_8; }
	inline void set_U3CmatrixU3E__2_8(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24this_9)); }
	inline SkewTextExample_t3460249701 * get_U24this_9() const { return ___U24this_9; }
	inline SkewTextExample_t3460249701 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(SkewTextExample_t3460249701 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t116130919, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T116130919_H
#ifndef OBJECTTYPE_T4082700821_H
#define OBJECTTYPE_T4082700821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01/objectType
struct  objectType_t4082700821 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(objectType_t4082700821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_T4082700821_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#define FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t1585798158 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t1585798158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T1585798158_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#define FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t2550331785 
{
public:
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t2550331785, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T2550331785_H
#ifndef U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#define U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1
struct  U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T865582314_H
#ifndef U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#define U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0
struct  U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_pos>__0
	Vector3_t3722313464  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_color>__0
	Color32_t2600501292  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$this
	TextMeshProFloatingText_t845872552 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t3722313464  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t3722313464 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t3722313464  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t2600501292  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t2600501292 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t2600501292  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24this_8)); }
	inline TextMeshProFloatingText_t845872552 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t845872552 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T2967292235_H
#ifndef CHARACTERSELECTIONEVENT_T3109943174_H
#define CHARACTERSELECTIONEVENT_T3109943174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct  CharacterSelectionEvent_t3109943174  : public UnityEvent_2_t1169440328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTIONEVENT_T3109943174_H
#ifndef LINESELECTIONEVENT_T2868010532_H
#define LINESELECTIONEVENT_T2868010532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct  LineSelectionEvent_t2868010532  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESELECTIONEVENT_T2868010532_H
#ifndef LINKSELECTIONEVENT_T1590929858_H
#define LINKSELECTIONEVENT_T1590929858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct  LinkSelectionEvent_t1590929858  : public UnityEvent_3_t2493613095
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSELECTIONEVENT_T1590929858_H
#ifndef SPRITESELECTIONEVENT_T2798445241_H
#define SPRITESELECTIONEVENT_T2798445241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/SpriteSelectionEvent
struct  SpriteSelectionEvent_t2798445241  : public UnityEvent_2_t1169440328
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESELECTIONEVENT_T2798445241_H
#ifndef WORDSELECTIONEVENT_T1841909953_H
#define WORDSELECTIONEVENT_T1841909953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct  WordSelectionEvent_t1841909953  : public UnityEvent_3_t1597070127
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_T1841909953_H
#ifndef COLLECTOBJECTS_T1049357338_H
#define COLLECTOBJECTS_T1049357338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.CollectObjects
struct  CollectObjects_t1049357338 
{
public:
	// System.Int32 UnityEngine.AI.CollectObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollectObjects_t1049357338, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTOBJECTS_T1049357338_H
#ifndef NAVMESHCOLLECTGEOMETRY_T1778480714_H
#define NAVMESHCOLLECTGEOMETRY_T1778480714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshCollectGeometry
struct  NavMeshCollectGeometry_t1778480714 
{
public:
	// System.Int32 UnityEngine.AI.NavMeshCollectGeometry::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NavMeshCollectGeometry_t1778480714, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHCOLLECTGEOMETRY_T1778480714_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DECISIONTYPE_T3995638880_H
#define DECISIONTYPE_T3995638880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.DecisionType
struct  DecisionType_t3995638880 
{
public:
	// System.Int32 UtilityAI.DecisionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DecisionType_t3995638880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECISIONTYPE_T3995638880_H
#ifndef VIEW_T4100663493_H
#define VIEW_T4100663493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// View
struct  View_t4100663493 
{
public:
	// System.Int32 View::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(View_t4100663493, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEW_T4100663493_H
#ifndef DOOR_T3003551444_H
#define DOOR_T3003551444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapGen.Door
struct  Door_t3003551444  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MapGen.Door::DoorVisualiser
	GameObject_t1113636619 * ___DoorVisualiser_0;
	// MapGen.Room MapGen.Door::Room
	Room_t1080437681 * ___Room_1;
	// Relative MapGen.Door::Relative
	int32_t ___Relative_2;
	// MapGen.Door MapGen.Door::Connection
	Door_t3003551444 * ___Connection_3;

public:
	inline static int32_t get_offset_of_DoorVisualiser_0() { return static_cast<int32_t>(offsetof(Door_t3003551444, ___DoorVisualiser_0)); }
	inline GameObject_t1113636619 * get_DoorVisualiser_0() const { return ___DoorVisualiser_0; }
	inline GameObject_t1113636619 ** get_address_of_DoorVisualiser_0() { return &___DoorVisualiser_0; }
	inline void set_DoorVisualiser_0(GameObject_t1113636619 * value)
	{
		___DoorVisualiser_0 = value;
		Il2CppCodeGenWriteBarrier((&___DoorVisualiser_0), value);
	}

	inline static int32_t get_offset_of_Room_1() { return static_cast<int32_t>(offsetof(Door_t3003551444, ___Room_1)); }
	inline Room_t1080437681 * get_Room_1() const { return ___Room_1; }
	inline Room_t1080437681 ** get_address_of_Room_1() { return &___Room_1; }
	inline void set_Room_1(Room_t1080437681 * value)
	{
		___Room_1 = value;
		Il2CppCodeGenWriteBarrier((&___Room_1), value);
	}

	inline static int32_t get_offset_of_Relative_2() { return static_cast<int32_t>(offsetof(Door_t3003551444, ___Relative_2)); }
	inline int32_t get_Relative_2() const { return ___Relative_2; }
	inline int32_t* get_address_of_Relative_2() { return &___Relative_2; }
	inline void set_Relative_2(int32_t value)
	{
		___Relative_2 = value;
	}

	inline static int32_t get_offset_of_Connection_3() { return static_cast<int32_t>(offsetof(Door_t3003551444, ___Connection_3)); }
	inline Door_t3003551444 * get_Connection_3() const { return ___Connection_3; }
	inline Door_t3003551444 ** get_address_of_Connection_3() { return &___Connection_3; }
	inline void set_Connection_3(Door_t3003551444 * value)
	{
		___Connection_3 = value;
		Il2CppCodeGenWriteBarrier((&___Connection_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOOR_T3003551444_H
#ifndef TWEENBASE_T3695616892_H
#define TWEENBASE_T3695616892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.TweenBase
struct  TweenBase_t3695616892  : public RuntimeObject
{
public:
	// System.Int32 Pixelplacement.TweenSystem.TweenBase::targetInstanceID
	int32_t ___targetInstanceID_0;
	// Pixelplacement.Tween/TweenType Pixelplacement.TweenSystem.TweenBase::tweenType
	int32_t ___tweenType_1;
	// Pixelplacement.Tween/TweenStatus Pixelplacement.TweenSystem.TweenBase::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_2;
	// System.Single Pixelplacement.TweenSystem.TweenBase::<Duration>k__BackingField
	float ___U3CDurationU3Ek__BackingField_3;
	// UnityEngine.AnimationCurve Pixelplacement.TweenSystem.TweenBase::<Curve>k__BackingField
	AnimationCurve_t3046754366 * ___U3CCurveU3Ek__BackingField_4;
	// UnityEngine.Keyframe[] Pixelplacement.TweenSystem.TweenBase::<CurveKeys>k__BackingField
	KeyframeU5BU5D_t1068524471* ___U3CCurveKeysU3Ek__BackingField_5;
	// System.Boolean Pixelplacement.TweenSystem.TweenBase::<ObeyTimescale>k__BackingField
	bool ___U3CObeyTimescaleU3Ek__BackingField_6;
	// System.Action Pixelplacement.TweenSystem.TweenBase::<StartCallback>k__BackingField
	Action_t1264377477 * ___U3CStartCallbackU3Ek__BackingField_7;
	// System.Action Pixelplacement.TweenSystem.TweenBase::<CompleteCallback>k__BackingField
	Action_t1264377477 * ___U3CCompleteCallbackU3Ek__BackingField_8;
	// System.Single Pixelplacement.TweenSystem.TweenBase::<Delay>k__BackingField
	float ___U3CDelayU3Ek__BackingField_9;
	// Pixelplacement.Tween/LoopType Pixelplacement.TweenSystem.TweenBase::<LoopType>k__BackingField
	int32_t ___U3CLoopTypeU3Ek__BackingField_10;
	// System.Single Pixelplacement.TweenSystem.TweenBase::<Percentage>k__BackingField
	float ___U3CPercentageU3Ek__BackingField_11;
	// System.Single Pixelplacement.TweenSystem.TweenBase::elapsedTime
	float ___elapsedTime_12;

public:
	inline static int32_t get_offset_of_targetInstanceID_0() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___targetInstanceID_0)); }
	inline int32_t get_targetInstanceID_0() const { return ___targetInstanceID_0; }
	inline int32_t* get_address_of_targetInstanceID_0() { return &___targetInstanceID_0; }
	inline void set_targetInstanceID_0(int32_t value)
	{
		___targetInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_tweenType_1() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___tweenType_1)); }
	inline int32_t get_tweenType_1() const { return ___tweenType_1; }
	inline int32_t* get_address_of_tweenType_1() { return &___tweenType_1; }
	inline void set_tweenType_1(int32_t value)
	{
		___tweenType_1 = value;
	}

	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CStatusU3Ek__BackingField_2)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_2() const { return ___U3CStatusU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_2() { return &___U3CStatusU3Ek__BackingField_2; }
	inline void set_U3CStatusU3Ek__BackingField_2(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CDurationU3Ek__BackingField_3)); }
	inline float get_U3CDurationU3Ek__BackingField_3() const { return ___U3CDurationU3Ek__BackingField_3; }
	inline float* get_address_of_U3CDurationU3Ek__BackingField_3() { return &___U3CDurationU3Ek__BackingField_3; }
	inline void set_U3CDurationU3Ek__BackingField_3(float value)
	{
		___U3CDurationU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCurveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CCurveU3Ek__BackingField_4)); }
	inline AnimationCurve_t3046754366 * get_U3CCurveU3Ek__BackingField_4() const { return ___U3CCurveU3Ek__BackingField_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3CCurveU3Ek__BackingField_4() { return &___U3CCurveU3Ek__BackingField_4; }
	inline void set_U3CCurveU3Ek__BackingField_4(AnimationCurve_t3046754366 * value)
	{
		___U3CCurveU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurveU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCurveKeysU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CCurveKeysU3Ek__BackingField_5)); }
	inline KeyframeU5BU5D_t1068524471* get_U3CCurveKeysU3Ek__BackingField_5() const { return ___U3CCurveKeysU3Ek__BackingField_5; }
	inline KeyframeU5BU5D_t1068524471** get_address_of_U3CCurveKeysU3Ek__BackingField_5() { return &___U3CCurveKeysU3Ek__BackingField_5; }
	inline void set_U3CCurveKeysU3Ek__BackingField_5(KeyframeU5BU5D_t1068524471* value)
	{
		___U3CCurveKeysU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurveKeysU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CObeyTimescaleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CObeyTimescaleU3Ek__BackingField_6)); }
	inline bool get_U3CObeyTimescaleU3Ek__BackingField_6() const { return ___U3CObeyTimescaleU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CObeyTimescaleU3Ek__BackingField_6() { return &___U3CObeyTimescaleU3Ek__BackingField_6; }
	inline void set_U3CObeyTimescaleU3Ek__BackingField_6(bool value)
	{
		___U3CObeyTimescaleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CStartCallbackU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CStartCallbackU3Ek__BackingField_7)); }
	inline Action_t1264377477 * get_U3CStartCallbackU3Ek__BackingField_7() const { return ___U3CStartCallbackU3Ek__BackingField_7; }
	inline Action_t1264377477 ** get_address_of_U3CStartCallbackU3Ek__BackingField_7() { return &___U3CStartCallbackU3Ek__BackingField_7; }
	inline void set_U3CStartCallbackU3Ek__BackingField_7(Action_t1264377477 * value)
	{
		___U3CStartCallbackU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartCallbackU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CCompleteCallbackU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CCompleteCallbackU3Ek__BackingField_8)); }
	inline Action_t1264377477 * get_U3CCompleteCallbackU3Ek__BackingField_8() const { return ___U3CCompleteCallbackU3Ek__BackingField_8; }
	inline Action_t1264377477 ** get_address_of_U3CCompleteCallbackU3Ek__BackingField_8() { return &___U3CCompleteCallbackU3Ek__BackingField_8; }
	inline void set_U3CCompleteCallbackU3Ek__BackingField_8(Action_t1264377477 * value)
	{
		___U3CCompleteCallbackU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCompleteCallbackU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CDelayU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CDelayU3Ek__BackingField_9)); }
	inline float get_U3CDelayU3Ek__BackingField_9() const { return ___U3CDelayU3Ek__BackingField_9; }
	inline float* get_address_of_U3CDelayU3Ek__BackingField_9() { return &___U3CDelayU3Ek__BackingField_9; }
	inline void set_U3CDelayU3Ek__BackingField_9(float value)
	{
		___U3CDelayU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CLoopTypeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CLoopTypeU3Ek__BackingField_10)); }
	inline int32_t get_U3CLoopTypeU3Ek__BackingField_10() const { return ___U3CLoopTypeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CLoopTypeU3Ek__BackingField_10() { return &___U3CLoopTypeU3Ek__BackingField_10; }
	inline void set_U3CLoopTypeU3Ek__BackingField_10(int32_t value)
	{
		___U3CLoopTypeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CPercentageU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CPercentageU3Ek__BackingField_11)); }
	inline float get_U3CPercentageU3Ek__BackingField_11() const { return ___U3CPercentageU3Ek__BackingField_11; }
	inline float* get_address_of_U3CPercentageU3Ek__BackingField_11() { return &___U3CPercentageU3Ek__BackingField_11; }
	inline void set_U3CPercentageU3Ek__BackingField_11(float value)
	{
		___U3CPercentageU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_elapsedTime_12() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___elapsedTime_12)); }
	inline float get_elapsedTime_12() const { return ___elapsedTime_12; }
	inline float* get_address_of_elapsedTime_12() { return &___elapsedTime_12; }
	inline void set_elapsedTime_12(float value)
	{
		___elapsedTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENBASE_T3695616892_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef VOLUME_T205472241_H
#define VOLUME_T205472241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.Volume
struct  Volume_t205472241  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.Volume::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.AudioSource Pixelplacement.TweenSystem.Volume::_target
	AudioSource_t3935305588 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.Volume::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Volume_t205472241, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(Volume_t205472241, ____target_14)); }
	inline AudioSource_t3935305588 * get__target_14() const { return ____target_14; }
	inline AudioSource_t3935305588 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(AudioSource_t3935305588 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(Volume_t205472241, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOLUME_T205472241_H
#ifndef TMP_INPUTVALIDATOR_T1385053824_H
#define TMP_INPUTVALIDATOR_T1385053824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t1385053824  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T1385053824_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TMP_DIGITVALIDATOR_T573672104_H
#define TMP_DIGITVALIDATOR_T573672104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_t573672104  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_T573672104_H
#ifndef TMP_PHONENUMBERVALIDATOR_T743649728_H
#define TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PhoneNumberValidator
struct  TMP_PhoneNumberValidator_t743649728  : public TMP_InputValidator_t1385053824
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PHONENUMBERVALIDATOR_T743649728_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BILLBOARD_T1300283564_H
#define BILLBOARD_T1300283564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Billboard
struct  Billboard_t1300283564  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARD_T1300283564_H
#ifndef CAMERAMANAGER_T3272490737_H
#define CAMERAMANAGER_T3272490737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraManager
struct  CameraManager_t3272490737  : public MonoBehaviour_t3962482529
{
public:
	// Competitor CameraManager::competitorViewing
	Competitor_t2775849210 * ___competitorViewing_4;
	// UnityEngine.GameObject CameraManager::thirdViewPos
	GameObject_t1113636619 * ___thirdViewPos_5;
	// View CameraManager::currentView
	int32_t ___currentView_6;
	// CompetitorManager CameraManager::competitorManager
	CompetitorManager_t1763731731 * ___competitorManager_7;
	// System.Single CameraManager::lerpTime
	float ___lerpTime_8;
	// System.Boolean CameraManager::viewChangeComplete
	bool ___viewChangeComplete_9;
	// System.Boolean CameraManager::isUpdating
	bool ___isUpdating_10;
	// System.Single CameraManager::time
	float ___time_11;
	// System.Single CameraManager::t
	float ___t_12;
	// UnityEngine.Vector3 CameraManager::averageCompetitorPosition
	Vector3_t3722313464  ___averageCompetitorPosition_13;

public:
	inline static int32_t get_offset_of_competitorViewing_4() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___competitorViewing_4)); }
	inline Competitor_t2775849210 * get_competitorViewing_4() const { return ___competitorViewing_4; }
	inline Competitor_t2775849210 ** get_address_of_competitorViewing_4() { return &___competitorViewing_4; }
	inline void set_competitorViewing_4(Competitor_t2775849210 * value)
	{
		___competitorViewing_4 = value;
		Il2CppCodeGenWriteBarrier((&___competitorViewing_4), value);
	}

	inline static int32_t get_offset_of_thirdViewPos_5() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___thirdViewPos_5)); }
	inline GameObject_t1113636619 * get_thirdViewPos_5() const { return ___thirdViewPos_5; }
	inline GameObject_t1113636619 ** get_address_of_thirdViewPos_5() { return &___thirdViewPos_5; }
	inline void set_thirdViewPos_5(GameObject_t1113636619 * value)
	{
		___thirdViewPos_5 = value;
		Il2CppCodeGenWriteBarrier((&___thirdViewPos_5), value);
	}

	inline static int32_t get_offset_of_currentView_6() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___currentView_6)); }
	inline int32_t get_currentView_6() const { return ___currentView_6; }
	inline int32_t* get_address_of_currentView_6() { return &___currentView_6; }
	inline void set_currentView_6(int32_t value)
	{
		___currentView_6 = value;
	}

	inline static int32_t get_offset_of_competitorManager_7() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___competitorManager_7)); }
	inline CompetitorManager_t1763731731 * get_competitorManager_7() const { return ___competitorManager_7; }
	inline CompetitorManager_t1763731731 ** get_address_of_competitorManager_7() { return &___competitorManager_7; }
	inline void set_competitorManager_7(CompetitorManager_t1763731731 * value)
	{
		___competitorManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___competitorManager_7), value);
	}

	inline static int32_t get_offset_of_lerpTime_8() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___lerpTime_8)); }
	inline float get_lerpTime_8() const { return ___lerpTime_8; }
	inline float* get_address_of_lerpTime_8() { return &___lerpTime_8; }
	inline void set_lerpTime_8(float value)
	{
		___lerpTime_8 = value;
	}

	inline static int32_t get_offset_of_viewChangeComplete_9() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___viewChangeComplete_9)); }
	inline bool get_viewChangeComplete_9() const { return ___viewChangeComplete_9; }
	inline bool* get_address_of_viewChangeComplete_9() { return &___viewChangeComplete_9; }
	inline void set_viewChangeComplete_9(bool value)
	{
		___viewChangeComplete_9 = value;
	}

	inline static int32_t get_offset_of_isUpdating_10() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___isUpdating_10)); }
	inline bool get_isUpdating_10() const { return ___isUpdating_10; }
	inline bool* get_address_of_isUpdating_10() { return &___isUpdating_10; }
	inline void set_isUpdating_10(bool value)
	{
		___isUpdating_10 = value;
	}

	inline static int32_t get_offset_of_time_11() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___time_11)); }
	inline float get_time_11() const { return ___time_11; }
	inline float* get_address_of_time_11() { return &___time_11; }
	inline void set_time_11(float value)
	{
		___time_11 = value;
	}

	inline static int32_t get_offset_of_t_12() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___t_12)); }
	inline float get_t_12() const { return ___t_12; }
	inline float* get_address_of_t_12() { return &___t_12; }
	inline void set_t_12(float value)
	{
		___t_12 = value;
	}

	inline static int32_t get_offset_of_averageCompetitorPosition_13() { return static_cast<int32_t>(offsetof(CameraManager_t3272490737, ___averageCompetitorPosition_13)); }
	inline Vector3_t3722313464  get_averageCompetitorPosition_13() const { return ___averageCompetitorPosition_13; }
	inline Vector3_t3722313464 * get_address_of_averageCompetitorPosition_13() { return &___averageCompetitorPosition_13; }
	inline void set_averageCompetitorPosition_13(Vector3_t3722313464  value)
	{
		___averageCompetitorPosition_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMANAGER_T3272490737_H
#ifndef CHATCONTROLLER_T3486202795_H
#define CHATCONTROLLER_T3486202795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t3486202795  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1099764886 * ___TMP_ChatInput_4;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t2599618874 * ___TMP_ChatOutput_5;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t1494447233 * ___ChatScrollbar_6;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_4() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatInput_4)); }
	inline TMP_InputField_t1099764886 * get_TMP_ChatInput_4() const { return ___TMP_ChatInput_4; }
	inline TMP_InputField_t1099764886 ** get_address_of_TMP_ChatInput_4() { return &___TMP_ChatInput_4; }
	inline void set_TMP_ChatInput_4(TMP_InputField_t1099764886 * value)
	{
		___TMP_ChatInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_4), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_5() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatOutput_5)); }
	inline TMP_Text_t2599618874 * get_TMP_ChatOutput_5() const { return ___TMP_ChatOutput_5; }
	inline TMP_Text_t2599618874 ** get_address_of_TMP_ChatOutput_5() { return &___TMP_ChatOutput_5; }
	inline void set_TMP_ChatOutput_5(TMP_Text_t2599618874 * value)
	{
		___TMP_ChatOutput_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_5), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_6() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___ChatScrollbar_6)); }
	inline Scrollbar_t1494447233 * get_ChatScrollbar_6() const { return ___ChatScrollbar_6; }
	inline Scrollbar_t1494447233 ** get_address_of_ChatScrollbar_6() { return &___ChatScrollbar_6; }
	inline void set_ChatScrollbar_6(Scrollbar_t1494447233 * value)
	{
		___ChatScrollbar_6 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T3486202795_H
#ifndef COMPETITOR_T2775849210_H
#define COMPETITOR_T2775849210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Competitor
struct  Competitor_t2775849210  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Competitor::maxSpeed
	float ___maxSpeed_4;
	// System.Single Competitor::speed
	float ___speed_5;
	// System.Single Competitor::healthRegenMultiplier
	float ___healthRegenMultiplier_6;
	// System.Single Competitor::hungerMultiplier
	float ___hungerMultiplier_7;
	// System.Single Competitor::thirstMultiplier
	float ___thirstMultiplier_8;
	// System.Single Competitor::maxHealth
	float ___maxHealth_9;
	// System.Single Competitor::maxHunger
	float ___maxHunger_10;
	// System.Single Competitor::maxThirst
	float ___maxThirst_11;
	// System.Single Competitor::health
	float ___health_12;
	// System.Single Competitor::hunger
	float ___hunger_13;
	// System.Single Competitor::thirst
	float ___thirst_14;
	// System.Single Competitor::hungerVariable
	float ___hungerVariable_15;
	// System.Single Competitor::thirstVariable
	float ___thirstVariable_16;
	// System.Single Competitor::healthVariable
	float ___healthVariable_17;
	// System.Single Competitor::maxHealthValue
	float ___maxHealthValue_18;
	// CompetitorUI Competitor::UI
	CompetitorUI_t472322071 * ___UI_19;
	// UnityEngine.GameObject Competitor::firstViewPos
	GameObject_t1113636619 * ___firstViewPos_20;
	// UnityEngine.UI.Button Competitor::changeViewButton
	Button_t4055032469 * ___changeViewButton_21;
	// CameraManager Competitor::cameraManager
	CameraManager_t3272490737 * ___cameraManager_22;

public:
	inline static int32_t get_offset_of_maxSpeed_4() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxSpeed_4)); }
	inline float get_maxSpeed_4() const { return ___maxSpeed_4; }
	inline float* get_address_of_maxSpeed_4() { return &___maxSpeed_4; }
	inline void set_maxSpeed_4(float value)
	{
		___maxSpeed_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_healthRegenMultiplier_6() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___healthRegenMultiplier_6)); }
	inline float get_healthRegenMultiplier_6() const { return ___healthRegenMultiplier_6; }
	inline float* get_address_of_healthRegenMultiplier_6() { return &___healthRegenMultiplier_6; }
	inline void set_healthRegenMultiplier_6(float value)
	{
		___healthRegenMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_hungerMultiplier_7() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___hungerMultiplier_7)); }
	inline float get_hungerMultiplier_7() const { return ___hungerMultiplier_7; }
	inline float* get_address_of_hungerMultiplier_7() { return &___hungerMultiplier_7; }
	inline void set_hungerMultiplier_7(float value)
	{
		___hungerMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_thirstMultiplier_8() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___thirstMultiplier_8)); }
	inline float get_thirstMultiplier_8() const { return ___thirstMultiplier_8; }
	inline float* get_address_of_thirstMultiplier_8() { return &___thirstMultiplier_8; }
	inline void set_thirstMultiplier_8(float value)
	{
		___thirstMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_maxHealth_9() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxHealth_9)); }
	inline float get_maxHealth_9() const { return ___maxHealth_9; }
	inline float* get_address_of_maxHealth_9() { return &___maxHealth_9; }
	inline void set_maxHealth_9(float value)
	{
		___maxHealth_9 = value;
	}

	inline static int32_t get_offset_of_maxHunger_10() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxHunger_10)); }
	inline float get_maxHunger_10() const { return ___maxHunger_10; }
	inline float* get_address_of_maxHunger_10() { return &___maxHunger_10; }
	inline void set_maxHunger_10(float value)
	{
		___maxHunger_10 = value;
	}

	inline static int32_t get_offset_of_maxThirst_11() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxThirst_11)); }
	inline float get_maxThirst_11() const { return ___maxThirst_11; }
	inline float* get_address_of_maxThirst_11() { return &___maxThirst_11; }
	inline void set_maxThirst_11(float value)
	{
		___maxThirst_11 = value;
	}

	inline static int32_t get_offset_of_health_12() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___health_12)); }
	inline float get_health_12() const { return ___health_12; }
	inline float* get_address_of_health_12() { return &___health_12; }
	inline void set_health_12(float value)
	{
		___health_12 = value;
	}

	inline static int32_t get_offset_of_hunger_13() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___hunger_13)); }
	inline float get_hunger_13() const { return ___hunger_13; }
	inline float* get_address_of_hunger_13() { return &___hunger_13; }
	inline void set_hunger_13(float value)
	{
		___hunger_13 = value;
	}

	inline static int32_t get_offset_of_thirst_14() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___thirst_14)); }
	inline float get_thirst_14() const { return ___thirst_14; }
	inline float* get_address_of_thirst_14() { return &___thirst_14; }
	inline void set_thirst_14(float value)
	{
		___thirst_14 = value;
	}

	inline static int32_t get_offset_of_hungerVariable_15() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___hungerVariable_15)); }
	inline float get_hungerVariable_15() const { return ___hungerVariable_15; }
	inline float* get_address_of_hungerVariable_15() { return &___hungerVariable_15; }
	inline void set_hungerVariable_15(float value)
	{
		___hungerVariable_15 = value;
	}

	inline static int32_t get_offset_of_thirstVariable_16() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___thirstVariable_16)); }
	inline float get_thirstVariable_16() const { return ___thirstVariable_16; }
	inline float* get_address_of_thirstVariable_16() { return &___thirstVariable_16; }
	inline void set_thirstVariable_16(float value)
	{
		___thirstVariable_16 = value;
	}

	inline static int32_t get_offset_of_healthVariable_17() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___healthVariable_17)); }
	inline float get_healthVariable_17() const { return ___healthVariable_17; }
	inline float* get_address_of_healthVariable_17() { return &___healthVariable_17; }
	inline void set_healthVariable_17(float value)
	{
		___healthVariable_17 = value;
	}

	inline static int32_t get_offset_of_maxHealthValue_18() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___maxHealthValue_18)); }
	inline float get_maxHealthValue_18() const { return ___maxHealthValue_18; }
	inline float* get_address_of_maxHealthValue_18() { return &___maxHealthValue_18; }
	inline void set_maxHealthValue_18(float value)
	{
		___maxHealthValue_18 = value;
	}

	inline static int32_t get_offset_of_UI_19() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___UI_19)); }
	inline CompetitorUI_t472322071 * get_UI_19() const { return ___UI_19; }
	inline CompetitorUI_t472322071 ** get_address_of_UI_19() { return &___UI_19; }
	inline void set_UI_19(CompetitorUI_t472322071 * value)
	{
		___UI_19 = value;
		Il2CppCodeGenWriteBarrier((&___UI_19), value);
	}

	inline static int32_t get_offset_of_firstViewPos_20() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___firstViewPos_20)); }
	inline GameObject_t1113636619 * get_firstViewPos_20() const { return ___firstViewPos_20; }
	inline GameObject_t1113636619 ** get_address_of_firstViewPos_20() { return &___firstViewPos_20; }
	inline void set_firstViewPos_20(GameObject_t1113636619 * value)
	{
		___firstViewPos_20 = value;
		Il2CppCodeGenWriteBarrier((&___firstViewPos_20), value);
	}

	inline static int32_t get_offset_of_changeViewButton_21() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___changeViewButton_21)); }
	inline Button_t4055032469 * get_changeViewButton_21() const { return ___changeViewButton_21; }
	inline Button_t4055032469 ** get_address_of_changeViewButton_21() { return &___changeViewButton_21; }
	inline void set_changeViewButton_21(Button_t4055032469 * value)
	{
		___changeViewButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___changeViewButton_21), value);
	}

	inline static int32_t get_offset_of_cameraManager_22() { return static_cast<int32_t>(offsetof(Competitor_t2775849210, ___cameraManager_22)); }
	inline CameraManager_t3272490737 * get_cameraManager_22() const { return ___cameraManager_22; }
	inline CameraManager_t3272490737 ** get_address_of_cameraManager_22() { return &___cameraManager_22; }
	inline void set_cameraManager_22(CameraManager_t3272490737 * value)
	{
		___cameraManager_22 = value;
		Il2CppCodeGenWriteBarrier((&___cameraManager_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPETITOR_T2775849210_H
#ifndef COMPETITORMANAGER_T1763731731_H
#define COMPETITORMANAGER_T1763731731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CompetitorManager
struct  CompetitorManager_t1763731731  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CompetitorManager::numberOfCompetitors
	int32_t ___numberOfCompetitors_4;
	// System.Collections.Generic.List`1<Competitor> CompetitorManager::competitors
	List_1_t4247923952 * ___competitors_5;

public:
	inline static int32_t get_offset_of_numberOfCompetitors_4() { return static_cast<int32_t>(offsetof(CompetitorManager_t1763731731, ___numberOfCompetitors_4)); }
	inline int32_t get_numberOfCompetitors_4() const { return ___numberOfCompetitors_4; }
	inline int32_t* get_address_of_numberOfCompetitors_4() { return &___numberOfCompetitors_4; }
	inline void set_numberOfCompetitors_4(int32_t value)
	{
		___numberOfCompetitors_4 = value;
	}

	inline static int32_t get_offset_of_competitors_5() { return static_cast<int32_t>(offsetof(CompetitorManager_t1763731731, ___competitors_5)); }
	inline List_1_t4247923952 * get_competitors_5() const { return ___competitors_5; }
	inline List_1_t4247923952 ** get_address_of_competitors_5() { return &___competitors_5; }
	inline void set_competitors_5(List_1_t4247923952 * value)
	{
		___competitors_5 = value;
		Il2CppCodeGenWriteBarrier((&___competitors_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPETITORMANAGER_T1763731731_H
#ifndef COMPETITORUI_T472322071_H
#define COMPETITORUI_T472322071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CompetitorUI
struct  CompetitorUI_t472322071  : public MonoBehaviour_t3962482529
{
public:
	// Competitor CompetitorUI::competitor
	Competitor_t2775849210 * ___competitor_4;
	// UtilityAI.DecisionManager CompetitorUI::decisionManager
	DecisionManager_t2018219593 * ___decisionManager_5;
	// UnityEngine.GameObject CompetitorUI::everything
	GameObject_t1113636619 * ___everything_6;
	// UnityEngine.UI.Text CompetitorUI::winnerText
	Text_t1901882714 * ___winnerText_7;
	// UnityEngine.GameObject CompetitorUI::path
	GameObject_t1113636619 * ___path_8;
	// UnityEngine.UI.Text CompetitorUI::bestDecisionText
	Text_t1901882714 * ___bestDecisionText_9;
	// UnityEngine.UI.Text CompetitorUI::healthWeightingText
	Text_t1901882714 * ___healthWeightingText_10;
	// SimpleHealthBar CompetitorUI::healthBar
	SimpleHealthBar_t721070758 * ___healthBar_11;
	// SimpleHealthBar CompetitorUI::hungerBar
	SimpleHealthBar_t721070758 * ___hungerBar_12;
	// SimpleHealthBar CompetitorUI::thirstBar
	SimpleHealthBar_t721070758 * ___thirstBar_13;
	// UnityEngine.GameObject CompetitorUI::health
	GameObject_t1113636619 * ___health_14;
	// UnityEngine.GameObject CompetitorUI::hunger
	GameObject_t1113636619 * ___hunger_15;
	// UnityEngine.GameObject CompetitorUI::thirst
	GameObject_t1113636619 * ___thirst_16;
	// UnityEngine.UI.Text CompetitorUI::decisionsText
	Text_t1901882714 * ___decisionsText_17;
	// UnityEngine.UI.Toggle CompetitorUI::everythingToggle
	Toggle_t2735377061 * ___everythingToggle_18;
	// UnityEngine.UI.Toggle CompetitorUI::pathToggle
	Toggle_t2735377061 * ___pathToggle_19;
	// UnityEngine.UI.Toggle CompetitorUI::bestDecisionToggle
	Toggle_t2735377061 * ___bestDecisionToggle_20;
	// UnityEngine.UI.Toggle CompetitorUI::healthToggle
	Toggle_t2735377061 * ___healthToggle_21;
	// UnityEngine.UI.Toggle CompetitorUI::healthBarToggle
	Toggle_t2735377061 * ___healthBarToggle_22;
	// UnityEngine.UI.Toggle CompetitorUI::thirstBarToggle
	Toggle_t2735377061 * ___thirstBarToggle_23;
	// UnityEngine.UI.Toggle CompetitorUI::hungerBarToggle
	Toggle_t2735377061 * ___hungerBarToggle_24;
	// UnityEngine.UI.Toggle CompetitorUI::decisionsToggle
	Toggle_t2735377061 * ___decisionsToggle_25;
	// System.Boolean CompetitorUI::lastUIToggleValue
	bool ___lastUIToggleValue_26;

public:
	inline static int32_t get_offset_of_competitor_4() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___competitor_4)); }
	inline Competitor_t2775849210 * get_competitor_4() const { return ___competitor_4; }
	inline Competitor_t2775849210 ** get_address_of_competitor_4() { return &___competitor_4; }
	inline void set_competitor_4(Competitor_t2775849210 * value)
	{
		___competitor_4 = value;
		Il2CppCodeGenWriteBarrier((&___competitor_4), value);
	}

	inline static int32_t get_offset_of_decisionManager_5() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___decisionManager_5)); }
	inline DecisionManager_t2018219593 * get_decisionManager_5() const { return ___decisionManager_5; }
	inline DecisionManager_t2018219593 ** get_address_of_decisionManager_5() { return &___decisionManager_5; }
	inline void set_decisionManager_5(DecisionManager_t2018219593 * value)
	{
		___decisionManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___decisionManager_5), value);
	}

	inline static int32_t get_offset_of_everything_6() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___everything_6)); }
	inline GameObject_t1113636619 * get_everything_6() const { return ___everything_6; }
	inline GameObject_t1113636619 ** get_address_of_everything_6() { return &___everything_6; }
	inline void set_everything_6(GameObject_t1113636619 * value)
	{
		___everything_6 = value;
		Il2CppCodeGenWriteBarrier((&___everything_6), value);
	}

	inline static int32_t get_offset_of_winnerText_7() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___winnerText_7)); }
	inline Text_t1901882714 * get_winnerText_7() const { return ___winnerText_7; }
	inline Text_t1901882714 ** get_address_of_winnerText_7() { return &___winnerText_7; }
	inline void set_winnerText_7(Text_t1901882714 * value)
	{
		___winnerText_7 = value;
		Il2CppCodeGenWriteBarrier((&___winnerText_7), value);
	}

	inline static int32_t get_offset_of_path_8() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___path_8)); }
	inline GameObject_t1113636619 * get_path_8() const { return ___path_8; }
	inline GameObject_t1113636619 ** get_address_of_path_8() { return &___path_8; }
	inline void set_path_8(GameObject_t1113636619 * value)
	{
		___path_8 = value;
		Il2CppCodeGenWriteBarrier((&___path_8), value);
	}

	inline static int32_t get_offset_of_bestDecisionText_9() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___bestDecisionText_9)); }
	inline Text_t1901882714 * get_bestDecisionText_9() const { return ___bestDecisionText_9; }
	inline Text_t1901882714 ** get_address_of_bestDecisionText_9() { return &___bestDecisionText_9; }
	inline void set_bestDecisionText_9(Text_t1901882714 * value)
	{
		___bestDecisionText_9 = value;
		Il2CppCodeGenWriteBarrier((&___bestDecisionText_9), value);
	}

	inline static int32_t get_offset_of_healthWeightingText_10() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___healthWeightingText_10)); }
	inline Text_t1901882714 * get_healthWeightingText_10() const { return ___healthWeightingText_10; }
	inline Text_t1901882714 ** get_address_of_healthWeightingText_10() { return &___healthWeightingText_10; }
	inline void set_healthWeightingText_10(Text_t1901882714 * value)
	{
		___healthWeightingText_10 = value;
		Il2CppCodeGenWriteBarrier((&___healthWeightingText_10), value);
	}

	inline static int32_t get_offset_of_healthBar_11() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___healthBar_11)); }
	inline SimpleHealthBar_t721070758 * get_healthBar_11() const { return ___healthBar_11; }
	inline SimpleHealthBar_t721070758 ** get_address_of_healthBar_11() { return &___healthBar_11; }
	inline void set_healthBar_11(SimpleHealthBar_t721070758 * value)
	{
		___healthBar_11 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_11), value);
	}

	inline static int32_t get_offset_of_hungerBar_12() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___hungerBar_12)); }
	inline SimpleHealthBar_t721070758 * get_hungerBar_12() const { return ___hungerBar_12; }
	inline SimpleHealthBar_t721070758 ** get_address_of_hungerBar_12() { return &___hungerBar_12; }
	inline void set_hungerBar_12(SimpleHealthBar_t721070758 * value)
	{
		___hungerBar_12 = value;
		Il2CppCodeGenWriteBarrier((&___hungerBar_12), value);
	}

	inline static int32_t get_offset_of_thirstBar_13() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___thirstBar_13)); }
	inline SimpleHealthBar_t721070758 * get_thirstBar_13() const { return ___thirstBar_13; }
	inline SimpleHealthBar_t721070758 ** get_address_of_thirstBar_13() { return &___thirstBar_13; }
	inline void set_thirstBar_13(SimpleHealthBar_t721070758 * value)
	{
		___thirstBar_13 = value;
		Il2CppCodeGenWriteBarrier((&___thirstBar_13), value);
	}

	inline static int32_t get_offset_of_health_14() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___health_14)); }
	inline GameObject_t1113636619 * get_health_14() const { return ___health_14; }
	inline GameObject_t1113636619 ** get_address_of_health_14() { return &___health_14; }
	inline void set_health_14(GameObject_t1113636619 * value)
	{
		___health_14 = value;
		Il2CppCodeGenWriteBarrier((&___health_14), value);
	}

	inline static int32_t get_offset_of_hunger_15() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___hunger_15)); }
	inline GameObject_t1113636619 * get_hunger_15() const { return ___hunger_15; }
	inline GameObject_t1113636619 ** get_address_of_hunger_15() { return &___hunger_15; }
	inline void set_hunger_15(GameObject_t1113636619 * value)
	{
		___hunger_15 = value;
		Il2CppCodeGenWriteBarrier((&___hunger_15), value);
	}

	inline static int32_t get_offset_of_thirst_16() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___thirst_16)); }
	inline GameObject_t1113636619 * get_thirst_16() const { return ___thirst_16; }
	inline GameObject_t1113636619 ** get_address_of_thirst_16() { return &___thirst_16; }
	inline void set_thirst_16(GameObject_t1113636619 * value)
	{
		___thirst_16 = value;
		Il2CppCodeGenWriteBarrier((&___thirst_16), value);
	}

	inline static int32_t get_offset_of_decisionsText_17() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___decisionsText_17)); }
	inline Text_t1901882714 * get_decisionsText_17() const { return ___decisionsText_17; }
	inline Text_t1901882714 ** get_address_of_decisionsText_17() { return &___decisionsText_17; }
	inline void set_decisionsText_17(Text_t1901882714 * value)
	{
		___decisionsText_17 = value;
		Il2CppCodeGenWriteBarrier((&___decisionsText_17), value);
	}

	inline static int32_t get_offset_of_everythingToggle_18() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___everythingToggle_18)); }
	inline Toggle_t2735377061 * get_everythingToggle_18() const { return ___everythingToggle_18; }
	inline Toggle_t2735377061 ** get_address_of_everythingToggle_18() { return &___everythingToggle_18; }
	inline void set_everythingToggle_18(Toggle_t2735377061 * value)
	{
		___everythingToggle_18 = value;
		Il2CppCodeGenWriteBarrier((&___everythingToggle_18), value);
	}

	inline static int32_t get_offset_of_pathToggle_19() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___pathToggle_19)); }
	inline Toggle_t2735377061 * get_pathToggle_19() const { return ___pathToggle_19; }
	inline Toggle_t2735377061 ** get_address_of_pathToggle_19() { return &___pathToggle_19; }
	inline void set_pathToggle_19(Toggle_t2735377061 * value)
	{
		___pathToggle_19 = value;
		Il2CppCodeGenWriteBarrier((&___pathToggle_19), value);
	}

	inline static int32_t get_offset_of_bestDecisionToggle_20() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___bestDecisionToggle_20)); }
	inline Toggle_t2735377061 * get_bestDecisionToggle_20() const { return ___bestDecisionToggle_20; }
	inline Toggle_t2735377061 ** get_address_of_bestDecisionToggle_20() { return &___bestDecisionToggle_20; }
	inline void set_bestDecisionToggle_20(Toggle_t2735377061 * value)
	{
		___bestDecisionToggle_20 = value;
		Il2CppCodeGenWriteBarrier((&___bestDecisionToggle_20), value);
	}

	inline static int32_t get_offset_of_healthToggle_21() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___healthToggle_21)); }
	inline Toggle_t2735377061 * get_healthToggle_21() const { return ___healthToggle_21; }
	inline Toggle_t2735377061 ** get_address_of_healthToggle_21() { return &___healthToggle_21; }
	inline void set_healthToggle_21(Toggle_t2735377061 * value)
	{
		___healthToggle_21 = value;
		Il2CppCodeGenWriteBarrier((&___healthToggle_21), value);
	}

	inline static int32_t get_offset_of_healthBarToggle_22() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___healthBarToggle_22)); }
	inline Toggle_t2735377061 * get_healthBarToggle_22() const { return ___healthBarToggle_22; }
	inline Toggle_t2735377061 ** get_address_of_healthBarToggle_22() { return &___healthBarToggle_22; }
	inline void set_healthBarToggle_22(Toggle_t2735377061 * value)
	{
		___healthBarToggle_22 = value;
		Il2CppCodeGenWriteBarrier((&___healthBarToggle_22), value);
	}

	inline static int32_t get_offset_of_thirstBarToggle_23() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___thirstBarToggle_23)); }
	inline Toggle_t2735377061 * get_thirstBarToggle_23() const { return ___thirstBarToggle_23; }
	inline Toggle_t2735377061 ** get_address_of_thirstBarToggle_23() { return &___thirstBarToggle_23; }
	inline void set_thirstBarToggle_23(Toggle_t2735377061 * value)
	{
		___thirstBarToggle_23 = value;
		Il2CppCodeGenWriteBarrier((&___thirstBarToggle_23), value);
	}

	inline static int32_t get_offset_of_hungerBarToggle_24() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___hungerBarToggle_24)); }
	inline Toggle_t2735377061 * get_hungerBarToggle_24() const { return ___hungerBarToggle_24; }
	inline Toggle_t2735377061 ** get_address_of_hungerBarToggle_24() { return &___hungerBarToggle_24; }
	inline void set_hungerBarToggle_24(Toggle_t2735377061 * value)
	{
		___hungerBarToggle_24 = value;
		Il2CppCodeGenWriteBarrier((&___hungerBarToggle_24), value);
	}

	inline static int32_t get_offset_of_decisionsToggle_25() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___decisionsToggle_25)); }
	inline Toggle_t2735377061 * get_decisionsToggle_25() const { return ___decisionsToggle_25; }
	inline Toggle_t2735377061 ** get_address_of_decisionsToggle_25() { return &___decisionsToggle_25; }
	inline void set_decisionsToggle_25(Toggle_t2735377061 * value)
	{
		___decisionsToggle_25 = value;
		Il2CppCodeGenWriteBarrier((&___decisionsToggle_25), value);
	}

	inline static int32_t get_offset_of_lastUIToggleValue_26() { return static_cast<int32_t>(offsetof(CompetitorUI_t472322071, ___lastUIToggleValue_26)); }
	inline bool get_lastUIToggleValue_26() const { return ___lastUIToggleValue_26; }
	inline bool* get_address_of_lastUIToggleValue_26() { return &___lastUIToggleValue_26; }
	inline void set_lastUIToggleValue_26(bool value)
	{
		___lastUIToggleValue_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPETITORUI_T472322071_H
#ifndef DEATHTRACKER_T1220569700_H
#define DEATHTRACKER_T1220569700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathTracker
struct  DeathTracker_t1220569700  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DeathTracker::background
	GameObject_t1113636619 * ___background_4;
	// UnityEngine.GameObject DeathTracker::deathText
	GameObject_t1113636619 * ___deathText_5;
	// System.Int32 DeathTracker::spacing
	int32_t ___spacing_6;
	// System.Int32 DeathTracker::numberOfDeaths
	int32_t ___numberOfDeaths_7;

public:
	inline static int32_t get_offset_of_background_4() { return static_cast<int32_t>(offsetof(DeathTracker_t1220569700, ___background_4)); }
	inline GameObject_t1113636619 * get_background_4() const { return ___background_4; }
	inline GameObject_t1113636619 ** get_address_of_background_4() { return &___background_4; }
	inline void set_background_4(GameObject_t1113636619 * value)
	{
		___background_4 = value;
		Il2CppCodeGenWriteBarrier((&___background_4), value);
	}

	inline static int32_t get_offset_of_deathText_5() { return static_cast<int32_t>(offsetof(DeathTracker_t1220569700, ___deathText_5)); }
	inline GameObject_t1113636619 * get_deathText_5() const { return ___deathText_5; }
	inline GameObject_t1113636619 ** get_address_of_deathText_5() { return &___deathText_5; }
	inline void set_deathText_5(GameObject_t1113636619 * value)
	{
		___deathText_5 = value;
		Il2CppCodeGenWriteBarrier((&___deathText_5), value);
	}

	inline static int32_t get_offset_of_spacing_6() { return static_cast<int32_t>(offsetof(DeathTracker_t1220569700, ___spacing_6)); }
	inline int32_t get_spacing_6() const { return ___spacing_6; }
	inline int32_t* get_address_of_spacing_6() { return &___spacing_6; }
	inline void set_spacing_6(int32_t value)
	{
		___spacing_6 = value;
	}

	inline static int32_t get_offset_of_numberOfDeaths_7() { return static_cast<int32_t>(offsetof(DeathTracker_t1220569700, ___numberOfDeaths_7)); }
	inline int32_t get_numberOfDeaths_7() const { return ___numberOfDeaths_7; }
	inline int32_t* get_address_of_numberOfDeaths_7() { return &___numberOfDeaths_7; }
	inline void set_numberOfDeaths_7(int32_t value)
	{
		___numberOfDeaths_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHTRACKER_T1220569700_H
#ifndef ENVMAPANIMATOR_T1140999784_H
#define ENVMAPANIMATOR_T1140999784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1140999784  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t3722313464  ___RotationSpeeds_4;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_5;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t340375123 * ___m_material_6;

public:
	inline static int32_t get_offset_of_RotationSpeeds_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___RotationSpeeds_4)); }
	inline Vector3_t3722313464  get_RotationSpeeds_4() const { return ___RotationSpeeds_4; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeeds_4() { return &___RotationSpeeds_4; }
	inline void set_RotationSpeeds_4(Vector3_t3722313464  value)
	{
		___RotationSpeeds_4 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_textMeshPro_5)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_material_6() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_material_6)); }
	inline Material_t340375123 * get_m_material_6() const { return ___m_material_6; }
	inline Material_t340375123 ** get_address_of_m_material_6() { return &___m_material_6; }
	inline void set_m_material_6(Material_t340375123 * value)
	{
		___m_material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1140999784_H
#ifndef ITEMMANAGER_T3254073967_H
#define ITEMMANAGER_T3254073967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemManager
struct  ItemManager_t3254073967  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 ItemManager::maxWaterInMap
	int32_t ___maxWaterInMap_4;
	// System.Int32 ItemManager::maxFoodInMap
	int32_t ___maxFoodInMap_5;
	// System.Int32 ItemManager::maxLandminesInMap
	int32_t ___maxLandminesInMap_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ItemManager::water
	List_1_t2585711361 * ___water_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ItemManager::food
	List_1_t2585711361 * ___food_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ItemManager::landmines
	List_1_t2585711361 * ___landmines_9;
	// UnityEngine.GameObject ItemManager::flag
	GameObject_t1113636619 * ___flag_10;

public:
	inline static int32_t get_offset_of_maxWaterInMap_4() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___maxWaterInMap_4)); }
	inline int32_t get_maxWaterInMap_4() const { return ___maxWaterInMap_4; }
	inline int32_t* get_address_of_maxWaterInMap_4() { return &___maxWaterInMap_4; }
	inline void set_maxWaterInMap_4(int32_t value)
	{
		___maxWaterInMap_4 = value;
	}

	inline static int32_t get_offset_of_maxFoodInMap_5() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___maxFoodInMap_5)); }
	inline int32_t get_maxFoodInMap_5() const { return ___maxFoodInMap_5; }
	inline int32_t* get_address_of_maxFoodInMap_5() { return &___maxFoodInMap_5; }
	inline void set_maxFoodInMap_5(int32_t value)
	{
		___maxFoodInMap_5 = value;
	}

	inline static int32_t get_offset_of_maxLandminesInMap_6() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___maxLandminesInMap_6)); }
	inline int32_t get_maxLandminesInMap_6() const { return ___maxLandminesInMap_6; }
	inline int32_t* get_address_of_maxLandminesInMap_6() { return &___maxLandminesInMap_6; }
	inline void set_maxLandminesInMap_6(int32_t value)
	{
		___maxLandminesInMap_6 = value;
	}

	inline static int32_t get_offset_of_water_7() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___water_7)); }
	inline List_1_t2585711361 * get_water_7() const { return ___water_7; }
	inline List_1_t2585711361 ** get_address_of_water_7() { return &___water_7; }
	inline void set_water_7(List_1_t2585711361 * value)
	{
		___water_7 = value;
		Il2CppCodeGenWriteBarrier((&___water_7), value);
	}

	inline static int32_t get_offset_of_food_8() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___food_8)); }
	inline List_1_t2585711361 * get_food_8() const { return ___food_8; }
	inline List_1_t2585711361 ** get_address_of_food_8() { return &___food_8; }
	inline void set_food_8(List_1_t2585711361 * value)
	{
		___food_8 = value;
		Il2CppCodeGenWriteBarrier((&___food_8), value);
	}

	inline static int32_t get_offset_of_landmines_9() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___landmines_9)); }
	inline List_1_t2585711361 * get_landmines_9() const { return ___landmines_9; }
	inline List_1_t2585711361 ** get_address_of_landmines_9() { return &___landmines_9; }
	inline void set_landmines_9(List_1_t2585711361 * value)
	{
		___landmines_9 = value;
		Il2CppCodeGenWriteBarrier((&___landmines_9), value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(ItemManager_t3254073967, ___flag_10)); }
	inline GameObject_t1113636619 * get_flag_10() const { return ___flag_10; }
	inline GameObject_t1113636619 ** get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(GameObject_t1113636619 * value)
	{
		___flag_10 = value;
		Il2CppCodeGenWriteBarrier((&___flag_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMMANAGER_T3254073967_H
#ifndef MAPGENERATION_T47126671_H
#define MAPGENERATION_T47126671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapGen.MapGeneration
struct  MapGeneration_t47126671  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MapGen.MapGeneration::loading
	GameObject_t1113636619 * ___loading_4;
	// System.Int32 MapGen.MapGeneration::numberOfRoomBranches
	int32_t ___numberOfRoomBranches_5;
	// System.Int32 MapGen.MapGeneration::roomExpandConnectionLimit
	int32_t ___roomExpandConnectionLimit_6;
	// UnityEngine.GameObject[] MapGen.MapGeneration::RoomPrefabs
	GameObjectU5BU5D_t3328599146* ___RoomPrefabs_7;
	// UnityEngine.GameObject MapGen.MapGeneration::StartingRoomPrefab
	GameObject_t1113636619 * ___StartingRoomPrefab_8;
	// MapGen.MapGrid MapGen.MapGeneration::grid
	MapGrid_t3370239467 * ___grid_9;
	// System.Collections.Generic.List`1<MapGen.Room> MapGen.MapGeneration::activeRooms
	List_1_t2552512423 * ___activeRooms_10;

public:
	inline static int32_t get_offset_of_loading_4() { return static_cast<int32_t>(offsetof(MapGeneration_t47126671, ___loading_4)); }
	inline GameObject_t1113636619 * get_loading_4() const { return ___loading_4; }
	inline GameObject_t1113636619 ** get_address_of_loading_4() { return &___loading_4; }
	inline void set_loading_4(GameObject_t1113636619 * value)
	{
		___loading_4 = value;
		Il2CppCodeGenWriteBarrier((&___loading_4), value);
	}

	inline static int32_t get_offset_of_numberOfRoomBranches_5() { return static_cast<int32_t>(offsetof(MapGeneration_t47126671, ___numberOfRoomBranches_5)); }
	inline int32_t get_numberOfRoomBranches_5() const { return ___numberOfRoomBranches_5; }
	inline int32_t* get_address_of_numberOfRoomBranches_5() { return &___numberOfRoomBranches_5; }
	inline void set_numberOfRoomBranches_5(int32_t value)
	{
		___numberOfRoomBranches_5 = value;
	}

	inline static int32_t get_offset_of_roomExpandConnectionLimit_6() { return static_cast<int32_t>(offsetof(MapGeneration_t47126671, ___roomExpandConnectionLimit_6)); }
	inline int32_t get_roomExpandConnectionLimit_6() const { return ___roomExpandConnectionLimit_6; }
	inline int32_t* get_address_of_roomExpandConnectionLimit_6() { return &___roomExpandConnectionLimit_6; }
	inline void set_roomExpandConnectionLimit_6(int32_t value)
	{
		___roomExpandConnectionLimit_6 = value;
	}

	inline static int32_t get_offset_of_RoomPrefabs_7() { return static_cast<int32_t>(offsetof(MapGeneration_t47126671, ___RoomPrefabs_7)); }
	inline GameObjectU5BU5D_t3328599146* get_RoomPrefabs_7() const { return ___RoomPrefabs_7; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_RoomPrefabs_7() { return &___RoomPrefabs_7; }
	inline void set_RoomPrefabs_7(GameObjectU5BU5D_t3328599146* value)
	{
		___RoomPrefabs_7 = value;
		Il2CppCodeGenWriteBarrier((&___RoomPrefabs_7), value);
	}

	inline static int32_t get_offset_of_StartingRoomPrefab_8() { return static_cast<int32_t>(offsetof(MapGeneration_t47126671, ___StartingRoomPrefab_8)); }
	inline GameObject_t1113636619 * get_StartingRoomPrefab_8() const { return ___StartingRoomPrefab_8; }
	inline GameObject_t1113636619 ** get_address_of_StartingRoomPrefab_8() { return &___StartingRoomPrefab_8; }
	inline void set_StartingRoomPrefab_8(GameObject_t1113636619 * value)
	{
		___StartingRoomPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___StartingRoomPrefab_8), value);
	}

	inline static int32_t get_offset_of_grid_9() { return static_cast<int32_t>(offsetof(MapGeneration_t47126671, ___grid_9)); }
	inline MapGrid_t3370239467 * get_grid_9() const { return ___grid_9; }
	inline MapGrid_t3370239467 ** get_address_of_grid_9() { return &___grid_9; }
	inline void set_grid_9(MapGrid_t3370239467 * value)
	{
		___grid_9 = value;
		Il2CppCodeGenWriteBarrier((&___grid_9), value);
	}

	inline static int32_t get_offset_of_activeRooms_10() { return static_cast<int32_t>(offsetof(MapGeneration_t47126671, ___activeRooms_10)); }
	inline List_1_t2552512423 * get_activeRooms_10() const { return ___activeRooms_10; }
	inline List_1_t2552512423 ** get_address_of_activeRooms_10() { return &___activeRooms_10; }
	inline void set_activeRooms_10(List_1_t2552512423 * value)
	{
		___activeRooms_10 = value;
		Il2CppCodeGenWriteBarrier((&___activeRooms_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPGENERATION_T47126671_H
#ifndef MAPMANAGER_T3095496401_H
#define MAPMANAGER_T3095496401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapManager
struct  MapManager_t3095496401  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MapManager::loading
	GameObject_t1113636619 * ___loading_4;
	// UnityEngine.UI.Slider MapManager::timeSlider
	Slider_t3903728902 * ___timeSlider_5;
	// UnityEngine.UI.Slider MapManager::roomBranchSlider
	Slider_t3903728902 * ___roomBranchSlider_6;
	// UnityEngine.UI.Slider MapManager::roomExpandSlider
	Slider_t3903728902 * ___roomExpandSlider_7;
	// UnityEngine.UI.Slider MapManager::waterAmountSlider
	Slider_t3903728902 * ___waterAmountSlider_8;
	// UnityEngine.UI.Slider MapManager::foodAmountSlider
	Slider_t3903728902 * ___foodAmountSlider_9;
	// UnityEngine.UI.Slider MapManager::landmineAmountSlider
	Slider_t3903728902 * ___landmineAmountSlider_10;
	// UnityEngine.UI.Slider MapManager::competitorAmountSlider
	Slider_t3903728902 * ___competitorAmountSlider_11;
	// System.Int32 MapManager::minFlagDistanceFromCompetitors
	int32_t ___minFlagDistanceFromCompetitors_12;

public:
	inline static int32_t get_offset_of_loading_4() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___loading_4)); }
	inline GameObject_t1113636619 * get_loading_4() const { return ___loading_4; }
	inline GameObject_t1113636619 ** get_address_of_loading_4() { return &___loading_4; }
	inline void set_loading_4(GameObject_t1113636619 * value)
	{
		___loading_4 = value;
		Il2CppCodeGenWriteBarrier((&___loading_4), value);
	}

	inline static int32_t get_offset_of_timeSlider_5() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___timeSlider_5)); }
	inline Slider_t3903728902 * get_timeSlider_5() const { return ___timeSlider_5; }
	inline Slider_t3903728902 ** get_address_of_timeSlider_5() { return &___timeSlider_5; }
	inline void set_timeSlider_5(Slider_t3903728902 * value)
	{
		___timeSlider_5 = value;
		Il2CppCodeGenWriteBarrier((&___timeSlider_5), value);
	}

	inline static int32_t get_offset_of_roomBranchSlider_6() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___roomBranchSlider_6)); }
	inline Slider_t3903728902 * get_roomBranchSlider_6() const { return ___roomBranchSlider_6; }
	inline Slider_t3903728902 ** get_address_of_roomBranchSlider_6() { return &___roomBranchSlider_6; }
	inline void set_roomBranchSlider_6(Slider_t3903728902 * value)
	{
		___roomBranchSlider_6 = value;
		Il2CppCodeGenWriteBarrier((&___roomBranchSlider_6), value);
	}

	inline static int32_t get_offset_of_roomExpandSlider_7() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___roomExpandSlider_7)); }
	inline Slider_t3903728902 * get_roomExpandSlider_7() const { return ___roomExpandSlider_7; }
	inline Slider_t3903728902 ** get_address_of_roomExpandSlider_7() { return &___roomExpandSlider_7; }
	inline void set_roomExpandSlider_7(Slider_t3903728902 * value)
	{
		___roomExpandSlider_7 = value;
		Il2CppCodeGenWriteBarrier((&___roomExpandSlider_7), value);
	}

	inline static int32_t get_offset_of_waterAmountSlider_8() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___waterAmountSlider_8)); }
	inline Slider_t3903728902 * get_waterAmountSlider_8() const { return ___waterAmountSlider_8; }
	inline Slider_t3903728902 ** get_address_of_waterAmountSlider_8() { return &___waterAmountSlider_8; }
	inline void set_waterAmountSlider_8(Slider_t3903728902 * value)
	{
		___waterAmountSlider_8 = value;
		Il2CppCodeGenWriteBarrier((&___waterAmountSlider_8), value);
	}

	inline static int32_t get_offset_of_foodAmountSlider_9() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___foodAmountSlider_9)); }
	inline Slider_t3903728902 * get_foodAmountSlider_9() const { return ___foodAmountSlider_9; }
	inline Slider_t3903728902 ** get_address_of_foodAmountSlider_9() { return &___foodAmountSlider_9; }
	inline void set_foodAmountSlider_9(Slider_t3903728902 * value)
	{
		___foodAmountSlider_9 = value;
		Il2CppCodeGenWriteBarrier((&___foodAmountSlider_9), value);
	}

	inline static int32_t get_offset_of_landmineAmountSlider_10() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___landmineAmountSlider_10)); }
	inline Slider_t3903728902 * get_landmineAmountSlider_10() const { return ___landmineAmountSlider_10; }
	inline Slider_t3903728902 ** get_address_of_landmineAmountSlider_10() { return &___landmineAmountSlider_10; }
	inline void set_landmineAmountSlider_10(Slider_t3903728902 * value)
	{
		___landmineAmountSlider_10 = value;
		Il2CppCodeGenWriteBarrier((&___landmineAmountSlider_10), value);
	}

	inline static int32_t get_offset_of_competitorAmountSlider_11() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___competitorAmountSlider_11)); }
	inline Slider_t3903728902 * get_competitorAmountSlider_11() const { return ___competitorAmountSlider_11; }
	inline Slider_t3903728902 ** get_address_of_competitorAmountSlider_11() { return &___competitorAmountSlider_11; }
	inline void set_competitorAmountSlider_11(Slider_t3903728902 * value)
	{
		___competitorAmountSlider_11 = value;
		Il2CppCodeGenWriteBarrier((&___competitorAmountSlider_11), value);
	}

	inline static int32_t get_offset_of_minFlagDistanceFromCompetitors_12() { return static_cast<int32_t>(offsetof(MapManager_t3095496401, ___minFlagDistanceFromCompetitors_12)); }
	inline int32_t get_minFlagDistanceFromCompetitors_12() const { return ___minFlagDistanceFromCompetitors_12; }
	inline int32_t* get_address_of_minFlagDistanceFromCompetitors_12() { return &___minFlagDistanceFromCompetitors_12; }
	inline void set_minFlagDistanceFromCompetitors_12(int32_t value)
	{
		___minFlagDistanceFromCompetitors_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPMANAGER_T3095496401_H
#ifndef PATHRENDERER_T3314173992_H
#define PATHRENDERER_T3314173992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathRenderer
struct  PathRenderer_t3314173992  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent PathRenderer::nv
	NavMeshAgent_t1276799816 * ___nv_4;
	// UnityEngine.LineRenderer PathRenderer::lr
	LineRenderer_t3154350270 * ___lr_5;

public:
	inline static int32_t get_offset_of_nv_4() { return static_cast<int32_t>(offsetof(PathRenderer_t3314173992, ___nv_4)); }
	inline NavMeshAgent_t1276799816 * get_nv_4() const { return ___nv_4; }
	inline NavMeshAgent_t1276799816 ** get_address_of_nv_4() { return &___nv_4; }
	inline void set_nv_4(NavMeshAgent_t1276799816 * value)
	{
		___nv_4 = value;
		Il2CppCodeGenWriteBarrier((&___nv_4), value);
	}

	inline static int32_t get_offset_of_lr_5() { return static_cast<int32_t>(offsetof(PathRenderer_t3314173992, ___lr_5)); }
	inline LineRenderer_t3154350270 * get_lr_5() const { return ___lr_5; }
	inline LineRenderer_t3154350270 ** get_address_of_lr_5() { return &___lr_5; }
	inline void set_lr_5(LineRenderer_t3154350270 * value)
	{
		___lr_5 = value;
		Il2CppCodeGenWriteBarrier((&___lr_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHRENDERER_T3314173992_H
#ifndef PICKUPABLEITEM_T2078450402_H
#define PICKUPABLEITEM_T2078450402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickupableItem
struct  PickupableItem_t2078450402  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKUPABLEITEM_T2078450402_H
#ifndef INITIALIZATION_T3119758612_H
#define INITIALIZATION_T3119758612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Initialization
struct  Initialization_t3119758612  : public MonoBehaviour_t3962482529
{
public:
	// Pixelplacement.StateMachine Pixelplacement.Initialization::_stateMachine
	StateMachine_t4243775295 * ____stateMachine_4;
	// Pixelplacement.DisplayObject Pixelplacement.Initialization::_displayObject
	DisplayObject_t314287876 * ____displayObject_5;

public:
	inline static int32_t get_offset_of__stateMachine_4() { return static_cast<int32_t>(offsetof(Initialization_t3119758612, ____stateMachine_4)); }
	inline StateMachine_t4243775295 * get__stateMachine_4() const { return ____stateMachine_4; }
	inline StateMachine_t4243775295 ** get_address_of__stateMachine_4() { return &____stateMachine_4; }
	inline void set__stateMachine_4(StateMachine_t4243775295 * value)
	{
		____stateMachine_4 = value;
		Il2CppCodeGenWriteBarrier((&____stateMachine_4), value);
	}

	inline static int32_t get_offset_of__displayObject_5() { return static_cast<int32_t>(offsetof(Initialization_t3119758612, ____displayObject_5)); }
	inline DisplayObject_t314287876 * get__displayObject_5() const { return ____displayObject_5; }
	inline DisplayObject_t314287876 ** get_address_of__displayObject_5() { return &____displayObject_5; }
	inline void set__displayObject_5(DisplayObject_t314287876 * value)
	{
		____displayObject_5 = value;
		Il2CppCodeGenWriteBarrier((&____displayObject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZATION_T3119758612_H
#ifndef SIMPLEHEALTHBAR_T721070758_H
#define SIMPLEHEALTHBAR_T721070758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar
struct  SimpleHealthBar_t721070758  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image SimpleHealthBar::barImage
	Image_t2670269651 * ___barImage_4;
	// SimpleHealthBar/ColorMode SimpleHealthBar::colorMode
	int32_t ___colorMode_5;
	// UnityEngine.Color SimpleHealthBar::barColor
	Color_t2555686324  ___barColor_6;
	// UnityEngine.Gradient SimpleHealthBar::barGradient
	Gradient_t3067099924 * ___barGradient_7;
	// SimpleHealthBar/DisplayText SimpleHealthBar::displayText
	int32_t ___displayText_8;
	// UnityEngine.UI.Text SimpleHealthBar::barText
	Text_t1901882714 * ___barText_9;
	// System.String SimpleHealthBar::additionalText
	String_t* ___additionalText_10;
	// System.Single SimpleHealthBar::_currentFraction
	float ____currentFraction_11;
	// System.Single SimpleHealthBar::_maxValue
	float ____maxValue_12;
	// System.Single SimpleHealthBar::targetFill
	float ___targetFill_13;

public:
	inline static int32_t get_offset_of_barImage_4() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barImage_4)); }
	inline Image_t2670269651 * get_barImage_4() const { return ___barImage_4; }
	inline Image_t2670269651 ** get_address_of_barImage_4() { return &___barImage_4; }
	inline void set_barImage_4(Image_t2670269651 * value)
	{
		___barImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___barImage_4), value);
	}

	inline static int32_t get_offset_of_colorMode_5() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___colorMode_5)); }
	inline int32_t get_colorMode_5() const { return ___colorMode_5; }
	inline int32_t* get_address_of_colorMode_5() { return &___colorMode_5; }
	inline void set_colorMode_5(int32_t value)
	{
		___colorMode_5 = value;
	}

	inline static int32_t get_offset_of_barColor_6() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barColor_6)); }
	inline Color_t2555686324  get_barColor_6() const { return ___barColor_6; }
	inline Color_t2555686324 * get_address_of_barColor_6() { return &___barColor_6; }
	inline void set_barColor_6(Color_t2555686324  value)
	{
		___barColor_6 = value;
	}

	inline static int32_t get_offset_of_barGradient_7() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barGradient_7)); }
	inline Gradient_t3067099924 * get_barGradient_7() const { return ___barGradient_7; }
	inline Gradient_t3067099924 ** get_address_of_barGradient_7() { return &___barGradient_7; }
	inline void set_barGradient_7(Gradient_t3067099924 * value)
	{
		___barGradient_7 = value;
		Il2CppCodeGenWriteBarrier((&___barGradient_7), value);
	}

	inline static int32_t get_offset_of_displayText_8() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___displayText_8)); }
	inline int32_t get_displayText_8() const { return ___displayText_8; }
	inline int32_t* get_address_of_displayText_8() { return &___displayText_8; }
	inline void set_displayText_8(int32_t value)
	{
		___displayText_8 = value;
	}

	inline static int32_t get_offset_of_barText_9() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___barText_9)); }
	inline Text_t1901882714 * get_barText_9() const { return ___barText_9; }
	inline Text_t1901882714 ** get_address_of_barText_9() { return &___barText_9; }
	inline void set_barText_9(Text_t1901882714 * value)
	{
		___barText_9 = value;
		Il2CppCodeGenWriteBarrier((&___barText_9), value);
	}

	inline static int32_t get_offset_of_additionalText_10() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___additionalText_10)); }
	inline String_t* get_additionalText_10() const { return ___additionalText_10; }
	inline String_t** get_address_of_additionalText_10() { return &___additionalText_10; }
	inline void set_additionalText_10(String_t* value)
	{
		___additionalText_10 = value;
		Il2CppCodeGenWriteBarrier((&___additionalText_10), value);
	}

	inline static int32_t get_offset_of__currentFraction_11() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ____currentFraction_11)); }
	inline float get__currentFraction_11() const { return ____currentFraction_11; }
	inline float* get_address_of__currentFraction_11() { return &____currentFraction_11; }
	inline void set__currentFraction_11(float value)
	{
		____currentFraction_11 = value;
	}

	inline static int32_t get_offset_of__maxValue_12() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ____maxValue_12)); }
	inline float get__maxValue_12() const { return ____maxValue_12; }
	inline float* get_address_of__maxValue_12() { return &____maxValue_12; }
	inline void set__maxValue_12(float value)
	{
		____maxValue_12 = value;
	}

	inline static int32_t get_offset_of_targetFill_13() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t721070758, ___targetFill_13)); }
	inline float get_targetFill_13() const { return ___targetFill_13; }
	inline float* get_address_of_targetFill_13() { return &___targetFill_13; }
	inline void set_targetFill_13(float value)
	{
		___targetFill_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEHEALTHBAR_T721070758_H
#ifndef ASTEROIDCONTROLLER_T3828879615_H
#define ASTEROIDCONTROLLER_T3828879615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.AsteroidController
struct  AsteroidController_t3828879615  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.AsteroidController::myRigidbody
	Rigidbody_t3916780224 * ___myRigidbody_4;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::canDestroy
	bool ___canDestroy_5;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::isDestroyed
	bool ___isDestroyed_6;
	// System.Boolean SimpleHealthBar_SpaceshipExample.AsteroidController::isDebris
	bool ___isDebris_7;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController::health
	float ___health_8;
	// System.Single SimpleHealthBar_SpaceshipExample.AsteroidController::maxHealth
	float ___maxHealth_9;

public:
	inline static int32_t get_offset_of_myRigidbody_4() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___myRigidbody_4)); }
	inline Rigidbody_t3916780224 * get_myRigidbody_4() const { return ___myRigidbody_4; }
	inline Rigidbody_t3916780224 ** get_address_of_myRigidbody_4() { return &___myRigidbody_4; }
	inline void set_myRigidbody_4(Rigidbody_t3916780224 * value)
	{
		___myRigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_4), value);
	}

	inline static int32_t get_offset_of_canDestroy_5() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___canDestroy_5)); }
	inline bool get_canDestroy_5() const { return ___canDestroy_5; }
	inline bool* get_address_of_canDestroy_5() { return &___canDestroy_5; }
	inline void set_canDestroy_5(bool value)
	{
		___canDestroy_5 = value;
	}

	inline static int32_t get_offset_of_isDestroyed_6() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___isDestroyed_6)); }
	inline bool get_isDestroyed_6() const { return ___isDestroyed_6; }
	inline bool* get_address_of_isDestroyed_6() { return &___isDestroyed_6; }
	inline void set_isDestroyed_6(bool value)
	{
		___isDestroyed_6 = value;
	}

	inline static int32_t get_offset_of_isDebris_7() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___isDebris_7)); }
	inline bool get_isDebris_7() const { return ___isDebris_7; }
	inline bool* get_address_of_isDebris_7() { return &___isDebris_7; }
	inline void set_isDebris_7(bool value)
	{
		___isDebris_7 = value;
	}

	inline static int32_t get_offset_of_health_8() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___health_8)); }
	inline float get_health_8() const { return ___health_8; }
	inline float* get_address_of_health_8() { return &___health_8; }
	inline void set_health_8(float value)
	{
		___health_8 = value;
	}

	inline static int32_t get_offset_of_maxHealth_9() { return static_cast<int32_t>(offsetof(AsteroidController_t3828879615, ___maxHealth_9)); }
	inline float get_maxHealth_9() const { return ___maxHealth_9; }
	inline float* get_address_of_maxHealth_9() { return &___maxHealth_9; }
	inline void set_maxHealth_9(float value)
	{
		___maxHealth_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASTEROIDCONTROLLER_T3828879615_H
#ifndef GAMEMANAGER_T3924726104_H
#define GAMEMANAGER_T3924726104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.GameManager
struct  GameManager_t3924726104  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::astroidPrefab
	GameObject_t1113636619 * ___astroidPrefab_5;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::debrisPrefab
	GameObject_t1113636619 * ___debrisPrefab_6;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::explosionPrefab
	GameObject_t1113636619 * ___explosionPrefab_7;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.GameManager::healthPickupPrefab
	GameObject_t1113636619 * ___healthPickupPrefab_8;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager::spawning
	bool ___spawning_9;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::spawnTimeMin
	float ___spawnTimeMin_10;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::spawnTimeMax
	float ___spawnTimeMax_11;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::startingAsteroids
	int32_t ___startingAsteroids_12;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::healthSpawnTimeMin
	float ___healthSpawnTimeMin_13;
	// System.Single SimpleHealthBar_SpaceshipExample.GameManager::healthSpawnTimeMax
	float ___healthSpawnTimeMax_14;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::scoreText
	Text_t1901882714 * ___scoreText_15;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::finalScoreText
	Text_t1901882714 * ___finalScoreText_16;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::score
	int32_t ___score_17;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::asteroidPoints
	int32_t ___asteroidPoints_18;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::debrisPoints
	int32_t ___debrisPoints_19;
	// UnityEngine.UI.Image SimpleHealthBar_SpaceshipExample.GameManager::gameOverScreen
	Image_t2670269651 * ___gameOverScreen_20;
	// UnityEngine.UI.Text SimpleHealthBar_SpaceshipExample.GameManager::gameOverText
	Text_t1901882714 * ___gameOverText_21;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::asteroidHealth
	int32_t ___asteroidHealth_22;
	// System.Int32 SimpleHealthBar_SpaceshipExample.GameManager::debrisHealth
	int32_t ___debrisHealth_23;
	// System.Boolean SimpleHealthBar_SpaceshipExample.GameManager::hasLost
	bool ___hasLost_24;

public:
	inline static int32_t get_offset_of_astroidPrefab_5() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___astroidPrefab_5)); }
	inline GameObject_t1113636619 * get_astroidPrefab_5() const { return ___astroidPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_astroidPrefab_5() { return &___astroidPrefab_5; }
	inline void set_astroidPrefab_5(GameObject_t1113636619 * value)
	{
		___astroidPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___astroidPrefab_5), value);
	}

	inline static int32_t get_offset_of_debrisPrefab_6() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___debrisPrefab_6)); }
	inline GameObject_t1113636619 * get_debrisPrefab_6() const { return ___debrisPrefab_6; }
	inline GameObject_t1113636619 ** get_address_of_debrisPrefab_6() { return &___debrisPrefab_6; }
	inline void set_debrisPrefab_6(GameObject_t1113636619 * value)
	{
		___debrisPrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___debrisPrefab_6), value);
	}

	inline static int32_t get_offset_of_explosionPrefab_7() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___explosionPrefab_7)); }
	inline GameObject_t1113636619 * get_explosionPrefab_7() const { return ___explosionPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_explosionPrefab_7() { return &___explosionPrefab_7; }
	inline void set_explosionPrefab_7(GameObject_t1113636619 * value)
	{
		___explosionPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___explosionPrefab_7), value);
	}

	inline static int32_t get_offset_of_healthPickupPrefab_8() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___healthPickupPrefab_8)); }
	inline GameObject_t1113636619 * get_healthPickupPrefab_8() const { return ___healthPickupPrefab_8; }
	inline GameObject_t1113636619 ** get_address_of_healthPickupPrefab_8() { return &___healthPickupPrefab_8; }
	inline void set_healthPickupPrefab_8(GameObject_t1113636619 * value)
	{
		___healthPickupPrefab_8 = value;
		Il2CppCodeGenWriteBarrier((&___healthPickupPrefab_8), value);
	}

	inline static int32_t get_offset_of_spawning_9() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___spawning_9)); }
	inline bool get_spawning_9() const { return ___spawning_9; }
	inline bool* get_address_of_spawning_9() { return &___spawning_9; }
	inline void set_spawning_9(bool value)
	{
		___spawning_9 = value;
	}

	inline static int32_t get_offset_of_spawnTimeMin_10() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___spawnTimeMin_10)); }
	inline float get_spawnTimeMin_10() const { return ___spawnTimeMin_10; }
	inline float* get_address_of_spawnTimeMin_10() { return &___spawnTimeMin_10; }
	inline void set_spawnTimeMin_10(float value)
	{
		___spawnTimeMin_10 = value;
	}

	inline static int32_t get_offset_of_spawnTimeMax_11() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___spawnTimeMax_11)); }
	inline float get_spawnTimeMax_11() const { return ___spawnTimeMax_11; }
	inline float* get_address_of_spawnTimeMax_11() { return &___spawnTimeMax_11; }
	inline void set_spawnTimeMax_11(float value)
	{
		___spawnTimeMax_11 = value;
	}

	inline static int32_t get_offset_of_startingAsteroids_12() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___startingAsteroids_12)); }
	inline int32_t get_startingAsteroids_12() const { return ___startingAsteroids_12; }
	inline int32_t* get_address_of_startingAsteroids_12() { return &___startingAsteroids_12; }
	inline void set_startingAsteroids_12(int32_t value)
	{
		___startingAsteroids_12 = value;
	}

	inline static int32_t get_offset_of_healthSpawnTimeMin_13() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___healthSpawnTimeMin_13)); }
	inline float get_healthSpawnTimeMin_13() const { return ___healthSpawnTimeMin_13; }
	inline float* get_address_of_healthSpawnTimeMin_13() { return &___healthSpawnTimeMin_13; }
	inline void set_healthSpawnTimeMin_13(float value)
	{
		___healthSpawnTimeMin_13 = value;
	}

	inline static int32_t get_offset_of_healthSpawnTimeMax_14() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___healthSpawnTimeMax_14)); }
	inline float get_healthSpawnTimeMax_14() const { return ___healthSpawnTimeMax_14; }
	inline float* get_address_of_healthSpawnTimeMax_14() { return &___healthSpawnTimeMax_14; }
	inline void set_healthSpawnTimeMax_14(float value)
	{
		___healthSpawnTimeMax_14 = value;
	}

	inline static int32_t get_offset_of_scoreText_15() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___scoreText_15)); }
	inline Text_t1901882714 * get_scoreText_15() const { return ___scoreText_15; }
	inline Text_t1901882714 ** get_address_of_scoreText_15() { return &___scoreText_15; }
	inline void set_scoreText_15(Text_t1901882714 * value)
	{
		___scoreText_15 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_15), value);
	}

	inline static int32_t get_offset_of_finalScoreText_16() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___finalScoreText_16)); }
	inline Text_t1901882714 * get_finalScoreText_16() const { return ___finalScoreText_16; }
	inline Text_t1901882714 ** get_address_of_finalScoreText_16() { return &___finalScoreText_16; }
	inline void set_finalScoreText_16(Text_t1901882714 * value)
	{
		___finalScoreText_16 = value;
		Il2CppCodeGenWriteBarrier((&___finalScoreText_16), value);
	}

	inline static int32_t get_offset_of_score_17() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___score_17)); }
	inline int32_t get_score_17() const { return ___score_17; }
	inline int32_t* get_address_of_score_17() { return &___score_17; }
	inline void set_score_17(int32_t value)
	{
		___score_17 = value;
	}

	inline static int32_t get_offset_of_asteroidPoints_18() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___asteroidPoints_18)); }
	inline int32_t get_asteroidPoints_18() const { return ___asteroidPoints_18; }
	inline int32_t* get_address_of_asteroidPoints_18() { return &___asteroidPoints_18; }
	inline void set_asteroidPoints_18(int32_t value)
	{
		___asteroidPoints_18 = value;
	}

	inline static int32_t get_offset_of_debrisPoints_19() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___debrisPoints_19)); }
	inline int32_t get_debrisPoints_19() const { return ___debrisPoints_19; }
	inline int32_t* get_address_of_debrisPoints_19() { return &___debrisPoints_19; }
	inline void set_debrisPoints_19(int32_t value)
	{
		___debrisPoints_19 = value;
	}

	inline static int32_t get_offset_of_gameOverScreen_20() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___gameOverScreen_20)); }
	inline Image_t2670269651 * get_gameOverScreen_20() const { return ___gameOverScreen_20; }
	inline Image_t2670269651 ** get_address_of_gameOverScreen_20() { return &___gameOverScreen_20; }
	inline void set_gameOverScreen_20(Image_t2670269651 * value)
	{
		___gameOverScreen_20 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScreen_20), value);
	}

	inline static int32_t get_offset_of_gameOverText_21() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___gameOverText_21)); }
	inline Text_t1901882714 * get_gameOverText_21() const { return ___gameOverText_21; }
	inline Text_t1901882714 ** get_address_of_gameOverText_21() { return &___gameOverText_21; }
	inline void set_gameOverText_21(Text_t1901882714 * value)
	{
		___gameOverText_21 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverText_21), value);
	}

	inline static int32_t get_offset_of_asteroidHealth_22() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___asteroidHealth_22)); }
	inline int32_t get_asteroidHealth_22() const { return ___asteroidHealth_22; }
	inline int32_t* get_address_of_asteroidHealth_22() { return &___asteroidHealth_22; }
	inline void set_asteroidHealth_22(int32_t value)
	{
		___asteroidHealth_22 = value;
	}

	inline static int32_t get_offset_of_debrisHealth_23() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___debrisHealth_23)); }
	inline int32_t get_debrisHealth_23() const { return ___debrisHealth_23; }
	inline int32_t* get_address_of_debrisHealth_23() { return &___debrisHealth_23; }
	inline void set_debrisHealth_23(int32_t value)
	{
		___debrisHealth_23 = value;
	}

	inline static int32_t get_offset_of_hasLost_24() { return static_cast<int32_t>(offsetof(GameManager_t3924726104, ___hasLost_24)); }
	inline bool get_hasLost_24() const { return ___hasLost_24; }
	inline bool* get_address_of_hasLost_24() { return &___hasLost_24; }
	inline void set_hasLost_24(bool value)
	{
		___hasLost_24 = value;
	}
};

struct GameManager_t3924726104_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.GameManager SimpleHealthBar_SpaceshipExample.GameManager::instance
	GameManager_t3924726104 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GameManager_t3924726104_StaticFields, ___instance_4)); }
	inline GameManager_t3924726104 * get_instance_4() const { return ___instance_4; }
	inline GameManager_t3924726104 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GameManager_t3924726104 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGER_T3924726104_H
#ifndef HEALTHPICKUPCONTROLLER_T1709822218_H
#define HEALTHPICKUPCONTROLLER_T1709822218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.HealthPickupController
struct  HealthPickupController_t1709822218  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.HealthPickupController::myRigidbody
	Rigidbody_t3916780224 * ___myRigidbody_4;
	// UnityEngine.ParticleSystem SimpleHealthBar_SpaceshipExample.HealthPickupController::particles
	ParticleSystem_t1800779281 * ___particles_5;
	// UnityEngine.SpriteRenderer SimpleHealthBar_SpaceshipExample.HealthPickupController::mySprite
	SpriteRenderer_t3235626157 * ___mySprite_6;
	// System.Boolean SimpleHealthBar_SpaceshipExample.HealthPickupController::canDestroy
	bool ___canDestroy_7;
	// System.Boolean SimpleHealthBar_SpaceshipExample.HealthPickupController::canPickup
	bool ___canPickup_8;

public:
	inline static int32_t get_offset_of_myRigidbody_4() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___myRigidbody_4)); }
	inline Rigidbody_t3916780224 * get_myRigidbody_4() const { return ___myRigidbody_4; }
	inline Rigidbody_t3916780224 ** get_address_of_myRigidbody_4() { return &___myRigidbody_4; }
	inline void set_myRigidbody_4(Rigidbody_t3916780224 * value)
	{
		___myRigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_4), value);
	}

	inline static int32_t get_offset_of_particles_5() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___particles_5)); }
	inline ParticleSystem_t1800779281 * get_particles_5() const { return ___particles_5; }
	inline ParticleSystem_t1800779281 ** get_address_of_particles_5() { return &___particles_5; }
	inline void set_particles_5(ParticleSystem_t1800779281 * value)
	{
		___particles_5 = value;
		Il2CppCodeGenWriteBarrier((&___particles_5), value);
	}

	inline static int32_t get_offset_of_mySprite_6() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___mySprite_6)); }
	inline SpriteRenderer_t3235626157 * get_mySprite_6() const { return ___mySprite_6; }
	inline SpriteRenderer_t3235626157 ** get_address_of_mySprite_6() { return &___mySprite_6; }
	inline void set_mySprite_6(SpriteRenderer_t3235626157 * value)
	{
		___mySprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___mySprite_6), value);
	}

	inline static int32_t get_offset_of_canDestroy_7() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___canDestroy_7)); }
	inline bool get_canDestroy_7() const { return ___canDestroy_7; }
	inline bool* get_address_of_canDestroy_7() { return &___canDestroy_7; }
	inline void set_canDestroy_7(bool value)
	{
		___canDestroy_7 = value;
	}

	inline static int32_t get_offset_of_canPickup_8() { return static_cast<int32_t>(offsetof(HealthPickupController_t1709822218, ___canPickup_8)); }
	inline bool get_canPickup_8() const { return ___canPickup_8; }
	inline bool* get_address_of_canPickup_8() { return &___canPickup_8; }
	inline void set_canPickup_8(bool value)
	{
		___canPickup_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHPICKUPCONTROLLER_T1709822218_H
#ifndef PLAYERCONTROLLER_T2576101795_H
#define PLAYERCONTROLLER_T2576101795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerController
struct  PlayerController_t2576101795  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::accelerationSpeed
	float ___accelerationSpeed_6;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::maxSpeed
	float ___maxSpeed_7;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::shootingCooldown
	float ___shootingCooldown_8;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.PlayerController::bulletPrefab
	GameObject_t1113636619 * ___bulletPrefab_9;
	// UnityEngine.Rigidbody SimpleHealthBar_SpaceshipExample.PlayerController::myRigidbody
	Rigidbody_t3916780224 * ___myRigidbody_10;
	// UnityEngine.Transform SimpleHealthBar_SpaceshipExample.PlayerController::gunTrans
	Transform_t3600365921 * ___gunTrans_11;
	// UnityEngine.Transform SimpleHealthBar_SpaceshipExample.PlayerController::bulletSpawnPos
	Transform_t3600365921 * ___bulletSpawnPos_12;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::shootingTimer
	float ___shootingTimer_13;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerController::canControl
	bool ___canControl_14;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::rotation
	float ___rotation_15;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::acceleration
	float ___acceleration_16;
	// UnityEngine.RectTransform SimpleHealthBar_SpaceshipExample.PlayerController::overheatVisual
	RectTransform_t3704657025 * ___overheatVisual_17;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::overheatTimer
	float ___overheatTimer_18;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::overheatTimerMax
	float ___overheatTimerMax_19;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerController::cooldownSpeed
	float ___cooldownSpeed_20;
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerController::canShoot
	bool ___canShoot_21;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerController::gunHeatBar
	SimpleHealthBar_t721070758 * ___gunHeatBar_22;

public:
	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_accelerationSpeed_6() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___accelerationSpeed_6)); }
	inline float get_accelerationSpeed_6() const { return ___accelerationSpeed_6; }
	inline float* get_address_of_accelerationSpeed_6() { return &___accelerationSpeed_6; }
	inline void set_accelerationSpeed_6(float value)
	{
		___accelerationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_7() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___maxSpeed_7)); }
	inline float get_maxSpeed_7() const { return ___maxSpeed_7; }
	inline float* get_address_of_maxSpeed_7() { return &___maxSpeed_7; }
	inline void set_maxSpeed_7(float value)
	{
		___maxSpeed_7 = value;
	}

	inline static int32_t get_offset_of_shootingCooldown_8() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___shootingCooldown_8)); }
	inline float get_shootingCooldown_8() const { return ___shootingCooldown_8; }
	inline float* get_address_of_shootingCooldown_8() { return &___shootingCooldown_8; }
	inline void set_shootingCooldown_8(float value)
	{
		___shootingCooldown_8 = value;
	}

	inline static int32_t get_offset_of_bulletPrefab_9() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___bulletPrefab_9)); }
	inline GameObject_t1113636619 * get_bulletPrefab_9() const { return ___bulletPrefab_9; }
	inline GameObject_t1113636619 ** get_address_of_bulletPrefab_9() { return &___bulletPrefab_9; }
	inline void set_bulletPrefab_9(GameObject_t1113636619 * value)
	{
		___bulletPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___bulletPrefab_9), value);
	}

	inline static int32_t get_offset_of_myRigidbody_10() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___myRigidbody_10)); }
	inline Rigidbody_t3916780224 * get_myRigidbody_10() const { return ___myRigidbody_10; }
	inline Rigidbody_t3916780224 ** get_address_of_myRigidbody_10() { return &___myRigidbody_10; }
	inline void set_myRigidbody_10(Rigidbody_t3916780224 * value)
	{
		___myRigidbody_10 = value;
		Il2CppCodeGenWriteBarrier((&___myRigidbody_10), value);
	}

	inline static int32_t get_offset_of_gunTrans_11() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___gunTrans_11)); }
	inline Transform_t3600365921 * get_gunTrans_11() const { return ___gunTrans_11; }
	inline Transform_t3600365921 ** get_address_of_gunTrans_11() { return &___gunTrans_11; }
	inline void set_gunTrans_11(Transform_t3600365921 * value)
	{
		___gunTrans_11 = value;
		Il2CppCodeGenWriteBarrier((&___gunTrans_11), value);
	}

	inline static int32_t get_offset_of_bulletSpawnPos_12() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___bulletSpawnPos_12)); }
	inline Transform_t3600365921 * get_bulletSpawnPos_12() const { return ___bulletSpawnPos_12; }
	inline Transform_t3600365921 ** get_address_of_bulletSpawnPos_12() { return &___bulletSpawnPos_12; }
	inline void set_bulletSpawnPos_12(Transform_t3600365921 * value)
	{
		___bulletSpawnPos_12 = value;
		Il2CppCodeGenWriteBarrier((&___bulletSpawnPos_12), value);
	}

	inline static int32_t get_offset_of_shootingTimer_13() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___shootingTimer_13)); }
	inline float get_shootingTimer_13() const { return ___shootingTimer_13; }
	inline float* get_address_of_shootingTimer_13() { return &___shootingTimer_13; }
	inline void set_shootingTimer_13(float value)
	{
		___shootingTimer_13 = value;
	}

	inline static int32_t get_offset_of_canControl_14() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___canControl_14)); }
	inline bool get_canControl_14() const { return ___canControl_14; }
	inline bool* get_address_of_canControl_14() { return &___canControl_14; }
	inline void set_canControl_14(bool value)
	{
		___canControl_14 = value;
	}

	inline static int32_t get_offset_of_rotation_15() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___rotation_15)); }
	inline float get_rotation_15() const { return ___rotation_15; }
	inline float* get_address_of_rotation_15() { return &___rotation_15; }
	inline void set_rotation_15(float value)
	{
		___rotation_15 = value;
	}

	inline static int32_t get_offset_of_acceleration_16() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___acceleration_16)); }
	inline float get_acceleration_16() const { return ___acceleration_16; }
	inline float* get_address_of_acceleration_16() { return &___acceleration_16; }
	inline void set_acceleration_16(float value)
	{
		___acceleration_16 = value;
	}

	inline static int32_t get_offset_of_overheatVisual_17() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___overheatVisual_17)); }
	inline RectTransform_t3704657025 * get_overheatVisual_17() const { return ___overheatVisual_17; }
	inline RectTransform_t3704657025 ** get_address_of_overheatVisual_17() { return &___overheatVisual_17; }
	inline void set_overheatVisual_17(RectTransform_t3704657025 * value)
	{
		___overheatVisual_17 = value;
		Il2CppCodeGenWriteBarrier((&___overheatVisual_17), value);
	}

	inline static int32_t get_offset_of_overheatTimer_18() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___overheatTimer_18)); }
	inline float get_overheatTimer_18() const { return ___overheatTimer_18; }
	inline float* get_address_of_overheatTimer_18() { return &___overheatTimer_18; }
	inline void set_overheatTimer_18(float value)
	{
		___overheatTimer_18 = value;
	}

	inline static int32_t get_offset_of_overheatTimerMax_19() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___overheatTimerMax_19)); }
	inline float get_overheatTimerMax_19() const { return ___overheatTimerMax_19; }
	inline float* get_address_of_overheatTimerMax_19() { return &___overheatTimerMax_19; }
	inline void set_overheatTimerMax_19(float value)
	{
		___overheatTimerMax_19 = value;
	}

	inline static int32_t get_offset_of_cooldownSpeed_20() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___cooldownSpeed_20)); }
	inline float get_cooldownSpeed_20() const { return ___cooldownSpeed_20; }
	inline float* get_address_of_cooldownSpeed_20() { return &___cooldownSpeed_20; }
	inline void set_cooldownSpeed_20(float value)
	{
		___cooldownSpeed_20 = value;
	}

	inline static int32_t get_offset_of_canShoot_21() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___canShoot_21)); }
	inline bool get_canShoot_21() const { return ___canShoot_21; }
	inline bool* get_address_of_canShoot_21() { return &___canShoot_21; }
	inline void set_canShoot_21(bool value)
	{
		___canShoot_21 = value;
	}

	inline static int32_t get_offset_of_gunHeatBar_22() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795, ___gunHeatBar_22)); }
	inline SimpleHealthBar_t721070758 * get_gunHeatBar_22() const { return ___gunHeatBar_22; }
	inline SimpleHealthBar_t721070758 ** get_address_of_gunHeatBar_22() { return &___gunHeatBar_22; }
	inline void set_gunHeatBar_22(SimpleHealthBar_t721070758 * value)
	{
		___gunHeatBar_22 = value;
		Il2CppCodeGenWriteBarrier((&___gunHeatBar_22), value);
	}
};

struct PlayerController_t2576101795_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.PlayerController SimpleHealthBar_SpaceshipExample.PlayerController::instance
	PlayerController_t2576101795 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayerController_t2576101795_StaticFields, ___instance_4)); }
	inline PlayerController_t2576101795 * get_instance_4() const { return ___instance_4; }
	inline PlayerController_t2576101795 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayerController_t2576101795 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T2576101795_H
#ifndef PLAYERHEALTH_T992877501_H
#define PLAYERHEALTH_T992877501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar_SpaceshipExample.PlayerHealth
struct  PlayerHealth_t992877501  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean SimpleHealthBar_SpaceshipExample.PlayerHealth::canTakeDamage
	bool ___canTakeDamage_5;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth::maxHealth
	int32_t ___maxHealth_6;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::currentHealth
	float ___currentHealth_7;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::invulnerabilityTime
	float ___invulnerabilityTime_8;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::currentShield
	float ___currentShield_9;
	// System.Int32 SimpleHealthBar_SpaceshipExample.PlayerHealth::maxShield
	int32_t ___maxShield_10;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::regenShieldTimer
	float ___regenShieldTimer_11;
	// System.Single SimpleHealthBar_SpaceshipExample.PlayerHealth::regenShieldTimerMax
	float ___regenShieldTimerMax_12;
	// UnityEngine.GameObject SimpleHealthBar_SpaceshipExample.PlayerHealth::explosionParticles
	GameObject_t1113636619 * ___explosionParticles_13;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerHealth::healthBar
	SimpleHealthBar_t721070758 * ___healthBar_14;
	// SimpleHealthBar SimpleHealthBar_SpaceshipExample.PlayerHealth::shieldBar
	SimpleHealthBar_t721070758 * ___shieldBar_15;

public:
	inline static int32_t get_offset_of_canTakeDamage_5() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___canTakeDamage_5)); }
	inline bool get_canTakeDamage_5() const { return ___canTakeDamage_5; }
	inline bool* get_address_of_canTakeDamage_5() { return &___canTakeDamage_5; }
	inline void set_canTakeDamage_5(bool value)
	{
		___canTakeDamage_5 = value;
	}

	inline static int32_t get_offset_of_maxHealth_6() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___maxHealth_6)); }
	inline int32_t get_maxHealth_6() const { return ___maxHealth_6; }
	inline int32_t* get_address_of_maxHealth_6() { return &___maxHealth_6; }
	inline void set_maxHealth_6(int32_t value)
	{
		___maxHealth_6 = value;
	}

	inline static int32_t get_offset_of_currentHealth_7() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___currentHealth_7)); }
	inline float get_currentHealth_7() const { return ___currentHealth_7; }
	inline float* get_address_of_currentHealth_7() { return &___currentHealth_7; }
	inline void set_currentHealth_7(float value)
	{
		___currentHealth_7 = value;
	}

	inline static int32_t get_offset_of_invulnerabilityTime_8() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___invulnerabilityTime_8)); }
	inline float get_invulnerabilityTime_8() const { return ___invulnerabilityTime_8; }
	inline float* get_address_of_invulnerabilityTime_8() { return &___invulnerabilityTime_8; }
	inline void set_invulnerabilityTime_8(float value)
	{
		___invulnerabilityTime_8 = value;
	}

	inline static int32_t get_offset_of_currentShield_9() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___currentShield_9)); }
	inline float get_currentShield_9() const { return ___currentShield_9; }
	inline float* get_address_of_currentShield_9() { return &___currentShield_9; }
	inline void set_currentShield_9(float value)
	{
		___currentShield_9 = value;
	}

	inline static int32_t get_offset_of_maxShield_10() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___maxShield_10)); }
	inline int32_t get_maxShield_10() const { return ___maxShield_10; }
	inline int32_t* get_address_of_maxShield_10() { return &___maxShield_10; }
	inline void set_maxShield_10(int32_t value)
	{
		___maxShield_10 = value;
	}

	inline static int32_t get_offset_of_regenShieldTimer_11() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___regenShieldTimer_11)); }
	inline float get_regenShieldTimer_11() const { return ___regenShieldTimer_11; }
	inline float* get_address_of_regenShieldTimer_11() { return &___regenShieldTimer_11; }
	inline void set_regenShieldTimer_11(float value)
	{
		___regenShieldTimer_11 = value;
	}

	inline static int32_t get_offset_of_regenShieldTimerMax_12() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___regenShieldTimerMax_12)); }
	inline float get_regenShieldTimerMax_12() const { return ___regenShieldTimerMax_12; }
	inline float* get_address_of_regenShieldTimerMax_12() { return &___regenShieldTimerMax_12; }
	inline void set_regenShieldTimerMax_12(float value)
	{
		___regenShieldTimerMax_12 = value;
	}

	inline static int32_t get_offset_of_explosionParticles_13() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___explosionParticles_13)); }
	inline GameObject_t1113636619 * get_explosionParticles_13() const { return ___explosionParticles_13; }
	inline GameObject_t1113636619 ** get_address_of_explosionParticles_13() { return &___explosionParticles_13; }
	inline void set_explosionParticles_13(GameObject_t1113636619 * value)
	{
		___explosionParticles_13 = value;
		Il2CppCodeGenWriteBarrier((&___explosionParticles_13), value);
	}

	inline static int32_t get_offset_of_healthBar_14() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___healthBar_14)); }
	inline SimpleHealthBar_t721070758 * get_healthBar_14() const { return ___healthBar_14; }
	inline SimpleHealthBar_t721070758 ** get_address_of_healthBar_14() { return &___healthBar_14; }
	inline void set_healthBar_14(SimpleHealthBar_t721070758 * value)
	{
		___healthBar_14 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_14), value);
	}

	inline static int32_t get_offset_of_shieldBar_15() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501, ___shieldBar_15)); }
	inline SimpleHealthBar_t721070758 * get_shieldBar_15() const { return ___shieldBar_15; }
	inline SimpleHealthBar_t721070758 ** get_address_of_shieldBar_15() { return &___shieldBar_15; }
	inline void set_shieldBar_15(SimpleHealthBar_t721070758 * value)
	{
		___shieldBar_15 = value;
		Il2CppCodeGenWriteBarrier((&___shieldBar_15), value);
	}
};

struct PlayerHealth_t992877501_StaticFields
{
public:
	// SimpleHealthBar_SpaceshipExample.PlayerHealth SimpleHealthBar_SpaceshipExample.PlayerHealth::instance
	PlayerHealth_t992877501 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(PlayerHealth_t992877501_StaticFields, ___instance_4)); }
	inline PlayerHealth_t992877501 * get_instance_4() const { return ___instance_4; }
	inline PlayerHealth_t992877501 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PlayerHealth_t992877501 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHEALTH_T992877501_H
#ifndef BENCHMARK01_T1571072624_H
#define BENCHMARK01_T1571072624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t1571072624  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_4;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_5;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_6;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_7;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_8;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t340375123 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t340375123 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_TMProFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TMProFont_5)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_5() const { return ___TMProFont_5; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_5() { return &___TMProFont_5; }
	inline void set_TMProFont_5(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_5), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TextMeshFont_6)); }
	inline Font_t1956802104 * get_TextMeshFont_6() const { return ___TextMeshFont_6; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_6() { return &___TextMeshFont_6; }
	inline void set_TextMeshFont_6(Font_t1956802104 * value)
	{
		___TextMeshFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_6), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_7() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMeshPro_7)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_7() const { return ___m_textMeshPro_7; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_7() { return &___m_textMeshPro_7; }
	inline void set_m_textMeshPro_7(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_7), value);
	}

	inline static int32_t get_offset_of_m_textContainer_8() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textContainer_8)); }
	inline TextContainer_t97923372 * get_m_textContainer_8() const { return ___m_textContainer_8; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_8() { return &___m_textContainer_8; }
	inline void set_m_textContainer_8(TextContainer_t97923372 * value)
	{
		___m_textContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMesh_9)); }
	inline TextMesh_t1536577757 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(TextMesh_t1536577757 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material01_12)); }
	inline Material_t340375123 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t340375123 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t340375123 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material02_13)); }
	inline Material_t340375123 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t340375123 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t340375123 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T1571072624_H
#ifndef BENCHMARK01_UGUI_T3264177817_H
#define BENCHMARK01_UGUI_T3264177817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_4;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_5;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_6;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_7;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_8;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_9;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_12;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_13;

public:
	inline static int32_t get_offset_of_BenchmarkType_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_4)); }
	inline int32_t get_BenchmarkType_4() const { return ___BenchmarkType_4; }
	inline int32_t* get_address_of_BenchmarkType_4() { return &___BenchmarkType_4; }
	inline void set_BenchmarkType_4(int32_t value)
	{
		___BenchmarkType_4 = value;
	}

	inline static int32_t get_offset_of_canvas_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_5)); }
	inline Canvas_t3310196443 * get_canvas_5() const { return ___canvas_5; }
	inline Canvas_t3310196443 ** get_address_of_canvas_5() { return &___canvas_5; }
	inline void set_canvas_5(Canvas_t3310196443 * value)
	{
		___canvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_5), value);
	}

	inline static int32_t get_offset_of_TMProFont_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_6)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_6() const { return ___TMProFont_6; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_6() { return &___TMProFont_6; }
	inline void set_TMProFont_6(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_6), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_7)); }
	inline Font_t1956802104 * get_TextMeshFont_7() const { return ___TextMeshFont_7; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_7() { return &___TextMeshFont_7; }
	inline void set_TextMeshFont_7(Font_t1956802104 * value)
	{
		___TextMeshFont_7 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_7), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_8() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_8)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_8() const { return ___m_textMeshPro_8; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_8() { return &___m_textMeshPro_8; }
	inline void set_m_textMeshPro_8(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_textMesh_9() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_9)); }
	inline Text_t1901882714 * get_m_textMesh_9() const { return ___m_textMesh_9; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_9() { return &___m_textMesh_9; }
	inline void set_m_textMesh_9(Text_t1901882714 * value)
	{
		___m_textMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_9), value);
	}

	inline static int32_t get_offset_of_m_material01_12() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_12)); }
	inline Material_t340375123 * get_m_material01_12() const { return ___m_material01_12; }
	inline Material_t340375123 ** get_address_of_m_material01_12() { return &___m_material01_12; }
	inline void set_m_material01_12(Material_t340375123 * value)
	{
		___m_material01_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_12), value);
	}

	inline static int32_t get_offset_of_m_material02_13() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_13)); }
	inline Material_t340375123 * get_m_material02_13() const { return ___m_material02_13; }
	inline Material_t340375123 ** get_address_of_m_material02_13() { return &___m_material02_13; }
	inline void set_m_material02_13(Material_t340375123 * value)
	{
		___m_material02_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3264177817_H
#ifndef BENCHMARK02_T1571269232_H
#define BENCHMARK02_T1571269232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t1571269232  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_6() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___floatingText_Script_6)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_6() const { return ___floatingText_Script_6; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_6() { return &___floatingText_Script_6; }
	inline void set_floatingText_Script_6(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_6 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T1571269232_H
#ifndef BENCHMARK03_T1571203696_H
#define BENCHMARK03_T1571203696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1571203696  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1956802104 * ___TheFont_6;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___TheFont_6)); }
	inline Font_t1956802104 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1956802104 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1956802104 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1571203696_H
#ifndef BENCHMARK04_T1570876016_H
#define BENCHMARK04_T1570876016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t1570876016  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_5;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_6;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_7;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3600365921 * ___m_Transform_8;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_5() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MinPointSize_5)); }
	inline int32_t get_MinPointSize_5() const { return ___MinPointSize_5; }
	inline int32_t* get_address_of_MinPointSize_5() { return &___MinPointSize_5; }
	inline void set_MinPointSize_5(int32_t value)
	{
		___MinPointSize_5 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_6() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MaxPointSize_6)); }
	inline int32_t get_MaxPointSize_6() const { return ___MaxPointSize_6; }
	inline int32_t* get_address_of_MaxPointSize_6() { return &___MaxPointSize_6; }
	inline void set_MaxPointSize_6(int32_t value)
	{
		___MaxPointSize_6 = value;
	}

	inline static int32_t get_offset_of_Steps_7() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___Steps_7)); }
	inline int32_t get_Steps_7() const { return ___Steps_7; }
	inline int32_t* get_address_of_Steps_7() { return &___Steps_7; }
	inline void set_Steps_7(int32_t value)
	{
		___Steps_7 = value;
	}

	inline static int32_t get_offset_of_m_Transform_8() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___m_Transform_8)); }
	inline Transform_t3600365921 * get_m_Transform_8() const { return ___m_Transform_8; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_8() { return &___m_Transform_8; }
	inline void set_m_Transform_8(Transform_t3600365921 * value)
	{
		___m_Transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T1570876016_H
#ifndef CAMERACONTROLLER_T2264742161_H
#define CAMERACONTROLLER_T2264742161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t2264742161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3600365921 * ___cameraTransform_4;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3600365921 * ___dummyTarget_5;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3600365921 * ___CameraTarget_6;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_7;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_8;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_9;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_11;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_12;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_13;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_14;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_15;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_16;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_17;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_18;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_19;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_20;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t3722313464  ___currentVelocity_21;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t3722313464  ___desiredPosition_22;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_23;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_24;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t3722313464  ___moveVector_25;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_26;

public:
	inline static int32_t get_offset_of_cameraTransform_4() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___cameraTransform_4)); }
	inline Transform_t3600365921 * get_cameraTransform_4() const { return ___cameraTransform_4; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_4() { return &___cameraTransform_4; }
	inline void set_cameraTransform_4(Transform_t3600365921 * value)
	{
		___cameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_4), value);
	}

	inline static int32_t get_offset_of_dummyTarget_5() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___dummyTarget_5)); }
	inline Transform_t3600365921 * get_dummyTarget_5() const { return ___dummyTarget_5; }
	inline Transform_t3600365921 ** get_address_of_dummyTarget_5() { return &___dummyTarget_5; }
	inline void set_dummyTarget_5(Transform_t3600365921 * value)
	{
		___dummyTarget_5 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_5), value);
	}

	inline static int32_t get_offset_of_CameraTarget_6() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraTarget_6)); }
	inline Transform_t3600365921 * get_CameraTarget_6() const { return ___CameraTarget_6; }
	inline Transform_t3600365921 ** get_address_of_CameraTarget_6() { return &___CameraTarget_6; }
	inline void set_CameraTarget_6(Transform_t3600365921 * value)
	{
		___CameraTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_6), value);
	}

	inline static int32_t get_offset_of_FollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___FollowDistance_7)); }
	inline float get_FollowDistance_7() const { return ___FollowDistance_7; }
	inline float* get_address_of_FollowDistance_7() { return &___FollowDistance_7; }
	inline void set_FollowDistance_7(float value)
	{
		___FollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_8() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxFollowDistance_8)); }
	inline float get_MaxFollowDistance_8() const { return ___MaxFollowDistance_8; }
	inline float* get_address_of_MaxFollowDistance_8() { return &___MaxFollowDistance_8; }
	inline void set_MaxFollowDistance_8(float value)
	{
		___MaxFollowDistance_8 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_9() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinFollowDistance_9)); }
	inline float get_MinFollowDistance_9() const { return ___MinFollowDistance_9; }
	inline float* get_address_of_MinFollowDistance_9() { return &___MinFollowDistance_9; }
	inline void set_MinFollowDistance_9(float value)
	{
		___MinFollowDistance_9 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___ElevationAngle_10)); }
	inline float get_ElevationAngle_10() const { return ___ElevationAngle_10; }
	inline float* get_address_of_ElevationAngle_10() { return &___ElevationAngle_10; }
	inline void set_ElevationAngle_10(float value)
	{
		___ElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxElevationAngle_11)); }
	inline float get_MaxElevationAngle_11() const { return ___MaxElevationAngle_11; }
	inline float* get_address_of_MaxElevationAngle_11() { return &___MaxElevationAngle_11; }
	inline void set_MaxElevationAngle_11(float value)
	{
		___MaxElevationAngle_11 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_12() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinElevationAngle_12)); }
	inline float get_MinElevationAngle_12() const { return ___MinElevationAngle_12; }
	inline float* get_address_of_MinElevationAngle_12() { return &___MinElevationAngle_12; }
	inline void set_MinElevationAngle_12(float value)
	{
		___MinElevationAngle_12 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_13() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___OrbitalAngle_13)); }
	inline float get_OrbitalAngle_13() const { return ___OrbitalAngle_13; }
	inline float* get_address_of_OrbitalAngle_13() { return &___OrbitalAngle_13; }
	inline void set_OrbitalAngle_13(float value)
	{
		___OrbitalAngle_13 = value;
	}

	inline static int32_t get_offset_of_CameraMode_14() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraMode_14)); }
	inline int32_t get_CameraMode_14() const { return ___CameraMode_14; }
	inline int32_t* get_address_of_CameraMode_14() { return &___CameraMode_14; }
	inline void set_CameraMode_14(int32_t value)
	{
		___CameraMode_14 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothing_15)); }
	inline bool get_MovementSmoothing_15() const { return ___MovementSmoothing_15; }
	inline bool* get_address_of_MovementSmoothing_15() { return &___MovementSmoothing_15; }
	inline void set_MovementSmoothing_15(bool value)
	{
		___MovementSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_16() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothing_16)); }
	inline bool get_RotationSmoothing_16() const { return ___RotationSmoothing_16; }
	inline bool* get_address_of_RotationSmoothing_16() { return &___RotationSmoothing_16; }
	inline void set_RotationSmoothing_16(bool value)
	{
		___RotationSmoothing_16 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_17() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___previousSmoothing_17)); }
	inline bool get_previousSmoothing_17() const { return ___previousSmoothing_17; }
	inline bool* get_address_of_previousSmoothing_17() { return &___previousSmoothing_17; }
	inline void set_previousSmoothing_17(bool value)
	{
		___previousSmoothing_17 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_18() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothingValue_18)); }
	inline float get_MovementSmoothingValue_18() const { return ___MovementSmoothingValue_18; }
	inline float* get_address_of_MovementSmoothingValue_18() { return &___MovementSmoothingValue_18; }
	inline void set_MovementSmoothingValue_18(float value)
	{
		___MovementSmoothingValue_18 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_19() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothingValue_19)); }
	inline float get_RotationSmoothingValue_19() const { return ___RotationSmoothingValue_19; }
	inline float* get_address_of_RotationSmoothingValue_19() { return &___RotationSmoothingValue_19; }
	inline void set_RotationSmoothingValue_19(float value)
	{
		___RotationSmoothingValue_19 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_20() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MoveSensitivity_20)); }
	inline float get_MoveSensitivity_20() const { return ___MoveSensitivity_20; }
	inline float* get_address_of_MoveSensitivity_20() { return &___MoveSensitivity_20; }
	inline void set_MoveSensitivity_20(float value)
	{
		___MoveSensitivity_20 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_21() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___currentVelocity_21)); }
	inline Vector3_t3722313464  get_currentVelocity_21() const { return ___currentVelocity_21; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_21() { return &___currentVelocity_21; }
	inline void set_currentVelocity_21(Vector3_t3722313464  value)
	{
		___currentVelocity_21 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_22() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___desiredPosition_22)); }
	inline Vector3_t3722313464  get_desiredPosition_22() const { return ___desiredPosition_22; }
	inline Vector3_t3722313464 * get_address_of_desiredPosition_22() { return &___desiredPosition_22; }
	inline void set_desiredPosition_22(Vector3_t3722313464  value)
	{
		___desiredPosition_22 = value;
	}

	inline static int32_t get_offset_of_mouseX_23() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseX_23)); }
	inline float get_mouseX_23() const { return ___mouseX_23; }
	inline float* get_address_of_mouseX_23() { return &___mouseX_23; }
	inline void set_mouseX_23(float value)
	{
		___mouseX_23 = value;
	}

	inline static int32_t get_offset_of_mouseY_24() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseY_24)); }
	inline float get_mouseY_24() const { return ___mouseY_24; }
	inline float* get_address_of_mouseY_24() { return &___mouseY_24; }
	inline void set_mouseY_24(float value)
	{
		___mouseY_24 = value;
	}

	inline static int32_t get_offset_of_moveVector_25() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___moveVector_25)); }
	inline Vector3_t3722313464  get_moveVector_25() const { return ___moveVector_25; }
	inline Vector3_t3722313464 * get_address_of_moveVector_25() { return &___moveVector_25; }
	inline void set_moveVector_25(Vector3_t3722313464  value)
	{
		___moveVector_25 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_26() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseWheel_26)); }
	inline float get_mouseWheel_26() const { return ___mouseWheel_26; }
	inline float* get_address_of_mouseWheel_26() { return &___mouseWheel_26; }
	inline void set_mouseWheel_26(float value)
	{
		___mouseWheel_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T2264742161_H
#ifndef OBJECTSPIN_T341713598_H
#define OBJECTSPIN_T341713598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t341713598  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_4;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_5;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3600365921 * ___m_transform_6;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t3722313464  ___m_prevPOS_8;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t3722313464  ___m_initial_Rotation_9;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t3722313464  ___m_initial_Position_10;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t2600501292  ___m_lightColor_11;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_12;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_13;

public:
	inline static int32_t get_offset_of_SpinSpeed_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___SpinSpeed_4)); }
	inline float get_SpinSpeed_4() const { return ___SpinSpeed_4; }
	inline float* get_address_of_SpinSpeed_4() { return &___SpinSpeed_4; }
	inline void set_SpinSpeed_4(float value)
	{
		___SpinSpeed_4 = value;
	}

	inline static int32_t get_offset_of_RotationRange_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___RotationRange_5)); }
	inline int32_t get_RotationRange_5() const { return ___RotationRange_5; }
	inline int32_t* get_address_of_RotationRange_5() { return &___RotationRange_5; }
	inline void set_RotationRange_5(int32_t value)
	{
		___RotationRange_5 = value;
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_transform_6)); }
	inline Transform_t3600365921 * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_t3600365921 * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_6), value);
	}

	inline static int32_t get_offset_of_m_time_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_time_7)); }
	inline float get_m_time_7() const { return ___m_time_7; }
	inline float* get_address_of_m_time_7() { return &___m_time_7; }
	inline void set_m_time_7(float value)
	{
		___m_time_7 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_prevPOS_8)); }
	inline Vector3_t3722313464  get_m_prevPOS_8() const { return ___m_prevPOS_8; }
	inline Vector3_t3722313464 * get_address_of_m_prevPOS_8() { return &___m_prevPOS_8; }
	inline void set_m_prevPOS_8(Vector3_t3722313464  value)
	{
		___m_prevPOS_8 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Rotation_9)); }
	inline Vector3_t3722313464  get_m_initial_Rotation_9() const { return ___m_initial_Rotation_9; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Rotation_9() { return &___m_initial_Rotation_9; }
	inline void set_m_initial_Rotation_9(Vector3_t3722313464  value)
	{
		___m_initial_Rotation_9 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Position_10)); }
	inline Vector3_t3722313464  get_m_initial_Position_10() const { return ___m_initial_Position_10; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Position_10() { return &___m_initial_Position_10; }
	inline void set_m_initial_Position_10(Vector3_t3722313464  value)
	{
		___m_initial_Position_10 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_lightColor_11)); }
	inline Color32_t2600501292  get_m_lightColor_11() const { return ___m_lightColor_11; }
	inline Color32_t2600501292 * get_address_of_m_lightColor_11() { return &___m_lightColor_11; }
	inline void set_m_lightColor_11(Color32_t2600501292  value)
	{
		___m_lightColor_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___frames_12)); }
	inline int32_t get_frames_12() const { return ___frames_12; }
	inline int32_t* get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(int32_t value)
	{
		___frames_12 = value;
	}

	inline static int32_t get_offset_of_Motion_13() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___Motion_13)); }
	inline int32_t get_Motion_13() const { return ___Motion_13; }
	inline int32_t* get_address_of_Motion_13() { return &___Motion_13; }
	inline void set_Motion_13(int32_t value)
	{
		___Motion_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T341713598_H
#ifndef SHADERPROPANIMATOR_T3617420994_H
#define SHADERPROPANIMATOR_T3617420994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t3617420994  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_4;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t340375123 * ___m_Material_5;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3046754366 * ___GlowCurve_6;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_7;

public:
	inline static int32_t get_offset_of_m_Renderer_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Renderer_4)); }
	inline Renderer_t2627027031 * get_m_Renderer_4() const { return ___m_Renderer_4; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_4() { return &___m_Renderer_4; }
	inline void set_m_Renderer_4(Renderer_t2627027031 * value)
	{
		___m_Renderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_4), value);
	}

	inline static int32_t get_offset_of_m_Material_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Material_5)); }
	inline Material_t340375123 * get_m_Material_5() const { return ___m_Material_5; }
	inline Material_t340375123 ** get_address_of_m_Material_5() { return &___m_Material_5; }
	inline void set_m_Material_5(Material_t340375123 * value)
	{
		___m_Material_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_5), value);
	}

	inline static int32_t get_offset_of_GlowCurve_6() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___GlowCurve_6)); }
	inline AnimationCurve_t3046754366 * get_GlowCurve_6() const { return ___GlowCurve_6; }
	inline AnimationCurve_t3046754366 ** get_address_of_GlowCurve_6() { return &___GlowCurve_6; }
	inline void set_GlowCurve_6(AnimationCurve_t3046754366 * value)
	{
		___GlowCurve_6 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_6), value);
	}

	inline static int32_t get_offset_of_m_frame_7() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_frame_7)); }
	inline float get_m_frame_7() const { return ___m_frame_7; }
	inline float* get_address_of_m_frame_7() { return &___m_frame_7; }
	inline void set_m_frame_7(float value)
	{
		___m_frame_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T3617420994_H
#ifndef SIMPLESCRIPT_T3279312205_H
#define SIMPLESCRIPT_T3279312205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t3279312205  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_4;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_6;

public:
	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_textMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_frame_6() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_frame_6)); }
	inline float get_m_frame_6() const { return ___m_frame_6; }
	inline float* get_address_of_m_frame_6() { return &___m_frame_6; }
	inline void set_m_frame_6(float value)
	{
		___m_frame_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T3279312205_H
#ifndef SKEWTEXTEXAMPLE_T3460249701_H
#define SKEWTEXTEXAMPLE_T3460249701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample
struct  SkewTextExample_t3460249701  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t3046754366 * ___VertexCurve_5;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_6;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_7;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___VertexCurve_5)); }
	inline AnimationCurve_t3046754366 * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_t3046754366 * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_5), value);
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_7() { return static_cast<int32_t>(offsetof(SkewTextExample_t3460249701, ___ShearAmount_7)); }
	inline float get_ShearAmount_7() const { return ___ShearAmount_7; }
	inline float* get_address_of_ShearAmount_7() { return &___ShearAmount_7; }
	inline void set_ShearAmount_7(float value)
	{
		___ShearAmount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEWTEXTEXAMPLE_T3460249701_H
#ifndef TMP_EXAMPLESCRIPT_01_T3051742005_H
#define TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01
struct  TMP_ExampleScript_01_t3051742005  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_4;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_5;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t2599618874 * ___m_text_6;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_8;

public:
	inline static int32_t get_offset_of_ObjectType_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___ObjectType_4)); }
	inline int32_t get_ObjectType_4() const { return ___ObjectType_4; }
	inline int32_t* get_address_of_ObjectType_4() { return &___ObjectType_4; }
	inline void set_ObjectType_4(int32_t value)
	{
		___ObjectType_4 = value;
	}

	inline static int32_t get_offset_of_isStatic_5() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___isStatic_5)); }
	inline bool get_isStatic_5() const { return ___isStatic_5; }
	inline bool* get_address_of_isStatic_5() { return &___isStatic_5; }
	inline void set_isStatic_5(bool value)
	{
		___isStatic_5 = value;
	}

	inline static int32_t get_offset_of_m_text_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___m_text_6)); }
	inline TMP_Text_t2599618874 * get_m_text_6() const { return ___m_text_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_text_6() { return &___m_text_6; }
	inline void set_m_text_6(TMP_Text_t2599618874 * value)
	{
		___m_text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_6), value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t3051742005, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_EXAMPLESCRIPT_01_T3051742005_H
#ifndef TMP_FRAMERATECOUNTER_T314972976_H
#define TMP_FRAMERATECOUNTER_T314972976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter
struct  TMP_FrameRateCounter_t314972976  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_10;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_t3600365921 * ___m_frameCounter_transform_11;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_t4157153871 * ___m_camera_12;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_13;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_8), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_TextMeshPro_10)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_10), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_frameCounter_transform_11)); }
	inline Transform_t3600365921 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline Transform_t3600365921 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(Transform_t3600365921 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_11), value);
	}

	inline static int32_t get_offset_of_m_camera_12() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___m_camera_12)); }
	inline Camera_t4157153871 * get_m_camera_12() const { return ___m_camera_12; }
	inline Camera_t4157153871 ** get_address_of_m_camera_12() { return &___m_camera_12; }
	inline void set_m_camera_12(Camera_t4157153871 * value)
	{
		___m_camera_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_12), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_13() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t314972976, ___last_AnchorPosition_13)); }
	inline int32_t get_last_AnchorPosition_13() const { return ___last_AnchorPosition_13; }
	inline int32_t* get_address_of_last_AnchorPosition_13() { return &___last_AnchorPosition_13; }
	inline void set_last_AnchorPosition_13(int32_t value)
	{
		___last_AnchorPosition_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FRAMERATECOUNTER_T314972976_H
#ifndef TMP_TEXTEVENTCHECK_T1103849140_H
#define TMP_TEXTEVENTCHECK_T1103849140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextEventCheck
struct  TMP_TextEventCheck_t1103849140  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler TMPro.Examples.TMP_TextEventCheck::TextEventHandler
	TMP_TextEventHandler_t1869054637 * ___TextEventHandler_4;

public:
	inline static int32_t get_offset_of_TextEventHandler_4() { return static_cast<int32_t>(offsetof(TMP_TextEventCheck_t1103849140, ___TextEventHandler_4)); }
	inline TMP_TextEventHandler_t1869054637 * get_TextEventHandler_4() const { return ___TextEventHandler_4; }
	inline TMP_TextEventHandler_t1869054637 ** get_address_of_TextEventHandler_4() { return &___TextEventHandler_4; }
	inline void set_TextEventHandler_4(TMP_TextEventHandler_t1869054637 * value)
	{
		___TextEventHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextEventHandler_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTCHECK_T1103849140_H
#ifndef TMP_TEXTINFODEBUGTOOL_T1868681444_H
#define TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextInfoDebugTool
struct  TMP_TextInfoDebugTool_t1868681444  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowCharacters
	bool ___ShowCharacters_4;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowWords
	bool ___ShowWords_5;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLinks
	bool ___ShowLinks_6;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLines
	bool ___ShowLines_7;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowMeshBounds
	bool ___ShowMeshBounds_8;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowTextBounds
	bool ___ShowTextBounds_9;
	// System.String TMPro.Examples.TMP_TextInfoDebugTool::ObjectStats
	String_t* ___ObjectStats_10;
	// TMPro.TMP_Text TMPro.Examples.TMP_TextInfoDebugTool::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_11;
	// UnityEngine.Transform TMPro.Examples.TMP_TextInfoDebugTool::m_Transform
	Transform_t3600365921 * ___m_Transform_12;

public:
	inline static int32_t get_offset_of_ShowCharacters_4() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowCharacters_4)); }
	inline bool get_ShowCharacters_4() const { return ___ShowCharacters_4; }
	inline bool* get_address_of_ShowCharacters_4() { return &___ShowCharacters_4; }
	inline void set_ShowCharacters_4(bool value)
	{
		___ShowCharacters_4 = value;
	}

	inline static int32_t get_offset_of_ShowWords_5() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowWords_5)); }
	inline bool get_ShowWords_5() const { return ___ShowWords_5; }
	inline bool* get_address_of_ShowWords_5() { return &___ShowWords_5; }
	inline void set_ShowWords_5(bool value)
	{
		___ShowWords_5 = value;
	}

	inline static int32_t get_offset_of_ShowLinks_6() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLinks_6)); }
	inline bool get_ShowLinks_6() const { return ___ShowLinks_6; }
	inline bool* get_address_of_ShowLinks_6() { return &___ShowLinks_6; }
	inline void set_ShowLinks_6(bool value)
	{
		___ShowLinks_6 = value;
	}

	inline static int32_t get_offset_of_ShowLines_7() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowLines_7)); }
	inline bool get_ShowLines_7() const { return ___ShowLines_7; }
	inline bool* get_address_of_ShowLines_7() { return &___ShowLines_7; }
	inline void set_ShowLines_7(bool value)
	{
		___ShowLines_7 = value;
	}

	inline static int32_t get_offset_of_ShowMeshBounds_8() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowMeshBounds_8)); }
	inline bool get_ShowMeshBounds_8() const { return ___ShowMeshBounds_8; }
	inline bool* get_address_of_ShowMeshBounds_8() { return &___ShowMeshBounds_8; }
	inline void set_ShowMeshBounds_8(bool value)
	{
		___ShowMeshBounds_8 = value;
	}

	inline static int32_t get_offset_of_ShowTextBounds_9() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ShowTextBounds_9)); }
	inline bool get_ShowTextBounds_9() const { return ___ShowTextBounds_9; }
	inline bool* get_address_of_ShowTextBounds_9() { return &___ShowTextBounds_9; }
	inline void set_ShowTextBounds_9(bool value)
	{
		___ShowTextBounds_9 = value;
	}

	inline static int32_t get_offset_of_ObjectStats_10() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___ObjectStats_10)); }
	inline String_t* get_ObjectStats_10() const { return ___ObjectStats_10; }
	inline String_t** get_address_of_ObjectStats_10() { return &___ObjectStats_10; }
	inline void set_ObjectStats_10(String_t* value)
	{
		___ObjectStats_10 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectStats_10), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_11() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_TextComponent_11)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_11() const { return ___m_TextComponent_11; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_11() { return &___m_TextComponent_11; }
	inline void set_m_TextComponent_11(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_11), value);
	}

	inline static int32_t get_offset_of_m_Transform_12() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t1868681444, ___m_Transform_12)); }
	inline Transform_t3600365921 * get_m_Transform_12() const { return ___m_Transform_12; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_12() { return &___m_Transform_12; }
	inline void set_m_Transform_12(Transform_t3600365921 * value)
	{
		___m_Transform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFODEBUGTOOL_T1868681444_H
#ifndef TMP_TEXTSELECTOR_A_T3982526506_H
#define TMP_TEXTSELECTOR_A_T3982526506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_A
struct  TMP_TextSelector_A_t3982526506  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.TMP_TextSelector_A::m_TextMeshPro
	TextMeshPro_t2393593166 * ___m_TextMeshPro_4;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_A::m_Camera
	Camera_t4157153871 * ___m_Camera_5;
	// System.Boolean TMPro.Examples.TMP_TextSelector_A::m_isHoveringObject
	bool ___m_isHoveringObject_6;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_selectedLink
	int32_t ___m_selectedLink_7;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastCharIndex
	int32_t ___m_lastCharIndex_8;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastWordIndex
	int32_t ___m_lastWordIndex_9;

public:
	inline static int32_t get_offset_of_m_TextMeshPro_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_TextMeshPro_4)); }
	inline TextMeshPro_t2393593166 * get_m_TextMeshPro_4() const { return ___m_TextMeshPro_4; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_TextMeshPro_4() { return &___m_TextMeshPro_4; }
	inline void set_m_TextMeshPro_4(TextMeshPro_t2393593166 * value)
	{
		___m_TextMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_Camera_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_Camera_5)); }
	inline Camera_t4157153871 * get_m_Camera_5() const { return ___m_Camera_5; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_5() { return &___m_Camera_5; }
	inline void set_m_Camera_5(Camera_t4157153871 * value)
	{
		___m_Camera_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_5), value);
	}

	inline static int32_t get_offset_of_m_isHoveringObject_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_isHoveringObject_6)); }
	inline bool get_m_isHoveringObject_6() const { return ___m_isHoveringObject_6; }
	inline bool* get_address_of_m_isHoveringObject_6() { return &___m_isHoveringObject_6; }
	inline void set_m_isHoveringObject_6(bool value)
	{
		___m_isHoveringObject_6 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_selectedLink_7)); }
	inline int32_t get_m_selectedLink_7() const { return ___m_selectedLink_7; }
	inline int32_t* get_address_of_m_selectedLink_7() { return &___m_selectedLink_7; }
	inline void set_m_selectedLink_7(int32_t value)
	{
		___m_selectedLink_7 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_8() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastCharIndex_8)); }
	inline int32_t get_m_lastCharIndex_8() const { return ___m_lastCharIndex_8; }
	inline int32_t* get_address_of_m_lastCharIndex_8() { return &___m_lastCharIndex_8; }
	inline void set_m_lastCharIndex_8(int32_t value)
	{
		___m_lastCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t3982526506, ___m_lastWordIndex_9)); }
	inline int32_t get_m_lastWordIndex_9() const { return ___m_lastWordIndex_9; }
	inline int32_t* get_address_of_m_lastWordIndex_9() { return &___m_lastWordIndex_9; }
	inline void set_m_lastWordIndex_9(int32_t value)
	{
		___m_lastWordIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_A_T3982526506_H
#ifndef TMP_TEXTSELECTOR_B_T3982526505_H
#define TMP_TEXTSELECTOR_B_T3982526505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_B
struct  TMP_TextSelector_B_t3982526505  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::TextPopup_Prefab_01
	RectTransform_t3704657025 * ___TextPopup_Prefab_01_4;
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::m_TextPopup_RectTransform
	RectTransform_t3704657025 * ___m_TextPopup_RectTransform_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextPopup_TMPComponent
	TextMeshProUGUI_t529313277 * ___m_TextPopup_TMPComponent_6;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_9;
	// UnityEngine.Canvas TMPro.Examples.TMP_TextSelector_B::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_10;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_B::m_Camera
	Camera_t4157153871 * ___m_Camera_11;
	// System.Boolean TMPro.Examples.TMP_TextSelector_B::isHoveringObject
	bool ___isHoveringObject_12;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedWord
	int32_t ___m_selectedWord_13;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedLink
	int32_t ___m_selectedLink_14;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_lastIndex
	int32_t ___m_lastIndex_15;
	// UnityEngine.Matrix4x4 TMPro.Examples.TMP_TextSelector_B::m_matrix
	Matrix4x4_t1817901843  ___m_matrix_16;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.TMP_TextSelector_B::m_cachedMeshInfoVertexData
	TMP_MeshInfoU5BU5D_t3365986247* ___m_cachedMeshInfoVertexData_17;

public:
	inline static int32_t get_offset_of_TextPopup_Prefab_01_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___TextPopup_Prefab_01_4)); }
	inline RectTransform_t3704657025 * get_TextPopup_Prefab_01_4() const { return ___TextPopup_Prefab_01_4; }
	inline RectTransform_t3704657025 ** get_address_of_TextPopup_Prefab_01_4() { return &___TextPopup_Prefab_01_4; }
	inline void set_TextPopup_Prefab_01_4(RectTransform_t3704657025 * value)
	{
		___TextPopup_Prefab_01_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextPopup_Prefab_01_4), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_RectTransform_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_RectTransform_5)); }
	inline RectTransform_t3704657025 * get_m_TextPopup_RectTransform_5() const { return ___m_TextPopup_RectTransform_5; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextPopup_RectTransform_5() { return &___m_TextPopup_RectTransform_5; }
	inline void set_m_TextPopup_RectTransform_5(RectTransform_t3704657025 * value)
	{
		___m_TextPopup_RectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_RectTransform_5), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_TMPComponent_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextPopup_TMPComponent_6)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextPopup_TMPComponent_6() const { return ___m_TextPopup_TMPComponent_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextPopup_TMPComponent_6() { return &___m_TextPopup_TMPComponent_6; }
	inline void set_m_TextPopup_TMPComponent_6(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextPopup_TMPComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_TMPComponent_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_TextMeshPro_9)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_9() const { return ___m_TextMeshPro_9; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_9() { return &___m_TextMeshPro_9; }
	inline void set_m_TextMeshPro_9(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_9), value);
	}

	inline static int32_t get_offset_of_m_Canvas_10() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Canvas_10)); }
	inline Canvas_t3310196443 * get_m_Canvas_10() const { return ___m_Canvas_10; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_10() { return &___m_Canvas_10; }
	inline void set_m_Canvas_10(Canvas_t3310196443 * value)
	{
		___m_Canvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_10), value);
	}

	inline static int32_t get_offset_of_m_Camera_11() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_Camera_11)); }
	inline Camera_t4157153871 * get_m_Camera_11() const { return ___m_Camera_11; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_11() { return &___m_Camera_11; }
	inline void set_m_Camera_11(Camera_t4157153871 * value)
	{
		___m_Camera_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_11), value);
	}

	inline static int32_t get_offset_of_isHoveringObject_12() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___isHoveringObject_12)); }
	inline bool get_isHoveringObject_12() const { return ___isHoveringObject_12; }
	inline bool* get_address_of_isHoveringObject_12() { return &___isHoveringObject_12; }
	inline void set_isHoveringObject_12(bool value)
	{
		___isHoveringObject_12 = value;
	}

	inline static int32_t get_offset_of_m_selectedWord_13() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedWord_13)); }
	inline int32_t get_m_selectedWord_13() const { return ___m_selectedWord_13; }
	inline int32_t* get_address_of_m_selectedWord_13() { return &___m_selectedWord_13; }
	inline void set_m_selectedWord_13(int32_t value)
	{
		___m_selectedWord_13 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_14() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_selectedLink_14)); }
	inline int32_t get_m_selectedLink_14() const { return ___m_selectedLink_14; }
	inline int32_t* get_address_of_m_selectedLink_14() { return &___m_selectedLink_14; }
	inline void set_m_selectedLink_14(int32_t value)
	{
		___m_selectedLink_14 = value;
	}

	inline static int32_t get_offset_of_m_lastIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_lastIndex_15)); }
	inline int32_t get_m_lastIndex_15() const { return ___m_lastIndex_15; }
	inline int32_t* get_address_of_m_lastIndex_15() { return &___m_lastIndex_15; }
	inline void set_m_lastIndex_15(int32_t value)
	{
		___m_lastIndex_15 = value;
	}

	inline static int32_t get_offset_of_m_matrix_16() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_matrix_16)); }
	inline Matrix4x4_t1817901843  get_m_matrix_16() const { return ___m_matrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_m_matrix_16() { return &___m_matrix_16; }
	inline void set_m_matrix_16(Matrix4x4_t1817901843  value)
	{
		___m_matrix_16 = value;
	}

	inline static int32_t get_offset_of_m_cachedMeshInfoVertexData_17() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t3982526505, ___m_cachedMeshInfoVertexData_17)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_cachedMeshInfoVertexData_17() const { return ___m_cachedMeshInfoVertexData_17; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_cachedMeshInfoVertexData_17() { return &___m_cachedMeshInfoVertexData_17; }
	inline void set_m_cachedMeshInfoVertexData_17(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_cachedMeshInfoVertexData_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedMeshInfoVertexData_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_B_T3982526505_H
#ifndef TMP_UIFRAMERATECOUNTER_T811747397_H
#define TMP_UIFRAMERATECOUNTER_T811747397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter
struct  TMP_UiFrameRateCounter_t811747397  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::UpdateInterval
	float ___UpdateInterval_4;
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::m_LastInterval
	float ___m_LastInterval_5;
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter::m_Frames
	int32_t ___m_Frames_6;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_7;
	// System.String TMPro.Examples.TMP_UiFrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_8;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_UiFrameRateCounter::m_TextMeshPro
	TextMeshProUGUI_t529313277 * ___m_TextMeshPro_10;
	// UnityEngine.RectTransform TMPro.Examples.TMP_UiFrameRateCounter::m_frameCounter_transform
	RectTransform_t3704657025 * ___m_frameCounter_transform_11;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_12;

public:
	inline static int32_t get_offset_of_UpdateInterval_4() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___UpdateInterval_4)); }
	inline float get_UpdateInterval_4() const { return ___UpdateInterval_4; }
	inline float* get_address_of_UpdateInterval_4() { return &___UpdateInterval_4; }
	inline void set_UpdateInterval_4(float value)
	{
		___UpdateInterval_4 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_5() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_LastInterval_5)); }
	inline float get_m_LastInterval_5() const { return ___m_LastInterval_5; }
	inline float* get_address_of_m_LastInterval_5() { return &___m_LastInterval_5; }
	inline void set_m_LastInterval_5(float value)
	{
		___m_LastInterval_5 = value;
	}

	inline static int32_t get_offset_of_m_Frames_6() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_Frames_6)); }
	inline int32_t get_m_Frames_6() const { return ___m_Frames_6; }
	inline int32_t* get_address_of_m_Frames_6() { return &___m_Frames_6; }
	inline void set_m_Frames_6(int32_t value)
	{
		___m_Frames_6 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_7() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___AnchorPosition_7)); }
	inline int32_t get_AnchorPosition_7() const { return ___AnchorPosition_7; }
	inline int32_t* get_address_of_AnchorPosition_7() { return &___AnchorPosition_7; }
	inline void set_AnchorPosition_7(int32_t value)
	{
		___AnchorPosition_7 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_8() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___htmlColorTag_8)); }
	inline String_t* get_htmlColorTag_8() const { return ___htmlColorTag_8; }
	inline String_t** get_address_of_htmlColorTag_8() { return &___htmlColorTag_8; }
	inline void set_htmlColorTag_8(String_t* value)
	{
		___htmlColorTag_8 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_8), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_10() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_TextMeshPro_10)); }
	inline TextMeshProUGUI_t529313277 * get_m_TextMeshPro_10() const { return ___m_TextMeshPro_10; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_TextMeshPro_10() { return &___m_TextMeshPro_10; }
	inline void set_m_TextMeshPro_10(TextMeshProUGUI_t529313277 * value)
	{
		___m_TextMeshPro_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_10), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_11() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___m_frameCounter_transform_11)); }
	inline RectTransform_t3704657025 * get_m_frameCounter_transform_11() const { return ___m_frameCounter_transform_11; }
	inline RectTransform_t3704657025 ** get_address_of_m_frameCounter_transform_11() { return &___m_frameCounter_transform_11; }
	inline void set_m_frameCounter_transform_11(RectTransform_t3704657025 * value)
	{
		___m_frameCounter_transform_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_11), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_12() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t811747397, ___last_AnchorPosition_12)); }
	inline int32_t get_last_AnchorPosition_12() const { return ___last_AnchorPosition_12; }
	inline int32_t* get_address_of_last_AnchorPosition_12() { return &___last_AnchorPosition_12; }
	inline void set_last_AnchorPosition_12(int32_t value)
	{
		___last_AnchorPosition_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UIFRAMERATECOUNTER_T811747397_H
#ifndef TELETYPE_T2409835159_H
#define TELETYPE_T2409835159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType
struct  TeleType_t2409835159  : public MonoBehaviour_t3962482529
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_4;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_5;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_6;

public:
	inline static int32_t get_offset_of_label01_4() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label01_4)); }
	inline String_t* get_label01_4() const { return ___label01_4; }
	inline String_t** get_address_of_label01_4() { return &___label01_4; }
	inline void set_label01_4(String_t* value)
	{
		___label01_4 = value;
		Il2CppCodeGenWriteBarrier((&___label01_4), value);
	}

	inline static int32_t get_offset_of_label02_5() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___label02_5)); }
	inline String_t* get_label02_5() const { return ___label02_5; }
	inline String_t** get_address_of_label02_5() { return &___label02_5; }
	inline void set_label02_5(String_t* value)
	{
		___label02_5 = value;
		Il2CppCodeGenWriteBarrier((&___label02_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TeleType_t2409835159, ___m_textMeshPro_6)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETYPE_T2409835159_H
#ifndef TEXTCONSOLESIMULATOR_T3766250034_H
#define TEXTCONSOLESIMULATOR_T3766250034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator
struct  TextConsoleSimulator_t3766250034  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_4;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___m_TextComponent_4)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_4), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_5() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t3766250034, ___hasTextChanged_5)); }
	inline bool get_hasTextChanged_5() const { return ___hasTextChanged_5; }
	inline bool* get_address_of_hasTextChanged_5() { return &___hasTextChanged_5; }
	inline void set_hasTextChanged_5(bool value)
	{
		___hasTextChanged_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONSOLESIMULATOR_T3766250034_H
#ifndef TEXTMESHPROFLOATINGTEXT_T845872552_H
#define TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText
struct  TextMeshProFloatingText_t845872552  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Font TMPro.Examples.TextMeshProFloatingText::TheFont
	Font_t1956802104 * ___TheFont_4;
	// UnityEngine.GameObject TMPro.Examples.TextMeshProFloatingText::m_floatingText
	GameObject_t1113636619 * ___m_floatingText_5;
	// TMPro.TextMeshPro TMPro.Examples.TextMeshProFloatingText::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_6;
	// UnityEngine.TextMesh TMPro.Examples.TextMeshProFloatingText::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_7;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_transform
	Transform_t3600365921 * ___m_transform_8;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_floatingText_Transform
	Transform_t3600365921 * ___m_floatingText_Transform_9;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_cameraTransform
	Transform_t3600365921 * ___m_cameraTransform_10;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText::lastPOS
	Vector3_t3722313464  ___lastPOS_11;
	// UnityEngine.Quaternion TMPro.Examples.TextMeshProFloatingText::lastRotation
	Quaternion_t2301928331  ___lastRotation_12;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText::SpawnType
	int32_t ___SpawnType_13;

public:
	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}

	inline static int32_t get_offset_of_m_floatingText_5() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_5)); }
	inline GameObject_t1113636619 * get_m_floatingText_5() const { return ___m_floatingText_5; }
	inline GameObject_t1113636619 ** get_address_of_m_floatingText_5() { return &___m_floatingText_5; }
	inline void set_m_floatingText_5(GameObject_t1113636619 * value)
	{
		___m_floatingText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMeshPro_6)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_textMesh_7)); }
	inline TextMesh_t1536577757 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1536577757 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_transform_8() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_transform_8)); }
	inline Transform_t3600365921 * get_m_transform_8() const { return ___m_transform_8; }
	inline Transform_t3600365921 ** get_address_of_m_transform_8() { return &___m_transform_8; }
	inline void set_m_transform_8(Transform_t3600365921 * value)
	{
		___m_transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_8), value);
	}

	inline static int32_t get_offset_of_m_floatingText_Transform_9() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_floatingText_Transform_9)); }
	inline Transform_t3600365921 * get_m_floatingText_Transform_9() const { return ___m_floatingText_Transform_9; }
	inline Transform_t3600365921 ** get_address_of_m_floatingText_Transform_9() { return &___m_floatingText_Transform_9; }
	inline void set_m_floatingText_Transform_9(Transform_t3600365921 * value)
	{
		___m_floatingText_Transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_Transform_9), value);
	}

	inline static int32_t get_offset_of_m_cameraTransform_10() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___m_cameraTransform_10)); }
	inline Transform_t3600365921 * get_m_cameraTransform_10() const { return ___m_cameraTransform_10; }
	inline Transform_t3600365921 ** get_address_of_m_cameraTransform_10() { return &___m_cameraTransform_10; }
	inline void set_m_cameraTransform_10(Transform_t3600365921 * value)
	{
		___m_cameraTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraTransform_10), value);
	}

	inline static int32_t get_offset_of_lastPOS_11() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastPOS_11)); }
	inline Vector3_t3722313464  get_lastPOS_11() const { return ___lastPOS_11; }
	inline Vector3_t3722313464 * get_address_of_lastPOS_11() { return &___lastPOS_11; }
	inline void set_lastPOS_11(Vector3_t3722313464  value)
	{
		___lastPOS_11 = value;
	}

	inline static int32_t get_offset_of_lastRotation_12() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___lastRotation_12)); }
	inline Quaternion_t2301928331  get_lastRotation_12() const { return ___lastRotation_12; }
	inline Quaternion_t2301928331 * get_address_of_lastRotation_12() { return &___lastRotation_12; }
	inline void set_lastRotation_12(Quaternion_t2301928331  value)
	{
		___lastRotation_12 = value;
	}

	inline static int32_t get_offset_of_SpawnType_13() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t845872552, ___SpawnType_13)); }
	inline int32_t get_SpawnType_13() const { return ___SpawnType_13; }
	inline int32_t* get_address_of_SpawnType_13() { return &___SpawnType_13; }
	inline void set_SpawnType_13(int32_t value)
	{
		___SpawnType_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROFLOATINGTEXT_T845872552_H
#ifndef TEXTMESHSPAWNER_T177691618_H
#define TEXTMESHSPAWNER_T177691618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshSpawner
struct  TextMeshSpawner_t177691618  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_t1956802104 * ___TheFont_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_7;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___TheFont_6)); }
	inline Font_t1956802104 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_t1956802104 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_t1956802104 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_6), value);
	}

	inline static int32_t get_offset_of_floatingText_Script_7() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t177691618, ___floatingText_Script_7)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_7() const { return ___floatingText_Script_7; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_7() { return &___floatingText_Script_7; }
	inline void set_floatingText_Script_7(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_7 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHSPAWNER_T177691618_H
#ifndef TMP_TEXTEVENTHANDLER_T1869054637_H
#define TMP_TEXTEVENTHANDLER_T1869054637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler
struct  TMP_TextEventHandler_t1869054637  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t3109943174 * ___m_OnCharacterSelection_4;
	// TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::m_OnSpriteSelection
	SpriteSelectionEvent_t2798445241 * ___m_OnSpriteSelection_5;
	// TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_t1841909953 * ___m_OnWordSelection_6;
	// TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_t2868010532 * ___m_OnLineSelection_7;
	// TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_t1590929858 * ___m_OnLinkSelection_8;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_9;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_t4157153871 * ___m_Camera_10;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_13;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_14;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_15;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnCharacterSelection_4)); }
	inline CharacterSelectionEvent_t3109943174 * get_m_OnCharacterSelection_4() const { return ___m_OnCharacterSelection_4; }
	inline CharacterSelectionEvent_t3109943174 ** get_address_of_m_OnCharacterSelection_4() { return &___m_OnCharacterSelection_4; }
	inline void set_m_OnCharacterSelection_4(CharacterSelectionEvent_t3109943174 * value)
	{
		___m_OnCharacterSelection_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCharacterSelection_4), value);
	}

	inline static int32_t get_offset_of_m_OnSpriteSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnSpriteSelection_5)); }
	inline SpriteSelectionEvent_t2798445241 * get_m_OnSpriteSelection_5() const { return ___m_OnSpriteSelection_5; }
	inline SpriteSelectionEvent_t2798445241 ** get_address_of_m_OnSpriteSelection_5() { return &___m_OnSpriteSelection_5; }
	inline void set_m_OnSpriteSelection_5(SpriteSelectionEvent_t2798445241 * value)
	{
		___m_OnSpriteSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSpriteSelection_5), value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnWordSelection_6)); }
	inline WordSelectionEvent_t1841909953 * get_m_OnWordSelection_6() const { return ___m_OnWordSelection_6; }
	inline WordSelectionEvent_t1841909953 ** get_address_of_m_OnWordSelection_6() { return &___m_OnWordSelection_6; }
	inline void set_m_OnWordSelection_6(WordSelectionEvent_t1841909953 * value)
	{
		___m_OnWordSelection_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnWordSelection_6), value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLineSelection_7)); }
	inline LineSelectionEvent_t2868010532 * get_m_OnLineSelection_7() const { return ___m_OnLineSelection_7; }
	inline LineSelectionEvent_t2868010532 ** get_address_of_m_OnLineSelection_7() { return &___m_OnLineSelection_7; }
	inline void set_m_OnLineSelection_7(LineSelectionEvent_t2868010532 * value)
	{
		___m_OnLineSelection_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLineSelection_7), value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_OnLinkSelection_8)); }
	inline LinkSelectionEvent_t1590929858 * get_m_OnLinkSelection_8() const { return ___m_OnLinkSelection_8; }
	inline LinkSelectionEvent_t1590929858 ** get_address_of_m_OnLinkSelection_8() { return &___m_OnLinkSelection_8; }
	inline void set_m_OnLinkSelection_8(LinkSelectionEvent_t1590929858 * value)
	{
		___m_OnLinkSelection_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLinkSelection_8), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_TextComponent_9)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_9), value);
	}

	inline static int32_t get_offset_of_m_Camera_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Camera_10)); }
	inline Camera_t4157153871 * get_m_Camera_10() const { return ___m_Camera_10; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_10() { return &___m_Camera_10; }
	inline void set_m_Camera_10(Camera_t4157153871 * value)
	{
		___m_Camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastCharIndex_13)); }
	inline int32_t get_m_lastCharIndex_13() const { return ___m_lastCharIndex_13; }
	inline int32_t* get_address_of_m_lastCharIndex_13() { return &___m_lastCharIndex_13; }
	inline void set_m_lastCharIndex_13(int32_t value)
	{
		___m_lastCharIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_14() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastWordIndex_14)); }
	inline int32_t get_m_lastWordIndex_14() const { return ___m_lastWordIndex_14; }
	inline int32_t* get_address_of_m_lastWordIndex_14() { return &___m_lastWordIndex_14; }
	inline void set_m_lastWordIndex_14(int32_t value)
	{
		___m_lastWordIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_15() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1869054637, ___m_lastLineIndex_15)); }
	inline int32_t get_m_lastLineIndex_15() const { return ___m_lastLineIndex_15; }
	inline int32_t* get_address_of_m_lastLineIndex_15() { return &___m_lastLineIndex_15; }
	inline void set_m_lastLineIndex_15(int32_t value)
	{
		___m_lastLineIndex_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTHANDLER_T1869054637_H
#ifndef UIMANAGER_T1042050227_H
#define UIMANAGER_T1042050227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_t1042050227  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Toggle UIManager::enableUIToggle
	Toggle_t2735377061 * ___enableUIToggle_4;
	// DeathTracker UIManager::deathTracker
	DeathTracker_t1220569700 * ___deathTracker_5;
	// UnityEngine.GameObject UIManager::selected
	GameObject_t1113636619 * ___selected_6;
	// UnityEngine.GameObject UIManager::DeselectedUI
	GameObject_t1113636619 * ___DeselectedUI_7;

public:
	inline static int32_t get_offset_of_enableUIToggle_4() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___enableUIToggle_4)); }
	inline Toggle_t2735377061 * get_enableUIToggle_4() const { return ___enableUIToggle_4; }
	inline Toggle_t2735377061 ** get_address_of_enableUIToggle_4() { return &___enableUIToggle_4; }
	inline void set_enableUIToggle_4(Toggle_t2735377061 * value)
	{
		___enableUIToggle_4 = value;
		Il2CppCodeGenWriteBarrier((&___enableUIToggle_4), value);
	}

	inline static int32_t get_offset_of_deathTracker_5() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___deathTracker_5)); }
	inline DeathTracker_t1220569700 * get_deathTracker_5() const { return ___deathTracker_5; }
	inline DeathTracker_t1220569700 ** get_address_of_deathTracker_5() { return &___deathTracker_5; }
	inline void set_deathTracker_5(DeathTracker_t1220569700 * value)
	{
		___deathTracker_5 = value;
		Il2CppCodeGenWriteBarrier((&___deathTracker_5), value);
	}

	inline static int32_t get_offset_of_selected_6() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___selected_6)); }
	inline GameObject_t1113636619 * get_selected_6() const { return ___selected_6; }
	inline GameObject_t1113636619 ** get_address_of_selected_6() { return &___selected_6; }
	inline void set_selected_6(GameObject_t1113636619 * value)
	{
		___selected_6 = value;
		Il2CppCodeGenWriteBarrier((&___selected_6), value);
	}

	inline static int32_t get_offset_of_DeselectedUI_7() { return static_cast<int32_t>(offsetof(UIManager_t1042050227, ___DeselectedUI_7)); }
	inline GameObject_t1113636619 * get_DeselectedUI_7() const { return ___DeselectedUI_7; }
	inline GameObject_t1113636619 ** get_address_of_DeselectedUI_7() { return &___DeselectedUI_7; }
	inline void set_DeselectedUI_7(GameObject_t1113636619 * value)
	{
		___DeselectedUI_7 = value;
		Il2CppCodeGenWriteBarrier((&___DeselectedUI_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_T1042050227_H
#ifndef NAVMESHLINK_T3494642236_H
#define NAVMESHLINK_T3494642236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshLink
struct  NavMeshLink_t3494642236  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.AI.NavMeshLink::m_AgentTypeID
	int32_t ___m_AgentTypeID_4;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshLink::m_StartPoint
	Vector3_t3722313464  ___m_StartPoint_5;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshLink::m_EndPoint
	Vector3_t3722313464  ___m_EndPoint_6;
	// System.Single UnityEngine.AI.NavMeshLink::m_Width
	float ___m_Width_7;
	// System.Int32 UnityEngine.AI.NavMeshLink::m_CostModifier
	int32_t ___m_CostModifier_8;
	// System.Boolean UnityEngine.AI.NavMeshLink::m_Bidirectional
	bool ___m_Bidirectional_9;
	// System.Boolean UnityEngine.AI.NavMeshLink::m_AutoUpdatePosition
	bool ___m_AutoUpdatePosition_10;
	// System.Int32 UnityEngine.AI.NavMeshLink::m_Area
	int32_t ___m_Area_11;
	// UnityEngine.AI.NavMeshLinkInstance UnityEngine.AI.NavMeshLink::m_LinkInstance
	NavMeshLinkInstance_t3753324387  ___m_LinkInstance_12;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshLink::m_LastPosition
	Vector3_t3722313464  ___m_LastPosition_13;
	// UnityEngine.Quaternion UnityEngine.AI.NavMeshLink::m_LastRotation
	Quaternion_t2301928331  ___m_LastRotation_14;

public:
	inline static int32_t get_offset_of_m_AgentTypeID_4() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_AgentTypeID_4)); }
	inline int32_t get_m_AgentTypeID_4() const { return ___m_AgentTypeID_4; }
	inline int32_t* get_address_of_m_AgentTypeID_4() { return &___m_AgentTypeID_4; }
	inline void set_m_AgentTypeID_4(int32_t value)
	{
		___m_AgentTypeID_4 = value;
	}

	inline static int32_t get_offset_of_m_StartPoint_5() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_StartPoint_5)); }
	inline Vector3_t3722313464  get_m_StartPoint_5() const { return ___m_StartPoint_5; }
	inline Vector3_t3722313464 * get_address_of_m_StartPoint_5() { return &___m_StartPoint_5; }
	inline void set_m_StartPoint_5(Vector3_t3722313464  value)
	{
		___m_StartPoint_5 = value;
	}

	inline static int32_t get_offset_of_m_EndPoint_6() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_EndPoint_6)); }
	inline Vector3_t3722313464  get_m_EndPoint_6() const { return ___m_EndPoint_6; }
	inline Vector3_t3722313464 * get_address_of_m_EndPoint_6() { return &___m_EndPoint_6; }
	inline void set_m_EndPoint_6(Vector3_t3722313464  value)
	{
		___m_EndPoint_6 = value;
	}

	inline static int32_t get_offset_of_m_Width_7() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_Width_7)); }
	inline float get_m_Width_7() const { return ___m_Width_7; }
	inline float* get_address_of_m_Width_7() { return &___m_Width_7; }
	inline void set_m_Width_7(float value)
	{
		___m_Width_7 = value;
	}

	inline static int32_t get_offset_of_m_CostModifier_8() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_CostModifier_8)); }
	inline int32_t get_m_CostModifier_8() const { return ___m_CostModifier_8; }
	inline int32_t* get_address_of_m_CostModifier_8() { return &___m_CostModifier_8; }
	inline void set_m_CostModifier_8(int32_t value)
	{
		___m_CostModifier_8 = value;
	}

	inline static int32_t get_offset_of_m_Bidirectional_9() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_Bidirectional_9)); }
	inline bool get_m_Bidirectional_9() const { return ___m_Bidirectional_9; }
	inline bool* get_address_of_m_Bidirectional_9() { return &___m_Bidirectional_9; }
	inline void set_m_Bidirectional_9(bool value)
	{
		___m_Bidirectional_9 = value;
	}

	inline static int32_t get_offset_of_m_AutoUpdatePosition_10() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_AutoUpdatePosition_10)); }
	inline bool get_m_AutoUpdatePosition_10() const { return ___m_AutoUpdatePosition_10; }
	inline bool* get_address_of_m_AutoUpdatePosition_10() { return &___m_AutoUpdatePosition_10; }
	inline void set_m_AutoUpdatePosition_10(bool value)
	{
		___m_AutoUpdatePosition_10 = value;
	}

	inline static int32_t get_offset_of_m_Area_11() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_Area_11)); }
	inline int32_t get_m_Area_11() const { return ___m_Area_11; }
	inline int32_t* get_address_of_m_Area_11() { return &___m_Area_11; }
	inline void set_m_Area_11(int32_t value)
	{
		___m_Area_11 = value;
	}

	inline static int32_t get_offset_of_m_LinkInstance_12() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_LinkInstance_12)); }
	inline NavMeshLinkInstance_t3753324387  get_m_LinkInstance_12() const { return ___m_LinkInstance_12; }
	inline NavMeshLinkInstance_t3753324387 * get_address_of_m_LinkInstance_12() { return &___m_LinkInstance_12; }
	inline void set_m_LinkInstance_12(NavMeshLinkInstance_t3753324387  value)
	{
		___m_LinkInstance_12 = value;
	}

	inline static int32_t get_offset_of_m_LastPosition_13() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_LastPosition_13)); }
	inline Vector3_t3722313464  get_m_LastPosition_13() const { return ___m_LastPosition_13; }
	inline Vector3_t3722313464 * get_address_of_m_LastPosition_13() { return &___m_LastPosition_13; }
	inline void set_m_LastPosition_13(Vector3_t3722313464  value)
	{
		___m_LastPosition_13 = value;
	}

	inline static int32_t get_offset_of_m_LastRotation_14() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236, ___m_LastRotation_14)); }
	inline Quaternion_t2301928331  get_m_LastRotation_14() const { return ___m_LastRotation_14; }
	inline Quaternion_t2301928331 * get_address_of_m_LastRotation_14() { return &___m_LastRotation_14; }
	inline void set_m_LastRotation_14(Quaternion_t2301928331  value)
	{
		___m_LastRotation_14 = value;
	}
};

struct NavMeshLink_t3494642236_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshLink> UnityEngine.AI.NavMeshLink::s_Tracked
	List_1_t671749682 * ___s_Tracked_15;
	// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate UnityEngine.AI.NavMeshLink::<>f__mg$cache0
	OnNavMeshPreUpdate_t1580782682 * ___U3CU3Ef__mgU24cache0_16;
	// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate UnityEngine.AI.NavMeshLink::<>f__mg$cache1
	OnNavMeshPreUpdate_t1580782682 * ___U3CU3Ef__mgU24cache1_17;

public:
	inline static int32_t get_offset_of_s_Tracked_15() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236_StaticFields, ___s_Tracked_15)); }
	inline List_1_t671749682 * get_s_Tracked_15() const { return ___s_Tracked_15; }
	inline List_1_t671749682 ** get_address_of_s_Tracked_15() { return &___s_Tracked_15; }
	inline void set_s_Tracked_15(List_1_t671749682 * value)
	{
		___s_Tracked_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Tracked_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_16() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236_StaticFields, ___U3CU3Ef__mgU24cache0_16)); }
	inline OnNavMeshPreUpdate_t1580782682 * get_U3CU3Ef__mgU24cache0_16() const { return ___U3CU3Ef__mgU24cache0_16; }
	inline OnNavMeshPreUpdate_t1580782682 ** get_address_of_U3CU3Ef__mgU24cache0_16() { return &___U3CU3Ef__mgU24cache0_16; }
	inline void set_U3CU3Ef__mgU24cache0_16(OnNavMeshPreUpdate_t1580782682 * value)
	{
		___U3CU3Ef__mgU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_17() { return static_cast<int32_t>(offsetof(NavMeshLink_t3494642236_StaticFields, ___U3CU3Ef__mgU24cache1_17)); }
	inline OnNavMeshPreUpdate_t1580782682 * get_U3CU3Ef__mgU24cache1_17() const { return ___U3CU3Ef__mgU24cache1_17; }
	inline OnNavMeshPreUpdate_t1580782682 ** get_address_of_U3CU3Ef__mgU24cache1_17() { return &___U3CU3Ef__mgU24cache1_17; }
	inline void set_U3CU3Ef__mgU24cache1_17(OnNavMeshPreUpdate_t1580782682 * value)
	{
		___U3CU3Ef__mgU24cache1_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHLINK_T3494642236_H
#ifndef NAVMESHMODIFIER_T1010705800_H
#define NAVMESHMODIFIER_T1010705800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshModifier
struct  NavMeshModifier_t1010705800  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UnityEngine.AI.NavMeshModifier::m_OverrideArea
	bool ___m_OverrideArea_4;
	// System.Int32 UnityEngine.AI.NavMeshModifier::m_Area
	int32_t ___m_Area_5;
	// System.Boolean UnityEngine.AI.NavMeshModifier::m_IgnoreFromBuild
	bool ___m_IgnoreFromBuild_6;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.AI.NavMeshModifier::m_AffectedAgents
	List_1_t128053199 * ___m_AffectedAgents_7;

public:
	inline static int32_t get_offset_of_m_OverrideArea_4() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_OverrideArea_4)); }
	inline bool get_m_OverrideArea_4() const { return ___m_OverrideArea_4; }
	inline bool* get_address_of_m_OverrideArea_4() { return &___m_OverrideArea_4; }
	inline void set_m_OverrideArea_4(bool value)
	{
		___m_OverrideArea_4 = value;
	}

	inline static int32_t get_offset_of_m_Area_5() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_Area_5)); }
	inline int32_t get_m_Area_5() const { return ___m_Area_5; }
	inline int32_t* get_address_of_m_Area_5() { return &___m_Area_5; }
	inline void set_m_Area_5(int32_t value)
	{
		___m_Area_5 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreFromBuild_6() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_IgnoreFromBuild_6)); }
	inline bool get_m_IgnoreFromBuild_6() const { return ___m_IgnoreFromBuild_6; }
	inline bool* get_address_of_m_IgnoreFromBuild_6() { return &___m_IgnoreFromBuild_6; }
	inline void set_m_IgnoreFromBuild_6(bool value)
	{
		___m_IgnoreFromBuild_6 = value;
	}

	inline static int32_t get_offset_of_m_AffectedAgents_7() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800, ___m_AffectedAgents_7)); }
	inline List_1_t128053199 * get_m_AffectedAgents_7() const { return ___m_AffectedAgents_7; }
	inline List_1_t128053199 ** get_address_of_m_AffectedAgents_7() { return &___m_AffectedAgents_7; }
	inline void set_m_AffectedAgents_7(List_1_t128053199 * value)
	{
		___m_AffectedAgents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AffectedAgents_7), value);
	}
};

struct NavMeshModifier_t1010705800_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifier> UnityEngine.AI.NavMeshModifier::s_NavMeshModifiers
	List_1_t2482780542 * ___s_NavMeshModifiers_8;

public:
	inline static int32_t get_offset_of_s_NavMeshModifiers_8() { return static_cast<int32_t>(offsetof(NavMeshModifier_t1010705800_StaticFields, ___s_NavMeshModifiers_8)); }
	inline List_1_t2482780542 * get_s_NavMeshModifiers_8() const { return ___s_NavMeshModifiers_8; }
	inline List_1_t2482780542 ** get_address_of_s_NavMeshModifiers_8() { return &___s_NavMeshModifiers_8; }
	inline void set_s_NavMeshModifiers_8(List_1_t2482780542 * value)
	{
		___s_NavMeshModifiers_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_NavMeshModifiers_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHMODIFIER_T1010705800_H
#ifndef NAVMESHMODIFIERVOLUME_T3293887184_H
#define NAVMESHMODIFIERVOLUME_T3293887184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshModifierVolume
struct  NavMeshModifierVolume_t3293887184  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::m_Size
	Vector3_t3722313464  ___m_Size_4;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshModifierVolume::m_Center
	Vector3_t3722313464  ___m_Center_5;
	// System.Int32 UnityEngine.AI.NavMeshModifierVolume::m_Area
	int32_t ___m_Area_6;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.AI.NavMeshModifierVolume::m_AffectedAgents
	List_1_t128053199 * ___m_AffectedAgents_7;

public:
	inline static int32_t get_offset_of_m_Size_4() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_Size_4)); }
	inline Vector3_t3722313464  get_m_Size_4() const { return ___m_Size_4; }
	inline Vector3_t3722313464 * get_address_of_m_Size_4() { return &___m_Size_4; }
	inline void set_m_Size_4(Vector3_t3722313464  value)
	{
		___m_Size_4 = value;
	}

	inline static int32_t get_offset_of_m_Center_5() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_Center_5)); }
	inline Vector3_t3722313464  get_m_Center_5() const { return ___m_Center_5; }
	inline Vector3_t3722313464 * get_address_of_m_Center_5() { return &___m_Center_5; }
	inline void set_m_Center_5(Vector3_t3722313464  value)
	{
		___m_Center_5 = value;
	}

	inline static int32_t get_offset_of_m_Area_6() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_Area_6)); }
	inline int32_t get_m_Area_6() const { return ___m_Area_6; }
	inline int32_t* get_address_of_m_Area_6() { return &___m_Area_6; }
	inline void set_m_Area_6(int32_t value)
	{
		___m_Area_6 = value;
	}

	inline static int32_t get_offset_of_m_AffectedAgents_7() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184, ___m_AffectedAgents_7)); }
	inline List_1_t128053199 * get_m_AffectedAgents_7() const { return ___m_AffectedAgents_7; }
	inline List_1_t128053199 ** get_address_of_m_AffectedAgents_7() { return &___m_AffectedAgents_7; }
	inline void set_m_AffectedAgents_7(List_1_t128053199 * value)
	{
		___m_AffectedAgents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AffectedAgents_7), value);
	}
};

struct NavMeshModifierVolume_t3293887184_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshModifierVolume> UnityEngine.AI.NavMeshModifierVolume::s_NavMeshModifiers
	List_1_t470994630 * ___s_NavMeshModifiers_8;

public:
	inline static int32_t get_offset_of_s_NavMeshModifiers_8() { return static_cast<int32_t>(offsetof(NavMeshModifierVolume_t3293887184_StaticFields, ___s_NavMeshModifiers_8)); }
	inline List_1_t470994630 * get_s_NavMeshModifiers_8() const { return ___s_NavMeshModifiers_8; }
	inline List_1_t470994630 ** get_address_of_s_NavMeshModifiers_8() { return &___s_NavMeshModifiers_8; }
	inline void set_s_NavMeshModifiers_8(List_1_t470994630 * value)
	{
		___s_NavMeshModifiers_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_NavMeshModifiers_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHMODIFIERVOLUME_T3293887184_H
#ifndef NAVMESHSURFACE_T986947041_H
#define NAVMESHSURFACE_T986947041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshSurface
struct  NavMeshSurface_t986947041  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.AI.NavMeshSurface::m_AgentTypeID
	int32_t ___m_AgentTypeID_4;
	// UnityEngine.AI.CollectObjects UnityEngine.AI.NavMeshSurface::m_CollectObjects
	int32_t ___m_CollectObjects_5;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::m_Size
	Vector3_t3722313464  ___m_Size_6;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::m_Center
	Vector3_t3722313464  ___m_Center_7;
	// UnityEngine.LayerMask UnityEngine.AI.NavMeshSurface::m_LayerMask
	LayerMask_t3493934918  ___m_LayerMask_8;
	// UnityEngine.AI.NavMeshCollectGeometry UnityEngine.AI.NavMeshSurface::m_UseGeometry
	int32_t ___m_UseGeometry_9;
	// System.Int32 UnityEngine.AI.NavMeshSurface::m_DefaultArea
	int32_t ___m_DefaultArea_10;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_IgnoreNavMeshAgent
	bool ___m_IgnoreNavMeshAgent_11;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_IgnoreNavMeshObstacle
	bool ___m_IgnoreNavMeshObstacle_12;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_OverrideTileSize
	bool ___m_OverrideTileSize_13;
	// System.Int32 UnityEngine.AI.NavMeshSurface::m_TileSize
	int32_t ___m_TileSize_14;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_OverrideVoxelSize
	bool ___m_OverrideVoxelSize_15;
	// System.Single UnityEngine.AI.NavMeshSurface::m_VoxelSize
	float ___m_VoxelSize_16;
	// System.Boolean UnityEngine.AI.NavMeshSurface::m_BuildHeightMesh
	bool ___m_BuildHeightMesh_17;
	// UnityEngine.AI.NavMeshData UnityEngine.AI.NavMeshSurface::m_NavMeshData
	NavMeshData_t1084598030 * ___m_NavMeshData_18;
	// UnityEngine.AI.NavMeshDataInstance UnityEngine.AI.NavMeshSurface::m_NavMeshDataInstance
	NavMeshDataInstance_t1498462893  ___m_NavMeshDataInstance_19;
	// UnityEngine.Vector3 UnityEngine.AI.NavMeshSurface::m_LastPosition
	Vector3_t3722313464  ___m_LastPosition_20;
	// UnityEngine.Quaternion UnityEngine.AI.NavMeshSurface::m_LastRotation
	Quaternion_t2301928331  ___m_LastRotation_21;

public:
	inline static int32_t get_offset_of_m_AgentTypeID_4() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_AgentTypeID_4)); }
	inline int32_t get_m_AgentTypeID_4() const { return ___m_AgentTypeID_4; }
	inline int32_t* get_address_of_m_AgentTypeID_4() { return &___m_AgentTypeID_4; }
	inline void set_m_AgentTypeID_4(int32_t value)
	{
		___m_AgentTypeID_4 = value;
	}

	inline static int32_t get_offset_of_m_CollectObjects_5() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_CollectObjects_5)); }
	inline int32_t get_m_CollectObjects_5() const { return ___m_CollectObjects_5; }
	inline int32_t* get_address_of_m_CollectObjects_5() { return &___m_CollectObjects_5; }
	inline void set_m_CollectObjects_5(int32_t value)
	{
		___m_CollectObjects_5 = value;
	}

	inline static int32_t get_offset_of_m_Size_6() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_Size_6)); }
	inline Vector3_t3722313464  get_m_Size_6() const { return ___m_Size_6; }
	inline Vector3_t3722313464 * get_address_of_m_Size_6() { return &___m_Size_6; }
	inline void set_m_Size_6(Vector3_t3722313464  value)
	{
		___m_Size_6 = value;
	}

	inline static int32_t get_offset_of_m_Center_7() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_Center_7)); }
	inline Vector3_t3722313464  get_m_Center_7() const { return ___m_Center_7; }
	inline Vector3_t3722313464 * get_address_of_m_Center_7() { return &___m_Center_7; }
	inline void set_m_Center_7(Vector3_t3722313464  value)
	{
		___m_Center_7 = value;
	}

	inline static int32_t get_offset_of_m_LayerMask_8() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_LayerMask_8)); }
	inline LayerMask_t3493934918  get_m_LayerMask_8() const { return ___m_LayerMask_8; }
	inline LayerMask_t3493934918 * get_address_of_m_LayerMask_8() { return &___m_LayerMask_8; }
	inline void set_m_LayerMask_8(LayerMask_t3493934918  value)
	{
		___m_LayerMask_8 = value;
	}

	inline static int32_t get_offset_of_m_UseGeometry_9() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_UseGeometry_9)); }
	inline int32_t get_m_UseGeometry_9() const { return ___m_UseGeometry_9; }
	inline int32_t* get_address_of_m_UseGeometry_9() { return &___m_UseGeometry_9; }
	inline void set_m_UseGeometry_9(int32_t value)
	{
		___m_UseGeometry_9 = value;
	}

	inline static int32_t get_offset_of_m_DefaultArea_10() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_DefaultArea_10)); }
	inline int32_t get_m_DefaultArea_10() const { return ___m_DefaultArea_10; }
	inline int32_t* get_address_of_m_DefaultArea_10() { return &___m_DefaultArea_10; }
	inline void set_m_DefaultArea_10(int32_t value)
	{
		___m_DefaultArea_10 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreNavMeshAgent_11() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_IgnoreNavMeshAgent_11)); }
	inline bool get_m_IgnoreNavMeshAgent_11() const { return ___m_IgnoreNavMeshAgent_11; }
	inline bool* get_address_of_m_IgnoreNavMeshAgent_11() { return &___m_IgnoreNavMeshAgent_11; }
	inline void set_m_IgnoreNavMeshAgent_11(bool value)
	{
		___m_IgnoreNavMeshAgent_11 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreNavMeshObstacle_12() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_IgnoreNavMeshObstacle_12)); }
	inline bool get_m_IgnoreNavMeshObstacle_12() const { return ___m_IgnoreNavMeshObstacle_12; }
	inline bool* get_address_of_m_IgnoreNavMeshObstacle_12() { return &___m_IgnoreNavMeshObstacle_12; }
	inline void set_m_IgnoreNavMeshObstacle_12(bool value)
	{
		___m_IgnoreNavMeshObstacle_12 = value;
	}

	inline static int32_t get_offset_of_m_OverrideTileSize_13() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_OverrideTileSize_13)); }
	inline bool get_m_OverrideTileSize_13() const { return ___m_OverrideTileSize_13; }
	inline bool* get_address_of_m_OverrideTileSize_13() { return &___m_OverrideTileSize_13; }
	inline void set_m_OverrideTileSize_13(bool value)
	{
		___m_OverrideTileSize_13 = value;
	}

	inline static int32_t get_offset_of_m_TileSize_14() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_TileSize_14)); }
	inline int32_t get_m_TileSize_14() const { return ___m_TileSize_14; }
	inline int32_t* get_address_of_m_TileSize_14() { return &___m_TileSize_14; }
	inline void set_m_TileSize_14(int32_t value)
	{
		___m_TileSize_14 = value;
	}

	inline static int32_t get_offset_of_m_OverrideVoxelSize_15() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_OverrideVoxelSize_15)); }
	inline bool get_m_OverrideVoxelSize_15() const { return ___m_OverrideVoxelSize_15; }
	inline bool* get_address_of_m_OverrideVoxelSize_15() { return &___m_OverrideVoxelSize_15; }
	inline void set_m_OverrideVoxelSize_15(bool value)
	{
		___m_OverrideVoxelSize_15 = value;
	}

	inline static int32_t get_offset_of_m_VoxelSize_16() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_VoxelSize_16)); }
	inline float get_m_VoxelSize_16() const { return ___m_VoxelSize_16; }
	inline float* get_address_of_m_VoxelSize_16() { return &___m_VoxelSize_16; }
	inline void set_m_VoxelSize_16(float value)
	{
		___m_VoxelSize_16 = value;
	}

	inline static int32_t get_offset_of_m_BuildHeightMesh_17() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_BuildHeightMesh_17)); }
	inline bool get_m_BuildHeightMesh_17() const { return ___m_BuildHeightMesh_17; }
	inline bool* get_address_of_m_BuildHeightMesh_17() { return &___m_BuildHeightMesh_17; }
	inline void set_m_BuildHeightMesh_17(bool value)
	{
		___m_BuildHeightMesh_17 = value;
	}

	inline static int32_t get_offset_of_m_NavMeshData_18() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_NavMeshData_18)); }
	inline NavMeshData_t1084598030 * get_m_NavMeshData_18() const { return ___m_NavMeshData_18; }
	inline NavMeshData_t1084598030 ** get_address_of_m_NavMeshData_18() { return &___m_NavMeshData_18; }
	inline void set_m_NavMeshData_18(NavMeshData_t1084598030 * value)
	{
		___m_NavMeshData_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_NavMeshData_18), value);
	}

	inline static int32_t get_offset_of_m_NavMeshDataInstance_19() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_NavMeshDataInstance_19)); }
	inline NavMeshDataInstance_t1498462893  get_m_NavMeshDataInstance_19() const { return ___m_NavMeshDataInstance_19; }
	inline NavMeshDataInstance_t1498462893 * get_address_of_m_NavMeshDataInstance_19() { return &___m_NavMeshDataInstance_19; }
	inline void set_m_NavMeshDataInstance_19(NavMeshDataInstance_t1498462893  value)
	{
		___m_NavMeshDataInstance_19 = value;
	}

	inline static int32_t get_offset_of_m_LastPosition_20() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_LastPosition_20)); }
	inline Vector3_t3722313464  get_m_LastPosition_20() const { return ___m_LastPosition_20; }
	inline Vector3_t3722313464 * get_address_of_m_LastPosition_20() { return &___m_LastPosition_20; }
	inline void set_m_LastPosition_20(Vector3_t3722313464  value)
	{
		___m_LastPosition_20 = value;
	}

	inline static int32_t get_offset_of_m_LastRotation_21() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041, ___m_LastRotation_21)); }
	inline Quaternion_t2301928331  get_m_LastRotation_21() const { return ___m_LastRotation_21; }
	inline Quaternion_t2301928331 * get_address_of_m_LastRotation_21() { return &___m_LastRotation_21; }
	inline void set_m_LastRotation_21(Quaternion_t2301928331  value)
	{
		___m_LastRotation_21 = value;
	}
};

struct NavMeshSurface_t986947041_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AI.NavMeshSurface> UnityEngine.AI.NavMeshSurface::s_NavMeshSurfaces
	List_1_t2459021783 * ___s_NavMeshSurfaces_22;
	// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate UnityEngine.AI.NavMeshSurface::<>f__mg$cache0
	OnNavMeshPreUpdate_t1580782682 * ___U3CU3Ef__mgU24cache0_23;
	// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate UnityEngine.AI.NavMeshSurface::<>f__mg$cache1
	OnNavMeshPreUpdate_t1580782682 * ___U3CU3Ef__mgU24cache1_24;
	// System.Predicate`1<UnityEngine.AI.NavMeshModifierVolume> UnityEngine.AI.NavMeshSurface::<>f__am$cache0
	Predicate_1_t4119181308 * ___U3CU3Ef__amU24cache0_25;
	// System.Predicate`1<UnityEngine.AI.NavMeshModifier> UnityEngine.AI.NavMeshSurface::<>f__am$cache1
	Predicate_1_t1835999924 * ___U3CU3Ef__amU24cache1_26;
	// System.Predicate`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface::<>f__am$cache2
	Predicate_1_t1513480349 * ___U3CU3Ef__amU24cache2_27;
	// System.Predicate`1<UnityEngine.AI.NavMeshBuildSource> UnityEngine.AI.NavMeshSurface::<>f__am$cache3
	Predicate_1_t1513480349 * ___U3CU3Ef__amU24cache3_28;

public:
	inline static int32_t get_offset_of_s_NavMeshSurfaces_22() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___s_NavMeshSurfaces_22)); }
	inline List_1_t2459021783 * get_s_NavMeshSurfaces_22() const { return ___s_NavMeshSurfaces_22; }
	inline List_1_t2459021783 ** get_address_of_s_NavMeshSurfaces_22() { return &___s_NavMeshSurfaces_22; }
	inline void set_s_NavMeshSurfaces_22(List_1_t2459021783 * value)
	{
		___s_NavMeshSurfaces_22 = value;
		Il2CppCodeGenWriteBarrier((&___s_NavMeshSurfaces_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_23() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__mgU24cache0_23)); }
	inline OnNavMeshPreUpdate_t1580782682 * get_U3CU3Ef__mgU24cache0_23() const { return ___U3CU3Ef__mgU24cache0_23; }
	inline OnNavMeshPreUpdate_t1580782682 ** get_address_of_U3CU3Ef__mgU24cache0_23() { return &___U3CU3Ef__mgU24cache0_23; }
	inline void set_U3CU3Ef__mgU24cache0_23(OnNavMeshPreUpdate_t1580782682 * value)
	{
		___U3CU3Ef__mgU24cache0_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_24() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__mgU24cache1_24)); }
	inline OnNavMeshPreUpdate_t1580782682 * get_U3CU3Ef__mgU24cache1_24() const { return ___U3CU3Ef__mgU24cache1_24; }
	inline OnNavMeshPreUpdate_t1580782682 ** get_address_of_U3CU3Ef__mgU24cache1_24() { return &___U3CU3Ef__mgU24cache1_24; }
	inline void set_U3CU3Ef__mgU24cache1_24(OnNavMeshPreUpdate_t1580782682 * value)
	{
		___U3CU3Ef__mgU24cache1_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_25() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache0_25)); }
	inline Predicate_1_t4119181308 * get_U3CU3Ef__amU24cache0_25() const { return ___U3CU3Ef__amU24cache0_25; }
	inline Predicate_1_t4119181308 ** get_address_of_U3CU3Ef__amU24cache0_25() { return &___U3CU3Ef__amU24cache0_25; }
	inline void set_U3CU3Ef__amU24cache0_25(Predicate_1_t4119181308 * value)
	{
		___U3CU3Ef__amU24cache0_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_26() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache1_26)); }
	inline Predicate_1_t1835999924 * get_U3CU3Ef__amU24cache1_26() const { return ___U3CU3Ef__amU24cache1_26; }
	inline Predicate_1_t1835999924 ** get_address_of_U3CU3Ef__amU24cache1_26() { return &___U3CU3Ef__amU24cache1_26; }
	inline void set_U3CU3Ef__amU24cache1_26(Predicate_1_t1835999924 * value)
	{
		___U3CU3Ef__amU24cache1_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_27() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache2_27)); }
	inline Predicate_1_t1513480349 * get_U3CU3Ef__amU24cache2_27() const { return ___U3CU3Ef__amU24cache2_27; }
	inline Predicate_1_t1513480349 ** get_address_of_U3CU3Ef__amU24cache2_27() { return &___U3CU3Ef__amU24cache2_27; }
	inline void set_U3CU3Ef__amU24cache2_27(Predicate_1_t1513480349 * value)
	{
		___U3CU3Ef__amU24cache2_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_27), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_28() { return static_cast<int32_t>(offsetof(NavMeshSurface_t986947041_StaticFields, ___U3CU3Ef__amU24cache3_28)); }
	inline Predicate_1_t1513480349 * get_U3CU3Ef__amU24cache3_28() const { return ___U3CU3Ef__amU24cache3_28; }
	inline Predicate_1_t1513480349 ** get_address_of_U3CU3Ef__amU24cache3_28() { return &___U3CU3Ef__amU24cache3_28; }
	inline void set_U3CU3Ef__amU24cache3_28(Predicate_1_t1513480349 * value)
	{
		___U3CU3Ef__amU24cache3_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHSURFACE_T986947041_H
#ifndef DECISION_T2344985003_H
#define DECISION_T2344985003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.Decision
struct  Decision_t2344985003  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECISION_T2344985003_H
#ifndef DECISIONMANAGER_T2018219593_H
#define DECISIONMANAGER_T2018219593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.DecisionManager
struct  DecisionManager_t2018219593  : public MonoBehaviour_t3962482529
{
public:
	// UtilityAI.Decision UtilityAI.DecisionManager::bestDecision
	Decision_t2344985003 * ___bestDecision_4;
	// System.Collections.Generic.List`1<UtilityAI.Decision> UtilityAI.DecisionManager::decisions
	List_1_t3817059745 * ___decisions_5;

public:
	inline static int32_t get_offset_of_bestDecision_4() { return static_cast<int32_t>(offsetof(DecisionManager_t2018219593, ___bestDecision_4)); }
	inline Decision_t2344985003 * get_bestDecision_4() const { return ___bestDecision_4; }
	inline Decision_t2344985003 ** get_address_of_bestDecision_4() { return &___bestDecision_4; }
	inline void set_bestDecision_4(Decision_t2344985003 * value)
	{
		___bestDecision_4 = value;
		Il2CppCodeGenWriteBarrier((&___bestDecision_4), value);
	}

	inline static int32_t get_offset_of_decisions_5() { return static_cast<int32_t>(offsetof(DecisionManager_t2018219593, ___decisions_5)); }
	inline List_1_t3817059745 * get_decisions_5() const { return ___decisions_5; }
	inline List_1_t3817059745 ** get_address_of_decisions_5() { return &___decisions_5; }
	inline void set_decisions_5(List_1_t3817059745 * value)
	{
		___decisions_5 = value;
		Il2CppCodeGenWriteBarrier((&___decisions_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECISIONMANAGER_T2018219593_H
#ifndef FLAG_T3011139145_H
#define FLAG_T3011139145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Flag
struct  Flag_t3011139145  : public PickupableItem_t2078450402
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAG_T3011139145_H
#ifndef FOOD_T274165011_H
#define FOOD_T274165011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Food
struct  Food_t274165011  : public PickupableItem_t2078450402
{
public:
	// System.Single Food::hungerBoost
	float ___hungerBoost_4;

public:
	inline static int32_t get_offset_of_hungerBoost_4() { return static_cast<int32_t>(offsetof(Food_t274165011, ___hungerBoost_4)); }
	inline float get_hungerBoost_4() const { return ___hungerBoost_4; }
	inline float* get_address_of_hungerBoost_4() { return &___hungerBoost_4; }
	inline void set_hungerBoost_4(float value)
	{
		___hungerBoost_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOOD_T274165011_H
#ifndef LANDMINE_T1634836811_H
#define LANDMINE_T1634836811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Landmine
struct  Landmine_t1634836811  : public PickupableItem_t2078450402
{
public:
	// System.Single Landmine::damage
	float ___damage_4;

public:
	inline static int32_t get_offset_of_damage_4() { return static_cast<int32_t>(offsetof(Landmine_t1634836811, ___damage_4)); }
	inline float get_damage_4() const { return ___damage_4; }
	inline float* get_address_of_damage_4() { return &___damage_4; }
	inline void set_damage_4(float value)
	{
		___damage_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANDMINE_T1634836811_H
#ifndef FIGHT_COMPETITOR_T2592966111_H
#define FIGHT_COMPETITOR_T2592966111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIGHT_COMPETITOR
struct  FIGHT_COMPETITOR_t2592966111  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIGHT_COMPETITOR::type
	int32_t ___type_4;
	// System.Single UtilityAI.FIGHT_COMPETITOR::distanceToFlagVariable
	float ___distanceToFlagVariable_5;
	// System.Single UtilityAI.FIGHT_COMPETITOR::distanceVariable
	float ___distanceVariable_6;
	// System.Single UtilityAI.FIGHT_COMPETITOR::maxDistanceToFight
	float ___maxDistanceToFight_7;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_distanceToFlagVariable_5() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___distanceToFlagVariable_5)); }
	inline float get_distanceToFlagVariable_5() const { return ___distanceToFlagVariable_5; }
	inline float* get_address_of_distanceToFlagVariable_5() { return &___distanceToFlagVariable_5; }
	inline void set_distanceToFlagVariable_5(float value)
	{
		___distanceToFlagVariable_5 = value;
	}

	inline static int32_t get_offset_of_distanceVariable_6() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___distanceVariable_6)); }
	inline float get_distanceVariable_6() const { return ___distanceVariable_6; }
	inline float* get_address_of_distanceVariable_6() { return &___distanceVariable_6; }
	inline void set_distanceVariable_6(float value)
	{
		___distanceVariable_6 = value;
	}

	inline static int32_t get_offset_of_maxDistanceToFight_7() { return static_cast<int32_t>(offsetof(FIGHT_COMPETITOR_t2592966111, ___maxDistanceToFight_7)); }
	inline float get_maxDistanceToFight_7() const { return ___maxDistanceToFight_7; }
	inline float* get_address_of_maxDistanceToFight_7() { return &___maxDistanceToFight_7; }
	inline void set_maxDistanceToFight_7(float value)
	{
		___maxDistanceToFight_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIGHT_COMPETITOR_T2592966111_H
#ifndef FIND_FLAG_T1938094438_H
#define FIND_FLAG_T1938094438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIND_FLAG
struct  FIND_FLAG_t1938094438  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIND_FLAG::type
	int32_t ___type_4;
	// System.Int32 UtilityAI.FIND_FLAG::maxDistanceForAffect
	int32_t ___maxDistanceForAffect_5;
	// System.Single UtilityAI.FIND_FLAG::maxDistanceWeighting
	float ___maxDistanceWeighting_6;
	// System.Single UtilityAI.FIND_FLAG::maxEnemyWeighting
	float ___maxEnemyWeighting_7;
	// System.Single UtilityAI.FIND_FLAG::maxHealthWeighting
	float ___maxHealthWeighting_8;
	// System.Single UtilityAI.FIND_FLAG::hungerVariable
	float ___hungerVariable_9;
	// System.Single UtilityAI.FIND_FLAG::thirstVariable
	float ___thirstVariable_10;
	// System.Single UtilityAI.FIND_FLAG::healthVariable
	float ___healthVariable_11;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_maxDistanceForAffect_5() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxDistanceForAffect_5)); }
	inline int32_t get_maxDistanceForAffect_5() const { return ___maxDistanceForAffect_5; }
	inline int32_t* get_address_of_maxDistanceForAffect_5() { return &___maxDistanceForAffect_5; }
	inline void set_maxDistanceForAffect_5(int32_t value)
	{
		___maxDistanceForAffect_5 = value;
	}

	inline static int32_t get_offset_of_maxDistanceWeighting_6() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxDistanceWeighting_6)); }
	inline float get_maxDistanceWeighting_6() const { return ___maxDistanceWeighting_6; }
	inline float* get_address_of_maxDistanceWeighting_6() { return &___maxDistanceWeighting_6; }
	inline void set_maxDistanceWeighting_6(float value)
	{
		___maxDistanceWeighting_6 = value;
	}

	inline static int32_t get_offset_of_maxEnemyWeighting_7() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxEnemyWeighting_7)); }
	inline float get_maxEnemyWeighting_7() const { return ___maxEnemyWeighting_7; }
	inline float* get_address_of_maxEnemyWeighting_7() { return &___maxEnemyWeighting_7; }
	inline void set_maxEnemyWeighting_7(float value)
	{
		___maxEnemyWeighting_7 = value;
	}

	inline static int32_t get_offset_of_maxHealthWeighting_8() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___maxHealthWeighting_8)); }
	inline float get_maxHealthWeighting_8() const { return ___maxHealthWeighting_8; }
	inline float* get_address_of_maxHealthWeighting_8() { return &___maxHealthWeighting_8; }
	inline void set_maxHealthWeighting_8(float value)
	{
		___maxHealthWeighting_8 = value;
	}

	inline static int32_t get_offset_of_hungerVariable_9() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___hungerVariable_9)); }
	inline float get_hungerVariable_9() const { return ___hungerVariable_9; }
	inline float* get_address_of_hungerVariable_9() { return &___hungerVariable_9; }
	inline void set_hungerVariable_9(float value)
	{
		___hungerVariable_9 = value;
	}

	inline static int32_t get_offset_of_thirstVariable_10() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___thirstVariable_10)); }
	inline float get_thirstVariable_10() const { return ___thirstVariable_10; }
	inline float* get_address_of_thirstVariable_10() { return &___thirstVariable_10; }
	inline void set_thirstVariable_10(float value)
	{
		___thirstVariable_10 = value;
	}

	inline static int32_t get_offset_of_healthVariable_11() { return static_cast<int32_t>(offsetof(FIND_FLAG_t1938094438, ___healthVariable_11)); }
	inline float get_healthVariable_11() const { return ___healthVariable_11; }
	inline float* get_address_of_healthVariable_11() { return &___healthVariable_11; }
	inline void set_healthVariable_11(float value)
	{
		___healthVariable_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIND_FLAG_T1938094438_H
#ifndef FIND_FOOD_T1534940982_H
#define FIND_FOOD_T1534940982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIND_FOOD
struct  FIND_FOOD_t1534940982  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIND_FOOD::type
	int32_t ___type_4;
	// System.Single UtilityAI.FIND_FOOD::maxDistanceForAffect
	float ___maxDistanceForAffect_5;
	// System.Single UtilityAI.FIND_FOOD::maxDistanceWeighting
	float ___maxDistanceWeighting_6;
	// System.Single UtilityAI.FIND_FOOD::maxHungerWeighting
	float ___maxHungerWeighting_7;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_maxDistanceForAffect_5() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___maxDistanceForAffect_5)); }
	inline float get_maxDistanceForAffect_5() const { return ___maxDistanceForAffect_5; }
	inline float* get_address_of_maxDistanceForAffect_5() { return &___maxDistanceForAffect_5; }
	inline void set_maxDistanceForAffect_5(float value)
	{
		___maxDistanceForAffect_5 = value;
	}

	inline static int32_t get_offset_of_maxDistanceWeighting_6() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___maxDistanceWeighting_6)); }
	inline float get_maxDistanceWeighting_6() const { return ___maxDistanceWeighting_6; }
	inline float* get_address_of_maxDistanceWeighting_6() { return &___maxDistanceWeighting_6; }
	inline void set_maxDistanceWeighting_6(float value)
	{
		___maxDistanceWeighting_6 = value;
	}

	inline static int32_t get_offset_of_maxHungerWeighting_7() { return static_cast<int32_t>(offsetof(FIND_FOOD_t1534940982, ___maxHungerWeighting_7)); }
	inline float get_maxHungerWeighting_7() const { return ___maxHungerWeighting_7; }
	inline float* get_address_of_maxHungerWeighting_7() { return &___maxHungerWeighting_7; }
	inline void set_maxHungerWeighting_7(float value)
	{
		___maxHungerWeighting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIND_FOOD_T1534940982_H
#ifndef FIND_WATER_T1027203745_H
#define FIND_WATER_T1027203745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UtilityAI.FIND_WATER
struct  FIND_WATER_t1027203745  : public Decision_t2344985003
{
public:
	// UtilityAI.DecisionType UtilityAI.FIND_WATER::type
	int32_t ___type_4;
	// System.Single UtilityAI.FIND_WATER::maxDistanceForAffect
	float ___maxDistanceForAffect_5;
	// System.Single UtilityAI.FIND_WATER::maxDistanceWeighting
	float ___maxDistanceWeighting_6;
	// System.Single UtilityAI.FIND_WATER::maxThirstWeighting
	float ___maxThirstWeighting_7;

public:
	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}

	inline static int32_t get_offset_of_maxDistanceForAffect_5() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___maxDistanceForAffect_5)); }
	inline float get_maxDistanceForAffect_5() const { return ___maxDistanceForAffect_5; }
	inline float* get_address_of_maxDistanceForAffect_5() { return &___maxDistanceForAffect_5; }
	inline void set_maxDistanceForAffect_5(float value)
	{
		___maxDistanceForAffect_5 = value;
	}

	inline static int32_t get_offset_of_maxDistanceWeighting_6() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___maxDistanceWeighting_6)); }
	inline float get_maxDistanceWeighting_6() const { return ___maxDistanceWeighting_6; }
	inline float* get_address_of_maxDistanceWeighting_6() { return &___maxDistanceWeighting_6; }
	inline void set_maxDistanceWeighting_6(float value)
	{
		___maxDistanceWeighting_6 = value;
	}

	inline static int32_t get_offset_of_maxThirstWeighting_7() { return static_cast<int32_t>(offsetof(FIND_WATER_t1027203745, ___maxThirstWeighting_7)); }
	inline float get_maxThirstWeighting_7() const { return ___maxThirstWeighting_7; }
	inline float* get_address_of_maxThirstWeighting_7() { return &___maxThirstWeighting_7; }
	inline void set_maxThirstWeighting_7(float value)
	{
		___maxThirstWeighting_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIND_WATER_T1027203745_H
#ifndef WATER_T1083516957_H
#define WATER_T1083516957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Water
struct  Water_t1083516957  : public PickupableItem_t2078450402
{
public:
	// System.Single Water::thirstBoost
	float ___thirstBoost_4;

public:
	inline static int32_t get_offset_of_thirstBoost_4() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___thirstBoost_4)); }
	inline float get_thirstBoost_4() const { return ___thirstBoost_4; }
	inline float* get_address_of_thirstBoost_4() { return &___thirstBoost_4; }
	inline void set_thirstBoost_4(float value)
	{
		___thirstBoost_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T1083516957_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (Volume_t205472241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2600[3] = 
{
	Volume_t205472241::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	Volume_t205472241::get_offset_of__target_14(),
	Volume_t205472241::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (Initialization_t3119758612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[2] = 
{
	Initialization_t3119758612::get_offset_of__stateMachine_4(),
	Initialization_t3119758612::get_offset_of__displayObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (LayerMaskHelper_t4276256109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (CoreMath_t1603124386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (View_t4100663493)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2604[3] = 
{
	View_t4100663493::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (CameraManager_t3272490737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[10] = 
{
	CameraManager_t3272490737::get_offset_of_competitorViewing_4(),
	CameraManager_t3272490737::get_offset_of_thirdViewPos_5(),
	CameraManager_t3272490737::get_offset_of_currentView_6(),
	CameraManager_t3272490737::get_offset_of_competitorManager_7(),
	CameraManager_t3272490737::get_offset_of_lerpTime_8(),
	CameraManager_t3272490737::get_offset_of_viewChangeComplete_9(),
	CameraManager_t3272490737::get_offset_of_isUpdating_10(),
	CameraManager_t3272490737::get_offset_of_time_11(),
	CameraManager_t3272490737::get_offset_of_t_12(),
	CameraManager_t3272490737::get_offset_of_averageCompetitorPosition_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (Competitor_t2775849210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[19] = 
{
	Competitor_t2775849210::get_offset_of_maxSpeed_4(),
	Competitor_t2775849210::get_offset_of_speed_5(),
	Competitor_t2775849210::get_offset_of_healthRegenMultiplier_6(),
	Competitor_t2775849210::get_offset_of_hungerMultiplier_7(),
	Competitor_t2775849210::get_offset_of_thirstMultiplier_8(),
	Competitor_t2775849210::get_offset_of_maxHealth_9(),
	Competitor_t2775849210::get_offset_of_maxHunger_10(),
	Competitor_t2775849210::get_offset_of_maxThirst_11(),
	Competitor_t2775849210::get_offset_of_health_12(),
	Competitor_t2775849210::get_offset_of_hunger_13(),
	Competitor_t2775849210::get_offset_of_thirst_14(),
	Competitor_t2775849210::get_offset_of_hungerVariable_15(),
	Competitor_t2775849210::get_offset_of_thirstVariable_16(),
	Competitor_t2775849210::get_offset_of_healthVariable_17(),
	Competitor_t2775849210::get_offset_of_maxHealthValue_18(),
	Competitor_t2775849210::get_offset_of_UI_19(),
	Competitor_t2775849210::get_offset_of_firstViewPos_20(),
	Competitor_t2775849210::get_offset_of_changeViewButton_21(),
	Competitor_t2775849210::get_offset_of_cameraManager_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (CompetitorManager_t1763731731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[2] = 
{
	CompetitorManager_t1763731731::get_offset_of_numberOfCompetitors_4(),
	CompetitorManager_t1763731731::get_offset_of_competitors_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (CompetitorUI_t472322071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[23] = 
{
	CompetitorUI_t472322071::get_offset_of_competitor_4(),
	CompetitorUI_t472322071::get_offset_of_decisionManager_5(),
	CompetitorUI_t472322071::get_offset_of_everything_6(),
	CompetitorUI_t472322071::get_offset_of_winnerText_7(),
	CompetitorUI_t472322071::get_offset_of_path_8(),
	CompetitorUI_t472322071::get_offset_of_bestDecisionText_9(),
	CompetitorUI_t472322071::get_offset_of_healthWeightingText_10(),
	CompetitorUI_t472322071::get_offset_of_healthBar_11(),
	CompetitorUI_t472322071::get_offset_of_hungerBar_12(),
	CompetitorUI_t472322071::get_offset_of_thirstBar_13(),
	CompetitorUI_t472322071::get_offset_of_health_14(),
	CompetitorUI_t472322071::get_offset_of_hunger_15(),
	CompetitorUI_t472322071::get_offset_of_thirst_16(),
	CompetitorUI_t472322071::get_offset_of_decisionsText_17(),
	CompetitorUI_t472322071::get_offset_of_everythingToggle_18(),
	CompetitorUI_t472322071::get_offset_of_pathToggle_19(),
	CompetitorUI_t472322071::get_offset_of_bestDecisionToggle_20(),
	CompetitorUI_t472322071::get_offset_of_healthToggle_21(),
	CompetitorUI_t472322071::get_offset_of_healthBarToggle_22(),
	CompetitorUI_t472322071::get_offset_of_thirstBarToggle_23(),
	CompetitorUI_t472322071::get_offset_of_hungerBarToggle_24(),
	CompetitorUI_t472322071::get_offset_of_decisionsToggle_25(),
	CompetitorUI_t472322071::get_offset_of_lastUIToggleValue_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (ItemManager_t3254073967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[7] = 
{
	ItemManager_t3254073967::get_offset_of_maxWaterInMap_4(),
	ItemManager_t3254073967::get_offset_of_maxFoodInMap_5(),
	ItemManager_t3254073967::get_offset_of_maxLandminesInMap_6(),
	ItemManager_t3254073967::get_offset_of_water_7(),
	ItemManager_t3254073967::get_offset_of_food_8(),
	ItemManager_t3254073967::get_offset_of_landmines_9(),
	ItemManager_t3254073967::get_offset_of_flag_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (Relative_t934610588)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2610[6] = 
{
	Relative_t934610588::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (Door_t3003551444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2611[4] = 
{
	Door_t3003551444::get_offset_of_DoorVisualiser_0(),
	Door_t3003551444::get_offset_of_Room_1(),
	Door_t3003551444::get_offset_of_Relative_2(),
	Door_t3003551444::get_offset_of_Connection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (MapGeneration_t47126671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2612[7] = 
{
	MapGeneration_t47126671::get_offset_of_loading_4(),
	MapGeneration_t47126671::get_offset_of_numberOfRoomBranches_5(),
	MapGeneration_t47126671::get_offset_of_roomExpandConnectionLimit_6(),
	MapGeneration_t47126671::get_offset_of_RoomPrefabs_7(),
	MapGeneration_t47126671::get_offset_of_StartingRoomPrefab_8(),
	MapGeneration_t47126671::get_offset_of_grid_9(),
	MapGeneration_t47126671::get_offset_of_activeRooms_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (MapGrid_t3370239467), -1, sizeof(MapGrid_t3370239467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2613[6] = 
{
	MapGrid_t3370239467::get_offset_of_visualise_0(),
	MapGrid_t3370239467::get_offset_of_spacing_1(),
	MapGrid_t3370239467::get_offset_of_GridWidthHeight_2(),
	MapGrid_t3370239467::get_offset_of_Size_3(),
	MapGrid_t3370239467_StaticFields::get_offset_of_MiddleRoom_4(),
	MapGrid_t3370239467_StaticFields::get_offset_of_Rooms_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (NavMeshLink_t3494642236), -1, sizeof(NavMeshLink_t3494642236_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[14] = 
{
	NavMeshLink_t3494642236::get_offset_of_m_AgentTypeID_4(),
	NavMeshLink_t3494642236::get_offset_of_m_StartPoint_5(),
	NavMeshLink_t3494642236::get_offset_of_m_EndPoint_6(),
	NavMeshLink_t3494642236::get_offset_of_m_Width_7(),
	NavMeshLink_t3494642236::get_offset_of_m_CostModifier_8(),
	NavMeshLink_t3494642236::get_offset_of_m_Bidirectional_9(),
	NavMeshLink_t3494642236::get_offset_of_m_AutoUpdatePosition_10(),
	NavMeshLink_t3494642236::get_offset_of_m_Area_11(),
	NavMeshLink_t3494642236::get_offset_of_m_LinkInstance_12(),
	NavMeshLink_t3494642236::get_offset_of_m_LastPosition_13(),
	NavMeshLink_t3494642236::get_offset_of_m_LastRotation_14(),
	NavMeshLink_t3494642236_StaticFields::get_offset_of_s_Tracked_15(),
	NavMeshLink_t3494642236_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_16(),
	NavMeshLink_t3494642236_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (NavMeshModifier_t1010705800), -1, sizeof(NavMeshModifier_t1010705800_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2615[5] = 
{
	NavMeshModifier_t1010705800::get_offset_of_m_OverrideArea_4(),
	NavMeshModifier_t1010705800::get_offset_of_m_Area_5(),
	NavMeshModifier_t1010705800::get_offset_of_m_IgnoreFromBuild_6(),
	NavMeshModifier_t1010705800::get_offset_of_m_AffectedAgents_7(),
	NavMeshModifier_t1010705800_StaticFields::get_offset_of_s_NavMeshModifiers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (NavMeshModifierVolume_t3293887184), -1, sizeof(NavMeshModifierVolume_t3293887184_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2616[5] = 
{
	NavMeshModifierVolume_t3293887184::get_offset_of_m_Size_4(),
	NavMeshModifierVolume_t3293887184::get_offset_of_m_Center_5(),
	NavMeshModifierVolume_t3293887184::get_offset_of_m_Area_6(),
	NavMeshModifierVolume_t3293887184::get_offset_of_m_AffectedAgents_7(),
	NavMeshModifierVolume_t3293887184_StaticFields::get_offset_of_s_NavMeshModifiers_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (CollectObjects_t1049357338)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2617[4] = 
{
	CollectObjects_t1049357338::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (NavMeshSurface_t986947041), -1, sizeof(NavMeshSurface_t986947041_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2618[25] = 
{
	NavMeshSurface_t986947041::get_offset_of_m_AgentTypeID_4(),
	NavMeshSurface_t986947041::get_offset_of_m_CollectObjects_5(),
	NavMeshSurface_t986947041::get_offset_of_m_Size_6(),
	NavMeshSurface_t986947041::get_offset_of_m_Center_7(),
	NavMeshSurface_t986947041::get_offset_of_m_LayerMask_8(),
	NavMeshSurface_t986947041::get_offset_of_m_UseGeometry_9(),
	NavMeshSurface_t986947041::get_offset_of_m_DefaultArea_10(),
	NavMeshSurface_t986947041::get_offset_of_m_IgnoreNavMeshAgent_11(),
	NavMeshSurface_t986947041::get_offset_of_m_IgnoreNavMeshObstacle_12(),
	NavMeshSurface_t986947041::get_offset_of_m_OverrideTileSize_13(),
	NavMeshSurface_t986947041::get_offset_of_m_TileSize_14(),
	NavMeshSurface_t986947041::get_offset_of_m_OverrideVoxelSize_15(),
	NavMeshSurface_t986947041::get_offset_of_m_VoxelSize_16(),
	NavMeshSurface_t986947041::get_offset_of_m_BuildHeightMesh_17(),
	NavMeshSurface_t986947041::get_offset_of_m_NavMeshData_18(),
	NavMeshSurface_t986947041::get_offset_of_m_NavMeshDataInstance_19(),
	NavMeshSurface_t986947041::get_offset_of_m_LastPosition_20(),
	NavMeshSurface_t986947041::get_offset_of_m_LastRotation_21(),
	NavMeshSurface_t986947041_StaticFields::get_offset_of_s_NavMeshSurfaces_22(),
	NavMeshSurface_t986947041_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_23(),
	NavMeshSurface_t986947041_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_24(),
	NavMeshSurface_t986947041_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_25(),
	NavMeshSurface_t986947041_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_26(),
	NavMeshSurface_t986947041_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_27(),
	NavMeshSurface_t986947041_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (PathRenderer_t3314173992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[2] = 
{
	PathRenderer_t3314173992::get_offset_of_nv_4(),
	PathRenderer_t3314173992::get_offset_of_lr_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (Room_t1080437681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[7] = 
{
	Room_t1080437681::get_offset_of_ID_0(),
	Room_t1080437681::get_offset_of_U3CActivatedU3Ek__BackingField_1(),
	Room_t1080437681::get_offset_of_Position_2(),
	Room_t1080437681::get_offset_of_RoomVisualiser_3(),
	Room_t1080437681::get_offset_of_Doors_4(),
	Room_t1080437681::get_offset_of_ConnectedRooms_5(),
	Room_t1080437681::get_offset_of_Unlocked_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (MapManager_t3095496401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[9] = 
{
	MapManager_t3095496401::get_offset_of_loading_4(),
	MapManager_t3095496401::get_offset_of_timeSlider_5(),
	MapManager_t3095496401::get_offset_of_roomBranchSlider_6(),
	MapManager_t3095496401::get_offset_of_roomExpandSlider_7(),
	MapManager_t3095496401::get_offset_of_waterAmountSlider_8(),
	MapManager_t3095496401::get_offset_of_foodAmountSlider_9(),
	MapManager_t3095496401::get_offset_of_landmineAmountSlider_10(),
	MapManager_t3095496401::get_offset_of_competitorAmountSlider_11(),
	MapManager_t3095496401::get_offset_of_minFlagDistanceFromCompetitors_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (U3CResetMapU3Ec__Iterator0_t1993735930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[8] = 
{
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U3CmapGeneratorU3E__0_0(),
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U3CcompetitorManagerU3E__0_1(),
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U3CitemManagerU3E__0_2(),
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U3CuiManagerU3E__0_3(),
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U24this_4(),
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U24current_5(),
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U24disposing_6(),
	U3CResetMapU3Ec__Iterator0_t1993735930::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (Flag_t3011139145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (Food_t274165011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[1] = 
{
	Food_t274165011::get_offset_of_hungerBoost_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (Landmine_t1634836811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[1] = 
{
	Landmine_t1634836811::get_offset_of_damage_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (Water_t1083516957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[1] = 
{
	Water_t1083516957::get_offset_of_thirstBoost_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (PickupableItem_t2078450402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (Billboard_t1300283564), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (DeathTracker_t1220569700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[4] = 
{
	DeathTracker_t1220569700::get_offset_of_background_4(),
	DeathTracker_t1220569700::get_offset_of_deathText_5(),
	DeathTracker_t1220569700::get_offset_of_spacing_6(),
	DeathTracker_t1220569700::get_offset_of_numberOfDeaths_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (DeathType_t3371451543)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2630[5] = 
{
	DeathType_t3371451543::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (UIManager_t1042050227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[4] = 
{
	UIManager_t1042050227::get_offset_of_enableUIToggle_4(),
	UIManager_t1042050227::get_offset_of_deathTracker_5(),
	UIManager_t1042050227::get_offset_of_selected_6(),
	UIManager_t1042050227::get_offset_of_DeselectedUI_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (DecisionType_t3995638880)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2632[5] = 
{
	DecisionType_t3995638880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (Decision_t2344985003), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (DecisionManager_t2018219593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[2] = 
{
	DecisionManager_t2018219593::get_offset_of_bestDecision_4(),
	DecisionManager_t2018219593::get_offset_of_decisions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (FIGHT_COMPETITOR_t2592966111), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[4] = 
{
	FIGHT_COMPETITOR_t2592966111::get_offset_of_type_4(),
	FIGHT_COMPETITOR_t2592966111::get_offset_of_distanceToFlagVariable_5(),
	FIGHT_COMPETITOR_t2592966111::get_offset_of_distanceVariable_6(),
	FIGHT_COMPETITOR_t2592966111::get_offset_of_maxDistanceToFight_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (FIND_FLAG_t1938094438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[8] = 
{
	FIND_FLAG_t1938094438::get_offset_of_type_4(),
	FIND_FLAG_t1938094438::get_offset_of_maxDistanceForAffect_5(),
	FIND_FLAG_t1938094438::get_offset_of_maxDistanceWeighting_6(),
	FIND_FLAG_t1938094438::get_offset_of_maxEnemyWeighting_7(),
	FIND_FLAG_t1938094438::get_offset_of_maxHealthWeighting_8(),
	FIND_FLAG_t1938094438::get_offset_of_hungerVariable_9(),
	FIND_FLAG_t1938094438::get_offset_of_thirstVariable_10(),
	FIND_FLAG_t1938094438::get_offset_of_healthVariable_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (FIND_FOOD_t1534940982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[4] = 
{
	FIND_FOOD_t1534940982::get_offset_of_type_4(),
	FIND_FOOD_t1534940982::get_offset_of_maxDistanceForAffect_5(),
	FIND_FOOD_t1534940982::get_offset_of_maxDistanceWeighting_6(),
	FIND_FOOD_t1534940982::get_offset_of_maxHungerWeighting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (FIND_WATER_t1027203745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[4] = 
{
	FIND_WATER_t1027203745::get_offset_of_type_4(),
	FIND_WATER_t1027203745::get_offset_of_maxDistanceForAffect_5(),
	FIND_WATER_t1027203745::get_offset_of_maxDistanceWeighting_6(),
	FIND_WATER_t1027203745::get_offset_of_maxThirstWeighting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (AsteroidController_t3828879615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[6] = 
{
	AsteroidController_t3828879615::get_offset_of_myRigidbody_4(),
	AsteroidController_t3828879615::get_offset_of_canDestroy_5(),
	AsteroidController_t3828879615::get_offset_of_isDestroyed_6(),
	AsteroidController_t3828879615::get_offset_of_isDebris_7(),
	AsteroidController_t3828879615::get_offset_of_health_8(),
	AsteroidController_t3828879615::get_offset_of_maxHealth_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[5] = 
{
	U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592::get_offset_of_delayTime_0(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592::get_offset_of_U24this_1(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592::get_offset_of_U24current_2(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592::get_offset_of_U24disposing_3(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2232836592::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (GameManager_t3924726104), -1, sizeof(GameManager_t3924726104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2641[21] = 
{
	GameManager_t3924726104_StaticFields::get_offset_of_instance_4(),
	GameManager_t3924726104::get_offset_of_astroidPrefab_5(),
	GameManager_t3924726104::get_offset_of_debrisPrefab_6(),
	GameManager_t3924726104::get_offset_of_explosionPrefab_7(),
	GameManager_t3924726104::get_offset_of_healthPickupPrefab_8(),
	GameManager_t3924726104::get_offset_of_spawning_9(),
	GameManager_t3924726104::get_offset_of_spawnTimeMin_10(),
	GameManager_t3924726104::get_offset_of_spawnTimeMax_11(),
	GameManager_t3924726104::get_offset_of_startingAsteroids_12(),
	GameManager_t3924726104::get_offset_of_healthSpawnTimeMin_13(),
	GameManager_t3924726104::get_offset_of_healthSpawnTimeMax_14(),
	GameManager_t3924726104::get_offset_of_scoreText_15(),
	GameManager_t3924726104::get_offset_of_finalScoreText_16(),
	GameManager_t3924726104::get_offset_of_score_17(),
	GameManager_t3924726104::get_offset_of_asteroidPoints_18(),
	GameManager_t3924726104::get_offset_of_debrisPoints_19(),
	GameManager_t3924726104::get_offset_of_gameOverScreen_20(),
	GameManager_t3924726104::get_offset_of_gameOverText_21(),
	GameManager_t3924726104::get_offset_of_asteroidHealth_22(),
	GameManager_t3924726104::get_offset_of_debrisHealth_23(),
	GameManager_t3924726104::get_offset_of_hasLost_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[4] = 
{
	U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009::get_offset_of_U24this_0(),
	U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009::get_offset_of_U24current_1(),
	U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009::get_offset_of_U24disposing_2(),
	U3CSpawnHealthTimerU3Ec__Iterator0_t4082045009::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (U3CSpawnTimerU3Ec__Iterator1_t3789933097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[4] = 
{
	U3CSpawnTimerU3Ec__Iterator1_t3789933097::get_offset_of_U24this_0(),
	U3CSpawnTimerU3Ec__Iterator1_t3789933097::get_offset_of_U24current_1(),
	U3CSpawnTimerU3Ec__Iterator1_t3789933097::get_offset_of_U24disposing_2(),
	U3CSpawnTimerU3Ec__Iterator1_t3789933097::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (U3CFadeDeathScreenU3Ec__Iterator2_t688632665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[8] = 
{
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U3CimageColorU3E__0_0(),
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U3CtextColorU3E__0_1(),
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U3CfinalScoreTextColorU3E__0_2(),
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U3CtU3E__1_3(),
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U24this_4(),
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U24current_5(),
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U24disposing_6(),
	U3CFadeDeathScreenU3Ec__Iterator2_t688632665::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (HealthPickupController_t1709822218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[5] = 
{
	HealthPickupController_t1709822218::get_offset_of_myRigidbody_4(),
	HealthPickupController_t1709822218::get_offset_of_particles_5(),
	HealthPickupController_t1709822218::get_offset_of_mySprite_6(),
	HealthPickupController_t1709822218::get_offset_of_canDestroy_7(),
	HealthPickupController_t1709822218::get_offset_of_canPickup_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[5] = 
{
	U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325::get_offset_of_delayTime_0(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325::get_offset_of_U24this_1(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325::get_offset_of_U24current_2(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325::get_offset_of_U24disposing_3(),
	U3CDelayInitialDestructionU3Ec__Iterator0_t2740395325::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (PlayerController_t2576101795), -1, sizeof(PlayerController_t2576101795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2647[19] = 
{
	PlayerController_t2576101795_StaticFields::get_offset_of_instance_4(),
	PlayerController_t2576101795::get_offset_of_rotationSpeed_5(),
	PlayerController_t2576101795::get_offset_of_accelerationSpeed_6(),
	PlayerController_t2576101795::get_offset_of_maxSpeed_7(),
	PlayerController_t2576101795::get_offset_of_shootingCooldown_8(),
	PlayerController_t2576101795::get_offset_of_bulletPrefab_9(),
	PlayerController_t2576101795::get_offset_of_myRigidbody_10(),
	PlayerController_t2576101795::get_offset_of_gunTrans_11(),
	PlayerController_t2576101795::get_offset_of_bulletSpawnPos_12(),
	PlayerController_t2576101795::get_offset_of_shootingTimer_13(),
	PlayerController_t2576101795::get_offset_of_canControl_14(),
	PlayerController_t2576101795::get_offset_of_rotation_15(),
	PlayerController_t2576101795::get_offset_of_acceleration_16(),
	PlayerController_t2576101795::get_offset_of_overheatVisual_17(),
	PlayerController_t2576101795::get_offset_of_overheatTimer_18(),
	PlayerController_t2576101795::get_offset_of_overheatTimerMax_19(),
	PlayerController_t2576101795::get_offset_of_cooldownSpeed_20(),
	PlayerController_t2576101795::get_offset_of_canShoot_21(),
	PlayerController_t2576101795::get_offset_of_gunHeatBar_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (U3CDelayOverheatU3Ec__Iterator0_t3589708321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[5] = 
{
	U3CDelayOverheatU3Ec__Iterator0_t3589708321::get_offset_of_U3CtU3E__1_0(),
	U3CDelayOverheatU3Ec__Iterator0_t3589708321::get_offset_of_U24this_1(),
	U3CDelayOverheatU3Ec__Iterator0_t3589708321::get_offset_of_U24current_2(),
	U3CDelayOverheatU3Ec__Iterator0_t3589708321::get_offset_of_U24disposing_3(),
	U3CDelayOverheatU3Ec__Iterator0_t3589708321::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (PlayerHealth_t992877501), -1, sizeof(PlayerHealth_t992877501_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2649[12] = 
{
	PlayerHealth_t992877501_StaticFields::get_offset_of_instance_4(),
	PlayerHealth_t992877501::get_offset_of_canTakeDamage_5(),
	PlayerHealth_t992877501::get_offset_of_maxHealth_6(),
	PlayerHealth_t992877501::get_offset_of_currentHealth_7(),
	PlayerHealth_t992877501::get_offset_of_invulnerabilityTime_8(),
	PlayerHealth_t992877501::get_offset_of_currentShield_9(),
	PlayerHealth_t992877501::get_offset_of_maxShield_10(),
	PlayerHealth_t992877501::get_offset_of_regenShieldTimer_11(),
	PlayerHealth_t992877501::get_offset_of_regenShieldTimerMax_12(),
	PlayerHealth_t992877501::get_offset_of_explosionParticles_13(),
	PlayerHealth_t992877501::get_offset_of_healthBar_14(),
	PlayerHealth_t992877501::get_offset_of_shieldBar_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (U3CInvulnerabliltyU3Ec__Iterator0_t1065921800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[4] = 
{
	U3CInvulnerabliltyU3Ec__Iterator0_t1065921800::get_offset_of_U24this_0(),
	U3CInvulnerabliltyU3Ec__Iterator0_t1065921800::get_offset_of_U24current_1(),
	U3CInvulnerabliltyU3Ec__Iterator0_t1065921800::get_offset_of_U24disposing_2(),
	U3CInvulnerabliltyU3Ec__Iterator0_t1065921800::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (U3CShakeCameraU3Ec__Iterator1_t2884820085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[6] = 
{
	U3CShakeCameraU3Ec__Iterator1_t2884820085::get_offset_of_U3CorigPosU3E__0_0(),
	U3CShakeCameraU3Ec__Iterator1_t2884820085::get_offset_of_U3CtU3E__1_1(),
	U3CShakeCameraU3Ec__Iterator1_t2884820085::get_offset_of_U3CtempVecU3E__2_2(),
	U3CShakeCameraU3Ec__Iterator1_t2884820085::get_offset_of_U24current_3(),
	U3CShakeCameraU3Ec__Iterator1_t2884820085::get_offset_of_U24disposing_4(),
	U3CShakeCameraU3Ec__Iterator1_t2884820085::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (SimpleHealthBar_t721070758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[10] = 
{
	SimpleHealthBar_t721070758::get_offset_of_barImage_4(),
	SimpleHealthBar_t721070758::get_offset_of_colorMode_5(),
	SimpleHealthBar_t721070758::get_offset_of_barColor_6(),
	SimpleHealthBar_t721070758::get_offset_of_barGradient_7(),
	SimpleHealthBar_t721070758::get_offset_of_displayText_8(),
	SimpleHealthBar_t721070758::get_offset_of_barText_9(),
	SimpleHealthBar_t721070758::get_offset_of_additionalText_10(),
	SimpleHealthBar_t721070758::get_offset_of__currentFraction_11(),
	SimpleHealthBar_t721070758::get_offset_of__maxValue_12(),
	SimpleHealthBar_t721070758::get_offset_of_targetFill_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (ColorMode_t635806747)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2653[3] = 
{
	ColorMode_t635806747::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (DisplayText_t3312458044)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2654[5] = 
{
	DisplayText_t3312458044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (Benchmark01_t1571072624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[10] = 
{
	Benchmark01_t1571072624::get_offset_of_BenchmarkType_4(),
	Benchmark01_t1571072624::get_offset_of_TMProFont_5(),
	Benchmark01_t1571072624::get_offset_of_TextMeshFont_6(),
	Benchmark01_t1571072624::get_offset_of_m_textMeshPro_7(),
	Benchmark01_t1571072624::get_offset_of_m_textContainer_8(),
	Benchmark01_t1571072624::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_t1571072624::get_offset_of_m_material01_12(),
	Benchmark01_t1571072624::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (U3CStartU3Ec__Iterator0_t2216151886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[5] = 
{
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (Benchmark01_UGUI_t3264177817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[10] = 
{
	Benchmark01_UGUI_t3264177817::get_offset_of_BenchmarkType_4(),
	Benchmark01_UGUI_t3264177817::get_offset_of_canvas_5(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TMProFont_6(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TextMeshFont_7(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMeshPro_8(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMesh_9(),
	0,
	0,
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material01_12(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material02_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (U3CStartU3Ec__Iterator0_t2622988697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[5] = 
{
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (Benchmark02_t1571269232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[3] = 
{
	Benchmark02_t1571269232::get_offset_of_SpawnType_4(),
	Benchmark02_t1571269232::get_offset_of_NumberOfNPC_5(),
	Benchmark02_t1571269232::get_offset_of_floatingText_Script_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (Benchmark03_t1571203696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[3] = 
{
	Benchmark03_t1571203696::get_offset_of_SpawnType_4(),
	Benchmark03_t1571203696::get_offset_of_NumberOfNPC_5(),
	Benchmark03_t1571203696::get_offset_of_TheFont_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (Benchmark04_t1570876016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[5] = 
{
	Benchmark04_t1570876016::get_offset_of_SpawnType_4(),
	Benchmark04_t1570876016::get_offset_of_MinPointSize_5(),
	Benchmark04_t1570876016::get_offset_of_MaxPointSize_6(),
	Benchmark04_t1570876016::get_offset_of_Steps_7(),
	Benchmark04_t1570876016::get_offset_of_m_Transform_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (CameraController_t2264742161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[25] = 
{
	CameraController_t2264742161::get_offset_of_cameraTransform_4(),
	CameraController_t2264742161::get_offset_of_dummyTarget_5(),
	CameraController_t2264742161::get_offset_of_CameraTarget_6(),
	CameraController_t2264742161::get_offset_of_FollowDistance_7(),
	CameraController_t2264742161::get_offset_of_MaxFollowDistance_8(),
	CameraController_t2264742161::get_offset_of_MinFollowDistance_9(),
	CameraController_t2264742161::get_offset_of_ElevationAngle_10(),
	CameraController_t2264742161::get_offset_of_MaxElevationAngle_11(),
	CameraController_t2264742161::get_offset_of_MinElevationAngle_12(),
	CameraController_t2264742161::get_offset_of_OrbitalAngle_13(),
	CameraController_t2264742161::get_offset_of_CameraMode_14(),
	CameraController_t2264742161::get_offset_of_MovementSmoothing_15(),
	CameraController_t2264742161::get_offset_of_RotationSmoothing_16(),
	CameraController_t2264742161::get_offset_of_previousSmoothing_17(),
	CameraController_t2264742161::get_offset_of_MovementSmoothingValue_18(),
	CameraController_t2264742161::get_offset_of_RotationSmoothingValue_19(),
	CameraController_t2264742161::get_offset_of_MoveSensitivity_20(),
	CameraController_t2264742161::get_offset_of_currentVelocity_21(),
	CameraController_t2264742161::get_offset_of_desiredPosition_22(),
	CameraController_t2264742161::get_offset_of_mouseX_23(),
	CameraController_t2264742161::get_offset_of_mouseY_24(),
	CameraController_t2264742161::get_offset_of_moveVector_25(),
	CameraController_t2264742161::get_offset_of_mouseWheel_26(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (CameraModes_t3200559075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2663[4] = 
{
	CameraModes_t3200559075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (ChatController_t3486202795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[3] = 
{
	ChatController_t3486202795::get_offset_of_TMP_ChatInput_4(),
	ChatController_t3486202795::get_offset_of_TMP_ChatOutput_5(),
	ChatController_t3486202795::get_offset_of_ChatScrollbar_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (EnvMapAnimator_t1140999784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[3] = 
{
	EnvMapAnimator_t1140999784::get_offset_of_RotationSpeeds_4(),
	EnvMapAnimator_t1140999784::get_offset_of_m_textMeshPro_5(),
	EnvMapAnimator_t1140999784::get_offset_of_m_material_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (U3CStartU3Ec__Iterator0_t1520811813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[5] = 
{
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (ObjectSpin_t341713598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[10] = 
{
	ObjectSpin_t341713598::get_offset_of_SpinSpeed_4(),
	ObjectSpin_t341713598::get_offset_of_RotationRange_5(),
	ObjectSpin_t341713598::get_offset_of_m_transform_6(),
	ObjectSpin_t341713598::get_offset_of_m_time_7(),
	ObjectSpin_t341713598::get_offset_of_m_prevPOS_8(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Rotation_9(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Position_10(),
	ObjectSpin_t341713598::get_offset_of_m_lightColor_11(),
	ObjectSpin_t341713598::get_offset_of_frames_12(),
	ObjectSpin_t341713598::get_offset_of_Motion_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (MotionType_t1905163921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2668[4] = 
{
	MotionType_t1905163921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (ShaderPropAnimator_t3617420994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[4] = 
{
	ShaderPropAnimator_t3617420994::get_offset_of_m_Renderer_4(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_Material_5(),
	ShaderPropAnimator_t3617420994::get_offset_of_GlowCurve_6(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_frame_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t4041402054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (SimpleScript_t3279312205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[3] = 
{
	SimpleScript_t3279312205::get_offset_of_m_textMeshPro_4(),
	0,
	SimpleScript_t3279312205::get_offset_of_m_frame_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (SkewTextExample_t3460249701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[4] = 
{
	SkewTextExample_t3460249701::get_offset_of_m_TextComponent_4(),
	SkewTextExample_t3460249701::get_offset_of_VertexCurve_5(),
	SkewTextExample_t3460249701::get_offset_of_CurveScale_6(),
	SkewTextExample_t3460249701::get_offset_of_ShearAmount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (U3CWarpTextU3Ec__Iterator0_t116130919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[13] = 
{
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_ShearValueU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3Cold_curveU3E__0_2(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CtextInfoU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMinXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CboundsMaxXU3E__1_6(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CverticesU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U3CmatrixU3E__2_8(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24this_9(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24current_10(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24disposing_11(),
	U3CWarpTextU3Ec__Iterator0_t116130919::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (TeleType_t2409835159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[3] = 
{
	TeleType_t2409835159::get_offset_of_label01_4(),
	TeleType_t2409835159::get_offset_of_label02_5(),
	TeleType_t2409835159::get_offset_of_m_textMeshPro_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (U3CStartU3Ec__Iterator0_t3341539328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[7] = 
{
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CtotalVisibleCharactersU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CcounterU3E__0_1(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U3CvisibleCountU3E__0_2(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3341539328::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (TextConsoleSimulator_t3766250034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[2] = 
{
	TextConsoleSimulator_t3766250034::get_offset_of_m_TextComponent_4(),
	TextConsoleSimulator_t3766250034::get_offset_of_hasTextChanged_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (U3CRevealCharactersU3Ec__Iterator0_t860191687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[8] = 
{
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_textComponent_0(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtextInfoU3E__0_1(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U3CvisibleCountU3E__0_3(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24this_4(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24current_5(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24disposing_6(),
	U3CRevealCharactersU3Ec__Iterator0_t860191687::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (U3CRevealWordsU3Ec__Iterator1_t1343183262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[9] = 
{
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_textComponent_0(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalWordCountU3E__0_1(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcounterU3E__0_3(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CcurrentWordU3E__0_4(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U3CvisibleCountU3E__0_5(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24current_6(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24disposing_7(),
	U3CRevealWordsU3Ec__Iterator1_t1343183262::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (TextMeshProFloatingText_t845872552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[10] = 
{
	TextMeshProFloatingText_t845872552::get_offset_of_TheFont_4(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_5(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMeshPro_6(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_textMesh_7(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_transform_8(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_floatingText_Transform_9(),
	TextMeshProFloatingText_t845872552::get_offset_of_m_cameraTransform_10(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastPOS_11(),
	TextMeshProFloatingText_t845872552::get_offset_of_lastRotation_12(),
	TextMeshProFloatingText_t845872552::get_offset_of_SpawnType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[12] = 
{
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24this_8(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24current_9(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t2967292235::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[12] = 
{
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24this_8(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24current_9(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t865582314::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (TextMeshSpawner_t177691618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[4] = 
{
	TextMeshSpawner_t177691618::get_offset_of_SpawnType_4(),
	TextMeshSpawner_t177691618::get_offset_of_NumberOfNPC_5(),
	TextMeshSpawner_t177691618::get_offset_of_TheFont_6(),
	TextMeshSpawner_t177691618::get_offset_of_floatingText_Script_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (TMP_DigitValidator_t573672104), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (TMP_ExampleScript_01_t3051742005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[5] = 
{
	TMP_ExampleScript_01_t3051742005::get_offset_of_ObjectType_4(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_isStatic_5(),
	TMP_ExampleScript_01_t3051742005::get_offset_of_m_text_6(),
	0,
	TMP_ExampleScript_01_t3051742005::get_offset_of_count_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (objectType_t4082700821)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2685[3] = 
{
	objectType_t4082700821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (TMP_FrameRateCounter_t314972976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[10] = 
{
	TMP_FrameRateCounter_t314972976::get_offset_of_UpdateInterval_4(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_LastInterval_5(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_Frames_6(),
	TMP_FrameRateCounter_t314972976::get_offset_of_AnchorPosition_7(),
	TMP_FrameRateCounter_t314972976::get_offset_of_htmlColorTag_8(),
	0,
	TMP_FrameRateCounter_t314972976::get_offset_of_m_TextMeshPro_10(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_frameCounter_transform_11(),
	TMP_FrameRateCounter_t314972976::get_offset_of_m_camera_12(),
	TMP_FrameRateCounter_t314972976::get_offset_of_last_AnchorPosition_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (FpsCounterAnchorPositions_t1585798158)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2687[5] = 
{
	FpsCounterAnchorPositions_t1585798158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (TMP_PhoneNumberValidator_t743649728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (TMP_TextEventCheck_t1103849140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[1] = 
{
	TMP_TextEventCheck_t1103849140::get_offset_of_TextEventHandler_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (TMP_TextEventHandler_t1869054637), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[12] = 
{
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnCharacterSelection_4(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnSpriteSelection_5(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnWordSelection_6(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLineSelection_7(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_OnLinkSelection_8(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_TextComponent_9(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Camera_10(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_Canvas_11(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_selectedLink_12(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastCharIndex_13(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastWordIndex_14(),
	TMP_TextEventHandler_t1869054637::get_offset_of_m_lastLineIndex_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (CharacterSelectionEvent_t3109943174), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (SpriteSelectionEvent_t2798445241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (WordSelectionEvent_t1841909953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (LineSelectionEvent_t2868010532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (LinkSelectionEvent_t1590929858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (TMP_TextInfoDebugTool_t1868681444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[9] = 
{
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowCharacters_4(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowWords_5(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLinks_6(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowLines_7(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowMeshBounds_8(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ShowTextBounds_9(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_ObjectStats_10(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_TextComponent_11(),
	TMP_TextInfoDebugTool_t1868681444::get_offset_of_m_Transform_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (TMP_TextSelector_A_t3982526506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[6] = 
{
	TMP_TextSelector_A_t3982526506::get_offset_of_m_TextMeshPro_4(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_Camera_5(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_isHoveringObject_6(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_selectedLink_7(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastCharIndex_8(),
	TMP_TextSelector_A_t3982526506::get_offset_of_m_lastWordIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (TMP_TextSelector_B_t3982526505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[14] = 
{
	TMP_TextSelector_B_t3982526505::get_offset_of_TextPopup_Prefab_01_4(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_RectTransform_5(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextPopup_TMPComponent_6(),
	0,
	0,
	TMP_TextSelector_B_t3982526505::get_offset_of_m_TextMeshPro_9(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Canvas_10(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_Camera_11(),
	TMP_TextSelector_B_t3982526505::get_offset_of_isHoveringObject_12(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedWord_13(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_selectedLink_14(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_lastIndex_15(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_matrix_16(),
	TMP_TextSelector_B_t3982526505::get_offset_of_m_cachedMeshInfoVertexData_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (TMP_UiFrameRateCounter_t811747397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[9] = 
{
	TMP_UiFrameRateCounter_t811747397::get_offset_of_UpdateInterval_4(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_LastInterval_5(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_Frames_6(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_AnchorPosition_7(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_htmlColorTag_8(),
	0,
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_TextMeshPro_10(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_m_frameCounter_transform_11(),
	TMP_UiFrameRateCounter_t811747397::get_offset_of_last_AnchorPosition_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Pixelplacement.Chooser
struct Chooser_t2844814028;
// Pixelplacement.ColliderButtonEvent
struct ColliderButtonEvent_t5483853;
// Pixelplacement.ColliderButton[]
struct ColliderButtonU5BU5D_t3310678905;
// Pixelplacement.GameObjectEvent
struct GameObjectEvent_t2867809866;
// Pixelplacement.Server/ServerInstance
struct ServerInstance_t67659343;
// Pixelplacement.Spline
struct Spline_t1246320346;
// Pixelplacement.SplineAnchor[]
struct SplineAnchorU5BU5D_t3299042520;
// Pixelplacement.SplineFollower[]
struct SplineFollowerU5BU5D_t4227083941;
// Pixelplacement.StateMachine
struct StateMachine_t4243775295;
// Pixelplacement.TweenSystem.TweenBase
struct TweenBase_t3695616892;
// Pixelplacement.TweenSystem.TweenEngine
struct TweenEngine_t2258245354;
// System.Action
struct Action_t1264377477;
// System.Action`1<Pixelplacement.BoolArrayMessage>
struct Action_1_t3343860168;
// System.Action`1<Pixelplacement.BoolMessage>
struct Action_1_t1152822336;
// System.Action`1<Pixelplacement.ByteArrayMessage>
struct Action_1_t2266368787;
// System.Action`1<Pixelplacement.ByteMessage>
struct Action_1_t3421907995;
// System.Action`1<Pixelplacement.ColliderButton>
struct Action_1_t2232274291;
// System.Action`1<Pixelplacement.Color32ArrayMessage>
struct Action_1_t261809231;
// System.Action`1<Pixelplacement.Color32Message>
struct Action_1_t1709086795;
// System.Action`1<Pixelplacement.ColorArrayMessage>
struct Action_1_t1631047211;
// System.Action`1<Pixelplacement.ColorMessage>
struct Action_1_t313855902;
// System.Action`1<Pixelplacement.FloatArrayMessage>
struct Action_1_t1733437200;
// System.Action`1<Pixelplacement.FloatMessage>
struct Action_1_t1704364940;
// System.Action`1<Pixelplacement.IntArrayMessage>
struct Action_1_t712253493;
// System.Action`1<Pixelplacement.IntMessage>
struct Action_1_t581520478;
// System.Action`1<Pixelplacement.Matrix4x4ArrayMessage>
struct Action_1_t3562615688;
// System.Action`1<Pixelplacement.Matrix4x4Message>
struct Action_1_t1401304032;
// System.Action`1<Pixelplacement.QuaternionArrayMessage>
struct Action_1_t2191742972;
// System.Action`1<Pixelplacement.QuaternionMessage>
struct Action_1_t3056131525;
// System.Action`1<Pixelplacement.RectArrayMessage>
struct Action_1_t1842167626;
// System.Action`1<Pixelplacement.RectMessage>
struct Action_1_t1590883354;
// System.Action`1<Pixelplacement.ServerAvailableMessage>
struct Action_1_t257386514;
// System.Action`1<Pixelplacement.StringArrayMessage>
struct Action_1_t4240794820;
// System.Action`1<Pixelplacement.StringMessage>
struct Action_1_t640713362;
// System.Action`1<Pixelplacement.Vector2ArrayMessage>
struct Action_1_t2571979132;
// System.Action`1<Pixelplacement.Vector2Message>
struct Action_1_t2063388938;
// System.Action`1<Pixelplacement.Vector3ArrayMessage>
struct Action_1_t900614475;
// System.Action`1<Pixelplacement.Vector3Message>
struct Action_1_t2134757642;
// System.Action`1<Pixelplacement.Vector4ArrayMessage>
struct Action_1_t1628391807;
// System.Action`1<Pixelplacement.Vector4Message>
struct Action_1_t1635176714;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Action`1<UnityEngine.Color>
struct Action_1_t2728153919;
// System.Action`1<UnityEngine.Networking.NetworkConnection>
struct Action_1_t2877687686;
// System.Action`1<UnityEngine.Networking.NetworkError>
struct Action_1_t2210661120;
// System.Action`1<UnityEngine.Rect>
struct Action_1_t2532947454;
// System.Action`1<UnityEngine.Vector2>
struct Action_1_t2328697118;
// System.Action`1<UnityEngine.Vector3>
struct Action_1_t3894781059;
// System.Action`1<UnityEngine.Vector4>
struct Action_1_t3491496532;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t736164020;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult>
struct Dictionary_2_t1959671187;
// System.Collections.Generic.List`1<Pixelplacement.ClientConnector/AvailableServer>
struct List_1_t140742778;
// System.Collections.Generic.List`1<Pixelplacement.Spline/SplineReparam>
struct List_1_t801671833;
// System.Collections.Generic.List`1<Pixelplacement.TweenSystem.TweenBase>
struct List_1_t872724338;
// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection>
struct List_1_t4177294833;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkConnection>
struct ReadOnlyCollection_1_t3917796378;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t1076084509;
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t3344766165;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t2223234056;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t2302988098;
// UnityEngine.Networking.HostTopology
struct HostTopology_t4059263395;
// UnityEngine.Networking.NetworkClient
struct NetworkClient_t3758195968;
// UnityEngine.Networking.NetworkMessageHandlers
struct NetworkMessageHandlers_t82575973;
// UnityEngine.Networking.NetworkReader
struct NetworkReader_t1574750186;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t2571361770;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Rect[]
struct RectU5BU5D_t2936723554;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BEZIERCURVES_T996170987_H
#define BEZIERCURVES_T996170987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.BezierCurves
struct  BezierCurves_t996170987  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERCURVES_T996170987_H
#ifndef AVAILABLESERVER_T2963635332_H
#define AVAILABLESERVER_T2963635332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ClientConnector/AvailableServer
struct  AvailableServer_t2963635332  : public RuntimeObject
{
public:
	// System.String Pixelplacement.ClientConnector/AvailableServer::ip
	String_t* ___ip_0;
	// System.Int32 Pixelplacement.ClientConnector/AvailableServer::port
	int32_t ___port_1;
	// System.String Pixelplacement.ClientConnector/AvailableServer::deviceID
	String_t* ___deviceID_2;

public:
	inline static int32_t get_offset_of_ip_0() { return static_cast<int32_t>(offsetof(AvailableServer_t2963635332, ___ip_0)); }
	inline String_t* get_ip_0() const { return ___ip_0; }
	inline String_t** get_address_of_ip_0() { return &___ip_0; }
	inline void set_ip_0(String_t* value)
	{
		___ip_0 = value;
		Il2CppCodeGenWriteBarrier((&___ip_0), value);
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(AvailableServer_t2963635332, ___port_1)); }
	inline int32_t get_port_1() const { return ___port_1; }
	inline int32_t* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(int32_t value)
	{
		___port_1 = value;
	}

	inline static int32_t get_offset_of_deviceID_2() { return static_cast<int32_t>(offsetof(AvailableServer_t2963635332, ___deviceID_2)); }
	inline String_t* get_deviceID_2() const { return ___deviceID_2; }
	inline String_t** get_address_of_deviceID_2() { return &___deviceID_2; }
	inline void set_deviceID_2(String_t* value)
	{
		___deviceID_2 = value;
		Il2CppCodeGenWriteBarrier((&___deviceID_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVAILABLESERVER_T2963635332_H
#ifndef IPMANAGER_T1059072560_H
#define IPMANAGER_T1059072560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.IPManager
struct  IPManager_t1059072560  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPMANAGER_T1059072560_H
#ifndef SPLINEREPARAM_T3624564387_H
#define SPLINEREPARAM_T3624564387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Spline/SplineReparam
struct  SplineReparam_t3624564387  : public RuntimeObject
{
public:
	// System.Single Pixelplacement.Spline/SplineReparam::length
	float ___length_0;
	// System.Single Pixelplacement.Spline/SplineReparam::percentage
	float ___percentage_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(SplineReparam_t3624564387, ___length_0)); }
	inline float get_length_0() const { return ___length_0; }
	inline float* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(float value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_percentage_1() { return static_cast<int32_t>(offsetof(SplineReparam_t3624564387, ___percentage_1)); }
	inline float get_percentage_1() const { return ___percentage_1; }
	inline float* get_address_of_percentage_1() { return &___percentage_1; }
	inline void set_percentage_1(float value)
	{
		___percentage_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEREPARAM_T3624564387_H
#ifndef SPLINEFOLLOWER_T1156113196_H
#define SPLINEFOLLOWER_T1156113196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.SplineFollower
struct  SplineFollower_t1156113196  : public RuntimeObject
{
public:
	// UnityEngine.Transform Pixelplacement.SplineFollower::target
	Transform_t3600365921 * ___target_0;
	// System.Single Pixelplacement.SplineFollower::percentage
	float ___percentage_1;
	// System.Boolean Pixelplacement.SplineFollower::faceDirection
	bool ___faceDirection_2;
	// System.Single Pixelplacement.SplineFollower::_previousPercentage
	float ____previousPercentage_3;
	// System.Boolean Pixelplacement.SplineFollower::_previousFaceDirection
	bool ____previousFaceDirection_4;
	// System.Boolean Pixelplacement.SplineFollower::_detached
	bool ____detached_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(SplineFollower_t1156113196, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_percentage_1() { return static_cast<int32_t>(offsetof(SplineFollower_t1156113196, ___percentage_1)); }
	inline float get_percentage_1() const { return ___percentage_1; }
	inline float* get_address_of_percentage_1() { return &___percentage_1; }
	inline void set_percentage_1(float value)
	{
		___percentage_1 = value;
	}

	inline static int32_t get_offset_of_faceDirection_2() { return static_cast<int32_t>(offsetof(SplineFollower_t1156113196, ___faceDirection_2)); }
	inline bool get_faceDirection_2() const { return ___faceDirection_2; }
	inline bool* get_address_of_faceDirection_2() { return &___faceDirection_2; }
	inline void set_faceDirection_2(bool value)
	{
		___faceDirection_2 = value;
	}

	inline static int32_t get_offset_of__previousPercentage_3() { return static_cast<int32_t>(offsetof(SplineFollower_t1156113196, ____previousPercentage_3)); }
	inline float get__previousPercentage_3() const { return ____previousPercentage_3; }
	inline float* get_address_of__previousPercentage_3() { return &____previousPercentage_3; }
	inline void set__previousPercentage_3(float value)
	{
		____previousPercentage_3 = value;
	}

	inline static int32_t get_offset_of__previousFaceDirection_4() { return static_cast<int32_t>(offsetof(SplineFollower_t1156113196, ____previousFaceDirection_4)); }
	inline bool get__previousFaceDirection_4() const { return ____previousFaceDirection_4; }
	inline bool* get_address_of__previousFaceDirection_4() { return &____previousFaceDirection_4; }
	inline void set__previousFaceDirection_4(bool value)
	{
		____previousFaceDirection_4 = value;
	}

	inline static int32_t get_offset_of__detached_5() { return static_cast<int32_t>(offsetof(SplineFollower_t1156113196, ____detached_5)); }
	inline bool get__detached_5() const { return ____detached_5; }
	inline bool* get_address_of__detached_5() { return &____detached_5; }
	inline void set__detached_5(bool value)
	{
		____detached_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEFOLLOWER_T1156113196_H
#ifndef TWEEN_T1172390970_H
#define TWEEN_T1172390970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Tween
struct  Tween_t1172390970  : public RuntimeObject
{
public:

public:
};

struct Tween_t1172390970_StaticFields
{
public:
	// System.Collections.Generic.List`1<Pixelplacement.TweenSystem.TweenBase> Pixelplacement.Tween::activeTweens
	List_1_t872724338 * ___activeTweens_0;
	// Pixelplacement.TweenSystem.TweenEngine Pixelplacement.Tween::_instance
	TweenEngine_t2258245354 * ____instance_1;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeIn
	AnimationCurve_t3046754366 * ____easeIn_2;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeInStrong
	AnimationCurve_t3046754366 * ____easeInStrong_3;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeOut
	AnimationCurve_t3046754366 * ____easeOut_4;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeOutStrong
	AnimationCurve_t3046754366 * ____easeOutStrong_5;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeInOut
	AnimationCurve_t3046754366 * ____easeInOut_6;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeInOutStrong
	AnimationCurve_t3046754366 * ____easeInOutStrong_7;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeInBack
	AnimationCurve_t3046754366 * ____easeInBack_8;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeOutBack
	AnimationCurve_t3046754366 * ____easeOutBack_9;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeInOutBack
	AnimationCurve_t3046754366 * ____easeInOutBack_10;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeSpring
	AnimationCurve_t3046754366 * ____easeSpring_11;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeBounce
	AnimationCurve_t3046754366 * ____easeBounce_12;
	// UnityEngine.AnimationCurve Pixelplacement.Tween::_easeWobble
	AnimationCurve_t3046754366 * ____easeWobble_13;

public:
	inline static int32_t get_offset_of_activeTweens_0() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ___activeTweens_0)); }
	inline List_1_t872724338 * get_activeTweens_0() const { return ___activeTweens_0; }
	inline List_1_t872724338 ** get_address_of_activeTweens_0() { return &___activeTweens_0; }
	inline void set_activeTweens_0(List_1_t872724338 * value)
	{
		___activeTweens_0 = value;
		Il2CppCodeGenWriteBarrier((&___activeTweens_0), value);
	}

	inline static int32_t get_offset_of__instance_1() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____instance_1)); }
	inline TweenEngine_t2258245354 * get__instance_1() const { return ____instance_1; }
	inline TweenEngine_t2258245354 ** get_address_of__instance_1() { return &____instance_1; }
	inline void set__instance_1(TweenEngine_t2258245354 * value)
	{
		____instance_1 = value;
		Il2CppCodeGenWriteBarrier((&____instance_1), value);
	}

	inline static int32_t get_offset_of__easeIn_2() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeIn_2)); }
	inline AnimationCurve_t3046754366 * get__easeIn_2() const { return ____easeIn_2; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeIn_2() { return &____easeIn_2; }
	inline void set__easeIn_2(AnimationCurve_t3046754366 * value)
	{
		____easeIn_2 = value;
		Il2CppCodeGenWriteBarrier((&____easeIn_2), value);
	}

	inline static int32_t get_offset_of__easeInStrong_3() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeInStrong_3)); }
	inline AnimationCurve_t3046754366 * get__easeInStrong_3() const { return ____easeInStrong_3; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeInStrong_3() { return &____easeInStrong_3; }
	inline void set__easeInStrong_3(AnimationCurve_t3046754366 * value)
	{
		____easeInStrong_3 = value;
		Il2CppCodeGenWriteBarrier((&____easeInStrong_3), value);
	}

	inline static int32_t get_offset_of__easeOut_4() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeOut_4)); }
	inline AnimationCurve_t3046754366 * get__easeOut_4() const { return ____easeOut_4; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeOut_4() { return &____easeOut_4; }
	inline void set__easeOut_4(AnimationCurve_t3046754366 * value)
	{
		____easeOut_4 = value;
		Il2CppCodeGenWriteBarrier((&____easeOut_4), value);
	}

	inline static int32_t get_offset_of__easeOutStrong_5() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeOutStrong_5)); }
	inline AnimationCurve_t3046754366 * get__easeOutStrong_5() const { return ____easeOutStrong_5; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeOutStrong_5() { return &____easeOutStrong_5; }
	inline void set__easeOutStrong_5(AnimationCurve_t3046754366 * value)
	{
		____easeOutStrong_5 = value;
		Il2CppCodeGenWriteBarrier((&____easeOutStrong_5), value);
	}

	inline static int32_t get_offset_of__easeInOut_6() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeInOut_6)); }
	inline AnimationCurve_t3046754366 * get__easeInOut_6() const { return ____easeInOut_6; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeInOut_6() { return &____easeInOut_6; }
	inline void set__easeInOut_6(AnimationCurve_t3046754366 * value)
	{
		____easeInOut_6 = value;
		Il2CppCodeGenWriteBarrier((&____easeInOut_6), value);
	}

	inline static int32_t get_offset_of__easeInOutStrong_7() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeInOutStrong_7)); }
	inline AnimationCurve_t3046754366 * get__easeInOutStrong_7() const { return ____easeInOutStrong_7; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeInOutStrong_7() { return &____easeInOutStrong_7; }
	inline void set__easeInOutStrong_7(AnimationCurve_t3046754366 * value)
	{
		____easeInOutStrong_7 = value;
		Il2CppCodeGenWriteBarrier((&____easeInOutStrong_7), value);
	}

	inline static int32_t get_offset_of__easeInBack_8() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeInBack_8)); }
	inline AnimationCurve_t3046754366 * get__easeInBack_8() const { return ____easeInBack_8; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeInBack_8() { return &____easeInBack_8; }
	inline void set__easeInBack_8(AnimationCurve_t3046754366 * value)
	{
		____easeInBack_8 = value;
		Il2CppCodeGenWriteBarrier((&____easeInBack_8), value);
	}

	inline static int32_t get_offset_of__easeOutBack_9() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeOutBack_9)); }
	inline AnimationCurve_t3046754366 * get__easeOutBack_9() const { return ____easeOutBack_9; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeOutBack_9() { return &____easeOutBack_9; }
	inline void set__easeOutBack_9(AnimationCurve_t3046754366 * value)
	{
		____easeOutBack_9 = value;
		Il2CppCodeGenWriteBarrier((&____easeOutBack_9), value);
	}

	inline static int32_t get_offset_of__easeInOutBack_10() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeInOutBack_10)); }
	inline AnimationCurve_t3046754366 * get__easeInOutBack_10() const { return ____easeInOutBack_10; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeInOutBack_10() { return &____easeInOutBack_10; }
	inline void set__easeInOutBack_10(AnimationCurve_t3046754366 * value)
	{
		____easeInOutBack_10 = value;
		Il2CppCodeGenWriteBarrier((&____easeInOutBack_10), value);
	}

	inline static int32_t get_offset_of__easeSpring_11() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeSpring_11)); }
	inline AnimationCurve_t3046754366 * get__easeSpring_11() const { return ____easeSpring_11; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeSpring_11() { return &____easeSpring_11; }
	inline void set__easeSpring_11(AnimationCurve_t3046754366 * value)
	{
		____easeSpring_11 = value;
		Il2CppCodeGenWriteBarrier((&____easeSpring_11), value);
	}

	inline static int32_t get_offset_of__easeBounce_12() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeBounce_12)); }
	inline AnimationCurve_t3046754366 * get__easeBounce_12() const { return ____easeBounce_12; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeBounce_12() { return &____easeBounce_12; }
	inline void set__easeBounce_12(AnimationCurve_t3046754366 * value)
	{
		____easeBounce_12 = value;
		Il2CppCodeGenWriteBarrier((&____easeBounce_12), value);
	}

	inline static int32_t get_offset_of__easeWobble_13() { return static_cast<int32_t>(offsetof(Tween_t1172390970_StaticFields, ____easeWobble_13)); }
	inline AnimationCurve_t3046754366 * get__easeWobble_13() const { return ____easeWobble_13; }
	inline AnimationCurve_t3046754366 ** get_address_of__easeWobble_13() { return &____easeWobble_13; }
	inline void set__easeWobble_13(AnimationCurve_t3046754366 * value)
	{
		____easeWobble_13 = value;
		Il2CppCodeGenWriteBarrier((&____easeWobble_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T1172390970_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef MESSAGEBASE_T3584795631_H
#define MESSAGEBASE_T3584795631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.MessageBase
struct  MessageBase_t3584795631  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEBASE_T3584795631_H
#ifndef NETWORKSERVERSIMPLE_T2785584229_H
#define NETWORKSERVERSIMPLE_T2785584229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkServerSimple
struct  NetworkServerSimple_t2785584229  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Networking.NetworkServerSimple::m_Initialized
	bool ___m_Initialized_0;
	// System.Int32 UnityEngine.Networking.NetworkServerSimple::m_ListenPort
	int32_t ___m_ListenPort_1;
	// System.Int32 UnityEngine.Networking.NetworkServerSimple::m_ServerHostId
	int32_t ___m_ServerHostId_2;
	// System.Int32 UnityEngine.Networking.NetworkServerSimple::m_RelaySlotId
	int32_t ___m_RelaySlotId_3;
	// System.Boolean UnityEngine.Networking.NetworkServerSimple::m_UseWebSockets
	bool ___m_UseWebSockets_4;
	// System.Byte[] UnityEngine.Networking.NetworkServerSimple::m_MsgBuffer
	ByteU5BU5D_t4116647657* ___m_MsgBuffer_5;
	// UnityEngine.Networking.NetworkReader UnityEngine.Networking.NetworkServerSimple::m_MsgReader
	NetworkReader_t1574750186 * ___m_MsgReader_6;
	// System.Type UnityEngine.Networking.NetworkServerSimple::m_NetworkConnectionClass
	Type_t * ___m_NetworkConnectionClass_7;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkServerSimple::m_HostTopology
	HostTopology_t4059263395 * ___m_HostTopology_8;
	// System.Collections.Generic.List`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.NetworkServerSimple::m_Connections
	List_1_t4177294833 * ___m_Connections_9;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.NetworkConnection> UnityEngine.Networking.NetworkServerSimple::m_ConnectionsReadOnly
	ReadOnlyCollection_1_t3917796378 * ___m_ConnectionsReadOnly_10;
	// UnityEngine.Networking.NetworkMessageHandlers UnityEngine.Networking.NetworkServerSimple::m_MessageHandlers
	NetworkMessageHandlers_t82575973 * ___m_MessageHandlers_11;

public:
	inline static int32_t get_offset_of_m_Initialized_0() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_Initialized_0)); }
	inline bool get_m_Initialized_0() const { return ___m_Initialized_0; }
	inline bool* get_address_of_m_Initialized_0() { return &___m_Initialized_0; }
	inline void set_m_Initialized_0(bool value)
	{
		___m_Initialized_0 = value;
	}

	inline static int32_t get_offset_of_m_ListenPort_1() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_ListenPort_1)); }
	inline int32_t get_m_ListenPort_1() const { return ___m_ListenPort_1; }
	inline int32_t* get_address_of_m_ListenPort_1() { return &___m_ListenPort_1; }
	inline void set_m_ListenPort_1(int32_t value)
	{
		___m_ListenPort_1 = value;
	}

	inline static int32_t get_offset_of_m_ServerHostId_2() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_ServerHostId_2)); }
	inline int32_t get_m_ServerHostId_2() const { return ___m_ServerHostId_2; }
	inline int32_t* get_address_of_m_ServerHostId_2() { return &___m_ServerHostId_2; }
	inline void set_m_ServerHostId_2(int32_t value)
	{
		___m_ServerHostId_2 = value;
	}

	inline static int32_t get_offset_of_m_RelaySlotId_3() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_RelaySlotId_3)); }
	inline int32_t get_m_RelaySlotId_3() const { return ___m_RelaySlotId_3; }
	inline int32_t* get_address_of_m_RelaySlotId_3() { return &___m_RelaySlotId_3; }
	inline void set_m_RelaySlotId_3(int32_t value)
	{
		___m_RelaySlotId_3 = value;
	}

	inline static int32_t get_offset_of_m_UseWebSockets_4() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_UseWebSockets_4)); }
	inline bool get_m_UseWebSockets_4() const { return ___m_UseWebSockets_4; }
	inline bool* get_address_of_m_UseWebSockets_4() { return &___m_UseWebSockets_4; }
	inline void set_m_UseWebSockets_4(bool value)
	{
		___m_UseWebSockets_4 = value;
	}

	inline static int32_t get_offset_of_m_MsgBuffer_5() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_MsgBuffer_5)); }
	inline ByteU5BU5D_t4116647657* get_m_MsgBuffer_5() const { return ___m_MsgBuffer_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_MsgBuffer_5() { return &___m_MsgBuffer_5; }
	inline void set_m_MsgBuffer_5(ByteU5BU5D_t4116647657* value)
	{
		___m_MsgBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgBuffer_5), value);
	}

	inline static int32_t get_offset_of_m_MsgReader_6() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_MsgReader_6)); }
	inline NetworkReader_t1574750186 * get_m_MsgReader_6() const { return ___m_MsgReader_6; }
	inline NetworkReader_t1574750186 ** get_address_of_m_MsgReader_6() { return &___m_MsgReader_6; }
	inline void set_m_MsgReader_6(NetworkReader_t1574750186 * value)
	{
		___m_MsgReader_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgReader_6), value);
	}

	inline static int32_t get_offset_of_m_NetworkConnectionClass_7() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_NetworkConnectionClass_7)); }
	inline Type_t * get_m_NetworkConnectionClass_7() const { return ___m_NetworkConnectionClass_7; }
	inline Type_t ** get_address_of_m_NetworkConnectionClass_7() { return &___m_NetworkConnectionClass_7; }
	inline void set_m_NetworkConnectionClass_7(Type_t * value)
	{
		___m_NetworkConnectionClass_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_NetworkConnectionClass_7), value);
	}

	inline static int32_t get_offset_of_m_HostTopology_8() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_HostTopology_8)); }
	inline HostTopology_t4059263395 * get_m_HostTopology_8() const { return ___m_HostTopology_8; }
	inline HostTopology_t4059263395 ** get_address_of_m_HostTopology_8() { return &___m_HostTopology_8; }
	inline void set_m_HostTopology_8(HostTopology_t4059263395 * value)
	{
		___m_HostTopology_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_HostTopology_8), value);
	}

	inline static int32_t get_offset_of_m_Connections_9() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_Connections_9)); }
	inline List_1_t4177294833 * get_m_Connections_9() const { return ___m_Connections_9; }
	inline List_1_t4177294833 ** get_address_of_m_Connections_9() { return &___m_Connections_9; }
	inline void set_m_Connections_9(List_1_t4177294833 * value)
	{
		___m_Connections_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Connections_9), value);
	}

	inline static int32_t get_offset_of_m_ConnectionsReadOnly_10() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_ConnectionsReadOnly_10)); }
	inline ReadOnlyCollection_1_t3917796378 * get_m_ConnectionsReadOnly_10() const { return ___m_ConnectionsReadOnly_10; }
	inline ReadOnlyCollection_1_t3917796378 ** get_address_of_m_ConnectionsReadOnly_10() { return &___m_ConnectionsReadOnly_10; }
	inline void set_m_ConnectionsReadOnly_10(ReadOnlyCollection_1_t3917796378 * value)
	{
		___m_ConnectionsReadOnly_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConnectionsReadOnly_10), value);
	}

	inline static int32_t get_offset_of_m_MessageHandlers_11() { return static_cast<int32_t>(offsetof(NetworkServerSimple_t2785584229, ___m_MessageHandlers_11)); }
	inline NetworkMessageHandlers_t82575973 * get_m_MessageHandlers_11() const { return ___m_MessageHandlers_11; }
	inline NetworkMessageHandlers_t82575973 ** get_address_of_m_MessageHandlers_11() { return &___m_MessageHandlers_11; }
	inline void set_m_MessageHandlers_11(NetworkMessageHandlers_t82575973 * value)
	{
		___m_MessageHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_MessageHandlers_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSERVERSIMPLE_T2785584229_H
#ifndef BOOLARRAYMESSAGE_T3171392573_H
#define BOOLARRAYMESSAGE_T3171392573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.BoolArrayMessage
struct  BoolArrayMessage_t3171392573  : public MessageBase_t3584795631
{
public:
	// System.Boolean[] Pixelplacement.BoolArrayMessage::value
	BooleanU5BU5D_t2897418192* ___value_0;
	// System.String Pixelplacement.BoolArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.BoolArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(BoolArrayMessage_t3171392573, ___value_0)); }
	inline BooleanU5BU5D_t2897418192* get_value_0() const { return ___value_0; }
	inline BooleanU5BU5D_t2897418192** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(BooleanU5BU5D_t2897418192* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(BoolArrayMessage_t3171392573, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(BoolArrayMessage_t3171392573, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLARRAYMESSAGE_T3171392573_H
#ifndef BOOLMESSAGE_T980354741_H
#define BOOLMESSAGE_T980354741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.BoolMessage
struct  BoolMessage_t980354741  : public MessageBase_t3584795631
{
public:
	// System.Boolean Pixelplacement.BoolMessage::value
	bool ___value_0;
	// System.String Pixelplacement.BoolMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.BoolMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(BoolMessage_t980354741, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(BoolMessage_t980354741, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(BoolMessage_t980354741, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLMESSAGE_T980354741_H
#ifndef BYTEARRAYMESSAGE_T2093901192_H
#define BYTEARRAYMESSAGE_T2093901192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ByteArrayMessage
struct  ByteArrayMessage_t2093901192  : public MessageBase_t3584795631
{
public:
	// System.Byte[] Pixelplacement.ByteArrayMessage::value
	ByteU5BU5D_t4116647657* ___value_0;
	// System.String Pixelplacement.ByteArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.ByteArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(ByteArrayMessage_t2093901192, ___value_0)); }
	inline ByteU5BU5D_t4116647657* get_value_0() const { return ___value_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(ByteU5BU5D_t4116647657* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ByteArrayMessage_t2093901192, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(ByteArrayMessage_t2093901192, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEARRAYMESSAGE_T2093901192_H
#ifndef BYTEMESSAGE_T3249440400_H
#define BYTEMESSAGE_T3249440400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ByteMessage
struct  ByteMessage_t3249440400  : public MessageBase_t3584795631
{
public:
	// System.Byte Pixelplacement.ByteMessage::value
	uint8_t ___value_0;
	// System.String Pixelplacement.ByteMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.ByteMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(ByteMessage_t3249440400, ___value_0)); }
	inline uint8_t get_value_0() const { return ___value_0; }
	inline uint8_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(uint8_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ByteMessage_t3249440400, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(ByteMessage_t3249440400, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEMESSAGE_T3249440400_H
#ifndef COLOR32ARRAYMESSAGE_T89341636_H
#define COLOR32ARRAYMESSAGE_T89341636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Color32ArrayMessage
struct  Color32ArrayMessage_t89341636  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Color32[] Pixelplacement.Color32ArrayMessage::value
	Color32U5BU5D_t3850468773* ___value_0;
	// System.String Pixelplacement.Color32ArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.Color32ArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Color32ArrayMessage_t89341636, ___value_0)); }
	inline Color32U5BU5D_t3850468773* get_value_0() const { return ___value_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color32U5BU5D_t3850468773* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Color32ArrayMessage_t89341636, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Color32ArrayMessage_t89341636, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32ARRAYMESSAGE_T89341636_H
#ifndef COLORARRAYMESSAGE_T1458579616_H
#define COLORARRAYMESSAGE_T1458579616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ColorArrayMessage
struct  ColorArrayMessage_t1458579616  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Color[] Pixelplacement.ColorArrayMessage::value
	ColorU5BU5D_t941916413* ___value_0;
	// System.String Pixelplacement.ColorArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.ColorArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(ColorArrayMessage_t1458579616, ___value_0)); }
	inline ColorU5BU5D_t941916413* get_value_0() const { return ___value_0; }
	inline ColorU5BU5D_t941916413** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(ColorU5BU5D_t941916413* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ColorArrayMessage_t1458579616, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(ColorArrayMessage_t1458579616, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORARRAYMESSAGE_T1458579616_H
#ifndef CURVEDETAIL_T910340028_H
#define CURVEDETAIL_T910340028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.CurveDetail
struct  CurveDetail_t910340028 
{
public:
	// System.Int32 Pixelplacement.CurveDetail::currentCurve
	int32_t ___currentCurve_0;
	// System.Single Pixelplacement.CurveDetail::currentCurvePercentage
	float ___currentCurvePercentage_1;

public:
	inline static int32_t get_offset_of_currentCurve_0() { return static_cast<int32_t>(offsetof(CurveDetail_t910340028, ___currentCurve_0)); }
	inline int32_t get_currentCurve_0() const { return ___currentCurve_0; }
	inline int32_t* get_address_of_currentCurve_0() { return &___currentCurve_0; }
	inline void set_currentCurve_0(int32_t value)
	{
		___currentCurve_0 = value;
	}

	inline static int32_t get_offset_of_currentCurvePercentage_1() { return static_cast<int32_t>(offsetof(CurveDetail_t910340028, ___currentCurvePercentage_1)); }
	inline float get_currentCurvePercentage_1() const { return ___currentCurvePercentage_1; }
	inline float* get_address_of_currentCurvePercentage_1() { return &___currentCurvePercentage_1; }
	inline void set_currentCurvePercentage_1(float value)
	{
		___currentCurvePercentage_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVEDETAIL_T910340028_H
#ifndef FLOATARRAYMESSAGE_T1560969605_H
#define FLOATARRAYMESSAGE_T1560969605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.FloatArrayMessage
struct  FloatArrayMessage_t1560969605  : public MessageBase_t3584795631
{
public:
	// System.Single[] Pixelplacement.FloatArrayMessage::value
	SingleU5BU5D_t1444911251* ___value_0;
	// System.String Pixelplacement.FloatArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.FloatArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(FloatArrayMessage_t1560969605, ___value_0)); }
	inline SingleU5BU5D_t1444911251* get_value_0() const { return ___value_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(SingleU5BU5D_t1444911251* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(FloatArrayMessage_t1560969605, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(FloatArrayMessage_t1560969605, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATARRAYMESSAGE_T1560969605_H
#ifndef FLOATMESSAGE_T1531897345_H
#define FLOATMESSAGE_T1531897345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.FloatMessage
struct  FloatMessage_t1531897345  : public MessageBase_t3584795631
{
public:
	// System.Single Pixelplacement.FloatMessage::value
	float ___value_0;
	// System.String Pixelplacement.FloatMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.FloatMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(FloatMessage_t1531897345, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(FloatMessage_t1531897345, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(FloatMessage_t1531897345, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATMESSAGE_T1531897345_H
#ifndef INTARRAYMESSAGE_T539785898_H
#define INTARRAYMESSAGE_T539785898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.IntArrayMessage
struct  IntArrayMessage_t539785898  : public MessageBase_t3584795631
{
public:
	// System.Int32[] Pixelplacement.IntArrayMessage::value
	Int32U5BU5D_t385246372* ___value_0;
	// System.String Pixelplacement.IntArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.IntArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(IntArrayMessage_t539785898, ___value_0)); }
	inline Int32U5BU5D_t385246372* get_value_0() const { return ___value_0; }
	inline Int32U5BU5D_t385246372** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Int32U5BU5D_t385246372* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(IntArrayMessage_t539785898, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(IntArrayMessage_t539785898, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTARRAYMESSAGE_T539785898_H
#ifndef INTMESSAGE_T409052883_H
#define INTMESSAGE_T409052883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.IntMessage
struct  IntMessage_t409052883  : public MessageBase_t3584795631
{
public:
	// System.Int32 Pixelplacement.IntMessage::value
	int32_t ___value_0;
	// System.String Pixelplacement.IntMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.IntMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(IntMessage_t409052883, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(IntMessage_t409052883, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(IntMessage_t409052883, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTMESSAGE_T409052883_H
#ifndef MATRIX4X4ARRAYMESSAGE_T3390148093_H
#define MATRIX4X4ARRAYMESSAGE_T3390148093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Matrix4x4ArrayMessage
struct  Matrix4x4ArrayMessage_t3390148093  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Matrix4x4[] Pixelplacement.Matrix4x4ArrayMessage::value
	Matrix4x4U5BU5D_t2302988098* ___value_0;
	// System.String Pixelplacement.Matrix4x4ArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.Matrix4x4ArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Matrix4x4ArrayMessage_t3390148093, ___value_0)); }
	inline Matrix4x4U5BU5D_t2302988098* get_value_0() const { return ___value_0; }
	inline Matrix4x4U5BU5D_t2302988098** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Matrix4x4U5BU5D_t2302988098* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Matrix4x4ArrayMessage_t3390148093, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Matrix4x4ArrayMessage_t3390148093, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4ARRAYMESSAGE_T3390148093_H
#ifndef QUATERNIONARRAYMESSAGE_T2019275377_H
#define QUATERNIONARRAYMESSAGE_T2019275377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.QuaternionArrayMessage
struct  QuaternionArrayMessage_t2019275377  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Quaternion[] Pixelplacement.QuaternionArrayMessage::value
	QuaternionU5BU5D_t2571361770* ___value_0;
	// System.String Pixelplacement.QuaternionArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.QuaternionArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(QuaternionArrayMessage_t2019275377, ___value_0)); }
	inline QuaternionU5BU5D_t2571361770* get_value_0() const { return ___value_0; }
	inline QuaternionU5BU5D_t2571361770** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(QuaternionU5BU5D_t2571361770* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(QuaternionArrayMessage_t2019275377, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(QuaternionArrayMessage_t2019275377, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONARRAYMESSAGE_T2019275377_H
#ifndef RECTARRAYMESSAGE_T1669700031_H
#define RECTARRAYMESSAGE_T1669700031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.RectArrayMessage
struct  RectArrayMessage_t1669700031  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Rect[] Pixelplacement.RectArrayMessage::value
	RectU5BU5D_t2936723554* ___value_0;
	// System.String Pixelplacement.RectArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.RectArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RectArrayMessage_t1669700031, ___value_0)); }
	inline RectU5BU5D_t2936723554* get_value_0() const { return ___value_0; }
	inline RectU5BU5D_t2936723554** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(RectU5BU5D_t2936723554* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(RectArrayMessage_t1669700031, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(RectArrayMessage_t1669700031, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTARRAYMESSAGE_T1669700031_H
#ifndef SERVERINSTANCE_T67659343_H
#define SERVERINSTANCE_T67659343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Server/ServerInstance
struct  ServerInstance_t67659343  : public NetworkServerSimple_t2785584229
{
public:
	// System.Action`1<UnityEngine.Networking.NetworkConnection> Pixelplacement.Server/ServerInstance::OnConnection
	Action_1_t2877687686 * ___OnConnection_12;
	// System.Action`1<UnityEngine.Networking.NetworkConnection> Pixelplacement.Server/ServerInstance::OnDisconnection
	Action_1_t2877687686 * ___OnDisconnection_13;

public:
	inline static int32_t get_offset_of_OnConnection_12() { return static_cast<int32_t>(offsetof(ServerInstance_t67659343, ___OnConnection_12)); }
	inline Action_1_t2877687686 * get_OnConnection_12() const { return ___OnConnection_12; }
	inline Action_1_t2877687686 ** get_address_of_OnConnection_12() { return &___OnConnection_12; }
	inline void set_OnConnection_12(Action_1_t2877687686 * value)
	{
		___OnConnection_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnConnection_12), value);
	}

	inline static int32_t get_offset_of_OnDisconnection_13() { return static_cast<int32_t>(offsetof(ServerInstance_t67659343, ___OnDisconnection_13)); }
	inline Action_1_t2877687686 * get_OnDisconnection_13() const { return ___OnDisconnection_13; }
	inline Action_1_t2877687686 ** get_address_of_OnDisconnection_13() { return &___OnDisconnection_13; }
	inline void set_OnDisconnection_13(Action_1_t2877687686 * value)
	{
		___OnDisconnection_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnDisconnection_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERINSTANCE_T67659343_H
#ifndef SERVERAVAILABLEMESSAGE_T84918919_H
#define SERVERAVAILABLEMESSAGE_T84918919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ServerAvailableMessage
struct  ServerAvailableMessage_t84918919  : public MessageBase_t3584795631
{
public:
	// System.Int32 Pixelplacement.ServerAvailableMessage::port
	int32_t ___port_0;
	// System.String Pixelplacement.ServerAvailableMessage::deviceId
	String_t* ___deviceId_1;
	// System.String Pixelplacement.ServerAvailableMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_port_0() { return static_cast<int32_t>(offsetof(ServerAvailableMessage_t84918919, ___port_0)); }
	inline int32_t get_port_0() const { return ___port_0; }
	inline int32_t* get_address_of_port_0() { return &___port_0; }
	inline void set_port_0(int32_t value)
	{
		___port_0 = value;
	}

	inline static int32_t get_offset_of_deviceId_1() { return static_cast<int32_t>(offsetof(ServerAvailableMessage_t84918919, ___deviceId_1)); }
	inline String_t* get_deviceId_1() const { return ___deviceId_1; }
	inline String_t** get_address_of_deviceId_1() { return &___deviceId_1; }
	inline void set_deviceId_1(String_t* value)
	{
		___deviceId_1 = value;
		Il2CppCodeGenWriteBarrier((&___deviceId_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(ServerAvailableMessage_t84918919, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERAVAILABLEMESSAGE_T84918919_H
#ifndef STRINGARRAYMESSAGE_T4068327225_H
#define STRINGARRAYMESSAGE_T4068327225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.StringArrayMessage
struct  StringArrayMessage_t4068327225  : public MessageBase_t3584795631
{
public:
	// System.String[] Pixelplacement.StringArrayMessage::value
	StringU5BU5D_t1281789340* ___value_0;
	// System.String Pixelplacement.StringArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.StringArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringArrayMessage_t4068327225, ___value_0)); }
	inline StringU5BU5D_t1281789340* get_value_0() const { return ___value_0; }
	inline StringU5BU5D_t1281789340** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(StringU5BU5D_t1281789340* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(StringArrayMessage_t4068327225, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(StringArrayMessage_t4068327225, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGARRAYMESSAGE_T4068327225_H
#ifndef STRINGMESSAGE_T468245767_H
#define STRINGMESSAGE_T468245767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.StringMessage
struct  StringMessage_t468245767  : public MessageBase_t3584795631
{
public:
	// System.String Pixelplacement.StringMessage::value
	String_t* ___value_0;
	// System.String Pixelplacement.StringMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.StringMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringMessage_t468245767, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(StringMessage_t468245767, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(StringMessage_t468245767, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGMESSAGE_T468245767_H
#ifndef VECTOR2ARRAYMESSAGE_T2399511537_H
#define VECTOR2ARRAYMESSAGE_T2399511537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Vector2ArrayMessage
struct  Vector2ArrayMessage_t2399511537  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Vector2[] Pixelplacement.Vector2ArrayMessage::value
	Vector2U5BU5D_t1457185986* ___value_0;
	// System.String Pixelplacement.Vector2ArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.Vector2ArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector2ArrayMessage_t2399511537, ___value_0)); }
	inline Vector2U5BU5D_t1457185986* get_value_0() const { return ___value_0; }
	inline Vector2U5BU5D_t1457185986** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector2U5BU5D_t1457185986* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Vector2ArrayMessage_t2399511537, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Vector2ArrayMessage_t2399511537, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2ARRAYMESSAGE_T2399511537_H
#ifndef VECTOR3ARRAYMESSAGE_T728146880_H
#define VECTOR3ARRAYMESSAGE_T728146880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Vector3ArrayMessage
struct  Vector3ArrayMessage_t728146880  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Vector3[] Pixelplacement.Vector3ArrayMessage::value
	Vector3U5BU5D_t1718750761* ___value_0;
	// System.String Pixelplacement.Vector3ArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.Vector3ArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3ArrayMessage_t728146880, ___value_0)); }
	inline Vector3U5BU5D_t1718750761* get_value_0() const { return ___value_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3U5BU5D_t1718750761* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Vector3ArrayMessage_t728146880, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Vector3ArrayMessage_t728146880, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ARRAYMESSAGE_T728146880_H
#ifndef VECTOR4ARRAYMESSAGE_T1455924212_H
#define VECTOR4ARRAYMESSAGE_T1455924212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Vector4ArrayMessage
struct  Vector4ArrayMessage_t1455924212  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Vector4[] Pixelplacement.Vector4ArrayMessage::value
	Vector4U5BU5D_t934056436* ___value_0;
	// System.String Pixelplacement.Vector4ArrayMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.Vector4ArrayMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector4ArrayMessage_t1455924212, ___value_0)); }
	inline Vector4U5BU5D_t934056436* get_value_0() const { return ___value_0; }
	inline Vector4U5BU5D_t934056436** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector4U5BU5D_t934056436* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Vector4ArrayMessage_t1455924212, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Vector4ArrayMessage_t1455924212, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4ARRAYMESSAGE_T1455924212_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_1_T2941466200_H
#define UNITYEVENT_1_T2941466200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Pixelplacement.ColliderButton>
struct  UnityEvent_1_t2941466200  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2941466200, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2941466200_H
#ifndef UNITYEVENT_1_T978947469_H
#define UNITYEVENT_1_T978947469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t978947469  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t978947469, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T978947469_H
#ifndef UNITYEVENT_1_T1995296123_H
#define UNITYEVENT_1_T1995296123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>
struct  UnityEvent_1_t1995296123  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1995296123, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1995296123_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef ADDRESSFAM_T3748262023_H
#define ADDRESSFAM_T3748262023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ADDRESSFAM
struct  ADDRESSFAM_t3748262023 
{
public:
	// System.Int32 Pixelplacement.ADDRESSFAM::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ADDRESSFAM_t3748262023, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESSFAM_T3748262023_H
#ifndef BOOLEVENT_T798030280_H
#define BOOLEVENT_T798030280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.BoolEvent
struct  BoolEvent_t798030280  : public UnityEvent_1_t978947469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEVENT_T798030280_H
#ifndef METHOD_T1244881256_H
#define METHOD_T1244881256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Chooser/Method
struct  Method_t1244881256 
{
public:
	// System.Int32 Pixelplacement.Chooser/Method::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Method_t1244881256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T1244881256_H
#ifndef EASETYPE_T3131528705_H
#define EASETYPE_T3131528705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ColliderButton/EaseType
struct  EaseType_t3131528705 
{
public:
	// System.Int32 Pixelplacement.ColliderButton/EaseType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EaseType_t3131528705, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASETYPE_T3131528705_H
#ifndef COLLIDERBUTTONEVENT_T5483853_H
#define COLLIDERBUTTONEVENT_T5483853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ColliderButtonEvent
struct  ColliderButtonEvent_t5483853  : public UnityEvent_1_t2941466200
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERBUTTONEVENT_T5483853_H
#ifndef COLOR32MESSAGE_T1536619200_H
#define COLOR32MESSAGE_T1536619200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Color32Message
struct  Color32Message_t1536619200  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Color32 Pixelplacement.Color32Message::value
	Color32_t2600501292  ___value_0;
	// System.String Pixelplacement.Color32Message::id
	String_t* ___id_1;
	// System.String Pixelplacement.Color32Message::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Color32Message_t1536619200, ___value_0)); }
	inline Color32_t2600501292  get_value_0() const { return ___value_0; }
	inline Color32_t2600501292 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color32_t2600501292  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Color32Message_t1536619200, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Color32Message_t1536619200, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32MESSAGE_T1536619200_H
#ifndef COLORMESSAGE_T141388307_H
#define COLORMESSAGE_T141388307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ColorMessage
struct  ColorMessage_t141388307  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Color Pixelplacement.ColorMessage::value
	Color_t2555686324  ___value_0;
	// System.String Pixelplacement.ColorMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.ColorMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(ColorMessage_t141388307, ___value_0)); }
	inline Color_t2555686324  get_value_0() const { return ___value_0; }
	inline Color_t2555686324 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t2555686324  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ColorMessage_t141388307, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(ColorMessage_t141388307, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMESSAGE_T141388307_H
#ifndef GAMEOBJECTEVENT_T2867809866_H
#define GAMEOBJECTEVENT_T2867809866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.GameObjectEvent
struct  GameObjectEvent_t2867809866  : public UnityEvent_1_t1995296123
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEVENT_T2867809866_H
#ifndef MATRIX4X4MESSAGE_T1228836437_H
#define MATRIX4X4MESSAGE_T1228836437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Matrix4x4Message
struct  Matrix4x4Message_t1228836437  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Matrix4x4 Pixelplacement.Matrix4x4Message::value
	Matrix4x4_t1817901843  ___value_0;
	// System.String Pixelplacement.Matrix4x4Message::id
	String_t* ___id_1;
	// System.String Pixelplacement.Matrix4x4Message::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Matrix4x4Message_t1228836437, ___value_0)); }
	inline Matrix4x4_t1817901843  get_value_0() const { return ___value_0; }
	inline Matrix4x4_t1817901843 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Matrix4x4_t1817901843  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Matrix4x4Message_t1228836437, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Matrix4x4Message_t1228836437, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4MESSAGE_T1228836437_H
#ifndef NETWORKMSG_T3804742521_H
#define NETWORKMSG_T3804742521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.NetworkMsg
struct  NetworkMsg_t3804742521 
{
public:
	// System.Int32 Pixelplacement.NetworkMsg::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkMsg_t3804742521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMSG_T3804742521_H
#ifndef QUATERNIONMESSAGE_T2883663930_H
#define QUATERNIONMESSAGE_T2883663930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.QuaternionMessage
struct  QuaternionMessage_t2883663930  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Quaternion Pixelplacement.QuaternionMessage::value
	Quaternion_t2301928331  ___value_0;
	// System.String Pixelplacement.QuaternionMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.QuaternionMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(QuaternionMessage_t2883663930, ___value_0)); }
	inline Quaternion_t2301928331  get_value_0() const { return ___value_0; }
	inline Quaternion_t2301928331 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Quaternion_t2301928331  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(QuaternionMessage_t2883663930, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(QuaternionMessage_t2883663930, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONMESSAGE_T2883663930_H
#ifndef RECTMESSAGE_T1418415759_H
#define RECTMESSAGE_T1418415759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.RectMessage
struct  RectMessage_t1418415759  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Rect Pixelplacement.RectMessage::value
	Rect_t2360479859  ___value_0;
	// System.String Pixelplacement.RectMessage::id
	String_t* ___id_1;
	// System.String Pixelplacement.RectMessage::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RectMessage_t1418415759, ___value_0)); }
	inline Rect_t2360479859  get_value_0() const { return ___value_0; }
	inline Rect_t2360479859 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Rect_t2360479859  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(RectMessage_t1418415759, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(RectMessage_t1418415759, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTMESSAGE_T1418415759_H
#ifndef SPLINEDIRECTION_T243856657_H
#define SPLINEDIRECTION_T243856657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.SplineDirection
struct  SplineDirection_t243856657 
{
public:
	// System.Int32 Pixelplacement.SplineDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SplineDirection_t243856657, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEDIRECTION_T243856657_H
#ifndef TANGENTMODE_T1256149096_H
#define TANGENTMODE_T1256149096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TangentMode
struct  TangentMode_t1256149096 
{
public:
	// System.Int32 Pixelplacement.TangentMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TangentMode_t1256149096, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TANGENTMODE_T1256149096_H
#ifndef LOOPTYPE_T2942026839_H
#define LOOPTYPE_T2942026839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Tween/LoopType
struct  LoopType_t2942026839 
{
public:
	// System.Int32 Pixelplacement.Tween/LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t2942026839, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T2942026839_H
#ifndef TWEENSTATUS_T3213007367_H
#define TWEENSTATUS_T3213007367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Tween/TweenStatus
struct  TweenStatus_t3213007367 
{
public:
	// System.Int32 Pixelplacement.Tween/TweenStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenStatus_t3213007367, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSTATUS_T3213007367_H
#ifndef TWEENTYPE_T2719964487_H
#define TWEENTYPE_T2719964487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Tween/TweenType
struct  TweenType_t2719964487 
{
public:
	// System.Int32 Pixelplacement.Tween/TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t2719964487, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T2719964487_H
#ifndef VECTOR2MESSAGE_T1890921343_H
#define VECTOR2MESSAGE_T1890921343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Vector2Message
struct  Vector2Message_t1890921343  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Vector2 Pixelplacement.Vector2Message::value
	Vector2_t2156229523  ___value_0;
	// System.String Pixelplacement.Vector2Message::id
	String_t* ___id_1;
	// System.String Pixelplacement.Vector2Message::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector2Message_t1890921343, ___value_0)); }
	inline Vector2_t2156229523  get_value_0() const { return ___value_0; }
	inline Vector2_t2156229523 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector2_t2156229523  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Vector2Message_t1890921343, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Vector2Message_t1890921343, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2MESSAGE_T1890921343_H
#ifndef VECTOR3MESSAGE_T1962290047_H
#define VECTOR3MESSAGE_T1962290047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Vector3Message
struct  Vector3Message_t1962290047  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Vector3 Pixelplacement.Vector3Message::value
	Vector3_t3722313464  ___value_0;
	// System.String Pixelplacement.Vector3Message::id
	String_t* ___id_1;
	// System.String Pixelplacement.Vector3Message::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3Message_t1962290047, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Vector3Message_t1962290047, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Vector3Message_t1962290047, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3MESSAGE_T1962290047_H
#ifndef VECTOR4MESSAGE_T1462709119_H
#define VECTOR4MESSAGE_T1462709119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Vector4Message
struct  Vector4Message_t1462709119  : public MessageBase_t3584795631
{
public:
	// UnityEngine.Vector4 Pixelplacement.Vector4Message::value
	Vector4_t3319028937  ___value_0;
	// System.String Pixelplacement.Vector4Message::id
	String_t* ___id_1;
	// System.String Pixelplacement.Vector4Message::fromIp
	String_t* ___fromIp_2;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector4Message_t1462709119, ___value_0)); }
	inline Vector4_t3319028937  get_value_0() const { return ___value_0; }
	inline Vector4_t3319028937 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector4_t3319028937  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Vector4Message_t1462709119, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_fromIp_2() { return static_cast<int32_t>(offsetof(Vector4Message_t1462709119, ___fromIp_2)); }
	inline String_t* get_fromIp_2() const { return ___fromIp_2; }
	inline String_t** get_address_of_fromIp_2() { return &___fromIp_2; }
	inline void set_fromIp_2(String_t* value)
	{
		___fromIp_2 = value;
		Il2CppCodeGenWriteBarrier((&___fromIp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4MESSAGE_T1462709119_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef QOSTYPE_T3566496866_H
#define QOSTYPE_T3566496866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.QosType
struct  QosType_t3566496866 
{
public:
	// System.Int32 UnityEngine.Networking.QosType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QosType_t3566496866, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QOSTYPE_T3566496866_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef TWEENBASE_T3695616892_H
#define TWEENBASE_T3695616892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.TweenBase
struct  TweenBase_t3695616892  : public RuntimeObject
{
public:
	// System.Int32 Pixelplacement.TweenSystem.TweenBase::targetInstanceID
	int32_t ___targetInstanceID_0;
	// Pixelplacement.Tween/TweenType Pixelplacement.TweenSystem.TweenBase::tweenType
	int32_t ___tweenType_1;
	// Pixelplacement.Tween/TweenStatus Pixelplacement.TweenSystem.TweenBase::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_2;
	// System.Single Pixelplacement.TweenSystem.TweenBase::<Duration>k__BackingField
	float ___U3CDurationU3Ek__BackingField_3;
	// UnityEngine.AnimationCurve Pixelplacement.TweenSystem.TweenBase::<Curve>k__BackingField
	AnimationCurve_t3046754366 * ___U3CCurveU3Ek__BackingField_4;
	// UnityEngine.Keyframe[] Pixelplacement.TweenSystem.TweenBase::<CurveKeys>k__BackingField
	KeyframeU5BU5D_t1068524471* ___U3CCurveKeysU3Ek__BackingField_5;
	// System.Boolean Pixelplacement.TweenSystem.TweenBase::<ObeyTimescale>k__BackingField
	bool ___U3CObeyTimescaleU3Ek__BackingField_6;
	// System.Action Pixelplacement.TweenSystem.TweenBase::<StartCallback>k__BackingField
	Action_t1264377477 * ___U3CStartCallbackU3Ek__BackingField_7;
	// System.Action Pixelplacement.TweenSystem.TweenBase::<CompleteCallback>k__BackingField
	Action_t1264377477 * ___U3CCompleteCallbackU3Ek__BackingField_8;
	// System.Single Pixelplacement.TweenSystem.TweenBase::<Delay>k__BackingField
	float ___U3CDelayU3Ek__BackingField_9;
	// Pixelplacement.Tween/LoopType Pixelplacement.TweenSystem.TweenBase::<LoopType>k__BackingField
	int32_t ___U3CLoopTypeU3Ek__BackingField_10;
	// System.Single Pixelplacement.TweenSystem.TweenBase::<Percentage>k__BackingField
	float ___U3CPercentageU3Ek__BackingField_11;
	// System.Single Pixelplacement.TweenSystem.TweenBase::elapsedTime
	float ___elapsedTime_12;

public:
	inline static int32_t get_offset_of_targetInstanceID_0() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___targetInstanceID_0)); }
	inline int32_t get_targetInstanceID_0() const { return ___targetInstanceID_0; }
	inline int32_t* get_address_of_targetInstanceID_0() { return &___targetInstanceID_0; }
	inline void set_targetInstanceID_0(int32_t value)
	{
		___targetInstanceID_0 = value;
	}

	inline static int32_t get_offset_of_tweenType_1() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___tweenType_1)); }
	inline int32_t get_tweenType_1() const { return ___tweenType_1; }
	inline int32_t* get_address_of_tweenType_1() { return &___tweenType_1; }
	inline void set_tweenType_1(int32_t value)
	{
		___tweenType_1 = value;
	}

	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CStatusU3Ek__BackingField_2)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_2() const { return ___U3CStatusU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_2() { return &___U3CStatusU3Ek__BackingField_2; }
	inline void set_U3CStatusU3Ek__BackingField_2(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDurationU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CDurationU3Ek__BackingField_3)); }
	inline float get_U3CDurationU3Ek__BackingField_3() const { return ___U3CDurationU3Ek__BackingField_3; }
	inline float* get_address_of_U3CDurationU3Ek__BackingField_3() { return &___U3CDurationU3Ek__BackingField_3; }
	inline void set_U3CDurationU3Ek__BackingField_3(float value)
	{
		___U3CDurationU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCurveU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CCurveU3Ek__BackingField_4)); }
	inline AnimationCurve_t3046754366 * get_U3CCurveU3Ek__BackingField_4() const { return ___U3CCurveU3Ek__BackingField_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_U3CCurveU3Ek__BackingField_4() { return &___U3CCurveU3Ek__BackingField_4; }
	inline void set_U3CCurveU3Ek__BackingField_4(AnimationCurve_t3046754366 * value)
	{
		___U3CCurveU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurveU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCurveKeysU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CCurveKeysU3Ek__BackingField_5)); }
	inline KeyframeU5BU5D_t1068524471* get_U3CCurveKeysU3Ek__BackingField_5() const { return ___U3CCurveKeysU3Ek__BackingField_5; }
	inline KeyframeU5BU5D_t1068524471** get_address_of_U3CCurveKeysU3Ek__BackingField_5() { return &___U3CCurveKeysU3Ek__BackingField_5; }
	inline void set_U3CCurveKeysU3Ek__BackingField_5(KeyframeU5BU5D_t1068524471* value)
	{
		___U3CCurveKeysU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurveKeysU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CObeyTimescaleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CObeyTimescaleU3Ek__BackingField_6)); }
	inline bool get_U3CObeyTimescaleU3Ek__BackingField_6() const { return ___U3CObeyTimescaleU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CObeyTimescaleU3Ek__BackingField_6() { return &___U3CObeyTimescaleU3Ek__BackingField_6; }
	inline void set_U3CObeyTimescaleU3Ek__BackingField_6(bool value)
	{
		___U3CObeyTimescaleU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CStartCallbackU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CStartCallbackU3Ek__BackingField_7)); }
	inline Action_t1264377477 * get_U3CStartCallbackU3Ek__BackingField_7() const { return ___U3CStartCallbackU3Ek__BackingField_7; }
	inline Action_t1264377477 ** get_address_of_U3CStartCallbackU3Ek__BackingField_7() { return &___U3CStartCallbackU3Ek__BackingField_7; }
	inline void set_U3CStartCallbackU3Ek__BackingField_7(Action_t1264377477 * value)
	{
		___U3CStartCallbackU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartCallbackU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CCompleteCallbackU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CCompleteCallbackU3Ek__BackingField_8)); }
	inline Action_t1264377477 * get_U3CCompleteCallbackU3Ek__BackingField_8() const { return ___U3CCompleteCallbackU3Ek__BackingField_8; }
	inline Action_t1264377477 ** get_address_of_U3CCompleteCallbackU3Ek__BackingField_8() { return &___U3CCompleteCallbackU3Ek__BackingField_8; }
	inline void set_U3CCompleteCallbackU3Ek__BackingField_8(Action_t1264377477 * value)
	{
		___U3CCompleteCallbackU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCompleteCallbackU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CDelayU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CDelayU3Ek__BackingField_9)); }
	inline float get_U3CDelayU3Ek__BackingField_9() const { return ___U3CDelayU3Ek__BackingField_9; }
	inline float* get_address_of_U3CDelayU3Ek__BackingField_9() { return &___U3CDelayU3Ek__BackingField_9; }
	inline void set_U3CDelayU3Ek__BackingField_9(float value)
	{
		___U3CDelayU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CLoopTypeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CLoopTypeU3Ek__BackingField_10)); }
	inline int32_t get_U3CLoopTypeU3Ek__BackingField_10() const { return ___U3CLoopTypeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CLoopTypeU3Ek__BackingField_10() { return &___U3CLoopTypeU3Ek__BackingField_10; }
	inline void set_U3CLoopTypeU3Ek__BackingField_10(int32_t value)
	{
		___U3CLoopTypeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CPercentageU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___U3CPercentageU3Ek__BackingField_11)); }
	inline float get_U3CPercentageU3Ek__BackingField_11() const { return ___U3CPercentageU3Ek__BackingField_11; }
	inline float* get_address_of_U3CPercentageU3Ek__BackingField_11() { return &___U3CPercentageU3Ek__BackingField_11; }
	inline void set_U3CPercentageU3Ek__BackingField_11(float value)
	{
		___U3CPercentageU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_elapsedTime_12() { return static_cast<int32_t>(offsetof(TweenBase_t3695616892, ___elapsedTime_12)); }
	inline float get_elapsedTime_12() const { return ___elapsedTime_12; }
	inline float* get_address_of_elapsedTime_12() { return &___elapsedTime_12; }
	inline void set_elapsedTime_12(float value)
	{
		___elapsedTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENBASE_T3695616892_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ANCHOREDPOSITION_T3182978813_H
#define ANCHOREDPOSITION_T3182978813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.AnchoredPosition
struct  AnchoredPosition_t3182978813  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector2 Pixelplacement.TweenSystem.AnchoredPosition::<EndValue>k__BackingField
	Vector2_t2156229523  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.RectTransform Pixelplacement.TweenSystem.AnchoredPosition::_target
	RectTransform_t3704657025 * ____target_14;
	// UnityEngine.Vector2 Pixelplacement.TweenSystem.AnchoredPosition::_start
	Vector2_t2156229523  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AnchoredPosition_t3182978813, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(AnchoredPosition_t3182978813, ____target_14)); }
	inline RectTransform_t3704657025 * get__target_14() const { return ____target_14; }
	inline RectTransform_t3704657025 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(RectTransform_t3704657025 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(AnchoredPosition_t3182978813, ____start_15)); }
	inline Vector2_t2156229523  get__start_15() const { return ____start_15; }
	inline Vector2_t2156229523 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector2_t2156229523  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOREDPOSITION_T3182978813_H
#ifndef CAMERABACKGROUNDCOLOR_T3950989133_H
#define CAMERABACKGROUNDCOLOR_T3950989133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.CameraBackgroundColor
struct  CameraBackgroundColor_t3950989133  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.CameraBackgroundColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Camera Pixelplacement.TweenSystem.CameraBackgroundColor::_target
	Camera_t4157153871 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.CameraBackgroundColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CameraBackgroundColor_t3950989133, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(CameraBackgroundColor_t3950989133, ____target_14)); }
	inline Camera_t4157153871 * get__target_14() const { return ____target_14; }
	inline Camera_t4157153871 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Camera_t4157153871 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(CameraBackgroundColor_t3950989133, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERABACKGROUNDCOLOR_T3950989133_H
#ifndef CANVASGROUPALPHA_T882788859_H
#define CANVASGROUPALPHA_T882788859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.CanvasGroupAlpha
struct  CanvasGroupAlpha_t882788859  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.CanvasGroupAlpha::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.CanvasGroup Pixelplacement.TweenSystem.CanvasGroupAlpha::_target
	CanvasGroup_t4083511760 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.CanvasGroupAlpha::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CanvasGroupAlpha_t882788859, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(CanvasGroupAlpha_t882788859, ____target_14)); }
	inline CanvasGroup_t4083511760 * get__target_14() const { return ____target_14; }
	inline CanvasGroup_t4083511760 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(CanvasGroup_t4083511760 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(CanvasGroupAlpha_t882788859, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUPALPHA_T882788859_H
#ifndef FIELDOFVIEW_T765943863_H
#define FIELDOFVIEW_T765943863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.FieldOfView
struct  FieldOfView_t765943863  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.FieldOfView::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Camera Pixelplacement.TweenSystem.FieldOfView::_target
	Camera_t4157153871 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.FieldOfView::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FieldOfView_t765943863, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(FieldOfView_t765943863, ____target_14)); }
	inline Camera_t4157153871 * get__target_14() const { return ____target_14; }
	inline Camera_t4157153871 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Camera_t4157153871 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(FieldOfView_t765943863, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDOFVIEW_T765943863_H
#ifndef IMAGECOLOR_T4158781697_H
#define IMAGECOLOR_T4158781697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ImageColor
struct  ImageColor_t4158781697  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.ImageColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.UI.Image Pixelplacement.TweenSystem.ImageColor::_target
	Image_t2670269651 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.ImageColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ImageColor_t4158781697, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(ImageColor_t4158781697, ____target_14)); }
	inline Image_t2670269651 * get__target_14() const { return ____target_14; }
	inline Image_t2670269651 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Image_t2670269651 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ImageColor_t4158781697, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGECOLOR_T4158781697_H
#ifndef LIGHTCOLOR_T419143490_H
#define LIGHTCOLOR_T419143490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.LightColor
struct  LightColor_t419143490  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.LightColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Light Pixelplacement.TweenSystem.LightColor::_target
	Light_t3756812086 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.LightColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LightColor_t419143490, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(LightColor_t419143490, ____target_14)); }
	inline Light_t3756812086 * get__target_14() const { return ____target_14; }
	inline Light_t3756812086 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Light_t3756812086 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(LightColor_t419143490, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTCOLOR_T419143490_H
#ifndef LIGHTINTENSITY_T3710475180_H
#define LIGHTINTENSITY_T3710475180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.LightIntensity
struct  LightIntensity_t3710475180  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.LightIntensity::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Light Pixelplacement.TweenSystem.LightIntensity::_target
	Light_t3756812086 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.LightIntensity::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LightIntensity_t3710475180, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(LightIntensity_t3710475180, ____target_14)); }
	inline Light_t3756812086 * get__target_14() const { return ____target_14; }
	inline Light_t3756812086 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Light_t3756812086 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(LightIntensity_t3710475180, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTINTENSITY_T3710475180_H
#ifndef LIGHTRANGE_T74221972_H
#define LIGHTRANGE_T74221972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.LightRange
struct  LightRange_t74221972  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.LightRange::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Light Pixelplacement.TweenSystem.LightRange::_target
	Light_t3756812086 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.LightRange::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LightRange_t74221972, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(LightRange_t74221972, ____target_14)); }
	inline Light_t3756812086 * get__target_14() const { return ____target_14; }
	inline Light_t3756812086 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Light_t3756812086 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(LightRange_t74221972, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTRANGE_T74221972_H
#ifndef LOCALPOSITION_T2591771983_H
#define LOCALPOSITION_T2591771983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.LocalPosition
struct  LocalPosition_t2591771983  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.LocalPosition::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.LocalPosition::_target
	Transform_t3600365921 * ____target_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.LocalPosition::_start
	Vector3_t3722313464  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LocalPosition_t2591771983, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(LocalPosition_t2591771983, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(LocalPosition_t2591771983, ____start_15)); }
	inline Vector3_t3722313464  get__start_15() const { return ____start_15; }
	inline Vector3_t3722313464 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector3_t3722313464  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALPOSITION_T2591771983_H
#ifndef LOCALROTATION_T2615760140_H
#define LOCALROTATION_T2615760140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.LocalRotation
struct  LocalRotation_t2615760140  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.LocalRotation::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.LocalRotation::_target
	Transform_t3600365921 * ____target_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.LocalRotation::_start
	Vector3_t3722313464  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LocalRotation_t2615760140, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(LocalRotation_t2615760140, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(LocalRotation_t2615760140, ____start_15)); }
	inline Vector3_t3722313464  get__start_15() const { return ____start_15; }
	inline Vector3_t3722313464 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector3_t3722313464  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALROTATION_T2615760140_H
#ifndef LOCALSCALE_T1605085753_H
#define LOCALSCALE_T1605085753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.LocalScale
struct  LocalScale_t1605085753  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.LocalScale::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.LocalScale::_target
	Transform_t3600365921 * ____target_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.LocalScale::_start
	Vector3_t3722313464  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(LocalScale_t1605085753, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(LocalScale_t1605085753, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(LocalScale_t1605085753, ____start_15)); }
	inline Vector3_t3722313464  get__start_15() const { return ____start_15; }
	inline Vector3_t3722313464 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector3_t3722313464  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALSCALE_T1605085753_H
#ifndef PANSTEREO_T2313910393_H
#define PANSTEREO_T2313910393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.PanStereo
struct  PanStereo_t2313910393  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.PanStereo::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.AudioSource Pixelplacement.TweenSystem.PanStereo::_target
	AudioSource_t3935305588 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.PanStereo::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PanStereo_t2313910393, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(PanStereo_t2313910393, ____target_14)); }
	inline AudioSource_t3935305588 * get__target_14() const { return ____target_14; }
	inline AudioSource_t3935305588 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(AudioSource_t3935305588 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(PanStereo_t2313910393, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANSTEREO_T2313910393_H
#ifndef PITCH_T853067966_H
#define PITCH_T853067966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.Pitch
struct  Pitch_t853067966  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.Pitch::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.AudioSource Pixelplacement.TweenSystem.Pitch::_target
	AudioSource_t3935305588 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.Pitch::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Pitch_t853067966, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(Pitch_t853067966, ____target_14)); }
	inline AudioSource_t3935305588 * get__target_14() const { return ____target_14; }
	inline AudioSource_t3935305588 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(AudioSource_t3935305588 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(Pitch_t853067966, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PITCH_T853067966_H
#ifndef POSITION_T4027175230_H
#define POSITION_T4027175230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.Position
struct  Position_t4027175230  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.Position::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.Position::_target
	Transform_t3600365921 * ____target_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.Position::_start
	Vector3_t3722313464  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Position_t4027175230, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(Position_t4027175230, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(Position_t4027175230, ____start_15)); }
	inline Vector3_t3722313464  get__start_15() const { return ____start_15; }
	inline Vector3_t3722313464 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector3_t3722313464  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T4027175230_H
#ifndef RAWIMAGECOLOR_T78192334_H
#define RAWIMAGECOLOR_T78192334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.RawImageColor
struct  RawImageColor_t78192334  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.RawImageColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.UI.RawImage Pixelplacement.TweenSystem.RawImageColor::_target
	RawImage_t3182918964 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.RawImageColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RawImageColor_t78192334, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(RawImageColor_t78192334, ____target_14)); }
	inline RawImage_t3182918964 * get__target_14() const { return ____target_14; }
	inline RawImage_t3182918964 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(RawImage_t3182918964 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(RawImageColor_t78192334, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGECOLOR_T78192334_H
#ifndef ROTATE_T3152225749_H
#define ROTATE_T3152225749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.Rotate
struct  Rotate_t3152225749  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.Rotate::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.Rotate::_target
	Transform_t3600365921 * ____target_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.Rotate::_start
	Vector3_t3722313464  ____start_15;
	// UnityEngine.Space Pixelplacement.TweenSystem.Rotate::_space
	int32_t ____space_16;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.Rotate::_previous
	Vector3_t3722313464  ____previous_17;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Rotate_t3152225749, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(Rotate_t3152225749, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(Rotate_t3152225749, ____start_15)); }
	inline Vector3_t3722313464  get__start_15() const { return ____start_15; }
	inline Vector3_t3722313464 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector3_t3722313464  value)
	{
		____start_15 = value;
	}

	inline static int32_t get_offset_of__space_16() { return static_cast<int32_t>(offsetof(Rotate_t3152225749, ____space_16)); }
	inline int32_t get__space_16() const { return ____space_16; }
	inline int32_t* get_address_of__space_16() { return &____space_16; }
	inline void set__space_16(int32_t value)
	{
		____space_16 = value;
	}

	inline static int32_t get_offset_of__previous_17() { return static_cast<int32_t>(offsetof(Rotate_t3152225749, ____previous_17)); }
	inline Vector3_t3722313464  get__previous_17() const { return ____previous_17; }
	inline Vector3_t3722313464 * get_address_of__previous_17() { return &____previous_17; }
	inline void set__previous_17(Vector3_t3722313464  value)
	{
		____previous_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T3152225749_H
#ifndef ROTATION_T901105510_H
#define ROTATION_T901105510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.Rotation
struct  Rotation_t901105510  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.Rotation::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.Rotation::_target
	Transform_t3600365921 * ____target_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.Rotation::_start
	Vector3_t3722313464  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Rotation_t901105510, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(Rotation_t901105510, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(Rotation_t901105510, ____start_15)); }
	inline Vector3_t3722313464  get__start_15() const { return ____start_15; }
	inline Vector3_t3722313464 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector3_t3722313464  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATION_T901105510_H
#ifndef SHADERCOLOR_T2333276916_H
#define SHADERCOLOR_T2333276916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ShaderColor
struct  ShaderColor_t2333276916  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.ShaderColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Material Pixelplacement.TweenSystem.ShaderColor::_target
	Material_t340375123 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.ShaderColor::_start
	Color_t2555686324  ____start_15;
	// System.String Pixelplacement.TweenSystem.ShaderColor::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ShaderColor_t2333276916, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(ShaderColor_t2333276916, ____target_14)); }
	inline Material_t340375123 * get__target_14() const { return ____target_14; }
	inline Material_t340375123 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Material_t340375123 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ShaderColor_t2333276916, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(ShaderColor_t2333276916, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERCOLOR_T2333276916_H
#ifndef SHADERFLOAT_T803773729_H
#define SHADERFLOAT_T803773729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ShaderFloat
struct  ShaderFloat_t803773729  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.ShaderFloat::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Material Pixelplacement.TweenSystem.ShaderFloat::_target
	Material_t340375123 * ____target_14;
	// System.Single Pixelplacement.TweenSystem.ShaderFloat::_start
	float ____start_15;
	// System.String Pixelplacement.TweenSystem.ShaderFloat::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ShaderFloat_t803773729, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(ShaderFloat_t803773729, ____target_14)); }
	inline Material_t340375123 * get__target_14() const { return ____target_14; }
	inline Material_t340375123 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Material_t340375123 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ShaderFloat_t803773729, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(ShaderFloat_t803773729, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERFLOAT_T803773729_H
#ifndef SHADERINT_T327647313_H
#define SHADERINT_T327647313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ShaderInt
struct  ShaderInt_t327647313  : public TweenBase_t3695616892
{
public:
	// System.Int32 Pixelplacement.TweenSystem.ShaderInt::<EndValue>k__BackingField
	int32_t ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Material Pixelplacement.TweenSystem.ShaderInt::_target
	Material_t340375123 * ____target_14;
	// System.Int32 Pixelplacement.TweenSystem.ShaderInt::_start
	int32_t ____start_15;
	// System.String Pixelplacement.TweenSystem.ShaderInt::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ShaderInt_t327647313, ___U3CEndValueU3Ek__BackingField_13)); }
	inline int32_t get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(int32_t value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(ShaderInt_t327647313, ____target_14)); }
	inline Material_t340375123 * get__target_14() const { return ____target_14; }
	inline Material_t340375123 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Material_t340375123 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ShaderInt_t327647313, ____start_15)); }
	inline int32_t get__start_15() const { return ____start_15; }
	inline int32_t* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(int32_t value)
	{
		____start_15 = value;
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(ShaderInt_t327647313, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERINT_T327647313_H
#ifndef SHADERVECTOR_T1632654171_H
#define SHADERVECTOR_T1632654171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ShaderVector
struct  ShaderVector_t1632654171  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector4 Pixelplacement.TweenSystem.ShaderVector::<EndValue>k__BackingField
	Vector4_t3319028937  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Material Pixelplacement.TweenSystem.ShaderVector::_target
	Material_t340375123 * ____target_14;
	// UnityEngine.Vector4 Pixelplacement.TweenSystem.ShaderVector::_start
	Vector4_t3319028937  ____start_15;
	// System.String Pixelplacement.TweenSystem.ShaderVector::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ShaderVector_t1632654171, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector4_t3319028937  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector4_t3319028937 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector4_t3319028937  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(ShaderVector_t1632654171, ____target_14)); }
	inline Material_t340375123 * get__target_14() const { return ____target_14; }
	inline Material_t340375123 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Material_t340375123 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ShaderVector_t1632654171, ____start_15)); }
	inline Vector4_t3319028937  get__start_15() const { return ____start_15; }
	inline Vector4_t3319028937 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector4_t3319028937  value)
	{
		____start_15 = value;
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(ShaderVector_t1632654171, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERVECTOR_T1632654171_H
#ifndef SHAKEPOSITION_T2037188575_H
#define SHAKEPOSITION_T2037188575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ShakePosition
struct  ShakePosition_t2037188575  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.ShakePosition::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.ShakePosition::_target
	Transform_t3600365921 * ____target_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.ShakePosition::_initialPosition
	Vector3_t3722313464  ____initialPosition_15;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.ShakePosition::_intensity
	Vector3_t3722313464  ____intensity_16;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ShakePosition_t2037188575, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(ShakePosition_t2037188575, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__initialPosition_15() { return static_cast<int32_t>(offsetof(ShakePosition_t2037188575, ____initialPosition_15)); }
	inline Vector3_t3722313464  get__initialPosition_15() const { return ____initialPosition_15; }
	inline Vector3_t3722313464 * get_address_of__initialPosition_15() { return &____initialPosition_15; }
	inline void set__initialPosition_15(Vector3_t3722313464  value)
	{
		____initialPosition_15 = value;
	}

	inline static int32_t get_offset_of__intensity_16() { return static_cast<int32_t>(offsetof(ShakePosition_t2037188575, ____intensity_16)); }
	inline Vector3_t3722313464  get__intensity_16() const { return ____intensity_16; }
	inline Vector3_t3722313464 * get_address_of__intensity_16() { return &____intensity_16; }
	inline void set__intensity_16(Vector3_t3722313464  value)
	{
		____intensity_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAKEPOSITION_T2037188575_H
#ifndef SIZE_T2613871712_H
#define SIZE_T2613871712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.Size
struct  Size_t2613871712  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector2 Pixelplacement.TweenSystem.Size::<EndValue>k__BackingField
	Vector2_t2156229523  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.RectTransform Pixelplacement.TweenSystem.Size::_target
	RectTransform_t3704657025 * ____target_14;
	// UnityEngine.Vector2 Pixelplacement.TweenSystem.Size::_start
	Vector2_t2156229523  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Size_t2613871712, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(Size_t2613871712, ____target_14)); }
	inline RectTransform_t3704657025 * get__target_14() const { return ____target_14; }
	inline RectTransform_t3704657025 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(RectTransform_t3704657025 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(Size_t2613871712, ____start_15)); }
	inline Vector2_t2156229523  get__start_15() const { return ____start_15; }
	inline Vector2_t2156229523 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector2_t2156229523  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZE_T2613871712_H
#ifndef SPLINEPERCENTAGE_T2841806011_H
#define SPLINEPERCENTAGE_T2841806011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.SplinePercentage
struct  SplinePercentage_t2841806011  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.SplinePercentage::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.Transform Pixelplacement.TweenSystem.SplinePercentage::_target
	Transform_t3600365921 * ____target_14;
	// Pixelplacement.Spline Pixelplacement.TweenSystem.SplinePercentage::_spline
	Spline_t1246320346 * ____spline_15;
	// System.Single Pixelplacement.TweenSystem.SplinePercentage::_startPercentage
	float ____startPercentage_16;
	// System.Boolean Pixelplacement.TweenSystem.SplinePercentage::_faceDirection
	bool ____faceDirection_17;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SplinePercentage_t2841806011, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(SplinePercentage_t2841806011, ____target_14)); }
	inline Transform_t3600365921 * get__target_14() const { return ____target_14; }
	inline Transform_t3600365921 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Transform_t3600365921 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__spline_15() { return static_cast<int32_t>(offsetof(SplinePercentage_t2841806011, ____spline_15)); }
	inline Spline_t1246320346 * get__spline_15() const { return ____spline_15; }
	inline Spline_t1246320346 ** get_address_of__spline_15() { return &____spline_15; }
	inline void set__spline_15(Spline_t1246320346 * value)
	{
		____spline_15 = value;
		Il2CppCodeGenWriteBarrier((&____spline_15), value);
	}

	inline static int32_t get_offset_of__startPercentage_16() { return static_cast<int32_t>(offsetof(SplinePercentage_t2841806011, ____startPercentage_16)); }
	inline float get__startPercentage_16() const { return ____startPercentage_16; }
	inline float* get_address_of__startPercentage_16() { return &____startPercentage_16; }
	inline void set__startPercentage_16(float value)
	{
		____startPercentage_16 = value;
	}

	inline static int32_t get_offset_of__faceDirection_17() { return static_cast<int32_t>(offsetof(SplinePercentage_t2841806011, ____faceDirection_17)); }
	inline bool get__faceDirection_17() const { return ____faceDirection_17; }
	inline bool* get_address_of__faceDirection_17() { return &____faceDirection_17; }
	inline void set__faceDirection_17(bool value)
	{
		____faceDirection_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEPERCENTAGE_T2841806011_H
#ifndef SPRITERENDERERCOLOR_T154867011_H
#define SPRITERENDERERCOLOR_T154867011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.SpriteRendererColor
struct  SpriteRendererColor_t154867011  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.SpriteRendererColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.SpriteRenderer Pixelplacement.TweenSystem.SpriteRendererColor::_target
	SpriteRenderer_t3235626157 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.SpriteRendererColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SpriteRendererColor_t154867011, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(SpriteRendererColor_t154867011, ____target_14)); }
	inline SpriteRenderer_t3235626157 * get__target_14() const { return ____target_14; }
	inline SpriteRenderer_t3235626157 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(SpriteRenderer_t3235626157 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(SpriteRendererColor_t154867011, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERERCOLOR_T154867011_H
#ifndef TEXTCOLOR_T3467464308_H
#define TEXTCOLOR_T3467464308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.TextColor
struct  TextColor_t3467464308  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.TextColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.UI.Text Pixelplacement.TweenSystem.TextColor::_target
	Text_t1901882714 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.TextColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TextColor_t3467464308, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(TextColor_t3467464308, ____target_14)); }
	inline Text_t1901882714 * get__target_14() const { return ____target_14; }
	inline Text_t1901882714 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(Text_t1901882714 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(TextColor_t3467464308, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCOLOR_T3467464308_H
#ifndef TEXTMESHCOLOR_T3131405086_H
#define TEXTMESHCOLOR_T3131405086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.TextMeshColor
struct  TextMeshColor_t3131405086  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.TextMeshColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// UnityEngine.TextMesh Pixelplacement.TweenSystem.TextMeshColor::_target
	TextMesh_t1536577757 * ____target_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.TextMeshColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(TextMeshColor_t3131405086, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__target_14() { return static_cast<int32_t>(offsetof(TextMeshColor_t3131405086, ____target_14)); }
	inline TextMesh_t1536577757 * get__target_14() const { return ____target_14; }
	inline TextMesh_t1536577757 ** get_address_of__target_14() { return &____target_14; }
	inline void set__target_14(TextMesh_t1536577757 * value)
	{
		____target_14 = value;
		Il2CppCodeGenWriteBarrier((&____target_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(TextMeshColor_t3131405086, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHCOLOR_T3131405086_H
#ifndef VALUECOLOR_T3781550707_H
#define VALUECOLOR_T3781550707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ValueColor
struct  ValueColor_t3781550707  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Color Pixelplacement.TweenSystem.ValueColor::<EndValue>k__BackingField
	Color_t2555686324  ___U3CEndValueU3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Color> Pixelplacement.TweenSystem.ValueColor::_valueUpdatedCallback
	Action_1_t2728153919 * ____valueUpdatedCallback_14;
	// UnityEngine.Color Pixelplacement.TweenSystem.ValueColor::_start
	Color_t2555686324  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ValueColor_t3781550707, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Color_t2555686324  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Color_t2555686324 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Color_t2555686324  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__valueUpdatedCallback_14() { return static_cast<int32_t>(offsetof(ValueColor_t3781550707, ____valueUpdatedCallback_14)); }
	inline Action_1_t2728153919 * get__valueUpdatedCallback_14() const { return ____valueUpdatedCallback_14; }
	inline Action_1_t2728153919 ** get_address_of__valueUpdatedCallback_14() { return &____valueUpdatedCallback_14; }
	inline void set__valueUpdatedCallback_14(Action_1_t2728153919 * value)
	{
		____valueUpdatedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&____valueUpdatedCallback_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ValueColor_t3781550707, ____start_15)); }
	inline Color_t2555686324  get__start_15() const { return ____start_15; }
	inline Color_t2555686324 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Color_t2555686324  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUECOLOR_T3781550707_H
#ifndef VALUEFLOAT_T265716779_H
#define VALUEFLOAT_T265716779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ValueFloat
struct  ValueFloat_t265716779  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.ValueFloat::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// System.Action`1<System.Single> Pixelplacement.TweenSystem.ValueFloat::_valueUpdatedCallback
	Action_1_t1569734369 * ____valueUpdatedCallback_14;
	// System.Single Pixelplacement.TweenSystem.ValueFloat::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ValueFloat_t265716779, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__valueUpdatedCallback_14() { return static_cast<int32_t>(offsetof(ValueFloat_t265716779, ____valueUpdatedCallback_14)); }
	inline Action_1_t1569734369 * get__valueUpdatedCallback_14() const { return ____valueUpdatedCallback_14; }
	inline Action_1_t1569734369 ** get_address_of__valueUpdatedCallback_14() { return &____valueUpdatedCallback_14; }
	inline void set__valueUpdatedCallback_14(Action_1_t1569734369 * value)
	{
		____valueUpdatedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&____valueUpdatedCallback_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ValueFloat_t265716779, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEFLOAT_T265716779_H
#ifndef VALUEINT_T3429980594_H
#define VALUEINT_T3429980594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ValueInt
struct  ValueInt_t3429980594  : public TweenBase_t3695616892
{
public:
	// System.Single Pixelplacement.TweenSystem.ValueInt::<EndValue>k__BackingField
	float ___U3CEndValueU3Ek__BackingField_13;
	// System.Action`1<System.Int32> Pixelplacement.TweenSystem.ValueInt::_valueUpdatedCallback
	Action_1_t3123413348 * ____valueUpdatedCallback_14;
	// System.Single Pixelplacement.TweenSystem.ValueInt::_start
	float ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ValueInt_t3429980594, ___U3CEndValueU3Ek__BackingField_13)); }
	inline float get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline float* get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(float value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__valueUpdatedCallback_14() { return static_cast<int32_t>(offsetof(ValueInt_t3429980594, ____valueUpdatedCallback_14)); }
	inline Action_1_t3123413348 * get__valueUpdatedCallback_14() const { return ____valueUpdatedCallback_14; }
	inline Action_1_t3123413348 ** get_address_of__valueUpdatedCallback_14() { return &____valueUpdatedCallback_14; }
	inline void set__valueUpdatedCallback_14(Action_1_t3123413348 * value)
	{
		____valueUpdatedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&____valueUpdatedCallback_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ValueInt_t3429980594, ____start_15)); }
	inline float get__start_15() const { return ____start_15; }
	inline float* get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(float value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEINT_T3429980594_H
#ifndef VALUERECT_T324067290_H
#define VALUERECT_T324067290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ValueRect
struct  ValueRect_t324067290  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Rect Pixelplacement.TweenSystem.ValueRect::<EndValue>k__BackingField
	Rect_t2360479859  ___U3CEndValueU3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Rect> Pixelplacement.TweenSystem.ValueRect::_valueUpdatedCallback
	Action_1_t2532947454 * ____valueUpdatedCallback_14;
	// UnityEngine.Rect Pixelplacement.TweenSystem.ValueRect::_start
	Rect_t2360479859  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ValueRect_t324067290, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Rect_t2360479859  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Rect_t2360479859 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Rect_t2360479859  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__valueUpdatedCallback_14() { return static_cast<int32_t>(offsetof(ValueRect_t324067290, ____valueUpdatedCallback_14)); }
	inline Action_1_t2532947454 * get__valueUpdatedCallback_14() const { return ____valueUpdatedCallback_14; }
	inline Action_1_t2532947454 ** get_address_of__valueUpdatedCallback_14() { return &____valueUpdatedCallback_14; }
	inline void set__valueUpdatedCallback_14(Action_1_t2532947454 * value)
	{
		____valueUpdatedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&____valueUpdatedCallback_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ValueRect_t324067290, ____start_15)); }
	inline Rect_t2360479859  get__start_15() const { return ____start_15; }
	inline Rect_t2360479859 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Rect_t2360479859  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUERECT_T324067290_H
#ifndef VALUEVECTOR2_T3152914_H
#define VALUEVECTOR2_T3152914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ValueVector2
struct  ValueVector2_t3152914  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector2 Pixelplacement.TweenSystem.ValueVector2::<EndValue>k__BackingField
	Vector2_t2156229523  ___U3CEndValueU3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Vector2> Pixelplacement.TweenSystem.ValueVector2::_valueUpdatedCallback
	Action_1_t2328697118 * ____valueUpdatedCallback_14;
	// UnityEngine.Vector2 Pixelplacement.TweenSystem.ValueVector2::_start
	Vector2_t2156229523  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ValueVector2_t3152914, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__valueUpdatedCallback_14() { return static_cast<int32_t>(offsetof(ValueVector2_t3152914, ____valueUpdatedCallback_14)); }
	inline Action_1_t2328697118 * get__valueUpdatedCallback_14() const { return ____valueUpdatedCallback_14; }
	inline Action_1_t2328697118 ** get_address_of__valueUpdatedCallback_14() { return &____valueUpdatedCallback_14; }
	inline void set__valueUpdatedCallback_14(Action_1_t2328697118 * value)
	{
		____valueUpdatedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&____valueUpdatedCallback_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ValueVector2_t3152914, ____start_15)); }
	inline Vector2_t2156229523  get__start_15() const { return ____start_15; }
	inline Vector2_t2156229523 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector2_t2156229523  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEVECTOR2_T3152914_H
#ifndef VALUEVECTOR3_T2732036269_H
#define VALUEVECTOR3_T2732036269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ValueVector3
struct  ValueVector3_t2732036269  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.ValueVector3::<EndValue>k__BackingField
	Vector3_t3722313464  ___U3CEndValueU3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Vector3> Pixelplacement.TweenSystem.ValueVector3::_valueUpdatedCallback
	Action_1_t3894781059 * ____valueUpdatedCallback_14;
	// UnityEngine.Vector3 Pixelplacement.TweenSystem.ValueVector3::_start
	Vector3_t3722313464  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ValueVector3_t2732036269, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector3_t3722313464  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector3_t3722313464 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector3_t3722313464  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__valueUpdatedCallback_14() { return static_cast<int32_t>(offsetof(ValueVector3_t2732036269, ____valueUpdatedCallback_14)); }
	inline Action_1_t3894781059 * get__valueUpdatedCallback_14() const { return ____valueUpdatedCallback_14; }
	inline Action_1_t3894781059 ** get_address_of__valueUpdatedCallback_14() { return &____valueUpdatedCallback_14; }
	inline void set__valueUpdatedCallback_14(Action_1_t3894781059 * value)
	{
		____valueUpdatedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&____valueUpdatedCallback_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ValueVector3_t2732036269, ____start_15)); }
	inline Vector3_t3722313464  get__start_15() const { return ____start_15; }
	inline Vector3_t3722313464 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector3_t3722313464  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEVECTOR3_T2732036269_H
#ifndef VALUEVECTOR4_T809721968_H
#define VALUEVECTOR4_T809721968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.ValueVector4
struct  ValueVector4_t809721968  : public TweenBase_t3695616892
{
public:
	// UnityEngine.Vector4 Pixelplacement.TweenSystem.ValueVector4::<EndValue>k__BackingField
	Vector4_t3319028937  ___U3CEndValueU3Ek__BackingField_13;
	// System.Action`1<UnityEngine.Vector4> Pixelplacement.TweenSystem.ValueVector4::_valueUpdatedCallback
	Action_1_t3491496532 * ____valueUpdatedCallback_14;
	// UnityEngine.Vector4 Pixelplacement.TweenSystem.ValueVector4::_start
	Vector4_t3319028937  ____start_15;

public:
	inline static int32_t get_offset_of_U3CEndValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ValueVector4_t809721968, ___U3CEndValueU3Ek__BackingField_13)); }
	inline Vector4_t3319028937  get_U3CEndValueU3Ek__BackingField_13() const { return ___U3CEndValueU3Ek__BackingField_13; }
	inline Vector4_t3319028937 * get_address_of_U3CEndValueU3Ek__BackingField_13() { return &___U3CEndValueU3Ek__BackingField_13; }
	inline void set_U3CEndValueU3Ek__BackingField_13(Vector4_t3319028937  value)
	{
		___U3CEndValueU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__valueUpdatedCallback_14() { return static_cast<int32_t>(offsetof(ValueVector4_t809721968, ____valueUpdatedCallback_14)); }
	inline Action_1_t3491496532 * get__valueUpdatedCallback_14() const { return ____valueUpdatedCallback_14; }
	inline Action_1_t3491496532 ** get_address_of__valueUpdatedCallback_14() { return &____valueUpdatedCallback_14; }
	inline void set__valueUpdatedCallback_14(Action_1_t3491496532 * value)
	{
		____valueUpdatedCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&____valueUpdatedCallback_14), value);
	}

	inline static int32_t get_offset_of__start_15() { return static_cast<int32_t>(offsetof(ValueVector4_t809721968, ____start_15)); }
	inline Vector4_t3319028937  get__start_15() const { return ____start_15; }
	inline Vector4_t3319028937 * get_address_of__start_15() { return &____start_15; }
	inline void set__start_15(Vector4_t3319028937  value)
	{
		____start_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEVECTOR4_T809721968_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef COLLIDERBUTTONINTERACTION_T392971596_H
#define COLLIDERBUTTONINTERACTION_T392971596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColliderButtonInteraction
struct  ColliderButtonInteraction_t392971596  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERBUTTONINTERACTION_T392971596_H
#ifndef COLLIDERBUTTONSELECTOR_T3248638654_H
#define COLLIDERBUTTONSELECTOR_T3248638654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColliderButtonSelector
struct  ColliderButtonSelector_t3248638654  : public MonoBehaviour_t3962482529
{
public:
	// Pixelplacement.Chooser ColliderButtonSelector::chooser
	Chooser_t2844814028 * ___chooser_4;
	// System.Boolean ColliderButtonSelector::loopAround
	bool ___loopAround_5;
	// UnityEngine.KeyCode ColliderButtonSelector::previousKey
	int32_t ___previousKey_6;
	// UnityEngine.KeyCode ColliderButtonSelector::nextKey
	int32_t ___nextKey_7;
	// Pixelplacement.ColliderButton[] ColliderButtonSelector::colliderButtons
	ColliderButtonU5BU5D_t3310678905* ___colliderButtons_8;
	// System.Int32 ColliderButtonSelector::_index
	int32_t ____index_9;

public:
	inline static int32_t get_offset_of_chooser_4() { return static_cast<int32_t>(offsetof(ColliderButtonSelector_t3248638654, ___chooser_4)); }
	inline Chooser_t2844814028 * get_chooser_4() const { return ___chooser_4; }
	inline Chooser_t2844814028 ** get_address_of_chooser_4() { return &___chooser_4; }
	inline void set_chooser_4(Chooser_t2844814028 * value)
	{
		___chooser_4 = value;
		Il2CppCodeGenWriteBarrier((&___chooser_4), value);
	}

	inline static int32_t get_offset_of_loopAround_5() { return static_cast<int32_t>(offsetof(ColliderButtonSelector_t3248638654, ___loopAround_5)); }
	inline bool get_loopAround_5() const { return ___loopAround_5; }
	inline bool* get_address_of_loopAround_5() { return &___loopAround_5; }
	inline void set_loopAround_5(bool value)
	{
		___loopAround_5 = value;
	}

	inline static int32_t get_offset_of_previousKey_6() { return static_cast<int32_t>(offsetof(ColliderButtonSelector_t3248638654, ___previousKey_6)); }
	inline int32_t get_previousKey_6() const { return ___previousKey_6; }
	inline int32_t* get_address_of_previousKey_6() { return &___previousKey_6; }
	inline void set_previousKey_6(int32_t value)
	{
		___previousKey_6 = value;
	}

	inline static int32_t get_offset_of_nextKey_7() { return static_cast<int32_t>(offsetof(ColliderButtonSelector_t3248638654, ___nextKey_7)); }
	inline int32_t get_nextKey_7() const { return ___nextKey_7; }
	inline int32_t* get_address_of_nextKey_7() { return &___nextKey_7; }
	inline void set_nextKey_7(int32_t value)
	{
		___nextKey_7 = value;
	}

	inline static int32_t get_offset_of_colliderButtons_8() { return static_cast<int32_t>(offsetof(ColliderButtonSelector_t3248638654, ___colliderButtons_8)); }
	inline ColliderButtonU5BU5D_t3310678905* get_colliderButtons_8() const { return ___colliderButtons_8; }
	inline ColliderButtonU5BU5D_t3310678905** get_address_of_colliderButtons_8() { return &___colliderButtons_8; }
	inline void set_colliderButtons_8(ColliderButtonU5BU5D_t3310678905* value)
	{
		___colliderButtons_8 = value;
		Il2CppCodeGenWriteBarrier((&___colliderButtons_8), value);
	}

	inline static int32_t get_offset_of__index_9() { return static_cast<int32_t>(offsetof(ColliderButtonSelector_t3248638654, ____index_9)); }
	inline int32_t get__index_9() const { return ____index_9; }
	inline int32_t* get_address_of__index_9() { return &____index_9; }
	inline void set__index_9(int32_t value)
	{
		____index_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERBUTTONSELECTOR_T3248638654_H
#ifndef CLIENTCONNECTOR_T2701089130_H
#define CLIENTCONNECTOR_T2701089130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ClientConnector
struct  ClientConnector_t2701089130  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Pixelplacement.ClientConnector::_connectToFirstAvailable
	bool ____connectToFirstAvailable_4;
	// System.String Pixelplacement.ClientConnector::_requiredDeviceId
	String_t* ____requiredDeviceId_5;
	// System.Collections.Generic.List`1<Pixelplacement.ClientConnector/AvailableServer> Pixelplacement.ClientConnector::_availableServers
	List_1_t140742778 * ____availableServers_6;
	// System.Boolean Pixelplacement.ClientConnector::_cleanUp
	bool ____cleanUp_7;
	// System.Single Pixelplacement.ClientConnector::_lastCleanUpTime
	float ____lastCleanUpTime_8;
	// System.Single Pixelplacement.ClientConnector::_cleanUpTimeout
	float ____cleanUpTimeout_9;
	// System.Single Pixelplacement.ClientConnector::_cleanUpTimeoutBackup
	float ____cleanUpTimeoutBackup_10;

public:
	inline static int32_t get_offset_of__connectToFirstAvailable_4() { return static_cast<int32_t>(offsetof(ClientConnector_t2701089130, ____connectToFirstAvailable_4)); }
	inline bool get__connectToFirstAvailable_4() const { return ____connectToFirstAvailable_4; }
	inline bool* get_address_of__connectToFirstAvailable_4() { return &____connectToFirstAvailable_4; }
	inline void set__connectToFirstAvailable_4(bool value)
	{
		____connectToFirstAvailable_4 = value;
	}

	inline static int32_t get_offset_of__requiredDeviceId_5() { return static_cast<int32_t>(offsetof(ClientConnector_t2701089130, ____requiredDeviceId_5)); }
	inline String_t* get__requiredDeviceId_5() const { return ____requiredDeviceId_5; }
	inline String_t** get_address_of__requiredDeviceId_5() { return &____requiredDeviceId_5; }
	inline void set__requiredDeviceId_5(String_t* value)
	{
		____requiredDeviceId_5 = value;
		Il2CppCodeGenWriteBarrier((&____requiredDeviceId_5), value);
	}

	inline static int32_t get_offset_of__availableServers_6() { return static_cast<int32_t>(offsetof(ClientConnector_t2701089130, ____availableServers_6)); }
	inline List_1_t140742778 * get__availableServers_6() const { return ____availableServers_6; }
	inline List_1_t140742778 ** get_address_of__availableServers_6() { return &____availableServers_6; }
	inline void set__availableServers_6(List_1_t140742778 * value)
	{
		____availableServers_6 = value;
		Il2CppCodeGenWriteBarrier((&____availableServers_6), value);
	}

	inline static int32_t get_offset_of__cleanUp_7() { return static_cast<int32_t>(offsetof(ClientConnector_t2701089130, ____cleanUp_7)); }
	inline bool get__cleanUp_7() const { return ____cleanUp_7; }
	inline bool* get_address_of__cleanUp_7() { return &____cleanUp_7; }
	inline void set__cleanUp_7(bool value)
	{
		____cleanUp_7 = value;
	}

	inline static int32_t get_offset_of__lastCleanUpTime_8() { return static_cast<int32_t>(offsetof(ClientConnector_t2701089130, ____lastCleanUpTime_8)); }
	inline float get__lastCleanUpTime_8() const { return ____lastCleanUpTime_8; }
	inline float* get_address_of__lastCleanUpTime_8() { return &____lastCleanUpTime_8; }
	inline void set__lastCleanUpTime_8(float value)
	{
		____lastCleanUpTime_8 = value;
	}

	inline static int32_t get_offset_of__cleanUpTimeout_9() { return static_cast<int32_t>(offsetof(ClientConnector_t2701089130, ____cleanUpTimeout_9)); }
	inline float get__cleanUpTimeout_9() const { return ____cleanUpTimeout_9; }
	inline float* get_address_of__cleanUpTimeout_9() { return &____cleanUpTimeout_9; }
	inline void set__cleanUpTimeout_9(float value)
	{
		____cleanUpTimeout_9 = value;
	}

	inline static int32_t get_offset_of__cleanUpTimeoutBackup_10() { return static_cast<int32_t>(offsetof(ClientConnector_t2701089130, ____cleanUpTimeoutBackup_10)); }
	inline float get__cleanUpTimeoutBackup_10() const { return ____cleanUpTimeoutBackup_10; }
	inline float* get_address_of__cleanUpTimeoutBackup_10() { return &____cleanUpTimeoutBackup_10; }
	inline void set__cleanUpTimeoutBackup_10(float value)
	{
		____cleanUpTimeoutBackup_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONNECTOR_T2701089130_H
#ifndef COLLIDERBUTTON_T2059806696_H
#define COLLIDERBUTTON_T2059806696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.ColliderButton
struct  ColliderButton_t2059806696  : public MonoBehaviour_t3962482529
{
public:
	// Pixelplacement.ColliderButtonEvent Pixelplacement.ColliderButton::OnSelected
	ColliderButtonEvent_t5483853 * ___OnSelected_4;
	// Pixelplacement.ColliderButtonEvent Pixelplacement.ColliderButton::OnDeselected
	ColliderButtonEvent_t5483853 * ___OnDeselected_5;
	// Pixelplacement.ColliderButtonEvent Pixelplacement.ColliderButton::OnClick
	ColliderButtonEvent_t5483853 * ___OnClick_6;
	// Pixelplacement.ColliderButtonEvent Pixelplacement.ColliderButton::OnPressed
	ColliderButtonEvent_t5483853 * ___OnPressed_7;
	// Pixelplacement.ColliderButtonEvent Pixelplacement.ColliderButton::OnReleased
	ColliderButtonEvent_t5483853 * ___OnReleased_8;
	// System.Boolean Pixelplacement.ColliderButton::<IsSelected>k__BackingField
	bool ___U3CIsSelectedU3Ek__BackingField_14;
	// UnityEngine.KeyCode[] Pixelplacement.ColliderButton::keyInput
	KeyCodeU5BU5D_t2223234056* ___keyInput_15;
	// System.Boolean Pixelplacement.ColliderButton::_unityEventsFolded
	bool ____unityEventsFolded_16;
	// System.Boolean Pixelplacement.ColliderButton::_scaleResponseFolded
	bool ____scaleResponseFolded_17;
	// System.Boolean Pixelplacement.ColliderButton::_colorResponseFolded
	bool ____colorResponseFolded_18;
	// System.Boolean Pixelplacement.ColliderButton::applyColor
	bool ___applyColor_19;
	// System.Boolean Pixelplacement.ColliderButton::applyScale
	bool ___applyScale_20;
	// UnityEngine.LayerMask Pixelplacement.ColliderButton::collisionLayerMask
	LayerMask_t3493934918  ___collisionLayerMask_21;
	// UnityEngine.Renderer Pixelplacement.ColliderButton::colorRendererTarget
	Renderer_t2627027031 * ___colorRendererTarget_22;
	// UnityEngine.UI.Image Pixelplacement.ColliderButton::colorImageTarget
	Image_t2670269651 * ___colorImageTarget_23;
	// UnityEngine.Color Pixelplacement.ColliderButton::selectedColor
	Color_t2555686324  ___selectedColor_24;
	// UnityEngine.Color Pixelplacement.ColliderButton::pressedColor
	Color_t2555686324  ___pressedColor_25;
	// UnityEngine.Color Pixelplacement.ColliderButton::disabledColor
	Color_t2555686324  ___disabledColor_26;
	// System.Single Pixelplacement.ColliderButton::colorDuration
	float ___colorDuration_27;
	// UnityEngine.Transform Pixelplacement.ColliderButton::scaleTarget
	Transform_t3600365921 * ___scaleTarget_28;
	// UnityEngine.Vector3 Pixelplacement.ColliderButton::normalScale
	Vector3_t3722313464  ___normalScale_29;
	// UnityEngine.Vector3 Pixelplacement.ColliderButton::selectedScale
	Vector3_t3722313464  ___selectedScale_30;
	// UnityEngine.Vector3 Pixelplacement.ColliderButton::pressedScale
	Vector3_t3722313464  ___pressedScale_31;
	// System.Single Pixelplacement.ColliderButton::scaleDuration
	float ___scaleDuration_32;
	// Pixelplacement.ColliderButton/EaseType Pixelplacement.ColliderButton::scaleEaseType
	int32_t ___scaleEaseType_33;
	// System.Boolean Pixelplacement.ColliderButton::resizeGUIBoxCollider
	bool ___resizeGUIBoxCollider_34;
	// System.Boolean Pixelplacement.ColliderButton::centerGUIBoxCollider
	bool ___centerGUIBoxCollider_35;
	// UnityEngine.Vector2 Pixelplacement.ColliderButton::guiBoxColliderPadding
	Vector2_t2156229523  ___guiBoxColliderPadding_36;
	// System.Boolean Pixelplacement.ColliderButton::interactable
	bool ___interactable_37;
	// System.Boolean Pixelplacement.ColliderButton::_clicking
	bool ____clicking_38;
	// System.Int32 Pixelplacement.ColliderButton::_selectedCount
	int32_t ____selectedCount_39;
	// System.Boolean Pixelplacement.ColliderButton::_colliderSelected
	bool ____colliderSelected_40;
	// System.Boolean Pixelplacement.ColliderButton::_pressed
	bool ____pressed_41;
	// System.Boolean Pixelplacement.ColliderButton::_released
	bool ____released_42;
	// System.Boolean Pixelplacement.ColliderButton::_vrRunning
	bool ____vrRunning_43;
	// UnityEngine.RectTransform Pixelplacement.ColliderButton::_rectTransform
	RectTransform_t3704657025 * ____rectTransform_44;
	// UnityEngine.EventSystems.EventTrigger Pixelplacement.ColliderButton::_eventTrigger
	EventTrigger_t1076084509 * ____eventTrigger_45;
	// UnityEngine.EventSystems.EventTrigger/Entry Pixelplacement.ColliderButton::_pressedEventTrigger
	Entry_t3344766165 * ____pressedEventTrigger_46;
	// UnityEngine.EventSystems.EventTrigger/Entry Pixelplacement.ColliderButton::_releasedEventTrigger
	Entry_t3344766165 * ____releasedEventTrigger_47;
	// UnityEngine.EventSystems.EventTrigger/Entry Pixelplacement.ColliderButton::_enterEventTrigger
	Entry_t3344766165 * ____enterEventTrigger_48;
	// UnityEngine.EventSystems.EventTrigger/Entry Pixelplacement.ColliderButton::_exitEventTrigger
	Entry_t3344766165 * ____exitEventTrigger_49;
	// System.Int32 Pixelplacement.ColliderButton::_colliderCount
	int32_t ____colliderCount_50;
	// UnityEngine.BoxCollider Pixelplacement.ColliderButton::_boxCollider
	BoxCollider_t1640800422 * ____boxCollider_51;
	// Pixelplacement.TweenSystem.TweenBase Pixelplacement.ColliderButton::_colorTweenImage
	TweenBase_t3695616892 * ____colorTweenImage_52;
	// Pixelplacement.TweenSystem.TweenBase Pixelplacement.ColliderButton::_colorTweenMaterial
	TweenBase_t3695616892 * ____colorTweenMaterial_53;
	// Pixelplacement.TweenSystem.TweenBase Pixelplacement.ColliderButton::_scaleTween
	TweenBase_t3695616892 * ____scaleTween_54;
	// UnityEngine.Color Pixelplacement.ColliderButton::_normalColorRenderer
	Color_t2555686324  ____normalColorRenderer_55;
	// UnityEngine.Color Pixelplacement.ColliderButton::_normalColorImage
	Color_t2555686324  ____normalColorImage_56;
	// System.Boolean Pixelplacement.ColliderButton::_interactableStatus
	bool ____interactableStatus_57;

public:
	inline static int32_t get_offset_of_OnSelected_4() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___OnSelected_4)); }
	inline ColliderButtonEvent_t5483853 * get_OnSelected_4() const { return ___OnSelected_4; }
	inline ColliderButtonEvent_t5483853 ** get_address_of_OnSelected_4() { return &___OnSelected_4; }
	inline void set_OnSelected_4(ColliderButtonEvent_t5483853 * value)
	{
		___OnSelected_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelected_4), value);
	}

	inline static int32_t get_offset_of_OnDeselected_5() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___OnDeselected_5)); }
	inline ColliderButtonEvent_t5483853 * get_OnDeselected_5() const { return ___OnDeselected_5; }
	inline ColliderButtonEvent_t5483853 ** get_address_of_OnDeselected_5() { return &___OnDeselected_5; }
	inline void set_OnDeselected_5(ColliderButtonEvent_t5483853 * value)
	{
		___OnDeselected_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeselected_5), value);
	}

	inline static int32_t get_offset_of_OnClick_6() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___OnClick_6)); }
	inline ColliderButtonEvent_t5483853 * get_OnClick_6() const { return ___OnClick_6; }
	inline ColliderButtonEvent_t5483853 ** get_address_of_OnClick_6() { return &___OnClick_6; }
	inline void set_OnClick_6(ColliderButtonEvent_t5483853 * value)
	{
		___OnClick_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnClick_6), value);
	}

	inline static int32_t get_offset_of_OnPressed_7() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___OnPressed_7)); }
	inline ColliderButtonEvent_t5483853 * get_OnPressed_7() const { return ___OnPressed_7; }
	inline ColliderButtonEvent_t5483853 ** get_address_of_OnPressed_7() { return &___OnPressed_7; }
	inline void set_OnPressed_7(ColliderButtonEvent_t5483853 * value)
	{
		___OnPressed_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressed_7), value);
	}

	inline static int32_t get_offset_of_OnReleased_8() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___OnReleased_8)); }
	inline ColliderButtonEvent_t5483853 * get_OnReleased_8() const { return ___OnReleased_8; }
	inline ColliderButtonEvent_t5483853 ** get_address_of_OnReleased_8() { return &___OnReleased_8; }
	inline void set_OnReleased_8(ColliderButtonEvent_t5483853 * value)
	{
		___OnReleased_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnReleased_8), value);
	}

	inline static int32_t get_offset_of_U3CIsSelectedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___U3CIsSelectedU3Ek__BackingField_14)); }
	inline bool get_U3CIsSelectedU3Ek__BackingField_14() const { return ___U3CIsSelectedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CIsSelectedU3Ek__BackingField_14() { return &___U3CIsSelectedU3Ek__BackingField_14; }
	inline void set_U3CIsSelectedU3Ek__BackingField_14(bool value)
	{
		___U3CIsSelectedU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_keyInput_15() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___keyInput_15)); }
	inline KeyCodeU5BU5D_t2223234056* get_keyInput_15() const { return ___keyInput_15; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keyInput_15() { return &___keyInput_15; }
	inline void set_keyInput_15(KeyCodeU5BU5D_t2223234056* value)
	{
		___keyInput_15 = value;
		Il2CppCodeGenWriteBarrier((&___keyInput_15), value);
	}

	inline static int32_t get_offset_of__unityEventsFolded_16() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____unityEventsFolded_16)); }
	inline bool get__unityEventsFolded_16() const { return ____unityEventsFolded_16; }
	inline bool* get_address_of__unityEventsFolded_16() { return &____unityEventsFolded_16; }
	inline void set__unityEventsFolded_16(bool value)
	{
		____unityEventsFolded_16 = value;
	}

	inline static int32_t get_offset_of__scaleResponseFolded_17() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____scaleResponseFolded_17)); }
	inline bool get__scaleResponseFolded_17() const { return ____scaleResponseFolded_17; }
	inline bool* get_address_of__scaleResponseFolded_17() { return &____scaleResponseFolded_17; }
	inline void set__scaleResponseFolded_17(bool value)
	{
		____scaleResponseFolded_17 = value;
	}

	inline static int32_t get_offset_of__colorResponseFolded_18() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____colorResponseFolded_18)); }
	inline bool get__colorResponseFolded_18() const { return ____colorResponseFolded_18; }
	inline bool* get_address_of__colorResponseFolded_18() { return &____colorResponseFolded_18; }
	inline void set__colorResponseFolded_18(bool value)
	{
		____colorResponseFolded_18 = value;
	}

	inline static int32_t get_offset_of_applyColor_19() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___applyColor_19)); }
	inline bool get_applyColor_19() const { return ___applyColor_19; }
	inline bool* get_address_of_applyColor_19() { return &___applyColor_19; }
	inline void set_applyColor_19(bool value)
	{
		___applyColor_19 = value;
	}

	inline static int32_t get_offset_of_applyScale_20() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___applyScale_20)); }
	inline bool get_applyScale_20() const { return ___applyScale_20; }
	inline bool* get_address_of_applyScale_20() { return &___applyScale_20; }
	inline void set_applyScale_20(bool value)
	{
		___applyScale_20 = value;
	}

	inline static int32_t get_offset_of_collisionLayerMask_21() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___collisionLayerMask_21)); }
	inline LayerMask_t3493934918  get_collisionLayerMask_21() const { return ___collisionLayerMask_21; }
	inline LayerMask_t3493934918 * get_address_of_collisionLayerMask_21() { return &___collisionLayerMask_21; }
	inline void set_collisionLayerMask_21(LayerMask_t3493934918  value)
	{
		___collisionLayerMask_21 = value;
	}

	inline static int32_t get_offset_of_colorRendererTarget_22() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___colorRendererTarget_22)); }
	inline Renderer_t2627027031 * get_colorRendererTarget_22() const { return ___colorRendererTarget_22; }
	inline Renderer_t2627027031 ** get_address_of_colorRendererTarget_22() { return &___colorRendererTarget_22; }
	inline void set_colorRendererTarget_22(Renderer_t2627027031 * value)
	{
		___colorRendererTarget_22 = value;
		Il2CppCodeGenWriteBarrier((&___colorRendererTarget_22), value);
	}

	inline static int32_t get_offset_of_colorImageTarget_23() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___colorImageTarget_23)); }
	inline Image_t2670269651 * get_colorImageTarget_23() const { return ___colorImageTarget_23; }
	inline Image_t2670269651 ** get_address_of_colorImageTarget_23() { return &___colorImageTarget_23; }
	inline void set_colorImageTarget_23(Image_t2670269651 * value)
	{
		___colorImageTarget_23 = value;
		Il2CppCodeGenWriteBarrier((&___colorImageTarget_23), value);
	}

	inline static int32_t get_offset_of_selectedColor_24() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___selectedColor_24)); }
	inline Color_t2555686324  get_selectedColor_24() const { return ___selectedColor_24; }
	inline Color_t2555686324 * get_address_of_selectedColor_24() { return &___selectedColor_24; }
	inline void set_selectedColor_24(Color_t2555686324  value)
	{
		___selectedColor_24 = value;
	}

	inline static int32_t get_offset_of_pressedColor_25() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___pressedColor_25)); }
	inline Color_t2555686324  get_pressedColor_25() const { return ___pressedColor_25; }
	inline Color_t2555686324 * get_address_of_pressedColor_25() { return &___pressedColor_25; }
	inline void set_pressedColor_25(Color_t2555686324  value)
	{
		___pressedColor_25 = value;
	}

	inline static int32_t get_offset_of_disabledColor_26() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___disabledColor_26)); }
	inline Color_t2555686324  get_disabledColor_26() const { return ___disabledColor_26; }
	inline Color_t2555686324 * get_address_of_disabledColor_26() { return &___disabledColor_26; }
	inline void set_disabledColor_26(Color_t2555686324  value)
	{
		___disabledColor_26 = value;
	}

	inline static int32_t get_offset_of_colorDuration_27() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___colorDuration_27)); }
	inline float get_colorDuration_27() const { return ___colorDuration_27; }
	inline float* get_address_of_colorDuration_27() { return &___colorDuration_27; }
	inline void set_colorDuration_27(float value)
	{
		___colorDuration_27 = value;
	}

	inline static int32_t get_offset_of_scaleTarget_28() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___scaleTarget_28)); }
	inline Transform_t3600365921 * get_scaleTarget_28() const { return ___scaleTarget_28; }
	inline Transform_t3600365921 ** get_address_of_scaleTarget_28() { return &___scaleTarget_28; }
	inline void set_scaleTarget_28(Transform_t3600365921 * value)
	{
		___scaleTarget_28 = value;
		Il2CppCodeGenWriteBarrier((&___scaleTarget_28), value);
	}

	inline static int32_t get_offset_of_normalScale_29() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___normalScale_29)); }
	inline Vector3_t3722313464  get_normalScale_29() const { return ___normalScale_29; }
	inline Vector3_t3722313464 * get_address_of_normalScale_29() { return &___normalScale_29; }
	inline void set_normalScale_29(Vector3_t3722313464  value)
	{
		___normalScale_29 = value;
	}

	inline static int32_t get_offset_of_selectedScale_30() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___selectedScale_30)); }
	inline Vector3_t3722313464  get_selectedScale_30() const { return ___selectedScale_30; }
	inline Vector3_t3722313464 * get_address_of_selectedScale_30() { return &___selectedScale_30; }
	inline void set_selectedScale_30(Vector3_t3722313464  value)
	{
		___selectedScale_30 = value;
	}

	inline static int32_t get_offset_of_pressedScale_31() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___pressedScale_31)); }
	inline Vector3_t3722313464  get_pressedScale_31() const { return ___pressedScale_31; }
	inline Vector3_t3722313464 * get_address_of_pressedScale_31() { return &___pressedScale_31; }
	inline void set_pressedScale_31(Vector3_t3722313464  value)
	{
		___pressedScale_31 = value;
	}

	inline static int32_t get_offset_of_scaleDuration_32() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___scaleDuration_32)); }
	inline float get_scaleDuration_32() const { return ___scaleDuration_32; }
	inline float* get_address_of_scaleDuration_32() { return &___scaleDuration_32; }
	inline void set_scaleDuration_32(float value)
	{
		___scaleDuration_32 = value;
	}

	inline static int32_t get_offset_of_scaleEaseType_33() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___scaleEaseType_33)); }
	inline int32_t get_scaleEaseType_33() const { return ___scaleEaseType_33; }
	inline int32_t* get_address_of_scaleEaseType_33() { return &___scaleEaseType_33; }
	inline void set_scaleEaseType_33(int32_t value)
	{
		___scaleEaseType_33 = value;
	}

	inline static int32_t get_offset_of_resizeGUIBoxCollider_34() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___resizeGUIBoxCollider_34)); }
	inline bool get_resizeGUIBoxCollider_34() const { return ___resizeGUIBoxCollider_34; }
	inline bool* get_address_of_resizeGUIBoxCollider_34() { return &___resizeGUIBoxCollider_34; }
	inline void set_resizeGUIBoxCollider_34(bool value)
	{
		___resizeGUIBoxCollider_34 = value;
	}

	inline static int32_t get_offset_of_centerGUIBoxCollider_35() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___centerGUIBoxCollider_35)); }
	inline bool get_centerGUIBoxCollider_35() const { return ___centerGUIBoxCollider_35; }
	inline bool* get_address_of_centerGUIBoxCollider_35() { return &___centerGUIBoxCollider_35; }
	inline void set_centerGUIBoxCollider_35(bool value)
	{
		___centerGUIBoxCollider_35 = value;
	}

	inline static int32_t get_offset_of_guiBoxColliderPadding_36() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___guiBoxColliderPadding_36)); }
	inline Vector2_t2156229523  get_guiBoxColliderPadding_36() const { return ___guiBoxColliderPadding_36; }
	inline Vector2_t2156229523 * get_address_of_guiBoxColliderPadding_36() { return &___guiBoxColliderPadding_36; }
	inline void set_guiBoxColliderPadding_36(Vector2_t2156229523  value)
	{
		___guiBoxColliderPadding_36 = value;
	}

	inline static int32_t get_offset_of_interactable_37() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ___interactable_37)); }
	inline bool get_interactable_37() const { return ___interactable_37; }
	inline bool* get_address_of_interactable_37() { return &___interactable_37; }
	inline void set_interactable_37(bool value)
	{
		___interactable_37 = value;
	}

	inline static int32_t get_offset_of__clicking_38() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____clicking_38)); }
	inline bool get__clicking_38() const { return ____clicking_38; }
	inline bool* get_address_of__clicking_38() { return &____clicking_38; }
	inline void set__clicking_38(bool value)
	{
		____clicking_38 = value;
	}

	inline static int32_t get_offset_of__selectedCount_39() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____selectedCount_39)); }
	inline int32_t get__selectedCount_39() const { return ____selectedCount_39; }
	inline int32_t* get_address_of__selectedCount_39() { return &____selectedCount_39; }
	inline void set__selectedCount_39(int32_t value)
	{
		____selectedCount_39 = value;
	}

	inline static int32_t get_offset_of__colliderSelected_40() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____colliderSelected_40)); }
	inline bool get__colliderSelected_40() const { return ____colliderSelected_40; }
	inline bool* get_address_of__colliderSelected_40() { return &____colliderSelected_40; }
	inline void set__colliderSelected_40(bool value)
	{
		____colliderSelected_40 = value;
	}

	inline static int32_t get_offset_of__pressed_41() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____pressed_41)); }
	inline bool get__pressed_41() const { return ____pressed_41; }
	inline bool* get_address_of__pressed_41() { return &____pressed_41; }
	inline void set__pressed_41(bool value)
	{
		____pressed_41 = value;
	}

	inline static int32_t get_offset_of__released_42() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____released_42)); }
	inline bool get__released_42() const { return ____released_42; }
	inline bool* get_address_of__released_42() { return &____released_42; }
	inline void set__released_42(bool value)
	{
		____released_42 = value;
	}

	inline static int32_t get_offset_of__vrRunning_43() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____vrRunning_43)); }
	inline bool get__vrRunning_43() const { return ____vrRunning_43; }
	inline bool* get_address_of__vrRunning_43() { return &____vrRunning_43; }
	inline void set__vrRunning_43(bool value)
	{
		____vrRunning_43 = value;
	}

	inline static int32_t get_offset_of__rectTransform_44() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____rectTransform_44)); }
	inline RectTransform_t3704657025 * get__rectTransform_44() const { return ____rectTransform_44; }
	inline RectTransform_t3704657025 ** get_address_of__rectTransform_44() { return &____rectTransform_44; }
	inline void set__rectTransform_44(RectTransform_t3704657025 * value)
	{
		____rectTransform_44 = value;
		Il2CppCodeGenWriteBarrier((&____rectTransform_44), value);
	}

	inline static int32_t get_offset_of__eventTrigger_45() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____eventTrigger_45)); }
	inline EventTrigger_t1076084509 * get__eventTrigger_45() const { return ____eventTrigger_45; }
	inline EventTrigger_t1076084509 ** get_address_of__eventTrigger_45() { return &____eventTrigger_45; }
	inline void set__eventTrigger_45(EventTrigger_t1076084509 * value)
	{
		____eventTrigger_45 = value;
		Il2CppCodeGenWriteBarrier((&____eventTrigger_45), value);
	}

	inline static int32_t get_offset_of__pressedEventTrigger_46() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____pressedEventTrigger_46)); }
	inline Entry_t3344766165 * get__pressedEventTrigger_46() const { return ____pressedEventTrigger_46; }
	inline Entry_t3344766165 ** get_address_of__pressedEventTrigger_46() { return &____pressedEventTrigger_46; }
	inline void set__pressedEventTrigger_46(Entry_t3344766165 * value)
	{
		____pressedEventTrigger_46 = value;
		Il2CppCodeGenWriteBarrier((&____pressedEventTrigger_46), value);
	}

	inline static int32_t get_offset_of__releasedEventTrigger_47() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____releasedEventTrigger_47)); }
	inline Entry_t3344766165 * get__releasedEventTrigger_47() const { return ____releasedEventTrigger_47; }
	inline Entry_t3344766165 ** get_address_of__releasedEventTrigger_47() { return &____releasedEventTrigger_47; }
	inline void set__releasedEventTrigger_47(Entry_t3344766165 * value)
	{
		____releasedEventTrigger_47 = value;
		Il2CppCodeGenWriteBarrier((&____releasedEventTrigger_47), value);
	}

	inline static int32_t get_offset_of__enterEventTrigger_48() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____enterEventTrigger_48)); }
	inline Entry_t3344766165 * get__enterEventTrigger_48() const { return ____enterEventTrigger_48; }
	inline Entry_t3344766165 ** get_address_of__enterEventTrigger_48() { return &____enterEventTrigger_48; }
	inline void set__enterEventTrigger_48(Entry_t3344766165 * value)
	{
		____enterEventTrigger_48 = value;
		Il2CppCodeGenWriteBarrier((&____enterEventTrigger_48), value);
	}

	inline static int32_t get_offset_of__exitEventTrigger_49() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____exitEventTrigger_49)); }
	inline Entry_t3344766165 * get__exitEventTrigger_49() const { return ____exitEventTrigger_49; }
	inline Entry_t3344766165 ** get_address_of__exitEventTrigger_49() { return &____exitEventTrigger_49; }
	inline void set__exitEventTrigger_49(Entry_t3344766165 * value)
	{
		____exitEventTrigger_49 = value;
		Il2CppCodeGenWriteBarrier((&____exitEventTrigger_49), value);
	}

	inline static int32_t get_offset_of__colliderCount_50() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____colliderCount_50)); }
	inline int32_t get__colliderCount_50() const { return ____colliderCount_50; }
	inline int32_t* get_address_of__colliderCount_50() { return &____colliderCount_50; }
	inline void set__colliderCount_50(int32_t value)
	{
		____colliderCount_50 = value;
	}

	inline static int32_t get_offset_of__boxCollider_51() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____boxCollider_51)); }
	inline BoxCollider_t1640800422 * get__boxCollider_51() const { return ____boxCollider_51; }
	inline BoxCollider_t1640800422 ** get_address_of__boxCollider_51() { return &____boxCollider_51; }
	inline void set__boxCollider_51(BoxCollider_t1640800422 * value)
	{
		____boxCollider_51 = value;
		Il2CppCodeGenWriteBarrier((&____boxCollider_51), value);
	}

	inline static int32_t get_offset_of__colorTweenImage_52() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____colorTweenImage_52)); }
	inline TweenBase_t3695616892 * get__colorTweenImage_52() const { return ____colorTweenImage_52; }
	inline TweenBase_t3695616892 ** get_address_of__colorTweenImage_52() { return &____colorTweenImage_52; }
	inline void set__colorTweenImage_52(TweenBase_t3695616892 * value)
	{
		____colorTweenImage_52 = value;
		Il2CppCodeGenWriteBarrier((&____colorTweenImage_52), value);
	}

	inline static int32_t get_offset_of__colorTweenMaterial_53() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____colorTweenMaterial_53)); }
	inline TweenBase_t3695616892 * get__colorTweenMaterial_53() const { return ____colorTweenMaterial_53; }
	inline TweenBase_t3695616892 ** get_address_of__colorTweenMaterial_53() { return &____colorTweenMaterial_53; }
	inline void set__colorTweenMaterial_53(TweenBase_t3695616892 * value)
	{
		____colorTweenMaterial_53 = value;
		Il2CppCodeGenWriteBarrier((&____colorTweenMaterial_53), value);
	}

	inline static int32_t get_offset_of__scaleTween_54() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____scaleTween_54)); }
	inline TweenBase_t3695616892 * get__scaleTween_54() const { return ____scaleTween_54; }
	inline TweenBase_t3695616892 ** get_address_of__scaleTween_54() { return &____scaleTween_54; }
	inline void set__scaleTween_54(TweenBase_t3695616892 * value)
	{
		____scaleTween_54 = value;
		Il2CppCodeGenWriteBarrier((&____scaleTween_54), value);
	}

	inline static int32_t get_offset_of__normalColorRenderer_55() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____normalColorRenderer_55)); }
	inline Color_t2555686324  get__normalColorRenderer_55() const { return ____normalColorRenderer_55; }
	inline Color_t2555686324 * get_address_of__normalColorRenderer_55() { return &____normalColorRenderer_55; }
	inline void set__normalColorRenderer_55(Color_t2555686324  value)
	{
		____normalColorRenderer_55 = value;
	}

	inline static int32_t get_offset_of__normalColorImage_56() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____normalColorImage_56)); }
	inline Color_t2555686324  get__normalColorImage_56() const { return ____normalColorImage_56; }
	inline Color_t2555686324 * get_address_of__normalColorImage_56() { return &____normalColorImage_56; }
	inline void set__normalColorImage_56(Color_t2555686324  value)
	{
		____normalColorImage_56 = value;
	}

	inline static int32_t get_offset_of__interactableStatus_57() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696, ____interactableStatus_57)); }
	inline bool get__interactableStatus_57() const { return ____interactableStatus_57; }
	inline bool* get_address_of__interactableStatus_57() { return &____interactableStatus_57; }
	inline void set__interactableStatus_57(bool value)
	{
		____interactableStatus_57 = value;
	}
};

struct ColliderButton_t2059806696_StaticFields
{
public:
	// System.Action`1<Pixelplacement.ColliderButton> Pixelplacement.ColliderButton::OnSelectedGlobal
	Action_1_t2232274291 * ___OnSelectedGlobal_9;
	// System.Action`1<Pixelplacement.ColliderButton> Pixelplacement.ColliderButton::OnDeselectedGlobal
	Action_1_t2232274291 * ___OnDeselectedGlobal_10;
	// System.Action`1<Pixelplacement.ColliderButton> Pixelplacement.ColliderButton::OnClickGlobal
	Action_1_t2232274291 * ___OnClickGlobal_11;
	// System.Action`1<Pixelplacement.ColliderButton> Pixelplacement.ColliderButton::OnPressedGlobal
	Action_1_t2232274291 * ___OnPressedGlobal_12;
	// System.Action`1<Pixelplacement.ColliderButton> Pixelplacement.ColliderButton::OnReleasedGlobal
	Action_1_t2232274291 * ___OnReleasedGlobal_13;

public:
	inline static int32_t get_offset_of_OnSelectedGlobal_9() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696_StaticFields, ___OnSelectedGlobal_9)); }
	inline Action_1_t2232274291 * get_OnSelectedGlobal_9() const { return ___OnSelectedGlobal_9; }
	inline Action_1_t2232274291 ** get_address_of_OnSelectedGlobal_9() { return &___OnSelectedGlobal_9; }
	inline void set_OnSelectedGlobal_9(Action_1_t2232274291 * value)
	{
		___OnSelectedGlobal_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnSelectedGlobal_9), value);
	}

	inline static int32_t get_offset_of_OnDeselectedGlobal_10() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696_StaticFields, ___OnDeselectedGlobal_10)); }
	inline Action_1_t2232274291 * get_OnDeselectedGlobal_10() const { return ___OnDeselectedGlobal_10; }
	inline Action_1_t2232274291 ** get_address_of_OnDeselectedGlobal_10() { return &___OnDeselectedGlobal_10; }
	inline void set_OnDeselectedGlobal_10(Action_1_t2232274291 * value)
	{
		___OnDeselectedGlobal_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnDeselectedGlobal_10), value);
	}

	inline static int32_t get_offset_of_OnClickGlobal_11() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696_StaticFields, ___OnClickGlobal_11)); }
	inline Action_1_t2232274291 * get_OnClickGlobal_11() const { return ___OnClickGlobal_11; }
	inline Action_1_t2232274291 ** get_address_of_OnClickGlobal_11() { return &___OnClickGlobal_11; }
	inline void set_OnClickGlobal_11(Action_1_t2232274291 * value)
	{
		___OnClickGlobal_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnClickGlobal_11), value);
	}

	inline static int32_t get_offset_of_OnPressedGlobal_12() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696_StaticFields, ___OnPressedGlobal_12)); }
	inline Action_1_t2232274291 * get_OnPressedGlobal_12() const { return ___OnPressedGlobal_12; }
	inline Action_1_t2232274291 ** get_address_of_OnPressedGlobal_12() { return &___OnPressedGlobal_12; }
	inline void set_OnPressedGlobal_12(Action_1_t2232274291 * value)
	{
		___OnPressedGlobal_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnPressedGlobal_12), value);
	}

	inline static int32_t get_offset_of_OnReleasedGlobal_13() { return static_cast<int32_t>(offsetof(ColliderButton_t2059806696_StaticFields, ___OnReleasedGlobal_13)); }
	inline Action_1_t2232274291 * get_OnReleasedGlobal_13() const { return ___OnReleasedGlobal_13; }
	inline Action_1_t2232274291 ** get_address_of_OnReleasedGlobal_13() { return &___OnReleasedGlobal_13; }
	inline void set_OnReleasedGlobal_13(Action_1_t2232274291 * value)
	{
		___OnReleasedGlobal_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnReleasedGlobal_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERBUTTON_T2059806696_H
#ifndef DISPLAYOBJECT_T314287876_H
#define DISPLAYOBJECT_T314287876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.DisplayObject
struct  DisplayObject_t314287876  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Pixelplacement.DisplayObject::_activated
	bool ____activated_4;

public:
	inline static int32_t get_offset_of__activated_4() { return static_cast<int32_t>(offsetof(DisplayObject_t314287876, ____activated_4)); }
	inline bool get__activated_4() const { return ____activated_4; }
	inline bool* get_address_of__activated_4() { return &____activated_4; }
	inline void set__activated_4(bool value)
	{
		____activated_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYOBJECT_T314287876_H
#ifndef SPLINE_T1246320346_H
#define SPLINE_T1246320346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Spline
struct  Spline_t1246320346  : public MonoBehaviour_t3962482529
{
public:
	// System.Action Pixelplacement.Spline::OnSplineChanged
	Action_t1264377477 * ___OnSplineChanged_4;
	// UnityEngine.Color Pixelplacement.Spline::color
	Color_t2555686324  ___color_5;
	// System.Single Pixelplacement.Spline::toolScale
	float ___toolScale_6;
	// Pixelplacement.TangentMode Pixelplacement.Spline::defaultTangentMode
	int32_t ___defaultTangentMode_7;
	// Pixelplacement.SplineDirection Pixelplacement.Spline::direction
	int32_t ___direction_8;
	// System.Boolean Pixelplacement.Spline::loop
	bool ___loop_9;
	// Pixelplacement.SplineFollower[] Pixelplacement.Spline::followers
	SplineFollowerU5BU5D_t4227083941* ___followers_10;
	// Pixelplacement.SplineAnchor[] Pixelplacement.Spline::_anchors
	SplineAnchorU5BU5D_t3299042520* ____anchors_11;
	// System.Int32 Pixelplacement.Spline::_curveCount
	int32_t ____curveCount_12;
	// System.Int32 Pixelplacement.Spline::_previousAnchorCount
	int32_t ____previousAnchorCount_13;
	// System.Int32 Pixelplacement.Spline::_previousChildCount
	int32_t ____previousChildCount_14;
	// System.Boolean Pixelplacement.Spline::_wasLooping
	bool ____wasLooping_15;
	// System.Boolean Pixelplacement.Spline::_previousLoopChoice
	bool ____previousLoopChoice_16;
	// System.Boolean Pixelplacement.Spline::_anchorsChanged
	bool ____anchorsChanged_17;
	// Pixelplacement.SplineDirection Pixelplacement.Spline::_previousDirection
	int32_t ____previousDirection_18;
	// System.Single Pixelplacement.Spline::_curvePercentage
	float ____curvePercentage_19;
	// System.Int32 Pixelplacement.Spline::_operatingCurve
	int32_t ____operatingCurve_20;
	// System.Single Pixelplacement.Spline::_currentCurve
	float ____currentCurve_21;
	// System.Boolean Pixelplacement.Spline::_removeRenderers
	bool ____removeRenderers_22;
	// System.Int32 Pixelplacement.Spline::_previousLength
	int32_t ____previousLength_23;
	// System.Int32 Pixelplacement.Spline::_slicesPerCurve
	int32_t ____slicesPerCurve_24;
	// System.Collections.Generic.List`1<Pixelplacement.Spline/SplineReparam> Pixelplacement.Spline::_splineReparams
	List_1_t801671833 * ____splineReparams_25;
	// System.Boolean Pixelplacement.Spline::_lengthDirty
	bool ____lengthDirty_26;
	// System.Single Pixelplacement.Spline::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_OnSplineChanged_4() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___OnSplineChanged_4)); }
	inline Action_t1264377477 * get_OnSplineChanged_4() const { return ___OnSplineChanged_4; }
	inline Action_t1264377477 ** get_address_of_OnSplineChanged_4() { return &___OnSplineChanged_4; }
	inline void set_OnSplineChanged_4(Action_t1264377477 * value)
	{
		___OnSplineChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnSplineChanged_4), value);
	}

	inline static int32_t get_offset_of_color_5() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___color_5)); }
	inline Color_t2555686324  get_color_5() const { return ___color_5; }
	inline Color_t2555686324 * get_address_of_color_5() { return &___color_5; }
	inline void set_color_5(Color_t2555686324  value)
	{
		___color_5 = value;
	}

	inline static int32_t get_offset_of_toolScale_6() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___toolScale_6)); }
	inline float get_toolScale_6() const { return ___toolScale_6; }
	inline float* get_address_of_toolScale_6() { return &___toolScale_6; }
	inline void set_toolScale_6(float value)
	{
		___toolScale_6 = value;
	}

	inline static int32_t get_offset_of_defaultTangentMode_7() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___defaultTangentMode_7)); }
	inline int32_t get_defaultTangentMode_7() const { return ___defaultTangentMode_7; }
	inline int32_t* get_address_of_defaultTangentMode_7() { return &___defaultTangentMode_7; }
	inline void set_defaultTangentMode_7(int32_t value)
	{
		___defaultTangentMode_7 = value;
	}

	inline static int32_t get_offset_of_direction_8() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___direction_8)); }
	inline int32_t get_direction_8() const { return ___direction_8; }
	inline int32_t* get_address_of_direction_8() { return &___direction_8; }
	inline void set_direction_8(int32_t value)
	{
		___direction_8 = value;
	}

	inline static int32_t get_offset_of_loop_9() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___loop_9)); }
	inline bool get_loop_9() const { return ___loop_9; }
	inline bool* get_address_of_loop_9() { return &___loop_9; }
	inline void set_loop_9(bool value)
	{
		___loop_9 = value;
	}

	inline static int32_t get_offset_of_followers_10() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___followers_10)); }
	inline SplineFollowerU5BU5D_t4227083941* get_followers_10() const { return ___followers_10; }
	inline SplineFollowerU5BU5D_t4227083941** get_address_of_followers_10() { return &___followers_10; }
	inline void set_followers_10(SplineFollowerU5BU5D_t4227083941* value)
	{
		___followers_10 = value;
		Il2CppCodeGenWriteBarrier((&___followers_10), value);
	}

	inline static int32_t get_offset_of__anchors_11() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____anchors_11)); }
	inline SplineAnchorU5BU5D_t3299042520* get__anchors_11() const { return ____anchors_11; }
	inline SplineAnchorU5BU5D_t3299042520** get_address_of__anchors_11() { return &____anchors_11; }
	inline void set__anchors_11(SplineAnchorU5BU5D_t3299042520* value)
	{
		____anchors_11 = value;
		Il2CppCodeGenWriteBarrier((&____anchors_11), value);
	}

	inline static int32_t get_offset_of__curveCount_12() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____curveCount_12)); }
	inline int32_t get__curveCount_12() const { return ____curveCount_12; }
	inline int32_t* get_address_of__curveCount_12() { return &____curveCount_12; }
	inline void set__curveCount_12(int32_t value)
	{
		____curveCount_12 = value;
	}

	inline static int32_t get_offset_of__previousAnchorCount_13() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____previousAnchorCount_13)); }
	inline int32_t get__previousAnchorCount_13() const { return ____previousAnchorCount_13; }
	inline int32_t* get_address_of__previousAnchorCount_13() { return &____previousAnchorCount_13; }
	inline void set__previousAnchorCount_13(int32_t value)
	{
		____previousAnchorCount_13 = value;
	}

	inline static int32_t get_offset_of__previousChildCount_14() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____previousChildCount_14)); }
	inline int32_t get__previousChildCount_14() const { return ____previousChildCount_14; }
	inline int32_t* get_address_of__previousChildCount_14() { return &____previousChildCount_14; }
	inline void set__previousChildCount_14(int32_t value)
	{
		____previousChildCount_14 = value;
	}

	inline static int32_t get_offset_of__wasLooping_15() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____wasLooping_15)); }
	inline bool get__wasLooping_15() const { return ____wasLooping_15; }
	inline bool* get_address_of__wasLooping_15() { return &____wasLooping_15; }
	inline void set__wasLooping_15(bool value)
	{
		____wasLooping_15 = value;
	}

	inline static int32_t get_offset_of__previousLoopChoice_16() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____previousLoopChoice_16)); }
	inline bool get__previousLoopChoice_16() const { return ____previousLoopChoice_16; }
	inline bool* get_address_of__previousLoopChoice_16() { return &____previousLoopChoice_16; }
	inline void set__previousLoopChoice_16(bool value)
	{
		____previousLoopChoice_16 = value;
	}

	inline static int32_t get_offset_of__anchorsChanged_17() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____anchorsChanged_17)); }
	inline bool get__anchorsChanged_17() const { return ____anchorsChanged_17; }
	inline bool* get_address_of__anchorsChanged_17() { return &____anchorsChanged_17; }
	inline void set__anchorsChanged_17(bool value)
	{
		____anchorsChanged_17 = value;
	}

	inline static int32_t get_offset_of__previousDirection_18() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____previousDirection_18)); }
	inline int32_t get__previousDirection_18() const { return ____previousDirection_18; }
	inline int32_t* get_address_of__previousDirection_18() { return &____previousDirection_18; }
	inline void set__previousDirection_18(int32_t value)
	{
		____previousDirection_18 = value;
	}

	inline static int32_t get_offset_of__curvePercentage_19() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____curvePercentage_19)); }
	inline float get__curvePercentage_19() const { return ____curvePercentage_19; }
	inline float* get_address_of__curvePercentage_19() { return &____curvePercentage_19; }
	inline void set__curvePercentage_19(float value)
	{
		____curvePercentage_19 = value;
	}

	inline static int32_t get_offset_of__operatingCurve_20() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____operatingCurve_20)); }
	inline int32_t get__operatingCurve_20() const { return ____operatingCurve_20; }
	inline int32_t* get_address_of__operatingCurve_20() { return &____operatingCurve_20; }
	inline void set__operatingCurve_20(int32_t value)
	{
		____operatingCurve_20 = value;
	}

	inline static int32_t get_offset_of__currentCurve_21() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____currentCurve_21)); }
	inline float get__currentCurve_21() const { return ____currentCurve_21; }
	inline float* get_address_of__currentCurve_21() { return &____currentCurve_21; }
	inline void set__currentCurve_21(float value)
	{
		____currentCurve_21 = value;
	}

	inline static int32_t get_offset_of__removeRenderers_22() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____removeRenderers_22)); }
	inline bool get__removeRenderers_22() const { return ____removeRenderers_22; }
	inline bool* get_address_of__removeRenderers_22() { return &____removeRenderers_22; }
	inline void set__removeRenderers_22(bool value)
	{
		____removeRenderers_22 = value;
	}

	inline static int32_t get_offset_of__previousLength_23() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____previousLength_23)); }
	inline int32_t get__previousLength_23() const { return ____previousLength_23; }
	inline int32_t* get_address_of__previousLength_23() { return &____previousLength_23; }
	inline void set__previousLength_23(int32_t value)
	{
		____previousLength_23 = value;
	}

	inline static int32_t get_offset_of__slicesPerCurve_24() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____slicesPerCurve_24)); }
	inline int32_t get__slicesPerCurve_24() const { return ____slicesPerCurve_24; }
	inline int32_t* get_address_of__slicesPerCurve_24() { return &____slicesPerCurve_24; }
	inline void set__slicesPerCurve_24(int32_t value)
	{
		____slicesPerCurve_24 = value;
	}

	inline static int32_t get_offset_of__splineReparams_25() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____splineReparams_25)); }
	inline List_1_t801671833 * get__splineReparams_25() const { return ____splineReparams_25; }
	inline List_1_t801671833 ** get_address_of__splineReparams_25() { return &____splineReparams_25; }
	inline void set__splineReparams_25(List_1_t801671833 * value)
	{
		____splineReparams_25 = value;
		Il2CppCodeGenWriteBarrier((&____splineReparams_25), value);
	}

	inline static int32_t get_offset_of__lengthDirty_26() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ____lengthDirty_26)); }
	inline bool get__lengthDirty_26() const { return ____lengthDirty_26; }
	inline bool* get_address_of__lengthDirty_26() { return &____lengthDirty_26; }
	inline void set__lengthDirty_26(bool value)
	{
		____lengthDirty_26 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(Spline_t1246320346, ___U3CLengthU3Ek__BackingField_27)); }
	inline float get_U3CLengthU3Ek__BackingField_27() const { return ___U3CLengthU3Ek__BackingField_27; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_27() { return &___U3CLengthU3Ek__BackingField_27; }
	inline void set_U3CLengthU3Ek__BackingField_27(float value)
	{
		___U3CLengthU3Ek__BackingField_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINE_T1246320346_H
#ifndef SPLINEANCHOR_T2835267765_H
#define SPLINEANCHOR_T2835267765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.SplineAnchor
struct  SplineAnchor_t2835267765  : public MonoBehaviour_t3962482529
{
public:
	// Pixelplacement.TangentMode Pixelplacement.SplineAnchor::tangentMode
	int32_t ___tangentMode_4;
	// System.Boolean Pixelplacement.SplineAnchor::<RenderingChange>k__BackingField
	bool ___U3CRenderingChangeU3Ek__BackingField_5;
	// System.Boolean Pixelplacement.SplineAnchor::<Changed>k__BackingField
	bool ___U3CChangedU3Ek__BackingField_6;
	// System.Boolean Pixelplacement.SplineAnchor::_initialized
	bool ____initialized_7;
	// UnityEngine.Transform Pixelplacement.SplineAnchor::_masterTangent
	Transform_t3600365921 * ____masterTangent_8;
	// UnityEngine.Transform Pixelplacement.SplineAnchor::_slaveTangent
	Transform_t3600365921 * ____slaveTangent_9;
	// Pixelplacement.TangentMode Pixelplacement.SplineAnchor::_previousTangentMode
	int32_t ____previousTangentMode_10;
	// UnityEngine.Vector3 Pixelplacement.SplineAnchor::_previousInPosition
	Vector3_t3722313464  ____previousInPosition_11;
	// UnityEngine.Vector3 Pixelplacement.SplineAnchor::_previousOutPosition
	Vector3_t3722313464  ____previousOutPosition_12;
	// UnityEngine.Vector3 Pixelplacement.SplineAnchor::_previousAnchorPosition
	Vector3_t3722313464  ____previousAnchorPosition_13;
	// UnityEngine.Bounds Pixelplacement.SplineAnchor::_skinnedBounds
	Bounds_t2266837910  ____skinnedBounds_14;
	// UnityEngine.Transform Pixelplacement.SplineAnchor::_anchor
	Transform_t3600365921 * ____anchor_15;
	// UnityEngine.Transform Pixelplacement.SplineAnchor::_inTangent
	Transform_t3600365921 * ____inTangent_16;
	// UnityEngine.Transform Pixelplacement.SplineAnchor::_outTangent
	Transform_t3600365921 * ____outTangent_17;

public:
	inline static int32_t get_offset_of_tangentMode_4() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ___tangentMode_4)); }
	inline int32_t get_tangentMode_4() const { return ___tangentMode_4; }
	inline int32_t* get_address_of_tangentMode_4() { return &___tangentMode_4; }
	inline void set_tangentMode_4(int32_t value)
	{
		___tangentMode_4 = value;
	}

	inline static int32_t get_offset_of_U3CRenderingChangeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ___U3CRenderingChangeU3Ek__BackingField_5)); }
	inline bool get_U3CRenderingChangeU3Ek__BackingField_5() const { return ___U3CRenderingChangeU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CRenderingChangeU3Ek__BackingField_5() { return &___U3CRenderingChangeU3Ek__BackingField_5; }
	inline void set_U3CRenderingChangeU3Ek__BackingField_5(bool value)
	{
		___U3CRenderingChangeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CChangedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ___U3CChangedU3Ek__BackingField_6)); }
	inline bool get_U3CChangedU3Ek__BackingField_6() const { return ___U3CChangedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CChangedU3Ek__BackingField_6() { return &___U3CChangedU3Ek__BackingField_6; }
	inline void set_U3CChangedU3Ek__BackingField_6(bool value)
	{
		___U3CChangedU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__initialized_7() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____initialized_7)); }
	inline bool get__initialized_7() const { return ____initialized_7; }
	inline bool* get_address_of__initialized_7() { return &____initialized_7; }
	inline void set__initialized_7(bool value)
	{
		____initialized_7 = value;
	}

	inline static int32_t get_offset_of__masterTangent_8() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____masterTangent_8)); }
	inline Transform_t3600365921 * get__masterTangent_8() const { return ____masterTangent_8; }
	inline Transform_t3600365921 ** get_address_of__masterTangent_8() { return &____masterTangent_8; }
	inline void set__masterTangent_8(Transform_t3600365921 * value)
	{
		____masterTangent_8 = value;
		Il2CppCodeGenWriteBarrier((&____masterTangent_8), value);
	}

	inline static int32_t get_offset_of__slaveTangent_9() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____slaveTangent_9)); }
	inline Transform_t3600365921 * get__slaveTangent_9() const { return ____slaveTangent_9; }
	inline Transform_t3600365921 ** get_address_of__slaveTangent_9() { return &____slaveTangent_9; }
	inline void set__slaveTangent_9(Transform_t3600365921 * value)
	{
		____slaveTangent_9 = value;
		Il2CppCodeGenWriteBarrier((&____slaveTangent_9), value);
	}

	inline static int32_t get_offset_of__previousTangentMode_10() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____previousTangentMode_10)); }
	inline int32_t get__previousTangentMode_10() const { return ____previousTangentMode_10; }
	inline int32_t* get_address_of__previousTangentMode_10() { return &____previousTangentMode_10; }
	inline void set__previousTangentMode_10(int32_t value)
	{
		____previousTangentMode_10 = value;
	}

	inline static int32_t get_offset_of__previousInPosition_11() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____previousInPosition_11)); }
	inline Vector3_t3722313464  get__previousInPosition_11() const { return ____previousInPosition_11; }
	inline Vector3_t3722313464 * get_address_of__previousInPosition_11() { return &____previousInPosition_11; }
	inline void set__previousInPosition_11(Vector3_t3722313464  value)
	{
		____previousInPosition_11 = value;
	}

	inline static int32_t get_offset_of__previousOutPosition_12() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____previousOutPosition_12)); }
	inline Vector3_t3722313464  get__previousOutPosition_12() const { return ____previousOutPosition_12; }
	inline Vector3_t3722313464 * get_address_of__previousOutPosition_12() { return &____previousOutPosition_12; }
	inline void set__previousOutPosition_12(Vector3_t3722313464  value)
	{
		____previousOutPosition_12 = value;
	}

	inline static int32_t get_offset_of__previousAnchorPosition_13() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____previousAnchorPosition_13)); }
	inline Vector3_t3722313464  get__previousAnchorPosition_13() const { return ____previousAnchorPosition_13; }
	inline Vector3_t3722313464 * get_address_of__previousAnchorPosition_13() { return &____previousAnchorPosition_13; }
	inline void set__previousAnchorPosition_13(Vector3_t3722313464  value)
	{
		____previousAnchorPosition_13 = value;
	}

	inline static int32_t get_offset_of__skinnedBounds_14() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____skinnedBounds_14)); }
	inline Bounds_t2266837910  get__skinnedBounds_14() const { return ____skinnedBounds_14; }
	inline Bounds_t2266837910 * get_address_of__skinnedBounds_14() { return &____skinnedBounds_14; }
	inline void set__skinnedBounds_14(Bounds_t2266837910  value)
	{
		____skinnedBounds_14 = value;
	}

	inline static int32_t get_offset_of__anchor_15() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____anchor_15)); }
	inline Transform_t3600365921 * get__anchor_15() const { return ____anchor_15; }
	inline Transform_t3600365921 ** get_address_of__anchor_15() { return &____anchor_15; }
	inline void set__anchor_15(Transform_t3600365921 * value)
	{
		____anchor_15 = value;
		Il2CppCodeGenWriteBarrier((&____anchor_15), value);
	}

	inline static int32_t get_offset_of__inTangent_16() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____inTangent_16)); }
	inline Transform_t3600365921 * get__inTangent_16() const { return ____inTangent_16; }
	inline Transform_t3600365921 ** get_address_of__inTangent_16() { return &____inTangent_16; }
	inline void set__inTangent_16(Transform_t3600365921 * value)
	{
		____inTangent_16 = value;
		Il2CppCodeGenWriteBarrier((&____inTangent_16), value);
	}

	inline static int32_t get_offset_of__outTangent_17() { return static_cast<int32_t>(offsetof(SplineAnchor_t2835267765, ____outTangent_17)); }
	inline Transform_t3600365921 * get__outTangent_17() const { return ____outTangent_17; }
	inline Transform_t3600365921 ** get_address_of__outTangent_17() { return &____outTangent_17; }
	inline void set__outTangent_17(Transform_t3600365921 * value)
	{
		____outTangent_17 = value;
		Il2CppCodeGenWriteBarrier((&____outTangent_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINEANCHOR_T2835267765_H
#ifndef SPLINERENDERER_T731933917_H
#define SPLINERENDERER_T731933917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.SplineRenderer
struct  SplineRenderer_t731933917  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Pixelplacement.SplineRenderer::segmentsPerCurve
	int32_t ___segmentsPerCurve_4;
	// System.Single Pixelplacement.SplineRenderer::startPercentage
	float ___startPercentage_5;
	// System.Single Pixelplacement.SplineRenderer::endPercentage
	float ___endPercentage_6;
	// UnityEngine.LineRenderer Pixelplacement.SplineRenderer::_lineRenderer
	LineRenderer_t3154350270 * ____lineRenderer_7;
	// Pixelplacement.Spline Pixelplacement.SplineRenderer::_spline
	Spline_t1246320346 * ____spline_8;
	// System.Boolean Pixelplacement.SplineRenderer::_initialized
	bool ____initialized_9;
	// System.Int32 Pixelplacement.SplineRenderer::_previousAnchorsLength
	int32_t ____previousAnchorsLength_10;
	// System.Int32 Pixelplacement.SplineRenderer::_previousSegmentsPerCurve
	int32_t ____previousSegmentsPerCurve_11;
	// System.Int32 Pixelplacement.SplineRenderer::_vertexCount
	int32_t ____vertexCount_12;
	// System.Single Pixelplacement.SplineRenderer::_previousStart
	float ____previousStart_13;
	// System.Single Pixelplacement.SplineRenderer::_previousEnd
	float ____previousEnd_14;

public:
	inline static int32_t get_offset_of_segmentsPerCurve_4() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ___segmentsPerCurve_4)); }
	inline int32_t get_segmentsPerCurve_4() const { return ___segmentsPerCurve_4; }
	inline int32_t* get_address_of_segmentsPerCurve_4() { return &___segmentsPerCurve_4; }
	inline void set_segmentsPerCurve_4(int32_t value)
	{
		___segmentsPerCurve_4 = value;
	}

	inline static int32_t get_offset_of_startPercentage_5() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ___startPercentage_5)); }
	inline float get_startPercentage_5() const { return ___startPercentage_5; }
	inline float* get_address_of_startPercentage_5() { return &___startPercentage_5; }
	inline void set_startPercentage_5(float value)
	{
		___startPercentage_5 = value;
	}

	inline static int32_t get_offset_of_endPercentage_6() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ___endPercentage_6)); }
	inline float get_endPercentage_6() const { return ___endPercentage_6; }
	inline float* get_address_of_endPercentage_6() { return &___endPercentage_6; }
	inline void set_endPercentage_6(float value)
	{
		___endPercentage_6 = value;
	}

	inline static int32_t get_offset_of__lineRenderer_7() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____lineRenderer_7)); }
	inline LineRenderer_t3154350270 * get__lineRenderer_7() const { return ____lineRenderer_7; }
	inline LineRenderer_t3154350270 ** get_address_of__lineRenderer_7() { return &____lineRenderer_7; }
	inline void set__lineRenderer_7(LineRenderer_t3154350270 * value)
	{
		____lineRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((&____lineRenderer_7), value);
	}

	inline static int32_t get_offset_of__spline_8() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____spline_8)); }
	inline Spline_t1246320346 * get__spline_8() const { return ____spline_8; }
	inline Spline_t1246320346 ** get_address_of__spline_8() { return &____spline_8; }
	inline void set__spline_8(Spline_t1246320346 * value)
	{
		____spline_8 = value;
		Il2CppCodeGenWriteBarrier((&____spline_8), value);
	}

	inline static int32_t get_offset_of__initialized_9() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____initialized_9)); }
	inline bool get__initialized_9() const { return ____initialized_9; }
	inline bool* get_address_of__initialized_9() { return &____initialized_9; }
	inline void set__initialized_9(bool value)
	{
		____initialized_9 = value;
	}

	inline static int32_t get_offset_of__previousAnchorsLength_10() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____previousAnchorsLength_10)); }
	inline int32_t get__previousAnchorsLength_10() const { return ____previousAnchorsLength_10; }
	inline int32_t* get_address_of__previousAnchorsLength_10() { return &____previousAnchorsLength_10; }
	inline void set__previousAnchorsLength_10(int32_t value)
	{
		____previousAnchorsLength_10 = value;
	}

	inline static int32_t get_offset_of__previousSegmentsPerCurve_11() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____previousSegmentsPerCurve_11)); }
	inline int32_t get__previousSegmentsPerCurve_11() const { return ____previousSegmentsPerCurve_11; }
	inline int32_t* get_address_of__previousSegmentsPerCurve_11() { return &____previousSegmentsPerCurve_11; }
	inline void set__previousSegmentsPerCurve_11(int32_t value)
	{
		____previousSegmentsPerCurve_11 = value;
	}

	inline static int32_t get_offset_of__vertexCount_12() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____vertexCount_12)); }
	inline int32_t get__vertexCount_12() const { return ____vertexCount_12; }
	inline int32_t* get_address_of__vertexCount_12() { return &____vertexCount_12; }
	inline void set__vertexCount_12(int32_t value)
	{
		____vertexCount_12 = value;
	}

	inline static int32_t get_offset_of__previousStart_13() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____previousStart_13)); }
	inline float get__previousStart_13() const { return ____previousStart_13; }
	inline float* get_address_of__previousStart_13() { return &____previousStart_13; }
	inline void set__previousStart_13(float value)
	{
		____previousStart_13 = value;
	}

	inline static int32_t get_offset_of__previousEnd_14() { return static_cast<int32_t>(offsetof(SplineRenderer_t731933917, ____previousEnd_14)); }
	inline float get__previousEnd_14() const { return ____previousEnd_14; }
	inline float* get_address_of__previousEnd_14() { return &____previousEnd_14; }
	inline void set__previousEnd_14(float value)
	{
		____previousEnd_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINERENDERER_T731933917_H
#ifndef SPLINETANGENT_T405982707_H
#define SPLINETANGENT_T405982707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.SplineTangent
struct  SplineTangent_t405982707  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINETANGENT_T405982707_H
#ifndef STATE_T1781471564_H
#define STATE_T1781471564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.State
struct  State_t1781471564  : public MonoBehaviour_t3962482529
{
public:
	// Pixelplacement.StateMachine Pixelplacement.State::_stateMachine
	StateMachine_t4243775295 * ____stateMachine_4;

public:
	inline static int32_t get_offset_of__stateMachine_4() { return static_cast<int32_t>(offsetof(State_t1781471564, ____stateMachine_4)); }
	inline StateMachine_t4243775295 * get__stateMachine_4() const { return ____stateMachine_4; }
	inline StateMachine_t4243775295 ** get_address_of__stateMachine_4() { return &____stateMachine_4; }
	inline void set__stateMachine_4(StateMachine_t4243775295 * value)
	{
		____stateMachine_4 = value;
		Il2CppCodeGenWriteBarrier((&____stateMachine_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T1781471564_H
#ifndef STATEMACHINE_T4243775295_H
#define STATEMACHINE_T4243775295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.StateMachine
struct  StateMachine_t4243775295  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Pixelplacement.StateMachine::defaultState
	GameObject_t1113636619 * ___defaultState_4;
	// UnityEngine.GameObject Pixelplacement.StateMachine::currentState
	GameObject_t1113636619 * ___currentState_5;
	// System.Boolean Pixelplacement.StateMachine::_unityEventsFolded
	bool ____unityEventsFolded_6;
	// System.Boolean Pixelplacement.StateMachine::verbose
	bool ___verbose_7;
	// System.Boolean Pixelplacement.StateMachine::allowReentry
	bool ___allowReentry_8;
	// System.Boolean Pixelplacement.StateMachine::returnToDefaultOnDisable
	bool ___returnToDefaultOnDisable_9;
	// Pixelplacement.GameObjectEvent Pixelplacement.StateMachine::OnStateExited
	GameObjectEvent_t2867809866 * ___OnStateExited_10;
	// Pixelplacement.GameObjectEvent Pixelplacement.StateMachine::OnStateEntered
	GameObjectEvent_t2867809866 * ___OnStateEntered_11;
	// UnityEngine.Events.UnityEvent Pixelplacement.StateMachine::OnFirstStateEntered
	UnityEvent_t2581268647 * ___OnFirstStateEntered_12;
	// UnityEngine.Events.UnityEvent Pixelplacement.StateMachine::OnFirstStateExited
	UnityEvent_t2581268647 * ___OnFirstStateExited_13;
	// UnityEngine.Events.UnityEvent Pixelplacement.StateMachine::OnLastStateEntered
	UnityEvent_t2581268647 * ___OnLastStateEntered_14;
	// UnityEngine.Events.UnityEvent Pixelplacement.StateMachine::OnLastStateExited
	UnityEvent_t2581268647 * ___OnLastStateExited_15;
	// System.Boolean Pixelplacement.StateMachine::<CleanSetup>k__BackingField
	bool ___U3CCleanSetupU3Ek__BackingField_16;
	// System.Boolean Pixelplacement.StateMachine::_initialized
	bool ____initialized_17;
	// System.Boolean Pixelplacement.StateMachine::_atFirst
	bool ____atFirst_18;
	// System.Boolean Pixelplacement.StateMachine::_atLast
	bool ____atLast_19;

public:
	inline static int32_t get_offset_of_defaultState_4() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___defaultState_4)); }
	inline GameObject_t1113636619 * get_defaultState_4() const { return ___defaultState_4; }
	inline GameObject_t1113636619 ** get_address_of_defaultState_4() { return &___defaultState_4; }
	inline void set_defaultState_4(GameObject_t1113636619 * value)
	{
		___defaultState_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultState_4), value);
	}

	inline static int32_t get_offset_of_currentState_5() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___currentState_5)); }
	inline GameObject_t1113636619 * get_currentState_5() const { return ___currentState_5; }
	inline GameObject_t1113636619 ** get_address_of_currentState_5() { return &___currentState_5; }
	inline void set_currentState_5(GameObject_t1113636619 * value)
	{
		___currentState_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentState_5), value);
	}

	inline static int32_t get_offset_of__unityEventsFolded_6() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ____unityEventsFolded_6)); }
	inline bool get__unityEventsFolded_6() const { return ____unityEventsFolded_6; }
	inline bool* get_address_of__unityEventsFolded_6() { return &____unityEventsFolded_6; }
	inline void set__unityEventsFolded_6(bool value)
	{
		____unityEventsFolded_6 = value;
	}

	inline static int32_t get_offset_of_verbose_7() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___verbose_7)); }
	inline bool get_verbose_7() const { return ___verbose_7; }
	inline bool* get_address_of_verbose_7() { return &___verbose_7; }
	inline void set_verbose_7(bool value)
	{
		___verbose_7 = value;
	}

	inline static int32_t get_offset_of_allowReentry_8() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___allowReentry_8)); }
	inline bool get_allowReentry_8() const { return ___allowReentry_8; }
	inline bool* get_address_of_allowReentry_8() { return &___allowReentry_8; }
	inline void set_allowReentry_8(bool value)
	{
		___allowReentry_8 = value;
	}

	inline static int32_t get_offset_of_returnToDefaultOnDisable_9() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___returnToDefaultOnDisable_9)); }
	inline bool get_returnToDefaultOnDisable_9() const { return ___returnToDefaultOnDisable_9; }
	inline bool* get_address_of_returnToDefaultOnDisable_9() { return &___returnToDefaultOnDisable_9; }
	inline void set_returnToDefaultOnDisable_9(bool value)
	{
		___returnToDefaultOnDisable_9 = value;
	}

	inline static int32_t get_offset_of_OnStateExited_10() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___OnStateExited_10)); }
	inline GameObjectEvent_t2867809866 * get_OnStateExited_10() const { return ___OnStateExited_10; }
	inline GameObjectEvent_t2867809866 ** get_address_of_OnStateExited_10() { return &___OnStateExited_10; }
	inline void set_OnStateExited_10(GameObjectEvent_t2867809866 * value)
	{
		___OnStateExited_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateExited_10), value);
	}

	inline static int32_t get_offset_of_OnStateEntered_11() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___OnStateEntered_11)); }
	inline GameObjectEvent_t2867809866 * get_OnStateEntered_11() const { return ___OnStateEntered_11; }
	inline GameObjectEvent_t2867809866 ** get_address_of_OnStateEntered_11() { return &___OnStateEntered_11; }
	inline void set_OnStateEntered_11(GameObjectEvent_t2867809866 * value)
	{
		___OnStateEntered_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateEntered_11), value);
	}

	inline static int32_t get_offset_of_OnFirstStateEntered_12() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___OnFirstStateEntered_12)); }
	inline UnityEvent_t2581268647 * get_OnFirstStateEntered_12() const { return ___OnFirstStateEntered_12; }
	inline UnityEvent_t2581268647 ** get_address_of_OnFirstStateEntered_12() { return &___OnFirstStateEntered_12; }
	inline void set_OnFirstStateEntered_12(UnityEvent_t2581268647 * value)
	{
		___OnFirstStateEntered_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnFirstStateEntered_12), value);
	}

	inline static int32_t get_offset_of_OnFirstStateExited_13() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___OnFirstStateExited_13)); }
	inline UnityEvent_t2581268647 * get_OnFirstStateExited_13() const { return ___OnFirstStateExited_13; }
	inline UnityEvent_t2581268647 ** get_address_of_OnFirstStateExited_13() { return &___OnFirstStateExited_13; }
	inline void set_OnFirstStateExited_13(UnityEvent_t2581268647 * value)
	{
		___OnFirstStateExited_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnFirstStateExited_13), value);
	}

	inline static int32_t get_offset_of_OnLastStateEntered_14() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___OnLastStateEntered_14)); }
	inline UnityEvent_t2581268647 * get_OnLastStateEntered_14() const { return ___OnLastStateEntered_14; }
	inline UnityEvent_t2581268647 ** get_address_of_OnLastStateEntered_14() { return &___OnLastStateEntered_14; }
	inline void set_OnLastStateEntered_14(UnityEvent_t2581268647 * value)
	{
		___OnLastStateEntered_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnLastStateEntered_14), value);
	}

	inline static int32_t get_offset_of_OnLastStateExited_15() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___OnLastStateExited_15)); }
	inline UnityEvent_t2581268647 * get_OnLastStateExited_15() const { return ___OnLastStateExited_15; }
	inline UnityEvent_t2581268647 ** get_address_of_OnLastStateExited_15() { return &___OnLastStateExited_15; }
	inline void set_OnLastStateExited_15(UnityEvent_t2581268647 * value)
	{
		___OnLastStateExited_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnLastStateExited_15), value);
	}

	inline static int32_t get_offset_of_U3CCleanSetupU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ___U3CCleanSetupU3Ek__BackingField_16)); }
	inline bool get_U3CCleanSetupU3Ek__BackingField_16() const { return ___U3CCleanSetupU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CCleanSetupU3Ek__BackingField_16() { return &___U3CCleanSetupU3Ek__BackingField_16; }
	inline void set_U3CCleanSetupU3Ek__BackingField_16(bool value)
	{
		___U3CCleanSetupU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of__initialized_17() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ____initialized_17)); }
	inline bool get__initialized_17() const { return ____initialized_17; }
	inline bool* get_address_of__initialized_17() { return &____initialized_17; }
	inline void set__initialized_17(bool value)
	{
		____initialized_17 = value;
	}

	inline static int32_t get_offset_of__atFirst_18() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ____atFirst_18)); }
	inline bool get__atFirst_18() const { return ____atFirst_18; }
	inline bool* get_address_of__atFirst_18() { return &____atFirst_18; }
	inline void set__atFirst_18(bool value)
	{
		____atFirst_18 = value;
	}

	inline static int32_t get_offset_of__atLast_19() { return static_cast<int32_t>(offsetof(StateMachine_t4243775295, ____atLast_19)); }
	inline bool get__atLast_19() const { return ____atLast_19; }
	inline bool* get_address_of__atLast_19() { return &____atLast_19; }
	inline void set__atLast_19(bool value)
	{
		____atLast_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINE_T4243775295_H
#ifndef TWEENENGINE_T2258245354_H
#define TWEENENGINE_T2258245354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.TweenEngine
struct  TweenEngine_t2258245354  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENENGINE_T2258245354_H
#ifndef TWEENUTILITIES_T308270123_H
#define TWEENUTILITIES_T308270123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.TweenSystem.TweenUtilities
struct  TweenUtilities_t308270123  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENUTILITIES_T308270123_H
#ifndef SPLINECONTROLLEDPARTICLESYSTEM_T1873087847_H
#define SPLINECONTROLLEDPARTICLESYSTEM_T1873087847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SplineControlledParticleSystem
struct  SplineControlledParticleSystem_t1873087847  : public MonoBehaviour_t3962482529
{
public:
	// System.Single SplineControlledParticleSystem::startRadius
	float ___startRadius_4;
	// System.Single SplineControlledParticleSystem::endRadius
	float ___endRadius_5;
	// UnityEngine.ParticleSystem SplineControlledParticleSystem::_particleSystem
	ParticleSystem_t1800779281 * ____particleSystem_6;
	// Pixelplacement.Spline SplineControlledParticleSystem::_spline
	Spline_t1246320346 * ____spline_7;
	// UnityEngine.ParticleSystem/Particle[] SplineControlledParticleSystem::_particles
	ParticleU5BU5D_t3069227754* ____particles_8;

public:
	inline static int32_t get_offset_of_startRadius_4() { return static_cast<int32_t>(offsetof(SplineControlledParticleSystem_t1873087847, ___startRadius_4)); }
	inline float get_startRadius_4() const { return ___startRadius_4; }
	inline float* get_address_of_startRadius_4() { return &___startRadius_4; }
	inline void set_startRadius_4(float value)
	{
		___startRadius_4 = value;
	}

	inline static int32_t get_offset_of_endRadius_5() { return static_cast<int32_t>(offsetof(SplineControlledParticleSystem_t1873087847, ___endRadius_5)); }
	inline float get_endRadius_5() const { return ___endRadius_5; }
	inline float* get_address_of_endRadius_5() { return &___endRadius_5; }
	inline void set_endRadius_5(float value)
	{
		___endRadius_5 = value;
	}

	inline static int32_t get_offset_of__particleSystem_6() { return static_cast<int32_t>(offsetof(SplineControlledParticleSystem_t1873087847, ____particleSystem_6)); }
	inline ParticleSystem_t1800779281 * get__particleSystem_6() const { return ____particleSystem_6; }
	inline ParticleSystem_t1800779281 ** get_address_of__particleSystem_6() { return &____particleSystem_6; }
	inline void set__particleSystem_6(ParticleSystem_t1800779281 * value)
	{
		____particleSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&____particleSystem_6), value);
	}

	inline static int32_t get_offset_of__spline_7() { return static_cast<int32_t>(offsetof(SplineControlledParticleSystem_t1873087847, ____spline_7)); }
	inline Spline_t1246320346 * get__spline_7() const { return ____spline_7; }
	inline Spline_t1246320346 ** get_address_of__spline_7() { return &____spline_7; }
	inline void set__spline_7(Spline_t1246320346 * value)
	{
		____spline_7 = value;
		Il2CppCodeGenWriteBarrier((&____spline_7), value);
	}

	inline static int32_t get_offset_of__particles_8() { return static_cast<int32_t>(offsetof(SplineControlledParticleSystem_t1873087847, ____particles_8)); }
	inline ParticleU5BU5D_t3069227754* get__particles_8() const { return ____particles_8; }
	inline ParticleU5BU5D_t3069227754** get_address_of__particles_8() { return &____particles_8; }
	inline void set__particles_8(ParticleU5BU5D_t3069227754* value)
	{
		____particles_8 = value;
		Il2CppCodeGenWriteBarrier((&____particles_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPLINECONTROLLEDPARTICLESYSTEM_T1873087847_H
#ifndef NETWORKDISCOVERY_T986186286_H
#define NETWORKDISCOVERY_T986186286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.NetworkDiscovery
struct  NetworkDiscovery_t986186286  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastPort
	int32_t ___m_BroadcastPort_5;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastKey
	int32_t ___m_BroadcastKey_6;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastVersion
	int32_t ___m_BroadcastVersion_7;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastSubVersion
	int32_t ___m_BroadcastSubVersion_8;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_BroadcastInterval
	int32_t ___m_BroadcastInterval_9;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_UseNetworkManager
	bool ___m_UseNetworkManager_10;
	// System.String UnityEngine.Networking.NetworkDiscovery::m_BroadcastData
	String_t* ___m_BroadcastData_11;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_ShowGUI
	bool ___m_ShowGUI_12;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetX
	int32_t ___m_OffsetX_13;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_OffsetY
	int32_t ___m_OffsetY_14;
	// System.Int32 UnityEngine.Networking.NetworkDiscovery::m_HostId
	int32_t ___m_HostId_15;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_Running
	bool ___m_Running_16;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsServer
	bool ___m_IsServer_17;
	// System.Boolean UnityEngine.Networking.NetworkDiscovery::m_IsClient
	bool ___m_IsClient_18;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgOutBuffer
	ByteU5BU5D_t4116647657* ___m_MsgOutBuffer_19;
	// System.Byte[] UnityEngine.Networking.NetworkDiscovery::m_MsgInBuffer
	ByteU5BU5D_t4116647657* ___m_MsgInBuffer_20;
	// UnityEngine.Networking.HostTopology UnityEngine.Networking.NetworkDiscovery::m_DefaultTopology
	HostTopology_t4059263395 * ___m_DefaultTopology_21;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Networking.NetworkBroadcastResult> UnityEngine.Networking.NetworkDiscovery::m_BroadcastsReceived
	Dictionary_2_t1959671187 * ___m_BroadcastsReceived_22;

public:
	inline static int32_t get_offset_of_m_BroadcastPort_5() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastPort_5)); }
	inline int32_t get_m_BroadcastPort_5() const { return ___m_BroadcastPort_5; }
	inline int32_t* get_address_of_m_BroadcastPort_5() { return &___m_BroadcastPort_5; }
	inline void set_m_BroadcastPort_5(int32_t value)
	{
		___m_BroadcastPort_5 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastKey_6() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastKey_6)); }
	inline int32_t get_m_BroadcastKey_6() const { return ___m_BroadcastKey_6; }
	inline int32_t* get_address_of_m_BroadcastKey_6() { return &___m_BroadcastKey_6; }
	inline void set_m_BroadcastKey_6(int32_t value)
	{
		___m_BroadcastKey_6 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastVersion_7() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastVersion_7)); }
	inline int32_t get_m_BroadcastVersion_7() const { return ___m_BroadcastVersion_7; }
	inline int32_t* get_address_of_m_BroadcastVersion_7() { return &___m_BroadcastVersion_7; }
	inline void set_m_BroadcastVersion_7(int32_t value)
	{
		___m_BroadcastVersion_7 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastSubVersion_8() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastSubVersion_8)); }
	inline int32_t get_m_BroadcastSubVersion_8() const { return ___m_BroadcastSubVersion_8; }
	inline int32_t* get_address_of_m_BroadcastSubVersion_8() { return &___m_BroadcastSubVersion_8; }
	inline void set_m_BroadcastSubVersion_8(int32_t value)
	{
		___m_BroadcastSubVersion_8 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastInterval_9() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastInterval_9)); }
	inline int32_t get_m_BroadcastInterval_9() const { return ___m_BroadcastInterval_9; }
	inline int32_t* get_address_of_m_BroadcastInterval_9() { return &___m_BroadcastInterval_9; }
	inline void set_m_BroadcastInterval_9(int32_t value)
	{
		___m_BroadcastInterval_9 = value;
	}

	inline static int32_t get_offset_of_m_UseNetworkManager_10() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_UseNetworkManager_10)); }
	inline bool get_m_UseNetworkManager_10() const { return ___m_UseNetworkManager_10; }
	inline bool* get_address_of_m_UseNetworkManager_10() { return &___m_UseNetworkManager_10; }
	inline void set_m_UseNetworkManager_10(bool value)
	{
		___m_UseNetworkManager_10 = value;
	}

	inline static int32_t get_offset_of_m_BroadcastData_11() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastData_11)); }
	inline String_t* get_m_BroadcastData_11() const { return ___m_BroadcastData_11; }
	inline String_t** get_address_of_m_BroadcastData_11() { return &___m_BroadcastData_11; }
	inline void set_m_BroadcastData_11(String_t* value)
	{
		___m_BroadcastData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastData_11), value);
	}

	inline static int32_t get_offset_of_m_ShowGUI_12() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_ShowGUI_12)); }
	inline bool get_m_ShowGUI_12() const { return ___m_ShowGUI_12; }
	inline bool* get_address_of_m_ShowGUI_12() { return &___m_ShowGUI_12; }
	inline void set_m_ShowGUI_12(bool value)
	{
		___m_ShowGUI_12 = value;
	}

	inline static int32_t get_offset_of_m_OffsetX_13() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_OffsetX_13)); }
	inline int32_t get_m_OffsetX_13() const { return ___m_OffsetX_13; }
	inline int32_t* get_address_of_m_OffsetX_13() { return &___m_OffsetX_13; }
	inline void set_m_OffsetX_13(int32_t value)
	{
		___m_OffsetX_13 = value;
	}

	inline static int32_t get_offset_of_m_OffsetY_14() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_OffsetY_14)); }
	inline int32_t get_m_OffsetY_14() const { return ___m_OffsetY_14; }
	inline int32_t* get_address_of_m_OffsetY_14() { return &___m_OffsetY_14; }
	inline void set_m_OffsetY_14(int32_t value)
	{
		___m_OffsetY_14 = value;
	}

	inline static int32_t get_offset_of_m_HostId_15() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_HostId_15)); }
	inline int32_t get_m_HostId_15() const { return ___m_HostId_15; }
	inline int32_t* get_address_of_m_HostId_15() { return &___m_HostId_15; }
	inline void set_m_HostId_15(int32_t value)
	{
		___m_HostId_15 = value;
	}

	inline static int32_t get_offset_of_m_Running_16() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_Running_16)); }
	inline bool get_m_Running_16() const { return ___m_Running_16; }
	inline bool* get_address_of_m_Running_16() { return &___m_Running_16; }
	inline void set_m_Running_16(bool value)
	{
		___m_Running_16 = value;
	}

	inline static int32_t get_offset_of_m_IsServer_17() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_IsServer_17)); }
	inline bool get_m_IsServer_17() const { return ___m_IsServer_17; }
	inline bool* get_address_of_m_IsServer_17() { return &___m_IsServer_17; }
	inline void set_m_IsServer_17(bool value)
	{
		___m_IsServer_17 = value;
	}

	inline static int32_t get_offset_of_m_IsClient_18() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_IsClient_18)); }
	inline bool get_m_IsClient_18() const { return ___m_IsClient_18; }
	inline bool* get_address_of_m_IsClient_18() { return &___m_IsClient_18; }
	inline void set_m_IsClient_18(bool value)
	{
		___m_IsClient_18 = value;
	}

	inline static int32_t get_offset_of_m_MsgOutBuffer_19() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_MsgOutBuffer_19)); }
	inline ByteU5BU5D_t4116647657* get_m_MsgOutBuffer_19() const { return ___m_MsgOutBuffer_19; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_MsgOutBuffer_19() { return &___m_MsgOutBuffer_19; }
	inline void set_m_MsgOutBuffer_19(ByteU5BU5D_t4116647657* value)
	{
		___m_MsgOutBuffer_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgOutBuffer_19), value);
	}

	inline static int32_t get_offset_of_m_MsgInBuffer_20() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_MsgInBuffer_20)); }
	inline ByteU5BU5D_t4116647657* get_m_MsgInBuffer_20() const { return ___m_MsgInBuffer_20; }
	inline ByteU5BU5D_t4116647657** get_address_of_m_MsgInBuffer_20() { return &___m_MsgInBuffer_20; }
	inline void set_m_MsgInBuffer_20(ByteU5BU5D_t4116647657* value)
	{
		___m_MsgInBuffer_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MsgInBuffer_20), value);
	}

	inline static int32_t get_offset_of_m_DefaultTopology_21() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_DefaultTopology_21)); }
	inline HostTopology_t4059263395 * get_m_DefaultTopology_21() const { return ___m_DefaultTopology_21; }
	inline HostTopology_t4059263395 ** get_address_of_m_DefaultTopology_21() { return &___m_DefaultTopology_21; }
	inline void set_m_DefaultTopology_21(HostTopology_t4059263395 * value)
	{
		___m_DefaultTopology_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultTopology_21), value);
	}

	inline static int32_t get_offset_of_m_BroadcastsReceived_22() { return static_cast<int32_t>(offsetof(NetworkDiscovery_t986186286, ___m_BroadcastsReceived_22)); }
	inline Dictionary_2_t1959671187 * get_m_BroadcastsReceived_22() const { return ___m_BroadcastsReceived_22; }
	inline Dictionary_2_t1959671187 ** get_address_of_m_BroadcastsReceived_22() { return &___m_BroadcastsReceived_22; }
	inline void set_m_BroadcastsReceived_22(Dictionary_2_t1959671187 * value)
	{
		___m_BroadcastsReceived_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_BroadcastsReceived_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKDISCOVERY_T986186286_H
#ifndef CLIENT_T1356520481_H
#define CLIENT_T1356520481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Client
struct  Client_t1356520481  : public NetworkDiscovery_t986186286
{
public:
	// System.Int32 Pixelplacement.Client::broadcastingPort
	int32_t ___broadcastingPort_53;
	// UnityEngine.Networking.QosType Pixelplacement.Client::primaryQualityOfService
	int32_t ___primaryQualityOfService_54;
	// UnityEngine.Networking.QosType Pixelplacement.Client::secondaryQualityOfService
	int32_t ___secondaryQualityOfService_55;
	// System.UInt32 Pixelplacement.Client::initialBandwidth
	uint32_t ___initialBandwidth_56;

public:
	inline static int32_t get_offset_of_broadcastingPort_53() { return static_cast<int32_t>(offsetof(Client_t1356520481, ___broadcastingPort_53)); }
	inline int32_t get_broadcastingPort_53() const { return ___broadcastingPort_53; }
	inline int32_t* get_address_of_broadcastingPort_53() { return &___broadcastingPort_53; }
	inline void set_broadcastingPort_53(int32_t value)
	{
		___broadcastingPort_53 = value;
	}

	inline static int32_t get_offset_of_primaryQualityOfService_54() { return static_cast<int32_t>(offsetof(Client_t1356520481, ___primaryQualityOfService_54)); }
	inline int32_t get_primaryQualityOfService_54() const { return ___primaryQualityOfService_54; }
	inline int32_t* get_address_of_primaryQualityOfService_54() { return &___primaryQualityOfService_54; }
	inline void set_primaryQualityOfService_54(int32_t value)
	{
		___primaryQualityOfService_54 = value;
	}

	inline static int32_t get_offset_of_secondaryQualityOfService_55() { return static_cast<int32_t>(offsetof(Client_t1356520481, ___secondaryQualityOfService_55)); }
	inline int32_t get_secondaryQualityOfService_55() const { return ___secondaryQualityOfService_55; }
	inline int32_t* get_address_of_secondaryQualityOfService_55() { return &___secondaryQualityOfService_55; }
	inline void set_secondaryQualityOfService_55(int32_t value)
	{
		___secondaryQualityOfService_55 = value;
	}

	inline static int32_t get_offset_of_initialBandwidth_56() { return static_cast<int32_t>(offsetof(Client_t1356520481, ___initialBandwidth_56)); }
	inline uint32_t get_initialBandwidth_56() const { return ___initialBandwidth_56; }
	inline uint32_t* get_address_of_initialBandwidth_56() { return &___initialBandwidth_56; }
	inline void set_initialBandwidth_56(uint32_t value)
	{
		___initialBandwidth_56 = value;
	}
};

struct Client_t1356520481_StaticFields
{
public:
	// System.Action`1<UnityEngine.Networking.NetworkError> Pixelplacement.Client::OnError
	Action_1_t2210661120 * ___OnError_23;
	// System.Action Pixelplacement.Client::OnConnected
	Action_t1264377477 * ___OnConnected_24;
	// System.Action Pixelplacement.Client::OnDisconnected
	Action_t1264377477 * ___OnDisconnected_25;
	// System.Action`1<Pixelplacement.ServerAvailableMessage> Pixelplacement.Client::OnServerAvailable
	Action_1_t257386514 * ___OnServerAvailable_26;
	// System.Action`1<Pixelplacement.FloatMessage> Pixelplacement.Client::OnFloat
	Action_1_t1704364940 * ___OnFloat_27;
	// System.Action`1<Pixelplacement.FloatArrayMessage> Pixelplacement.Client::OnFloatArray
	Action_1_t1733437200 * ___OnFloatArray_28;
	// System.Action`1<Pixelplacement.IntMessage> Pixelplacement.Client::OnInt
	Action_1_t581520478 * ___OnInt_29;
	// System.Action`1<Pixelplacement.IntArrayMessage> Pixelplacement.Client::OnIntArray
	Action_1_t712253493 * ___OnIntArray_30;
	// System.Action`1<Pixelplacement.Vector2Message> Pixelplacement.Client::OnVector2
	Action_1_t2063388938 * ___OnVector2_31;
	// System.Action`1<Pixelplacement.Vector2ArrayMessage> Pixelplacement.Client::OnVector2Array
	Action_1_t2571979132 * ___OnVector2Array_32;
	// System.Action`1<Pixelplacement.Vector3Message> Pixelplacement.Client::OnVector3
	Action_1_t2134757642 * ___OnVector3_33;
	// System.Action`1<Pixelplacement.Vector3ArrayMessage> Pixelplacement.Client::OnVector3Array
	Action_1_t900614475 * ___OnVector3Array_34;
	// System.Action`1<Pixelplacement.QuaternionMessage> Pixelplacement.Client::OnQuaternion
	Action_1_t3056131525 * ___OnQuaternion_35;
	// System.Action`1<Pixelplacement.QuaternionArrayMessage> Pixelplacement.Client::OnQuaternionArray
	Action_1_t2191742972 * ___OnQuaternionArray_36;
	// System.Action`1<Pixelplacement.Vector4Message> Pixelplacement.Client::OnVector4
	Action_1_t1635176714 * ___OnVector4_37;
	// System.Action`1<Pixelplacement.Vector4ArrayMessage> Pixelplacement.Client::OnVector4Array
	Action_1_t1628391807 * ___OnVector4Array_38;
	// System.Action`1<Pixelplacement.RectMessage> Pixelplacement.Client::OnRect
	Action_1_t1590883354 * ___OnRect_39;
	// System.Action`1<Pixelplacement.RectArrayMessage> Pixelplacement.Client::OnRectArray
	Action_1_t1842167626 * ___OnRectArray_40;
	// System.Action`1<Pixelplacement.StringMessage> Pixelplacement.Client::OnString
	Action_1_t640713362 * ___OnString_41;
	// System.Action`1<Pixelplacement.StringArrayMessage> Pixelplacement.Client::OnStringArray
	Action_1_t4240794820 * ___OnStringArray_42;
	// System.Action`1<Pixelplacement.ByteMessage> Pixelplacement.Client::OnByte
	Action_1_t3421907995 * ___OnByte_43;
	// System.Action`1<Pixelplacement.ByteArrayMessage> Pixelplacement.Client::OnByteArray
	Action_1_t2266368787 * ___OnByteArray_44;
	// System.Action`1<Pixelplacement.ColorMessage> Pixelplacement.Client::OnColor
	Action_1_t313855902 * ___OnColor_45;
	// System.Action`1<Pixelplacement.ColorArrayMessage> Pixelplacement.Client::OnColorArray
	Action_1_t1631047211 * ___OnColorArray_46;
	// System.Action`1<Pixelplacement.Color32Message> Pixelplacement.Client::OnColor32
	Action_1_t1709086795 * ___OnColor32_47;
	// System.Action`1<Pixelplacement.Color32ArrayMessage> Pixelplacement.Client::OnColor32Array
	Action_1_t261809231 * ___OnColor32Array_48;
	// System.Action`1<Pixelplacement.BoolMessage> Pixelplacement.Client::OnBool
	Action_1_t1152822336 * ___OnBool_49;
	// System.Action`1<Pixelplacement.BoolArrayMessage> Pixelplacement.Client::OnBoolArray
	Action_1_t3343860168 * ___OnBoolArray_50;
	// System.Action`1<Pixelplacement.Matrix4x4Message> Pixelplacement.Client::OnMatrix4x4
	Action_1_t1401304032 * ___OnMatrix4x4_51;
	// System.Action`1<Pixelplacement.Matrix4x4ArrayMessage> Pixelplacement.Client::OnMatrix4x4Array
	Action_1_t3562615688 * ___OnMatrix4x4Array_52;
	// System.Int32 Pixelplacement.Client::<PrimaryChannel>k__BackingField
	int32_t ___U3CPrimaryChannelU3Ek__BackingField_57;
	// System.Int32 Pixelplacement.Client::<SecondaryChannel>k__BackingField
	int32_t ___U3CSecondaryChannelU3Ek__BackingField_58;
	// System.Boolean Pixelplacement.Client::<Running>k__BackingField
	bool ___U3CRunningU3Ek__BackingField_59;
	// UnityEngine.Networking.NetworkClient Pixelplacement.Client::_client
	NetworkClient_t3758195968 * ____client_60;
	// System.String Pixelplacement.Client::ipAddress
	String_t* ___ipAddress_61;

public:
	inline static int32_t get_offset_of_OnError_23() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnError_23)); }
	inline Action_1_t2210661120 * get_OnError_23() const { return ___OnError_23; }
	inline Action_1_t2210661120 ** get_address_of_OnError_23() { return &___OnError_23; }
	inline void set_OnError_23(Action_1_t2210661120 * value)
	{
		___OnError_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_23), value);
	}

	inline static int32_t get_offset_of_OnConnected_24() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnConnected_24)); }
	inline Action_t1264377477 * get_OnConnected_24() const { return ___OnConnected_24; }
	inline Action_t1264377477 ** get_address_of_OnConnected_24() { return &___OnConnected_24; }
	inline void set_OnConnected_24(Action_t1264377477 * value)
	{
		___OnConnected_24 = value;
		Il2CppCodeGenWriteBarrier((&___OnConnected_24), value);
	}

	inline static int32_t get_offset_of_OnDisconnected_25() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnDisconnected_25)); }
	inline Action_t1264377477 * get_OnDisconnected_25() const { return ___OnDisconnected_25; }
	inline Action_t1264377477 ** get_address_of_OnDisconnected_25() { return &___OnDisconnected_25; }
	inline void set_OnDisconnected_25(Action_t1264377477 * value)
	{
		___OnDisconnected_25 = value;
		Il2CppCodeGenWriteBarrier((&___OnDisconnected_25), value);
	}

	inline static int32_t get_offset_of_OnServerAvailable_26() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnServerAvailable_26)); }
	inline Action_1_t257386514 * get_OnServerAvailable_26() const { return ___OnServerAvailable_26; }
	inline Action_1_t257386514 ** get_address_of_OnServerAvailable_26() { return &___OnServerAvailable_26; }
	inline void set_OnServerAvailable_26(Action_1_t257386514 * value)
	{
		___OnServerAvailable_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnServerAvailable_26), value);
	}

	inline static int32_t get_offset_of_OnFloat_27() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnFloat_27)); }
	inline Action_1_t1704364940 * get_OnFloat_27() const { return ___OnFloat_27; }
	inline Action_1_t1704364940 ** get_address_of_OnFloat_27() { return &___OnFloat_27; }
	inline void set_OnFloat_27(Action_1_t1704364940 * value)
	{
		___OnFloat_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnFloat_27), value);
	}

	inline static int32_t get_offset_of_OnFloatArray_28() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnFloatArray_28)); }
	inline Action_1_t1733437200 * get_OnFloatArray_28() const { return ___OnFloatArray_28; }
	inline Action_1_t1733437200 ** get_address_of_OnFloatArray_28() { return &___OnFloatArray_28; }
	inline void set_OnFloatArray_28(Action_1_t1733437200 * value)
	{
		___OnFloatArray_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnFloatArray_28), value);
	}

	inline static int32_t get_offset_of_OnInt_29() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnInt_29)); }
	inline Action_1_t581520478 * get_OnInt_29() const { return ___OnInt_29; }
	inline Action_1_t581520478 ** get_address_of_OnInt_29() { return &___OnInt_29; }
	inline void set_OnInt_29(Action_1_t581520478 * value)
	{
		___OnInt_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnInt_29), value);
	}

	inline static int32_t get_offset_of_OnIntArray_30() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnIntArray_30)); }
	inline Action_1_t712253493 * get_OnIntArray_30() const { return ___OnIntArray_30; }
	inline Action_1_t712253493 ** get_address_of_OnIntArray_30() { return &___OnIntArray_30; }
	inline void set_OnIntArray_30(Action_1_t712253493 * value)
	{
		___OnIntArray_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnIntArray_30), value);
	}

	inline static int32_t get_offset_of_OnVector2_31() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnVector2_31)); }
	inline Action_1_t2063388938 * get_OnVector2_31() const { return ___OnVector2_31; }
	inline Action_1_t2063388938 ** get_address_of_OnVector2_31() { return &___OnVector2_31; }
	inline void set_OnVector2_31(Action_1_t2063388938 * value)
	{
		___OnVector2_31 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector2_31), value);
	}

	inline static int32_t get_offset_of_OnVector2Array_32() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnVector2Array_32)); }
	inline Action_1_t2571979132 * get_OnVector2Array_32() const { return ___OnVector2Array_32; }
	inline Action_1_t2571979132 ** get_address_of_OnVector2Array_32() { return &___OnVector2Array_32; }
	inline void set_OnVector2Array_32(Action_1_t2571979132 * value)
	{
		___OnVector2Array_32 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector2Array_32), value);
	}

	inline static int32_t get_offset_of_OnVector3_33() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnVector3_33)); }
	inline Action_1_t2134757642 * get_OnVector3_33() const { return ___OnVector3_33; }
	inline Action_1_t2134757642 ** get_address_of_OnVector3_33() { return &___OnVector3_33; }
	inline void set_OnVector3_33(Action_1_t2134757642 * value)
	{
		___OnVector3_33 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector3_33), value);
	}

	inline static int32_t get_offset_of_OnVector3Array_34() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnVector3Array_34)); }
	inline Action_1_t900614475 * get_OnVector3Array_34() const { return ___OnVector3Array_34; }
	inline Action_1_t900614475 ** get_address_of_OnVector3Array_34() { return &___OnVector3Array_34; }
	inline void set_OnVector3Array_34(Action_1_t900614475 * value)
	{
		___OnVector3Array_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector3Array_34), value);
	}

	inline static int32_t get_offset_of_OnQuaternion_35() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnQuaternion_35)); }
	inline Action_1_t3056131525 * get_OnQuaternion_35() const { return ___OnQuaternion_35; }
	inline Action_1_t3056131525 ** get_address_of_OnQuaternion_35() { return &___OnQuaternion_35; }
	inline void set_OnQuaternion_35(Action_1_t3056131525 * value)
	{
		___OnQuaternion_35 = value;
		Il2CppCodeGenWriteBarrier((&___OnQuaternion_35), value);
	}

	inline static int32_t get_offset_of_OnQuaternionArray_36() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnQuaternionArray_36)); }
	inline Action_1_t2191742972 * get_OnQuaternionArray_36() const { return ___OnQuaternionArray_36; }
	inline Action_1_t2191742972 ** get_address_of_OnQuaternionArray_36() { return &___OnQuaternionArray_36; }
	inline void set_OnQuaternionArray_36(Action_1_t2191742972 * value)
	{
		___OnQuaternionArray_36 = value;
		Il2CppCodeGenWriteBarrier((&___OnQuaternionArray_36), value);
	}

	inline static int32_t get_offset_of_OnVector4_37() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnVector4_37)); }
	inline Action_1_t1635176714 * get_OnVector4_37() const { return ___OnVector4_37; }
	inline Action_1_t1635176714 ** get_address_of_OnVector4_37() { return &___OnVector4_37; }
	inline void set_OnVector4_37(Action_1_t1635176714 * value)
	{
		___OnVector4_37 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector4_37), value);
	}

	inline static int32_t get_offset_of_OnVector4Array_38() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnVector4Array_38)); }
	inline Action_1_t1628391807 * get_OnVector4Array_38() const { return ___OnVector4Array_38; }
	inline Action_1_t1628391807 ** get_address_of_OnVector4Array_38() { return &___OnVector4Array_38; }
	inline void set_OnVector4Array_38(Action_1_t1628391807 * value)
	{
		___OnVector4Array_38 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector4Array_38), value);
	}

	inline static int32_t get_offset_of_OnRect_39() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnRect_39)); }
	inline Action_1_t1590883354 * get_OnRect_39() const { return ___OnRect_39; }
	inline Action_1_t1590883354 ** get_address_of_OnRect_39() { return &___OnRect_39; }
	inline void set_OnRect_39(Action_1_t1590883354 * value)
	{
		___OnRect_39 = value;
		Il2CppCodeGenWriteBarrier((&___OnRect_39), value);
	}

	inline static int32_t get_offset_of_OnRectArray_40() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnRectArray_40)); }
	inline Action_1_t1842167626 * get_OnRectArray_40() const { return ___OnRectArray_40; }
	inline Action_1_t1842167626 ** get_address_of_OnRectArray_40() { return &___OnRectArray_40; }
	inline void set_OnRectArray_40(Action_1_t1842167626 * value)
	{
		___OnRectArray_40 = value;
		Il2CppCodeGenWriteBarrier((&___OnRectArray_40), value);
	}

	inline static int32_t get_offset_of_OnString_41() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnString_41)); }
	inline Action_1_t640713362 * get_OnString_41() const { return ___OnString_41; }
	inline Action_1_t640713362 ** get_address_of_OnString_41() { return &___OnString_41; }
	inline void set_OnString_41(Action_1_t640713362 * value)
	{
		___OnString_41 = value;
		Il2CppCodeGenWriteBarrier((&___OnString_41), value);
	}

	inline static int32_t get_offset_of_OnStringArray_42() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnStringArray_42)); }
	inline Action_1_t4240794820 * get_OnStringArray_42() const { return ___OnStringArray_42; }
	inline Action_1_t4240794820 ** get_address_of_OnStringArray_42() { return &___OnStringArray_42; }
	inline void set_OnStringArray_42(Action_1_t4240794820 * value)
	{
		___OnStringArray_42 = value;
		Il2CppCodeGenWriteBarrier((&___OnStringArray_42), value);
	}

	inline static int32_t get_offset_of_OnByte_43() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnByte_43)); }
	inline Action_1_t3421907995 * get_OnByte_43() const { return ___OnByte_43; }
	inline Action_1_t3421907995 ** get_address_of_OnByte_43() { return &___OnByte_43; }
	inline void set_OnByte_43(Action_1_t3421907995 * value)
	{
		___OnByte_43 = value;
		Il2CppCodeGenWriteBarrier((&___OnByte_43), value);
	}

	inline static int32_t get_offset_of_OnByteArray_44() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnByteArray_44)); }
	inline Action_1_t2266368787 * get_OnByteArray_44() const { return ___OnByteArray_44; }
	inline Action_1_t2266368787 ** get_address_of_OnByteArray_44() { return &___OnByteArray_44; }
	inline void set_OnByteArray_44(Action_1_t2266368787 * value)
	{
		___OnByteArray_44 = value;
		Il2CppCodeGenWriteBarrier((&___OnByteArray_44), value);
	}

	inline static int32_t get_offset_of_OnColor_45() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnColor_45)); }
	inline Action_1_t313855902 * get_OnColor_45() const { return ___OnColor_45; }
	inline Action_1_t313855902 ** get_address_of_OnColor_45() { return &___OnColor_45; }
	inline void set_OnColor_45(Action_1_t313855902 * value)
	{
		___OnColor_45 = value;
		Il2CppCodeGenWriteBarrier((&___OnColor_45), value);
	}

	inline static int32_t get_offset_of_OnColorArray_46() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnColorArray_46)); }
	inline Action_1_t1631047211 * get_OnColorArray_46() const { return ___OnColorArray_46; }
	inline Action_1_t1631047211 ** get_address_of_OnColorArray_46() { return &___OnColorArray_46; }
	inline void set_OnColorArray_46(Action_1_t1631047211 * value)
	{
		___OnColorArray_46 = value;
		Il2CppCodeGenWriteBarrier((&___OnColorArray_46), value);
	}

	inline static int32_t get_offset_of_OnColor32_47() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnColor32_47)); }
	inline Action_1_t1709086795 * get_OnColor32_47() const { return ___OnColor32_47; }
	inline Action_1_t1709086795 ** get_address_of_OnColor32_47() { return &___OnColor32_47; }
	inline void set_OnColor32_47(Action_1_t1709086795 * value)
	{
		___OnColor32_47 = value;
		Il2CppCodeGenWriteBarrier((&___OnColor32_47), value);
	}

	inline static int32_t get_offset_of_OnColor32Array_48() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnColor32Array_48)); }
	inline Action_1_t261809231 * get_OnColor32Array_48() const { return ___OnColor32Array_48; }
	inline Action_1_t261809231 ** get_address_of_OnColor32Array_48() { return &___OnColor32Array_48; }
	inline void set_OnColor32Array_48(Action_1_t261809231 * value)
	{
		___OnColor32Array_48 = value;
		Il2CppCodeGenWriteBarrier((&___OnColor32Array_48), value);
	}

	inline static int32_t get_offset_of_OnBool_49() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnBool_49)); }
	inline Action_1_t1152822336 * get_OnBool_49() const { return ___OnBool_49; }
	inline Action_1_t1152822336 ** get_address_of_OnBool_49() { return &___OnBool_49; }
	inline void set_OnBool_49(Action_1_t1152822336 * value)
	{
		___OnBool_49 = value;
		Il2CppCodeGenWriteBarrier((&___OnBool_49), value);
	}

	inline static int32_t get_offset_of_OnBoolArray_50() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnBoolArray_50)); }
	inline Action_1_t3343860168 * get_OnBoolArray_50() const { return ___OnBoolArray_50; }
	inline Action_1_t3343860168 ** get_address_of_OnBoolArray_50() { return &___OnBoolArray_50; }
	inline void set_OnBoolArray_50(Action_1_t3343860168 * value)
	{
		___OnBoolArray_50 = value;
		Il2CppCodeGenWriteBarrier((&___OnBoolArray_50), value);
	}

	inline static int32_t get_offset_of_OnMatrix4x4_51() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnMatrix4x4_51)); }
	inline Action_1_t1401304032 * get_OnMatrix4x4_51() const { return ___OnMatrix4x4_51; }
	inline Action_1_t1401304032 ** get_address_of_OnMatrix4x4_51() { return &___OnMatrix4x4_51; }
	inline void set_OnMatrix4x4_51(Action_1_t1401304032 * value)
	{
		___OnMatrix4x4_51 = value;
		Il2CppCodeGenWriteBarrier((&___OnMatrix4x4_51), value);
	}

	inline static int32_t get_offset_of_OnMatrix4x4Array_52() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___OnMatrix4x4Array_52)); }
	inline Action_1_t3562615688 * get_OnMatrix4x4Array_52() const { return ___OnMatrix4x4Array_52; }
	inline Action_1_t3562615688 ** get_address_of_OnMatrix4x4Array_52() { return &___OnMatrix4x4Array_52; }
	inline void set_OnMatrix4x4Array_52(Action_1_t3562615688 * value)
	{
		___OnMatrix4x4Array_52 = value;
		Il2CppCodeGenWriteBarrier((&___OnMatrix4x4Array_52), value);
	}

	inline static int32_t get_offset_of_U3CPrimaryChannelU3Ek__BackingField_57() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___U3CPrimaryChannelU3Ek__BackingField_57)); }
	inline int32_t get_U3CPrimaryChannelU3Ek__BackingField_57() const { return ___U3CPrimaryChannelU3Ek__BackingField_57; }
	inline int32_t* get_address_of_U3CPrimaryChannelU3Ek__BackingField_57() { return &___U3CPrimaryChannelU3Ek__BackingField_57; }
	inline void set_U3CPrimaryChannelU3Ek__BackingField_57(int32_t value)
	{
		___U3CPrimaryChannelU3Ek__BackingField_57 = value;
	}

	inline static int32_t get_offset_of_U3CSecondaryChannelU3Ek__BackingField_58() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___U3CSecondaryChannelU3Ek__BackingField_58)); }
	inline int32_t get_U3CSecondaryChannelU3Ek__BackingField_58() const { return ___U3CSecondaryChannelU3Ek__BackingField_58; }
	inline int32_t* get_address_of_U3CSecondaryChannelU3Ek__BackingField_58() { return &___U3CSecondaryChannelU3Ek__BackingField_58; }
	inline void set_U3CSecondaryChannelU3Ek__BackingField_58(int32_t value)
	{
		___U3CSecondaryChannelU3Ek__BackingField_58 = value;
	}

	inline static int32_t get_offset_of_U3CRunningU3Ek__BackingField_59() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___U3CRunningU3Ek__BackingField_59)); }
	inline bool get_U3CRunningU3Ek__BackingField_59() const { return ___U3CRunningU3Ek__BackingField_59; }
	inline bool* get_address_of_U3CRunningU3Ek__BackingField_59() { return &___U3CRunningU3Ek__BackingField_59; }
	inline void set_U3CRunningU3Ek__BackingField_59(bool value)
	{
		___U3CRunningU3Ek__BackingField_59 = value;
	}

	inline static int32_t get_offset_of__client_60() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ____client_60)); }
	inline NetworkClient_t3758195968 * get__client_60() const { return ____client_60; }
	inline NetworkClient_t3758195968 ** get_address_of__client_60() { return &____client_60; }
	inline void set__client_60(NetworkClient_t3758195968 * value)
	{
		____client_60 = value;
		Il2CppCodeGenWriteBarrier((&____client_60), value);
	}

	inline static int32_t get_offset_of_ipAddress_61() { return static_cast<int32_t>(offsetof(Client_t1356520481_StaticFields, ___ipAddress_61)); }
	inline String_t* get_ipAddress_61() const { return ___ipAddress_61; }
	inline String_t** get_address_of_ipAddress_61() { return &___ipAddress_61; }
	inline void set_ipAddress_61(String_t* value)
	{
		___ipAddress_61 = value;
		Il2CppCodeGenWriteBarrier((&___ipAddress_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENT_T1356520481_H
#ifndef SERVER_T2028837719_H
#define SERVER_T2028837719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pixelplacement.Server
struct  Server_t2028837719  : public NetworkDiscovery_t986186286
{
public:
	// UnityEngine.Networking.QosType Pixelplacement.Server::primaryQualityOfService
	int32_t ___primaryQualityOfService_51;
	// UnityEngine.Networking.QosType Pixelplacement.Server::secondaryQualityOfService
	int32_t ___secondaryQualityOfService_52;
	// System.String Pixelplacement.Server::customDeviceId
	String_t* ___customDeviceId_53;
	// System.Int32 Pixelplacement.Server::broadcastingPort
	int32_t ___broadcastingPort_54;
	// System.Int32 Pixelplacement.Server::maxConnections
	int32_t ___maxConnections_55;
	// System.UInt32 Pixelplacement.Server::initialBandwidth
	uint32_t ___initialBandwidth_56;

public:
	inline static int32_t get_offset_of_primaryQualityOfService_51() { return static_cast<int32_t>(offsetof(Server_t2028837719, ___primaryQualityOfService_51)); }
	inline int32_t get_primaryQualityOfService_51() const { return ___primaryQualityOfService_51; }
	inline int32_t* get_address_of_primaryQualityOfService_51() { return &___primaryQualityOfService_51; }
	inline void set_primaryQualityOfService_51(int32_t value)
	{
		___primaryQualityOfService_51 = value;
	}

	inline static int32_t get_offset_of_secondaryQualityOfService_52() { return static_cast<int32_t>(offsetof(Server_t2028837719, ___secondaryQualityOfService_52)); }
	inline int32_t get_secondaryQualityOfService_52() const { return ___secondaryQualityOfService_52; }
	inline int32_t* get_address_of_secondaryQualityOfService_52() { return &___secondaryQualityOfService_52; }
	inline void set_secondaryQualityOfService_52(int32_t value)
	{
		___secondaryQualityOfService_52 = value;
	}

	inline static int32_t get_offset_of_customDeviceId_53() { return static_cast<int32_t>(offsetof(Server_t2028837719, ___customDeviceId_53)); }
	inline String_t* get_customDeviceId_53() const { return ___customDeviceId_53; }
	inline String_t** get_address_of_customDeviceId_53() { return &___customDeviceId_53; }
	inline void set_customDeviceId_53(String_t* value)
	{
		___customDeviceId_53 = value;
		Il2CppCodeGenWriteBarrier((&___customDeviceId_53), value);
	}

	inline static int32_t get_offset_of_broadcastingPort_54() { return static_cast<int32_t>(offsetof(Server_t2028837719, ___broadcastingPort_54)); }
	inline int32_t get_broadcastingPort_54() const { return ___broadcastingPort_54; }
	inline int32_t* get_address_of_broadcastingPort_54() { return &___broadcastingPort_54; }
	inline void set_broadcastingPort_54(int32_t value)
	{
		___broadcastingPort_54 = value;
	}

	inline static int32_t get_offset_of_maxConnections_55() { return static_cast<int32_t>(offsetof(Server_t2028837719, ___maxConnections_55)); }
	inline int32_t get_maxConnections_55() const { return ___maxConnections_55; }
	inline int32_t* get_address_of_maxConnections_55() { return &___maxConnections_55; }
	inline void set_maxConnections_55(int32_t value)
	{
		___maxConnections_55 = value;
	}

	inline static int32_t get_offset_of_initialBandwidth_56() { return static_cast<int32_t>(offsetof(Server_t2028837719, ___initialBandwidth_56)); }
	inline uint32_t get_initialBandwidth_56() const { return ___initialBandwidth_56; }
	inline uint32_t* get_address_of_initialBandwidth_56() { return &___initialBandwidth_56; }
	inline void set_initialBandwidth_56(uint32_t value)
	{
		___initialBandwidth_56 = value;
	}
};

struct Server_t2028837719_StaticFields
{
public:
	// System.Action`1<System.String> Pixelplacement.Server::OnPlayerConnected
	Action_1_t2019918284 * ___OnPlayerConnected_23;
	// System.Action`1<System.String> Pixelplacement.Server::OnPlayerDisconnected
	Action_1_t2019918284 * ___OnPlayerDisconnected_24;
	// System.Action`1<Pixelplacement.FloatMessage> Pixelplacement.Server::OnFloat
	Action_1_t1704364940 * ___OnFloat_25;
	// System.Action`1<Pixelplacement.FloatArrayMessage> Pixelplacement.Server::OnFloatArray
	Action_1_t1733437200 * ___OnFloatArray_26;
	// System.Action`1<Pixelplacement.IntMessage> Pixelplacement.Server::OnInt
	Action_1_t581520478 * ___OnInt_27;
	// System.Action`1<Pixelplacement.IntArrayMessage> Pixelplacement.Server::OnIntArray
	Action_1_t712253493 * ___OnIntArray_28;
	// System.Action`1<Pixelplacement.Vector2Message> Pixelplacement.Server::OnVector2
	Action_1_t2063388938 * ___OnVector2_29;
	// System.Action`1<Pixelplacement.Vector2ArrayMessage> Pixelplacement.Server::OnVector2Array
	Action_1_t2571979132 * ___OnVector2Array_30;
	// System.Action`1<Pixelplacement.Vector3Message> Pixelplacement.Server::OnVector3
	Action_1_t2134757642 * ___OnVector3_31;
	// System.Action`1<Pixelplacement.Vector3ArrayMessage> Pixelplacement.Server::OnVector3Array
	Action_1_t900614475 * ___OnVector3Array_32;
	// System.Action`1<Pixelplacement.QuaternionMessage> Pixelplacement.Server::OnQuaternion
	Action_1_t3056131525 * ___OnQuaternion_33;
	// System.Action`1<Pixelplacement.QuaternionArrayMessage> Pixelplacement.Server::OnQuaternionArray
	Action_1_t2191742972 * ___OnQuaternionArray_34;
	// System.Action`1<Pixelplacement.Vector4Message> Pixelplacement.Server::OnVector4
	Action_1_t1635176714 * ___OnVector4_35;
	// System.Action`1<Pixelplacement.Vector4ArrayMessage> Pixelplacement.Server::OnVector4Array
	Action_1_t1628391807 * ___OnVector4Array_36;
	// System.Action`1<Pixelplacement.RectMessage> Pixelplacement.Server::OnRect
	Action_1_t1590883354 * ___OnRect_37;
	// System.Action`1<Pixelplacement.RectArrayMessage> Pixelplacement.Server::OnRectArray
	Action_1_t1842167626 * ___OnRectArray_38;
	// System.Action`1<Pixelplacement.StringMessage> Pixelplacement.Server::OnString
	Action_1_t640713362 * ___OnString_39;
	// System.Action`1<Pixelplacement.StringArrayMessage> Pixelplacement.Server::OnStringArray
	Action_1_t4240794820 * ___OnStringArray_40;
	// System.Action`1<Pixelplacement.ByteMessage> Pixelplacement.Server::OnByte
	Action_1_t3421907995 * ___OnByte_41;
	// System.Action`1<Pixelplacement.ByteArrayMessage> Pixelplacement.Server::OnByteArray
	Action_1_t2266368787 * ___OnByteArray_42;
	// System.Action`1<Pixelplacement.ColorMessage> Pixelplacement.Server::OnColor
	Action_1_t313855902 * ___OnColor_43;
	// System.Action`1<Pixelplacement.ColorArrayMessage> Pixelplacement.Server::OnColorArray
	Action_1_t1631047211 * ___OnColorArray_44;
	// System.Action`1<Pixelplacement.Color32Message> Pixelplacement.Server::OnColor32
	Action_1_t1709086795 * ___OnColor32_45;
	// System.Action`1<Pixelplacement.Color32ArrayMessage> Pixelplacement.Server::OnColor32Array
	Action_1_t261809231 * ___OnColor32Array_46;
	// System.Action`1<Pixelplacement.BoolMessage> Pixelplacement.Server::OnBool
	Action_1_t1152822336 * ___OnBool_47;
	// System.Action`1<Pixelplacement.BoolArrayMessage> Pixelplacement.Server::OnBoolArray
	Action_1_t3343860168 * ___OnBoolArray_48;
	// System.Action`1<Pixelplacement.Matrix4x4Message> Pixelplacement.Server::OnMatrix4x4
	Action_1_t1401304032 * ___OnMatrix4x4_49;
	// System.Action`1<Pixelplacement.Matrix4x4ArrayMessage> Pixelplacement.Server::OnMatrix4x4Array
	Action_1_t3562615688 * ___OnMatrix4x4Array_50;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> Pixelplacement.Server::_connections
	Dictionary_2_t736164020 * ____connections_57;
	// Pixelplacement.Server/ServerInstance Pixelplacement.Server::_server
	ServerInstance_t67659343 * ____server_58;
	// System.String Pixelplacement.Server::_randomIdKey
	String_t* ____randomIdKey_59;
	// System.String Pixelplacement.Server::ipAddress
	String_t* ___ipAddress_60;
	// System.Int32 Pixelplacement.Server::<PrimaryChannel>k__BackingField
	int32_t ___U3CPrimaryChannelU3Ek__BackingField_61;
	// System.Int32 Pixelplacement.Server::<SecondaryChannel>k__BackingField
	int32_t ___U3CSecondaryChannelU3Ek__BackingField_62;
	// System.Boolean Pixelplacement.Server::<Running>k__BackingField
	bool ___U3CRunningU3Ek__BackingField_63;

public:
	inline static int32_t get_offset_of_OnPlayerConnected_23() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnPlayerConnected_23)); }
	inline Action_1_t2019918284 * get_OnPlayerConnected_23() const { return ___OnPlayerConnected_23; }
	inline Action_1_t2019918284 ** get_address_of_OnPlayerConnected_23() { return &___OnPlayerConnected_23; }
	inline void set_OnPlayerConnected_23(Action_1_t2019918284 * value)
	{
		___OnPlayerConnected_23 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayerConnected_23), value);
	}

	inline static int32_t get_offset_of_OnPlayerDisconnected_24() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnPlayerDisconnected_24)); }
	inline Action_1_t2019918284 * get_OnPlayerDisconnected_24() const { return ___OnPlayerDisconnected_24; }
	inline Action_1_t2019918284 ** get_address_of_OnPlayerDisconnected_24() { return &___OnPlayerDisconnected_24; }
	inline void set_OnPlayerDisconnected_24(Action_1_t2019918284 * value)
	{
		___OnPlayerDisconnected_24 = value;
		Il2CppCodeGenWriteBarrier((&___OnPlayerDisconnected_24), value);
	}

	inline static int32_t get_offset_of_OnFloat_25() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnFloat_25)); }
	inline Action_1_t1704364940 * get_OnFloat_25() const { return ___OnFloat_25; }
	inline Action_1_t1704364940 ** get_address_of_OnFloat_25() { return &___OnFloat_25; }
	inline void set_OnFloat_25(Action_1_t1704364940 * value)
	{
		___OnFloat_25 = value;
		Il2CppCodeGenWriteBarrier((&___OnFloat_25), value);
	}

	inline static int32_t get_offset_of_OnFloatArray_26() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnFloatArray_26)); }
	inline Action_1_t1733437200 * get_OnFloatArray_26() const { return ___OnFloatArray_26; }
	inline Action_1_t1733437200 ** get_address_of_OnFloatArray_26() { return &___OnFloatArray_26; }
	inline void set_OnFloatArray_26(Action_1_t1733437200 * value)
	{
		___OnFloatArray_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnFloatArray_26), value);
	}

	inline static int32_t get_offset_of_OnInt_27() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnInt_27)); }
	inline Action_1_t581520478 * get_OnInt_27() const { return ___OnInt_27; }
	inline Action_1_t581520478 ** get_address_of_OnInt_27() { return &___OnInt_27; }
	inline void set_OnInt_27(Action_1_t581520478 * value)
	{
		___OnInt_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnInt_27), value);
	}

	inline static int32_t get_offset_of_OnIntArray_28() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnIntArray_28)); }
	inline Action_1_t712253493 * get_OnIntArray_28() const { return ___OnIntArray_28; }
	inline Action_1_t712253493 ** get_address_of_OnIntArray_28() { return &___OnIntArray_28; }
	inline void set_OnIntArray_28(Action_1_t712253493 * value)
	{
		___OnIntArray_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnIntArray_28), value);
	}

	inline static int32_t get_offset_of_OnVector2_29() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnVector2_29)); }
	inline Action_1_t2063388938 * get_OnVector2_29() const { return ___OnVector2_29; }
	inline Action_1_t2063388938 ** get_address_of_OnVector2_29() { return &___OnVector2_29; }
	inline void set_OnVector2_29(Action_1_t2063388938 * value)
	{
		___OnVector2_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector2_29), value);
	}

	inline static int32_t get_offset_of_OnVector2Array_30() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnVector2Array_30)); }
	inline Action_1_t2571979132 * get_OnVector2Array_30() const { return ___OnVector2Array_30; }
	inline Action_1_t2571979132 ** get_address_of_OnVector2Array_30() { return &___OnVector2Array_30; }
	inline void set_OnVector2Array_30(Action_1_t2571979132 * value)
	{
		___OnVector2Array_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector2Array_30), value);
	}

	inline static int32_t get_offset_of_OnVector3_31() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnVector3_31)); }
	inline Action_1_t2134757642 * get_OnVector3_31() const { return ___OnVector3_31; }
	inline Action_1_t2134757642 ** get_address_of_OnVector3_31() { return &___OnVector3_31; }
	inline void set_OnVector3_31(Action_1_t2134757642 * value)
	{
		___OnVector3_31 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector3_31), value);
	}

	inline static int32_t get_offset_of_OnVector3Array_32() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnVector3Array_32)); }
	inline Action_1_t900614475 * get_OnVector3Array_32() const { return ___OnVector3Array_32; }
	inline Action_1_t900614475 ** get_address_of_OnVector3Array_32() { return &___OnVector3Array_32; }
	inline void set_OnVector3Array_32(Action_1_t900614475 * value)
	{
		___OnVector3Array_32 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector3Array_32), value);
	}

	inline static int32_t get_offset_of_OnQuaternion_33() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnQuaternion_33)); }
	inline Action_1_t3056131525 * get_OnQuaternion_33() const { return ___OnQuaternion_33; }
	inline Action_1_t3056131525 ** get_address_of_OnQuaternion_33() { return &___OnQuaternion_33; }
	inline void set_OnQuaternion_33(Action_1_t3056131525 * value)
	{
		___OnQuaternion_33 = value;
		Il2CppCodeGenWriteBarrier((&___OnQuaternion_33), value);
	}

	inline static int32_t get_offset_of_OnQuaternionArray_34() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnQuaternionArray_34)); }
	inline Action_1_t2191742972 * get_OnQuaternionArray_34() const { return ___OnQuaternionArray_34; }
	inline Action_1_t2191742972 ** get_address_of_OnQuaternionArray_34() { return &___OnQuaternionArray_34; }
	inline void set_OnQuaternionArray_34(Action_1_t2191742972 * value)
	{
		___OnQuaternionArray_34 = value;
		Il2CppCodeGenWriteBarrier((&___OnQuaternionArray_34), value);
	}

	inline static int32_t get_offset_of_OnVector4_35() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnVector4_35)); }
	inline Action_1_t1635176714 * get_OnVector4_35() const { return ___OnVector4_35; }
	inline Action_1_t1635176714 ** get_address_of_OnVector4_35() { return &___OnVector4_35; }
	inline void set_OnVector4_35(Action_1_t1635176714 * value)
	{
		___OnVector4_35 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector4_35), value);
	}

	inline static int32_t get_offset_of_OnVector4Array_36() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnVector4Array_36)); }
	inline Action_1_t1628391807 * get_OnVector4Array_36() const { return ___OnVector4Array_36; }
	inline Action_1_t1628391807 ** get_address_of_OnVector4Array_36() { return &___OnVector4Array_36; }
	inline void set_OnVector4Array_36(Action_1_t1628391807 * value)
	{
		___OnVector4Array_36 = value;
		Il2CppCodeGenWriteBarrier((&___OnVector4Array_36), value);
	}

	inline static int32_t get_offset_of_OnRect_37() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnRect_37)); }
	inline Action_1_t1590883354 * get_OnRect_37() const { return ___OnRect_37; }
	inline Action_1_t1590883354 ** get_address_of_OnRect_37() { return &___OnRect_37; }
	inline void set_OnRect_37(Action_1_t1590883354 * value)
	{
		___OnRect_37 = value;
		Il2CppCodeGenWriteBarrier((&___OnRect_37), value);
	}

	inline static int32_t get_offset_of_OnRectArray_38() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnRectArray_38)); }
	inline Action_1_t1842167626 * get_OnRectArray_38() const { return ___OnRectArray_38; }
	inline Action_1_t1842167626 ** get_address_of_OnRectArray_38() { return &___OnRectArray_38; }
	inline void set_OnRectArray_38(Action_1_t1842167626 * value)
	{
		___OnRectArray_38 = value;
		Il2CppCodeGenWriteBarrier((&___OnRectArray_38), value);
	}

	inline static int32_t get_offset_of_OnString_39() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnString_39)); }
	inline Action_1_t640713362 * get_OnString_39() const { return ___OnString_39; }
	inline Action_1_t640713362 ** get_address_of_OnString_39() { return &___OnString_39; }
	inline void set_OnString_39(Action_1_t640713362 * value)
	{
		___OnString_39 = value;
		Il2CppCodeGenWriteBarrier((&___OnString_39), value);
	}

	inline static int32_t get_offset_of_OnStringArray_40() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnStringArray_40)); }
	inline Action_1_t4240794820 * get_OnStringArray_40() const { return ___OnStringArray_40; }
	inline Action_1_t4240794820 ** get_address_of_OnStringArray_40() { return &___OnStringArray_40; }
	inline void set_OnStringArray_40(Action_1_t4240794820 * value)
	{
		___OnStringArray_40 = value;
		Il2CppCodeGenWriteBarrier((&___OnStringArray_40), value);
	}

	inline static int32_t get_offset_of_OnByte_41() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnByte_41)); }
	inline Action_1_t3421907995 * get_OnByte_41() const { return ___OnByte_41; }
	inline Action_1_t3421907995 ** get_address_of_OnByte_41() { return &___OnByte_41; }
	inline void set_OnByte_41(Action_1_t3421907995 * value)
	{
		___OnByte_41 = value;
		Il2CppCodeGenWriteBarrier((&___OnByte_41), value);
	}

	inline static int32_t get_offset_of_OnByteArray_42() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnByteArray_42)); }
	inline Action_1_t2266368787 * get_OnByteArray_42() const { return ___OnByteArray_42; }
	inline Action_1_t2266368787 ** get_address_of_OnByteArray_42() { return &___OnByteArray_42; }
	inline void set_OnByteArray_42(Action_1_t2266368787 * value)
	{
		___OnByteArray_42 = value;
		Il2CppCodeGenWriteBarrier((&___OnByteArray_42), value);
	}

	inline static int32_t get_offset_of_OnColor_43() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnColor_43)); }
	inline Action_1_t313855902 * get_OnColor_43() const { return ___OnColor_43; }
	inline Action_1_t313855902 ** get_address_of_OnColor_43() { return &___OnColor_43; }
	inline void set_OnColor_43(Action_1_t313855902 * value)
	{
		___OnColor_43 = value;
		Il2CppCodeGenWriteBarrier((&___OnColor_43), value);
	}

	inline static int32_t get_offset_of_OnColorArray_44() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnColorArray_44)); }
	inline Action_1_t1631047211 * get_OnColorArray_44() const { return ___OnColorArray_44; }
	inline Action_1_t1631047211 ** get_address_of_OnColorArray_44() { return &___OnColorArray_44; }
	inline void set_OnColorArray_44(Action_1_t1631047211 * value)
	{
		___OnColorArray_44 = value;
		Il2CppCodeGenWriteBarrier((&___OnColorArray_44), value);
	}

	inline static int32_t get_offset_of_OnColor32_45() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnColor32_45)); }
	inline Action_1_t1709086795 * get_OnColor32_45() const { return ___OnColor32_45; }
	inline Action_1_t1709086795 ** get_address_of_OnColor32_45() { return &___OnColor32_45; }
	inline void set_OnColor32_45(Action_1_t1709086795 * value)
	{
		___OnColor32_45 = value;
		Il2CppCodeGenWriteBarrier((&___OnColor32_45), value);
	}

	inline static int32_t get_offset_of_OnColor32Array_46() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnColor32Array_46)); }
	inline Action_1_t261809231 * get_OnColor32Array_46() const { return ___OnColor32Array_46; }
	inline Action_1_t261809231 ** get_address_of_OnColor32Array_46() { return &___OnColor32Array_46; }
	inline void set_OnColor32Array_46(Action_1_t261809231 * value)
	{
		___OnColor32Array_46 = value;
		Il2CppCodeGenWriteBarrier((&___OnColor32Array_46), value);
	}

	inline static int32_t get_offset_of_OnBool_47() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnBool_47)); }
	inline Action_1_t1152822336 * get_OnBool_47() const { return ___OnBool_47; }
	inline Action_1_t1152822336 ** get_address_of_OnBool_47() { return &___OnBool_47; }
	inline void set_OnBool_47(Action_1_t1152822336 * value)
	{
		___OnBool_47 = value;
		Il2CppCodeGenWriteBarrier((&___OnBool_47), value);
	}

	inline static int32_t get_offset_of_OnBoolArray_48() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnBoolArray_48)); }
	inline Action_1_t3343860168 * get_OnBoolArray_48() const { return ___OnBoolArray_48; }
	inline Action_1_t3343860168 ** get_address_of_OnBoolArray_48() { return &___OnBoolArray_48; }
	inline void set_OnBoolArray_48(Action_1_t3343860168 * value)
	{
		___OnBoolArray_48 = value;
		Il2CppCodeGenWriteBarrier((&___OnBoolArray_48), value);
	}

	inline static int32_t get_offset_of_OnMatrix4x4_49() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnMatrix4x4_49)); }
	inline Action_1_t1401304032 * get_OnMatrix4x4_49() const { return ___OnMatrix4x4_49; }
	inline Action_1_t1401304032 ** get_address_of_OnMatrix4x4_49() { return &___OnMatrix4x4_49; }
	inline void set_OnMatrix4x4_49(Action_1_t1401304032 * value)
	{
		___OnMatrix4x4_49 = value;
		Il2CppCodeGenWriteBarrier((&___OnMatrix4x4_49), value);
	}

	inline static int32_t get_offset_of_OnMatrix4x4Array_50() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___OnMatrix4x4Array_50)); }
	inline Action_1_t3562615688 * get_OnMatrix4x4Array_50() const { return ___OnMatrix4x4Array_50; }
	inline Action_1_t3562615688 ** get_address_of_OnMatrix4x4Array_50() { return &___OnMatrix4x4Array_50; }
	inline void set_OnMatrix4x4Array_50(Action_1_t3562615688 * value)
	{
		___OnMatrix4x4Array_50 = value;
		Il2CppCodeGenWriteBarrier((&___OnMatrix4x4Array_50), value);
	}

	inline static int32_t get_offset_of__connections_57() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ____connections_57)); }
	inline Dictionary_2_t736164020 * get__connections_57() const { return ____connections_57; }
	inline Dictionary_2_t736164020 ** get_address_of__connections_57() { return &____connections_57; }
	inline void set__connections_57(Dictionary_2_t736164020 * value)
	{
		____connections_57 = value;
		Il2CppCodeGenWriteBarrier((&____connections_57), value);
	}

	inline static int32_t get_offset_of__server_58() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ____server_58)); }
	inline ServerInstance_t67659343 * get__server_58() const { return ____server_58; }
	inline ServerInstance_t67659343 ** get_address_of__server_58() { return &____server_58; }
	inline void set__server_58(ServerInstance_t67659343 * value)
	{
		____server_58 = value;
		Il2CppCodeGenWriteBarrier((&____server_58), value);
	}

	inline static int32_t get_offset_of__randomIdKey_59() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ____randomIdKey_59)); }
	inline String_t* get__randomIdKey_59() const { return ____randomIdKey_59; }
	inline String_t** get_address_of__randomIdKey_59() { return &____randomIdKey_59; }
	inline void set__randomIdKey_59(String_t* value)
	{
		____randomIdKey_59 = value;
		Il2CppCodeGenWriteBarrier((&____randomIdKey_59), value);
	}

	inline static int32_t get_offset_of_ipAddress_60() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___ipAddress_60)); }
	inline String_t* get_ipAddress_60() const { return ___ipAddress_60; }
	inline String_t** get_address_of_ipAddress_60() { return &___ipAddress_60; }
	inline void set_ipAddress_60(String_t* value)
	{
		___ipAddress_60 = value;
		Il2CppCodeGenWriteBarrier((&___ipAddress_60), value);
	}

	inline static int32_t get_offset_of_U3CPrimaryChannelU3Ek__BackingField_61() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___U3CPrimaryChannelU3Ek__BackingField_61)); }
	inline int32_t get_U3CPrimaryChannelU3Ek__BackingField_61() const { return ___U3CPrimaryChannelU3Ek__BackingField_61; }
	inline int32_t* get_address_of_U3CPrimaryChannelU3Ek__BackingField_61() { return &___U3CPrimaryChannelU3Ek__BackingField_61; }
	inline void set_U3CPrimaryChannelU3Ek__BackingField_61(int32_t value)
	{
		___U3CPrimaryChannelU3Ek__BackingField_61 = value;
	}

	inline static int32_t get_offset_of_U3CSecondaryChannelU3Ek__BackingField_62() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___U3CSecondaryChannelU3Ek__BackingField_62)); }
	inline int32_t get_U3CSecondaryChannelU3Ek__BackingField_62() const { return ___U3CSecondaryChannelU3Ek__BackingField_62; }
	inline int32_t* get_address_of_U3CSecondaryChannelU3Ek__BackingField_62() { return &___U3CSecondaryChannelU3Ek__BackingField_62; }
	inline void set_U3CSecondaryChannelU3Ek__BackingField_62(int32_t value)
	{
		___U3CSecondaryChannelU3Ek__BackingField_62 = value;
	}

	inline static int32_t get_offset_of_U3CRunningU3Ek__BackingField_63() { return static_cast<int32_t>(offsetof(Server_t2028837719_StaticFields, ___U3CRunningU3Ek__BackingField_63)); }
	inline bool get_U3CRunningU3Ek__BackingField_63() const { return ___U3CRunningU3Ek__BackingField_63; }
	inline bool* get_address_of_U3CRunningU3Ek__BackingField_63() { return &___U3CRunningU3Ek__BackingField_63; }
	inline void set_U3CRunningU3Ek__BackingField_63(bool value)
	{
		___U3CRunningU3Ek__BackingField_63 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVER_T2028837719_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (Method_t1244881256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2500[3] = 
{
	Method_t1244881256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (ColliderButton_t2059806696), -1, sizeof(ColliderButton_t2059806696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2502[54] = 
{
	ColliderButton_t2059806696::get_offset_of_OnSelected_4(),
	ColliderButton_t2059806696::get_offset_of_OnDeselected_5(),
	ColliderButton_t2059806696::get_offset_of_OnClick_6(),
	ColliderButton_t2059806696::get_offset_of_OnPressed_7(),
	ColliderButton_t2059806696::get_offset_of_OnReleased_8(),
	ColliderButton_t2059806696_StaticFields::get_offset_of_OnSelectedGlobal_9(),
	ColliderButton_t2059806696_StaticFields::get_offset_of_OnDeselectedGlobal_10(),
	ColliderButton_t2059806696_StaticFields::get_offset_of_OnClickGlobal_11(),
	ColliderButton_t2059806696_StaticFields::get_offset_of_OnPressedGlobal_12(),
	ColliderButton_t2059806696_StaticFields::get_offset_of_OnReleasedGlobal_13(),
	ColliderButton_t2059806696::get_offset_of_U3CIsSelectedU3Ek__BackingField_14(),
	ColliderButton_t2059806696::get_offset_of_keyInput_15(),
	ColliderButton_t2059806696::get_offset_of__unityEventsFolded_16(),
	ColliderButton_t2059806696::get_offset_of__scaleResponseFolded_17(),
	ColliderButton_t2059806696::get_offset_of__colorResponseFolded_18(),
	ColliderButton_t2059806696::get_offset_of_applyColor_19(),
	ColliderButton_t2059806696::get_offset_of_applyScale_20(),
	ColliderButton_t2059806696::get_offset_of_collisionLayerMask_21(),
	ColliderButton_t2059806696::get_offset_of_colorRendererTarget_22(),
	ColliderButton_t2059806696::get_offset_of_colorImageTarget_23(),
	ColliderButton_t2059806696::get_offset_of_selectedColor_24(),
	ColliderButton_t2059806696::get_offset_of_pressedColor_25(),
	ColliderButton_t2059806696::get_offset_of_disabledColor_26(),
	ColliderButton_t2059806696::get_offset_of_colorDuration_27(),
	ColliderButton_t2059806696::get_offset_of_scaleTarget_28(),
	ColliderButton_t2059806696::get_offset_of_normalScale_29(),
	ColliderButton_t2059806696::get_offset_of_selectedScale_30(),
	ColliderButton_t2059806696::get_offset_of_pressedScale_31(),
	ColliderButton_t2059806696::get_offset_of_scaleDuration_32(),
	ColliderButton_t2059806696::get_offset_of_scaleEaseType_33(),
	ColliderButton_t2059806696::get_offset_of_resizeGUIBoxCollider_34(),
	ColliderButton_t2059806696::get_offset_of_centerGUIBoxCollider_35(),
	ColliderButton_t2059806696::get_offset_of_guiBoxColliderPadding_36(),
	ColliderButton_t2059806696::get_offset_of_interactable_37(),
	ColliderButton_t2059806696::get_offset_of__clicking_38(),
	ColliderButton_t2059806696::get_offset_of__selectedCount_39(),
	ColliderButton_t2059806696::get_offset_of__colliderSelected_40(),
	ColliderButton_t2059806696::get_offset_of__pressed_41(),
	ColliderButton_t2059806696::get_offset_of__released_42(),
	ColliderButton_t2059806696::get_offset_of__vrRunning_43(),
	ColliderButton_t2059806696::get_offset_of__rectTransform_44(),
	ColliderButton_t2059806696::get_offset_of__eventTrigger_45(),
	ColliderButton_t2059806696::get_offset_of__pressedEventTrigger_46(),
	ColliderButton_t2059806696::get_offset_of__releasedEventTrigger_47(),
	ColliderButton_t2059806696::get_offset_of__enterEventTrigger_48(),
	ColliderButton_t2059806696::get_offset_of__exitEventTrigger_49(),
	ColliderButton_t2059806696::get_offset_of__colliderCount_50(),
	ColliderButton_t2059806696::get_offset_of__boxCollider_51(),
	ColliderButton_t2059806696::get_offset_of__colorTweenImage_52(),
	ColliderButton_t2059806696::get_offset_of__colorTweenMaterial_53(),
	ColliderButton_t2059806696::get_offset_of__scaleTween_54(),
	ColliderButton_t2059806696::get_offset_of__normalColorRenderer_55(),
	ColliderButton_t2059806696::get_offset_of__normalColorImage_56(),
	ColliderButton_t2059806696::get_offset_of__interactableStatus_57(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (EaseType_t3131528705)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[3] = 
{
	EaseType_t3131528705::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (ColliderButtonInteraction_t392971596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (ColliderButtonSelector_t3248638654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[6] = 
{
	ColliderButtonSelector_t3248638654::get_offset_of_chooser_4(),
	ColliderButtonSelector_t3248638654::get_offset_of_loopAround_5(),
	ColliderButtonSelector_t3248638654::get_offset_of_previousKey_6(),
	ColliderButtonSelector_t3248638654::get_offset_of_nextKey_7(),
	ColliderButtonSelector_t3248638654::get_offset_of_colliderButtons_8(),
	ColliderButtonSelector_t3248638654::get_offset_of__index_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (Client_t1356520481), -1, sizeof(Client_t1356520481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2506[39] = 
{
	Client_t1356520481_StaticFields::get_offset_of_OnError_23(),
	Client_t1356520481_StaticFields::get_offset_of_OnConnected_24(),
	Client_t1356520481_StaticFields::get_offset_of_OnDisconnected_25(),
	Client_t1356520481_StaticFields::get_offset_of_OnServerAvailable_26(),
	Client_t1356520481_StaticFields::get_offset_of_OnFloat_27(),
	Client_t1356520481_StaticFields::get_offset_of_OnFloatArray_28(),
	Client_t1356520481_StaticFields::get_offset_of_OnInt_29(),
	Client_t1356520481_StaticFields::get_offset_of_OnIntArray_30(),
	Client_t1356520481_StaticFields::get_offset_of_OnVector2_31(),
	Client_t1356520481_StaticFields::get_offset_of_OnVector2Array_32(),
	Client_t1356520481_StaticFields::get_offset_of_OnVector3_33(),
	Client_t1356520481_StaticFields::get_offset_of_OnVector3Array_34(),
	Client_t1356520481_StaticFields::get_offset_of_OnQuaternion_35(),
	Client_t1356520481_StaticFields::get_offset_of_OnQuaternionArray_36(),
	Client_t1356520481_StaticFields::get_offset_of_OnVector4_37(),
	Client_t1356520481_StaticFields::get_offset_of_OnVector4Array_38(),
	Client_t1356520481_StaticFields::get_offset_of_OnRect_39(),
	Client_t1356520481_StaticFields::get_offset_of_OnRectArray_40(),
	Client_t1356520481_StaticFields::get_offset_of_OnString_41(),
	Client_t1356520481_StaticFields::get_offset_of_OnStringArray_42(),
	Client_t1356520481_StaticFields::get_offset_of_OnByte_43(),
	Client_t1356520481_StaticFields::get_offset_of_OnByteArray_44(),
	Client_t1356520481_StaticFields::get_offset_of_OnColor_45(),
	Client_t1356520481_StaticFields::get_offset_of_OnColorArray_46(),
	Client_t1356520481_StaticFields::get_offset_of_OnColor32_47(),
	Client_t1356520481_StaticFields::get_offset_of_OnColor32Array_48(),
	Client_t1356520481_StaticFields::get_offset_of_OnBool_49(),
	Client_t1356520481_StaticFields::get_offset_of_OnBoolArray_50(),
	Client_t1356520481_StaticFields::get_offset_of_OnMatrix4x4_51(),
	Client_t1356520481_StaticFields::get_offset_of_OnMatrix4x4Array_52(),
	Client_t1356520481::get_offset_of_broadcastingPort_53(),
	Client_t1356520481::get_offset_of_primaryQualityOfService_54(),
	Client_t1356520481::get_offset_of_secondaryQualityOfService_55(),
	Client_t1356520481::get_offset_of_initialBandwidth_56(),
	Client_t1356520481_StaticFields::get_offset_of_U3CPrimaryChannelU3Ek__BackingField_57(),
	Client_t1356520481_StaticFields::get_offset_of_U3CSecondaryChannelU3Ek__BackingField_58(),
	Client_t1356520481_StaticFields::get_offset_of_U3CRunningU3Ek__BackingField_59(),
	Client_t1356520481_StaticFields::get_offset_of__client_60(),
	Client_t1356520481_StaticFields::get_offset_of_ipAddress_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (NetworkMsg_t3804742521)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[28] = 
{
	NetworkMsg_t3804742521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (ServerAvailableMessage_t84918919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[3] = 
{
	ServerAvailableMessage_t84918919::get_offset_of_port_0(),
	ServerAvailableMessage_t84918919::get_offset_of_deviceId_1(),
	ServerAvailableMessage_t84918919::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (Matrix4x4Message_t1228836437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[3] = 
{
	Matrix4x4Message_t1228836437::get_offset_of_value_0(),
	Matrix4x4Message_t1228836437::get_offset_of_id_1(),
	Matrix4x4Message_t1228836437::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (Matrix4x4ArrayMessage_t3390148093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[3] = 
{
	Matrix4x4ArrayMessage_t3390148093::get_offset_of_value_0(),
	Matrix4x4ArrayMessage_t3390148093::get_offset_of_id_1(),
	Matrix4x4ArrayMessage_t3390148093::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (FloatMessage_t1531897345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[3] = 
{
	FloatMessage_t1531897345::get_offset_of_value_0(),
	FloatMessage_t1531897345::get_offset_of_id_1(),
	FloatMessage_t1531897345::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (FloatArrayMessage_t1560969605), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[3] = 
{
	FloatArrayMessage_t1560969605::get_offset_of_value_0(),
	FloatArrayMessage_t1560969605::get_offset_of_id_1(),
	FloatArrayMessage_t1560969605::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (IntMessage_t409052883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[3] = 
{
	IntMessage_t409052883::get_offset_of_value_0(),
	IntMessage_t409052883::get_offset_of_id_1(),
	IntMessage_t409052883::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (IntArrayMessage_t539785898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[3] = 
{
	IntArrayMessage_t539785898::get_offset_of_value_0(),
	IntArrayMessage_t539785898::get_offset_of_id_1(),
	IntArrayMessage_t539785898::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (Vector2Message_t1890921343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[3] = 
{
	Vector2Message_t1890921343::get_offset_of_value_0(),
	Vector2Message_t1890921343::get_offset_of_id_1(),
	Vector2Message_t1890921343::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Vector2ArrayMessage_t2399511537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[3] = 
{
	Vector2ArrayMessage_t2399511537::get_offset_of_value_0(),
	Vector2ArrayMessage_t2399511537::get_offset_of_id_1(),
	Vector2ArrayMessage_t2399511537::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (Vector3Message_t1962290047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[3] = 
{
	Vector3Message_t1962290047::get_offset_of_value_0(),
	Vector3Message_t1962290047::get_offset_of_id_1(),
	Vector3Message_t1962290047::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (Vector3ArrayMessage_t728146880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2518[3] = 
{
	Vector3ArrayMessage_t728146880::get_offset_of_value_0(),
	Vector3ArrayMessage_t728146880::get_offset_of_id_1(),
	Vector3ArrayMessage_t728146880::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (QuaternionMessage_t2883663930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[3] = 
{
	QuaternionMessage_t2883663930::get_offset_of_value_0(),
	QuaternionMessage_t2883663930::get_offset_of_id_1(),
	QuaternionMessage_t2883663930::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (QuaternionArrayMessage_t2019275377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[3] = 
{
	QuaternionArrayMessage_t2019275377::get_offset_of_value_0(),
	QuaternionArrayMessage_t2019275377::get_offset_of_id_1(),
	QuaternionArrayMessage_t2019275377::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (Vector4Message_t1462709119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2521[3] = 
{
	Vector4Message_t1462709119::get_offset_of_value_0(),
	Vector4Message_t1462709119::get_offset_of_id_1(),
	Vector4Message_t1462709119::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (Vector4ArrayMessage_t1455924212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[3] = 
{
	Vector4ArrayMessage_t1455924212::get_offset_of_value_0(),
	Vector4ArrayMessage_t1455924212::get_offset_of_id_1(),
	Vector4ArrayMessage_t1455924212::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (RectMessage_t1418415759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[3] = 
{
	RectMessage_t1418415759::get_offset_of_value_0(),
	RectMessage_t1418415759::get_offset_of_id_1(),
	RectMessage_t1418415759::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (RectArrayMessage_t1669700031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[3] = 
{
	RectArrayMessage_t1669700031::get_offset_of_value_0(),
	RectArrayMessage_t1669700031::get_offset_of_id_1(),
	RectArrayMessage_t1669700031::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (ByteMessage_t3249440400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[3] = 
{
	ByteMessage_t3249440400::get_offset_of_value_0(),
	ByteMessage_t3249440400::get_offset_of_id_1(),
	ByteMessage_t3249440400::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (ByteArrayMessage_t2093901192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2526[3] = 
{
	ByteArrayMessage_t2093901192::get_offset_of_value_0(),
	ByteArrayMessage_t2093901192::get_offset_of_id_1(),
	ByteArrayMessage_t2093901192::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (ColorMessage_t141388307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[3] = 
{
	ColorMessage_t141388307::get_offset_of_value_0(),
	ColorMessage_t141388307::get_offset_of_id_1(),
	ColorMessage_t141388307::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (ColorArrayMessage_t1458579616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	ColorArrayMessage_t1458579616::get_offset_of_value_0(),
	ColorArrayMessage_t1458579616::get_offset_of_id_1(),
	ColorArrayMessage_t1458579616::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (Color32Message_t1536619200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[3] = 
{
	Color32Message_t1536619200::get_offset_of_value_0(),
	Color32Message_t1536619200::get_offset_of_id_1(),
	Color32Message_t1536619200::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (Color32ArrayMessage_t89341636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[3] = 
{
	Color32ArrayMessage_t89341636::get_offset_of_value_0(),
	Color32ArrayMessage_t89341636::get_offset_of_id_1(),
	Color32ArrayMessage_t89341636::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (StringMessage_t468245767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[3] = 
{
	StringMessage_t468245767::get_offset_of_value_0(),
	StringMessage_t468245767::get_offset_of_id_1(),
	StringMessage_t468245767::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (StringArrayMessage_t4068327225), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2532[3] = 
{
	StringArrayMessage_t4068327225::get_offset_of_value_0(),
	StringArrayMessage_t4068327225::get_offset_of_id_1(),
	StringArrayMessage_t4068327225::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (BoolMessage_t980354741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2533[3] = 
{
	BoolMessage_t980354741::get_offset_of_value_0(),
	BoolMessage_t980354741::get_offset_of_id_1(),
	BoolMessage_t980354741::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (BoolArrayMessage_t3171392573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[3] = 
{
	BoolArrayMessage_t3171392573::get_offset_of_value_0(),
	BoolArrayMessage_t3171392573::get_offset_of_id_1(),
	BoolArrayMessage_t3171392573::get_offset_of_fromIp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (Server_t2028837719), -1, sizeof(Server_t2028837719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2535[41] = 
{
	Server_t2028837719_StaticFields::get_offset_of_OnPlayerConnected_23(),
	Server_t2028837719_StaticFields::get_offset_of_OnPlayerDisconnected_24(),
	Server_t2028837719_StaticFields::get_offset_of_OnFloat_25(),
	Server_t2028837719_StaticFields::get_offset_of_OnFloatArray_26(),
	Server_t2028837719_StaticFields::get_offset_of_OnInt_27(),
	Server_t2028837719_StaticFields::get_offset_of_OnIntArray_28(),
	Server_t2028837719_StaticFields::get_offset_of_OnVector2_29(),
	Server_t2028837719_StaticFields::get_offset_of_OnVector2Array_30(),
	Server_t2028837719_StaticFields::get_offset_of_OnVector3_31(),
	Server_t2028837719_StaticFields::get_offset_of_OnVector3Array_32(),
	Server_t2028837719_StaticFields::get_offset_of_OnQuaternion_33(),
	Server_t2028837719_StaticFields::get_offset_of_OnQuaternionArray_34(),
	Server_t2028837719_StaticFields::get_offset_of_OnVector4_35(),
	Server_t2028837719_StaticFields::get_offset_of_OnVector4Array_36(),
	Server_t2028837719_StaticFields::get_offset_of_OnRect_37(),
	Server_t2028837719_StaticFields::get_offset_of_OnRectArray_38(),
	Server_t2028837719_StaticFields::get_offset_of_OnString_39(),
	Server_t2028837719_StaticFields::get_offset_of_OnStringArray_40(),
	Server_t2028837719_StaticFields::get_offset_of_OnByte_41(),
	Server_t2028837719_StaticFields::get_offset_of_OnByteArray_42(),
	Server_t2028837719_StaticFields::get_offset_of_OnColor_43(),
	Server_t2028837719_StaticFields::get_offset_of_OnColorArray_44(),
	Server_t2028837719_StaticFields::get_offset_of_OnColor32_45(),
	Server_t2028837719_StaticFields::get_offset_of_OnColor32Array_46(),
	Server_t2028837719_StaticFields::get_offset_of_OnBool_47(),
	Server_t2028837719_StaticFields::get_offset_of_OnBoolArray_48(),
	Server_t2028837719_StaticFields::get_offset_of_OnMatrix4x4_49(),
	Server_t2028837719_StaticFields::get_offset_of_OnMatrix4x4Array_50(),
	Server_t2028837719::get_offset_of_primaryQualityOfService_51(),
	Server_t2028837719::get_offset_of_secondaryQualityOfService_52(),
	Server_t2028837719::get_offset_of_customDeviceId_53(),
	Server_t2028837719::get_offset_of_broadcastingPort_54(),
	Server_t2028837719::get_offset_of_maxConnections_55(),
	Server_t2028837719::get_offset_of_initialBandwidth_56(),
	Server_t2028837719_StaticFields::get_offset_of__connections_57(),
	Server_t2028837719_StaticFields::get_offset_of__server_58(),
	Server_t2028837719_StaticFields::get_offset_of__randomIdKey_59(),
	Server_t2028837719_StaticFields::get_offset_of_ipAddress_60(),
	Server_t2028837719_StaticFields::get_offset_of_U3CPrimaryChannelU3Ek__BackingField_61(),
	Server_t2028837719_StaticFields::get_offset_of_U3CSecondaryChannelU3Ek__BackingField_62(),
	Server_t2028837719_StaticFields::get_offset_of_U3CRunningU3Ek__BackingField_63(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (ServerInstance_t67659343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[2] = 
{
	ServerInstance_t67659343::get_offset_of_OnConnection_12(),
	ServerInstance_t67659343::get_offset_of_OnDisconnection_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (ClientConnector_t2701089130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[7] = 
{
	ClientConnector_t2701089130::get_offset_of__connectToFirstAvailable_4(),
	ClientConnector_t2701089130::get_offset_of__requiredDeviceId_5(),
	ClientConnector_t2701089130::get_offset_of__availableServers_6(),
	ClientConnector_t2701089130::get_offset_of__cleanUp_7(),
	ClientConnector_t2701089130::get_offset_of__lastCleanUpTime_8(),
	ClientConnector_t2701089130::get_offset_of__cleanUpTimeout_9(),
	ClientConnector_t2701089130::get_offset_of__cleanUpTimeoutBackup_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (AvailableServer_t2963635332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[3] = 
{
	AvailableServer_t2963635332::get_offset_of_ip_0(),
	AvailableServer_t2963635332::get_offset_of_port_1(),
	AvailableServer_t2963635332::get_offset_of_deviceID_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (ADDRESSFAM_t3748262023)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[3] = 
{
	ADDRESSFAM_t3748262023::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (IPManager_t1059072560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (DisplayObject_t314287876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[1] = 
{
	DisplayObject_t314287876::get_offset_of__activated_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (GameObjectEvent_t2867809866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (ColliderButtonEvent_t5483853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (BoolEvent_t798030280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (BezierCurves_t996170987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (CurveDetail_t910340028)+ sizeof (RuntimeObject), sizeof(CurveDetail_t910340028 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	CurveDetail_t910340028::get_offset_of_currentCurve_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurveDetail_t910340028::get_offset_of_currentCurvePercentage_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (SplineDirection_t243856657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2548[3] = 
{
	SplineDirection_t243856657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (Spline_t1246320346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[24] = 
{
	Spline_t1246320346::get_offset_of_OnSplineChanged_4(),
	Spline_t1246320346::get_offset_of_color_5(),
	Spline_t1246320346::get_offset_of_toolScale_6(),
	Spline_t1246320346::get_offset_of_defaultTangentMode_7(),
	Spline_t1246320346::get_offset_of_direction_8(),
	Spline_t1246320346::get_offset_of_loop_9(),
	Spline_t1246320346::get_offset_of_followers_10(),
	Spline_t1246320346::get_offset_of__anchors_11(),
	Spline_t1246320346::get_offset_of__curveCount_12(),
	Spline_t1246320346::get_offset_of__previousAnchorCount_13(),
	Spline_t1246320346::get_offset_of__previousChildCount_14(),
	Spline_t1246320346::get_offset_of__wasLooping_15(),
	Spline_t1246320346::get_offset_of__previousLoopChoice_16(),
	Spline_t1246320346::get_offset_of__anchorsChanged_17(),
	Spline_t1246320346::get_offset_of__previousDirection_18(),
	Spline_t1246320346::get_offset_of__curvePercentage_19(),
	Spline_t1246320346::get_offset_of__operatingCurve_20(),
	Spline_t1246320346::get_offset_of__currentCurve_21(),
	Spline_t1246320346::get_offset_of__removeRenderers_22(),
	Spline_t1246320346::get_offset_of__previousLength_23(),
	Spline_t1246320346::get_offset_of__slicesPerCurve_24(),
	Spline_t1246320346::get_offset_of__splineReparams_25(),
	Spline_t1246320346::get_offset_of__lengthDirty_26(),
	Spline_t1246320346::get_offset_of_U3CLengthU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (SplineReparam_t3624564387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	SplineReparam_t3624564387::get_offset_of_length_0(),
	SplineReparam_t3624564387::get_offset_of_percentage_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (TangentMode_t1256149096)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2551[4] = 
{
	TangentMode_t1256149096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (SplineAnchor_t2835267765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[14] = 
{
	SplineAnchor_t2835267765::get_offset_of_tangentMode_4(),
	SplineAnchor_t2835267765::get_offset_of_U3CRenderingChangeU3Ek__BackingField_5(),
	SplineAnchor_t2835267765::get_offset_of_U3CChangedU3Ek__BackingField_6(),
	SplineAnchor_t2835267765::get_offset_of__initialized_7(),
	SplineAnchor_t2835267765::get_offset_of__masterTangent_8(),
	SplineAnchor_t2835267765::get_offset_of__slaveTangent_9(),
	SplineAnchor_t2835267765::get_offset_of__previousTangentMode_10(),
	SplineAnchor_t2835267765::get_offset_of__previousInPosition_11(),
	SplineAnchor_t2835267765::get_offset_of__previousOutPosition_12(),
	SplineAnchor_t2835267765::get_offset_of__previousAnchorPosition_13(),
	SplineAnchor_t2835267765::get_offset_of__skinnedBounds_14(),
	SplineAnchor_t2835267765::get_offset_of__anchor_15(),
	SplineAnchor_t2835267765::get_offset_of__inTangent_16(),
	SplineAnchor_t2835267765::get_offset_of__outTangent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (SplineFollower_t1156113196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[6] = 
{
	SplineFollower_t1156113196::get_offset_of_target_0(),
	SplineFollower_t1156113196::get_offset_of_percentage_1(),
	SplineFollower_t1156113196::get_offset_of_faceDirection_2(),
	SplineFollower_t1156113196::get_offset_of__previousPercentage_3(),
	SplineFollower_t1156113196::get_offset_of__previousFaceDirection_4(),
	SplineFollower_t1156113196::get_offset_of__detached_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (SplineTangent_t405982707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (SplineControlledParticleSystem_t1873087847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[6] = 
{
	SplineControlledParticleSystem_t1873087847::get_offset_of_startRadius_4(),
	SplineControlledParticleSystem_t1873087847::get_offset_of_endRadius_5(),
	SplineControlledParticleSystem_t1873087847::get_offset_of__particleSystem_6(),
	SplineControlledParticleSystem_t1873087847::get_offset_of__spline_7(),
	SplineControlledParticleSystem_t1873087847::get_offset_of__particles_8(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (SplineRenderer_t731933917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[11] = 
{
	SplineRenderer_t731933917::get_offset_of_segmentsPerCurve_4(),
	SplineRenderer_t731933917::get_offset_of_startPercentage_5(),
	SplineRenderer_t731933917::get_offset_of_endPercentage_6(),
	SplineRenderer_t731933917::get_offset_of__lineRenderer_7(),
	SplineRenderer_t731933917::get_offset_of__spline_8(),
	SplineRenderer_t731933917::get_offset_of__initialized_9(),
	SplineRenderer_t731933917::get_offset_of__previousAnchorsLength_10(),
	SplineRenderer_t731933917::get_offset_of__previousSegmentsPerCurve_11(),
	SplineRenderer_t731933917::get_offset_of__vertexCount_12(),
	SplineRenderer_t731933917::get_offset_of__previousStart_13(),
	SplineRenderer_t731933917::get_offset_of__previousEnd_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (State_t1781471564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[1] = 
{
	State_t1781471564::get_offset_of__stateMachine_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (StateMachine_t4243775295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[16] = 
{
	StateMachine_t4243775295::get_offset_of_defaultState_4(),
	StateMachine_t4243775295::get_offset_of_currentState_5(),
	StateMachine_t4243775295::get_offset_of__unityEventsFolded_6(),
	StateMachine_t4243775295::get_offset_of_verbose_7(),
	StateMachine_t4243775295::get_offset_of_allowReentry_8(),
	StateMachine_t4243775295::get_offset_of_returnToDefaultOnDisable_9(),
	StateMachine_t4243775295::get_offset_of_OnStateExited_10(),
	StateMachine_t4243775295::get_offset_of_OnStateEntered_11(),
	StateMachine_t4243775295::get_offset_of_OnFirstStateEntered_12(),
	StateMachine_t4243775295::get_offset_of_OnFirstStateExited_13(),
	StateMachine_t4243775295::get_offset_of_OnLastStateEntered_14(),
	StateMachine_t4243775295::get_offset_of_OnLastStateExited_15(),
	StateMachine_t4243775295::get_offset_of_U3CCleanSetupU3Ek__BackingField_16(),
	StateMachine_t4243775295::get_offset_of__initialized_17(),
	StateMachine_t4243775295::get_offset_of__atFirst_18(),
	StateMachine_t4243775295::get_offset_of__atLast_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (TweenBase_t3695616892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[13] = 
{
	TweenBase_t3695616892::get_offset_of_targetInstanceID_0(),
	TweenBase_t3695616892::get_offset_of_tweenType_1(),
	TweenBase_t3695616892::get_offset_of_U3CStatusU3Ek__BackingField_2(),
	TweenBase_t3695616892::get_offset_of_U3CDurationU3Ek__BackingField_3(),
	TweenBase_t3695616892::get_offset_of_U3CCurveU3Ek__BackingField_4(),
	TweenBase_t3695616892::get_offset_of_U3CCurveKeysU3Ek__BackingField_5(),
	TweenBase_t3695616892::get_offset_of_U3CObeyTimescaleU3Ek__BackingField_6(),
	TweenBase_t3695616892::get_offset_of_U3CStartCallbackU3Ek__BackingField_7(),
	TweenBase_t3695616892::get_offset_of_U3CCompleteCallbackU3Ek__BackingField_8(),
	TweenBase_t3695616892::get_offset_of_U3CDelayU3Ek__BackingField_9(),
	TweenBase_t3695616892::get_offset_of_U3CLoopTypeU3Ek__BackingField_10(),
	TweenBase_t3695616892::get_offset_of_U3CPercentageU3Ek__BackingField_11(),
	TweenBase_t3695616892::get_offset_of_elapsedTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (TweenEngine_t2258245354), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (TweenUtilities_t308270123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (Tween_t1172390970), -1, sizeof(Tween_t1172390970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2562[14] = 
{
	Tween_t1172390970_StaticFields::get_offset_of_activeTweens_0(),
	Tween_t1172390970_StaticFields::get_offset_of__instance_1(),
	Tween_t1172390970_StaticFields::get_offset_of__easeIn_2(),
	Tween_t1172390970_StaticFields::get_offset_of__easeInStrong_3(),
	Tween_t1172390970_StaticFields::get_offset_of__easeOut_4(),
	Tween_t1172390970_StaticFields::get_offset_of__easeOutStrong_5(),
	Tween_t1172390970_StaticFields::get_offset_of__easeInOut_6(),
	Tween_t1172390970_StaticFields::get_offset_of__easeInOutStrong_7(),
	Tween_t1172390970_StaticFields::get_offset_of__easeInBack_8(),
	Tween_t1172390970_StaticFields::get_offset_of__easeOutBack_9(),
	Tween_t1172390970_StaticFields::get_offset_of__easeInOutBack_10(),
	Tween_t1172390970_StaticFields::get_offset_of__easeSpring_11(),
	Tween_t1172390970_StaticFields::get_offset_of__easeBounce_12(),
	Tween_t1172390970_StaticFields::get_offset_of__easeWobble_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (TweenType_t2719964487)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2563[26] = 
{
	TweenType_t2719964487::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (LoopType_t2942026839)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2564[4] = 
{
	LoopType_t2942026839::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (TweenStatus_t3213007367)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2565[6] = 
{
	TweenStatus_t3213007367::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (AnchoredPosition_t3182978813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[3] = 
{
	AnchoredPosition_t3182978813::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	AnchoredPosition_t3182978813::get_offset_of__target_14(),
	AnchoredPosition_t3182978813::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (CameraBackgroundColor_t3950989133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[3] = 
{
	CameraBackgroundColor_t3950989133::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	CameraBackgroundColor_t3950989133::get_offset_of__target_14(),
	CameraBackgroundColor_t3950989133::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (CanvasGroupAlpha_t882788859), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2568[3] = 
{
	CanvasGroupAlpha_t882788859::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	CanvasGroupAlpha_t882788859::get_offset_of__target_14(),
	CanvasGroupAlpha_t882788859::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (FieldOfView_t765943863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[3] = 
{
	FieldOfView_t765943863::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	FieldOfView_t765943863::get_offset_of__target_14(),
	FieldOfView_t765943863::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (ImageColor_t4158781697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[3] = 
{
	ImageColor_t4158781697::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ImageColor_t4158781697::get_offset_of__target_14(),
	ImageColor_t4158781697::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (LightColor_t419143490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[3] = 
{
	LightColor_t419143490::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	LightColor_t419143490::get_offset_of__target_14(),
	LightColor_t419143490::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (LightIntensity_t3710475180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[3] = 
{
	LightIntensity_t3710475180::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	LightIntensity_t3710475180::get_offset_of__target_14(),
	LightIntensity_t3710475180::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (LightRange_t74221972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[3] = 
{
	LightRange_t74221972::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	LightRange_t74221972::get_offset_of__target_14(),
	LightRange_t74221972::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (LocalPosition_t2591771983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[3] = 
{
	LocalPosition_t2591771983::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	LocalPosition_t2591771983::get_offset_of__target_14(),
	LocalPosition_t2591771983::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (LocalRotation_t2615760140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[3] = 
{
	LocalRotation_t2615760140::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	LocalRotation_t2615760140::get_offset_of__target_14(),
	LocalRotation_t2615760140::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (LocalScale_t1605085753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[3] = 
{
	LocalScale_t1605085753::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	LocalScale_t1605085753::get_offset_of__target_14(),
	LocalScale_t1605085753::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (PanStereo_t2313910393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[3] = 
{
	PanStereo_t2313910393::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	PanStereo_t2313910393::get_offset_of__target_14(),
	PanStereo_t2313910393::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (Pitch_t853067966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[3] = 
{
	Pitch_t853067966::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	Pitch_t853067966::get_offset_of__target_14(),
	Pitch_t853067966::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (Position_t4027175230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[3] = 
{
	Position_t4027175230::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	Position_t4027175230::get_offset_of__target_14(),
	Position_t4027175230::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (RawImageColor_t78192334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[3] = 
{
	RawImageColor_t78192334::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	RawImageColor_t78192334::get_offset_of__target_14(),
	RawImageColor_t78192334::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (Rotate_t3152225749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[5] = 
{
	Rotate_t3152225749::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	Rotate_t3152225749::get_offset_of__target_14(),
	Rotate_t3152225749::get_offset_of__start_15(),
	Rotate_t3152225749::get_offset_of__space_16(),
	Rotate_t3152225749::get_offset_of__previous_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (Rotation_t901105510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[3] = 
{
	Rotation_t901105510::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	Rotation_t901105510::get_offset_of__target_14(),
	Rotation_t901105510::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (ShaderColor_t2333276916), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[4] = 
{
	ShaderColor_t2333276916::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ShaderColor_t2333276916::get_offset_of__target_14(),
	ShaderColor_t2333276916::get_offset_of__start_15(),
	ShaderColor_t2333276916::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (ShaderFloat_t803773729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[4] = 
{
	ShaderFloat_t803773729::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ShaderFloat_t803773729::get_offset_of__target_14(),
	ShaderFloat_t803773729::get_offset_of__start_15(),
	ShaderFloat_t803773729::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (ShaderInt_t327647313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[4] = 
{
	ShaderInt_t327647313::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ShaderInt_t327647313::get_offset_of__target_14(),
	ShaderInt_t327647313::get_offset_of__start_15(),
	ShaderInt_t327647313::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (ShaderVector_t1632654171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[4] = 
{
	ShaderVector_t1632654171::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ShaderVector_t1632654171::get_offset_of__target_14(),
	ShaderVector_t1632654171::get_offset_of__start_15(),
	ShaderVector_t1632654171::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (ShakePosition_t2037188575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[4] = 
{
	ShakePosition_t2037188575::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ShakePosition_t2037188575::get_offset_of__target_14(),
	ShakePosition_t2037188575::get_offset_of__initialPosition_15(),
	ShakePosition_t2037188575::get_offset_of__intensity_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (Size_t2613871712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[3] = 
{
	Size_t2613871712::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	Size_t2613871712::get_offset_of__target_14(),
	Size_t2613871712::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (SplinePercentage_t2841806011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[5] = 
{
	SplinePercentage_t2841806011::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	SplinePercentage_t2841806011::get_offset_of__target_14(),
	SplinePercentage_t2841806011::get_offset_of__spline_15(),
	SplinePercentage_t2841806011::get_offset_of__startPercentage_16(),
	SplinePercentage_t2841806011::get_offset_of__faceDirection_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (SpriteRendererColor_t154867011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[3] = 
{
	SpriteRendererColor_t154867011::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	SpriteRendererColor_t154867011::get_offset_of__target_14(),
	SpriteRendererColor_t154867011::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (TextColor_t3467464308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2591[3] = 
{
	TextColor_t3467464308::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	TextColor_t3467464308::get_offset_of__target_14(),
	TextColor_t3467464308::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (TextMeshColor_t3131405086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	TextMeshColor_t3131405086::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	TextMeshColor_t3131405086::get_offset_of__target_14(),
	TextMeshColor_t3131405086::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (ValueColor_t3781550707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[3] = 
{
	ValueColor_t3781550707::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ValueColor_t3781550707::get_offset_of__valueUpdatedCallback_14(),
	ValueColor_t3781550707::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (ValueFloat_t265716779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[3] = 
{
	ValueFloat_t265716779::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ValueFloat_t265716779::get_offset_of__valueUpdatedCallback_14(),
	ValueFloat_t265716779::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (ValueInt_t3429980594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[3] = 
{
	ValueInt_t3429980594::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ValueInt_t3429980594::get_offset_of__valueUpdatedCallback_14(),
	ValueInt_t3429980594::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (ValueRect_t324067290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[3] = 
{
	ValueRect_t324067290::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ValueRect_t324067290::get_offset_of__valueUpdatedCallback_14(),
	ValueRect_t324067290::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (ValueVector2_t3152914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[3] = 
{
	ValueVector2_t3152914::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ValueVector2_t3152914::get_offset_of__valueUpdatedCallback_14(),
	ValueVector2_t3152914::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (ValueVector3_t2732036269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[3] = 
{
	ValueVector3_t2732036269::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ValueVector3_t2732036269::get_offset_of__valueUpdatedCallback_14(),
	ValueVector3_t2732036269::get_offset_of__start_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (ValueVector4_t809721968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	ValueVector4_t809721968::get_offset_of_U3CEndValueU3Ek__BackingField_13(),
	ValueVector4_t809721968::get_offset_of__valueUpdatedCallback_14(),
	ValueVector4_t809721968::get_offset_of__start_15(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
